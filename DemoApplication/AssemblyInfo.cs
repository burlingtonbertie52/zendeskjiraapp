﻿// Assembly DemoApplication, Version 2.7.0.0

[assembly: System.Runtime.CompilerServices.RuntimeCompatibility(WrapNonExceptionThrows=true)]
//[assembly: System.Runtime.CompilerServices.Extension]
[assembly: System.Runtime.CompilerServices.CompilationRelaxations(8)]
[assembly: System.Reflection.AssemblyConfiguration("")]
[assembly: System.Reflection.AssemblyCompany("Mantin Enterprises, Inc.")]
[assembly: System.Diagnostics.Debuggable(System.Diagnostics.DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints)]

[assembly: System.Reflection.AssemblyTitle("TestApplication")]
[assembly: System.Reflection.AssemblyDescription("Test Application for Notification DLL")]
[assembly: System.Reflection.AssemblyTrademark("")]
[assembly: System.Reflection.AssemblyFileVersion("2.7.0.0")]
[assembly: System.Reflection.AssemblyCopyright("Copyright \x00a9  2014")]
[assembly: System.Reflection.AssemblyProduct("TestApplication")]
[assembly: System.Runtime.InteropServices.ComVisible(false)]
[assembly: System.Windows.ThemeInfo(System.Windows.ResourceDictionaryLocation.None, System.Windows.ResourceDictionaryLocation.SourceAssembly)]

