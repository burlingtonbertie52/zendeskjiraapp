﻿namespace DemoApplication
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Runtime.CompilerServices;

    public static class ExtensionMethods
    {
        public static List<EnumMember> ConvertToList(this Enum type)
        {
            List<EnumMember> members = new List<EnumMember>();
            Type enumType = type.GetType();
            Enum.GetNames(type.GetType()).ToList<string>().ForEach(delegate (string s) {
                EnumMember item = new EnumMember {
                    Value = (int) ((IConvertible) Enum.Parse(enumType, s)),
                    Description = s.GetDescriptionValue<string>()
                };
                members.Add(item);
            });
            return (from m in members
                orderby m.Description
                select m).ToList<EnumMember>();
        }

        public static string GetDescriptionValue<T>(this T source)
        {
            DescriptionAttribute[] customAttributes = (DescriptionAttribute[]) source.GetType().GetField(source.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            if ((customAttributes != null) && (customAttributes.Length > 0))
            {
                return customAttributes[0].Description;
            }
            return source.ToString();
        }
    }
}

