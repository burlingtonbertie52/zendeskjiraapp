﻿namespace DemoApplication
{
    using Mantin.Controls.Wpf.Notification;
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Markup;

    public partial class MainWindow : Window
        
                        //, IComponentConnector
    {
       // private bool _contentLoaded;
        private Balloon balloon;
        //internal TextBox textBoxGeneralUse;
        private ViewModel viewModel = new ViewModel();

        public MainWindow()
        {
            //this.InitializeComponent();
            base.DataContext = this.viewModel;
        }

        //[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), DebuggerNonUserCode]
        //public void InitializeComponent()
        //{
        //    if (!this._contentLoaded)
        //    {
        //        this._contentLoaded = true;
        //        Uri resourceLocator = new Uri("/DemoApplication;component/mainwindow.xaml", UriKind.Relative);
        //        Application.LoadComponent(this, resourceLocator);
        //    }
        //}

        //[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), EditorBrowsable(EditorBrowsableState.Never), DebuggerNonUserCode]
        //void IComponentConnector.Connect(int connectionId, object target)
        //{
        //    if (connectionId == 1)
        //    {
        //        this.textBoxGeneralUse = (TextBox)target;
        //        this.textBoxGeneralUse.MouseEnter += new MouseEventHandler(this.TextBoxGeneralUseMouseEnter);
        //    }
        //    else
        //    {
        //        this._contentLoaded = true;
        //    }
        //}

        private void TextBoxGeneralUseMouseEnter(object sender, MouseEventArgs e)
        {
            if ((this.balloon == null) || !this.balloon.IsLoaded)
            {
                this.balloon = new Balloon(null, "You have moused over this textbox.", this.viewModel.BalloonType, false, this.viewModel.ShowBalloonCloseButton);
                this.balloon.Show();
            }
        }
    }
}

