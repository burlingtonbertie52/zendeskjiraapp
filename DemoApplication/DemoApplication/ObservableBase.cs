﻿namespace DemoApplication
{
    using System;
    using System.ComponentModel;
    using System.Linq.Expressions;
    using System.Threading;

    [Serializable]
    public abstract class ObservableBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected ObservableBase()
        {
        }

        protected string GetPropertyName<T>(Expression<Func<T>> expression)
        {
            MemberExpression body = (MemberExpression) expression.Body;
            return body.Member.Name;
        }

        protected virtual void Notify<T>(Expression<Func<T>> expression)
        {
            string propertyName = this.GetPropertyName<T>(expression);
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if (propertyChanged != null)
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        protected virtual void OnPropertyChanged<T>(Expression<Func<T>> expression)
        {
            string propertyName = this.GetPropertyName<T>(expression);
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if (propertyChanged != null)
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

