﻿namespace DemoApplication
{
    using System;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows.Markup;

    public class EnumExtension : MarkupExtension
    {
        private Type enumType;

        public EnumExtension(Type enumType)
        {
            if (enumType == null)
            {
                throw new ArgumentNullException("enumType");
            }
            this.EnumType = enumType;
        }

        private string GetDescription(object enumValue)
        {
            DescriptionAttribute attribute = this.EnumType.GetField(enumValue.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false).FirstOrDefault<object>() as DescriptionAttribute;
            if (attribute == null)
            {
                return enumValue.ToString();
            }
            return attribute.Description;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return (from enumValue in Enum.GetValues(this.EnumType).Cast<object>() select new EnumMember { Value = (int) enumValue, Description = this.GetDescription(enumValue) }).ToArray<EnumMember>();
        }

        public Type EnumType
        {
            get
            {
                return this.enumType;
            }
            private set
            {
                if (this.enumType != value)
                {
                    Type type = Nullable.GetUnderlyingType(value) ?? value;
                    if (!type.IsEnum)
                    {
                        throw new ArgumentException("Type must be an Enum.");
                    }
                    this.enumType = value;
                }
            }
        }
    }
}

