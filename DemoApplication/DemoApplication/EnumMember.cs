﻿namespace DemoApplication
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class EnumMember
    {
        public static List<EnumMember> ConvertToList<T>()
        {
            Type enumType = typeof(T);
            if (!enumType.IsEnum)
            {
                throw new ArgumentException("T must be of type enumeration.");
            }
            List<EnumMember> list = new List<EnumMember>();
            foreach (string str in Enum.GetNames(enumType))
            {
                object source = Enum.Parse(enumType, str);
                EnumMember item = new EnumMember {
                    Description = source.GetDescriptionValue<object>(),
                    Value = ((IConvertible) source).ToInt32(null)
                };
                list.Add(item);
            }
            return list;
        }

        public string Description { get; set; }

        public int Value { get; set; }
    }
}

