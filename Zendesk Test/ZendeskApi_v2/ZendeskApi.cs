﻿namespace ZendeskApi_v2
{
    using System;
    using System.Net;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using ZendeskApi_v2.HelpCenter;
    using ZendeskApi_v2.Requests;

    public class ZendeskApi : IZendeskApi
    {
        public ZendeskApi(IWebProxy proxy, string yourZendeskUrl, string user, string password) : this(yourZendeskUrl, user, password, "", "en-us")
        {
            if (proxy != null)
            {
                ((ZendeskApi_v2.Requests.Tickets) this.Tickets).Proxy = proxy;
                ((ZendeskApi_v2.Requests.Attachments) this.Attachments).Proxy = proxy;
                ((ZendeskApi_v2.Requests.Views) this.Views).Proxy = proxy;
                ((ZendeskApi_v2.Requests.Users) this.Users).Proxy = proxy;
                ((ZendeskApi_v2.Requests.Requests) this.Requests).Proxy = proxy;
                ((ZendeskApi_v2.Requests.Groups) this.Groups).Proxy = proxy;
                ((ZendeskApi_v2.Requests.CustomAgentRoles) this.CustomAgentRoles).Proxy = proxy;
                ((ZendeskApi_v2.Requests.Organizations) this.Organizations).Proxy = proxy;
                ((ZendeskApi_v2.Requests.Search) this.Search).Proxy = proxy;
                ((ZendeskApi_v2.Requests.Tags) this.Tags).Proxy = proxy;
                ((ZendeskApi_v2.Requests.Forums) this.Forums).Proxy = proxy;
                ((ZendeskApi_v2.Requests.Categories) this.Categories).Proxy = proxy;
                ((ZendeskApi_v2.Requests.Topics) this.Topics).Proxy = proxy;
                ((ZendeskApi_v2.Requests.AccountsAndActivity) this.AccountsAndActivity).Proxy = proxy;
                ((ZendeskApi_v2.Requests.JobStatuses) this.JobStatuses).Proxy = proxy;
                ((ZendeskApi_v2.Requests.Locales) this.Locales).Proxy = proxy;
                ((ZendeskApi_v2.Requests.Macros) this.Macros).Proxy = proxy;
                ((ZendeskApi_v2.Requests.SatisfactionRatings) this.SatisfactionRatings).Proxy = proxy;
                ((ZendeskApi_v2.Requests.SharingAgreements) this.SharingAgreements).Proxy = proxy;
                ((ZendeskApi_v2.Requests.Triggers) this.Triggers).Proxy = proxy;
            }
        }

        public ZendeskApi(string yourZendeskUrl, string user, string password = "", string apiToken = "", string locale = "en-us")
        {
            string absoluteUri = this.GetFormattedZendeskUrl(yourZendeskUrl).AbsoluteUri;
            this.Tickets = new ZendeskApi_v2.Requests.Tickets(absoluteUri, user, password, apiToken);
            this.Attachments = new ZendeskApi_v2.Requests.Attachments(absoluteUri, user, password, apiToken);
            this.Views = new ZendeskApi_v2.Requests.Views(absoluteUri, user, password, apiToken);
            this.Users = new ZendeskApi_v2.Requests.Users(absoluteUri, user, password, apiToken);
            this.Requests = new ZendeskApi_v2.Requests.Requests(absoluteUri, user, password, apiToken);
            this.Groups = new ZendeskApi_v2.Requests.Groups(absoluteUri, user, password, apiToken);
            this.CustomAgentRoles = new ZendeskApi_v2.Requests.CustomAgentRoles(absoluteUri, user, password, apiToken);
            this.Organizations = new ZendeskApi_v2.Requests.Organizations(absoluteUri, user, password, apiToken);
            this.Search = new ZendeskApi_v2.Requests.Search(absoluteUri, user, password, apiToken);
            this.Tags = new ZendeskApi_v2.Requests.Tags(absoluteUri, user, password, apiToken);
            this.Forums = new ZendeskApi_v2.Requests.Forums(absoluteUri, user, password, apiToken);
            this.Categories = new ZendeskApi_v2.Requests.Categories(absoluteUri, user, password, apiToken);
            this.Topics = new ZendeskApi_v2.Requests.Topics(absoluteUri, user, password, apiToken);
            this.AccountsAndActivity = new ZendeskApi_v2.Requests.AccountsAndActivity(absoluteUri, user, password, apiToken);
            this.JobStatuses = new ZendeskApi_v2.Requests.JobStatuses(absoluteUri, user, password, apiToken);
            this.Locales = new ZendeskApi_v2.Requests.Locales(absoluteUri, user, password, apiToken);
            this.Macros = new ZendeskApi_v2.Requests.Macros(absoluteUri, user, password, apiToken);
            this.SatisfactionRatings = new ZendeskApi_v2.Requests.SatisfactionRatings(absoluteUri, user, password, apiToken);
            this.SharingAgreements = new ZendeskApi_v2.Requests.SharingAgreements(absoluteUri, user, password, apiToken);
            this.Triggers = new ZendeskApi_v2.Requests.Triggers(absoluteUri, user, password, apiToken);
            this.HelpCenter = new HelpCenterApi(absoluteUri, user, password, apiToken, locale);
            this.ZendeskUrl = absoluteUri;
        }

        private Uri GetFormattedZendeskUrl(string yourZendeskUrl)
        {
            yourZendeskUrl = yourZendeskUrl.ToLower();
            if (yourZendeskUrl.StartsWith("http://"))
            {
                yourZendeskUrl = yourZendeskUrl.Replace("http://", "https://");
            }
            if (!yourZendeskUrl.StartsWith("https://"))
            {
                yourZendeskUrl = "https://" + yourZendeskUrl;
            }
            if (!yourZendeskUrl.EndsWith("/api/v2"))
            {
                yourZendeskUrl = yourZendeskUrl.Split(new string[] { ".zendesk.com" }, StringSplitOptions.RemoveEmptyEntries)[0] + ".zendesk.com/api/v2";
            }
            return new Uri(yourZendeskUrl);
        }

        public IAccountsAndActivity AccountsAndActivity { get; set; }

        public IAttachments Attachments { get; set; }

        public ICategories Categories { get; set; }

        public ICustomAgentRoles CustomAgentRoles { get; set; }

        public IForums Forums { get; set; }

        public IGroups Groups { get; set; }

        public IHelpCenterApi HelpCenter { get; set; }

        public IJobStatuses JobStatuses { get; set; }

        public ILocales Locales { get; set; }

        public IMacros Macros { get; set; }

        public IOrganizations Organizations { get; set; }

        public IRequests Requests { get; set; }

        public ISatisfactionRatings SatisfactionRatings { get; set; }

        public ISearch Search { get; set; }

        public ISharingAgreements SharingAgreements { get; set; }

        public ITags Tags { get; set; }

        public ITickets Tickets { get; set; }

        public ITopics Topics { get; set; }

        public ITriggers Triggers { get; set; }

        public IUsers Users { get; set; }

        public IViews Views { get; set; }

        public string ZendeskUrl { get; set; }
    }
}

