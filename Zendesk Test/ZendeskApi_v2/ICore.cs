﻿namespace ZendeskApi_v2
{
    using System;
    using System.Runtime.InteropServices;

    public interface ICore
    {
        T GetByPageUrl<T>(string pageUrl, int perPage = 100);
        RequestResult RunRequest(string resource, string requestMethod, object body = null);
        T RunRequest<T>(string resource, string requestMethod, object body = null);
    }
}

