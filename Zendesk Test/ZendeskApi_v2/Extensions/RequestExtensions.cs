﻿namespace ZendeskApi_v2.Extensions
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Runtime.CompilerServices;
    using System.Threading;

    public static class RequestExtensions
    {
        public static T ConvertToObject<T>(this string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

        public static int GetEpoch(this DateTime date)
        {
            TimeSpan span = (TimeSpan) (date - new DateTime(0x7b2, 1, 1));
            return (int) span.TotalSeconds;
        }

        public static Stream GetWebRequestStream(this WebRequest request)
        {
            AutoResetEvent autoResetEvent = new AutoResetEvent(false);
            IAsyncResult asyncResult = request.BeginGetRequestStream(delegate (IAsyncResult r) {
                autoResetEvent.Set();
            }, null);
            autoResetEvent.WaitOne();
            return request.EndGetRequestStream(asyncResult);
        }

        public static WebResponse GetWebResponse(this WebRequest request)
        {
            AutoResetEvent autoResetEvent = new AutoResetEvent(false);
            IAsyncResult asyncResult = request.BeginGetResponse(delegate (IAsyncResult r) {
                autoResetEvent.Set();
            }, null);
            autoResetEvent.WaitOne();
            return request.EndGetResponse(asyncResult);
        }

        public static string ToCsv(this IEnumerable<long> ids)
        {
            return string.Join(",", (from x in ids select x.ToString(CultureInfo.InvariantCulture)).ToArray<string>());
        }
    }
}

