﻿namespace ZendeskApi_v2
{
    using System;
    using System.Net;
    using System.Runtime.CompilerServices;

    public class RequestResult
    {
        public string Content { get; set; }

        public System.Net.HttpStatusCode HttpStatusCode { get; set; }
    }
}

