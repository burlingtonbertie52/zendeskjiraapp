﻿namespace ZendeskApi_v2
{
    using System;
    using ZendeskApi_v2.HelpCenter;
    using ZendeskApi_v2.Requests;

    public interface IZendeskApi
    {
        IAccountsAndActivity AccountsAndActivity { get; }

        IAttachments Attachments { get; }

        ICategories Categories { get; }

        ICustomAgentRoles CustomAgentRoles { get; }

        IForums Forums { get; }

        IGroups Groups { get; }

        IHelpCenterApi HelpCenter { get; }

        IJobStatuses JobStatuses { get; }

        ILocales Locales { get; }

        IMacros Macros { get; }

        IOrganizations Organizations { get; }

        IRequests Requests { get; }

        ISatisfactionRatings SatisfactionRatings { get; }

        ISearch Search { get; }

        ISharingAgreements SharingAgreements { get; }

        ITags Tags { get; }

        ITickets Tickets { get; }

        ITopics Topics { get; }

        ITriggers Triggers { get; }

        IUsers Users { get; }

        IViews Views { get; }

        string ZendeskUrl { get; }
    }
}

