﻿namespace ZendeskApi_v2.Models.Macros
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class Restriction
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }
}

