﻿namespace ZendeskApi_v2.Models.Macros
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class IndividualMacroResponse
    {
        [JsonProperty("macro")]
        public ZendeskApi_v2.Models.Macros.Macro Macro { get; set; }
    }
}

