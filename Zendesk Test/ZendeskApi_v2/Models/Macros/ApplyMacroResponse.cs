﻿namespace ZendeskApi_v2.Models.Macros
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class ApplyMacroResponse
    {
        [JsonProperty("result")]
        public ZendeskApi_v2.Models.Macros.Result Result { get; set; }
    }
}

