﻿namespace ZendeskApi_v2.Models.Macros
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models;

    public class GroupMacroResponse : GroupResponseBase
    {
        [JsonProperty("macros")]
        public IList<Macro> Macros { get; set; }
    }
}

