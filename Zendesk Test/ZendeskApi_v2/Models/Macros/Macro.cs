﻿namespace ZendeskApi_v2.Models.Macros
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class Macro
    {
        [JsonProperty("actions")]
        public IList<ZendeskApi_v2.Models.Macros.Action> Actions { get; set; }

        [JsonProperty("active")]
        public bool? Active { get; set; }

        [JsonConverter(typeof(IsoDateTimeConverter)), JsonProperty("created_at")]
        public DateTimeOffset? CreatedAt { get; set; }

        [JsonProperty("id")]
        public long? Id { get; set; }

        [JsonProperty("restriction")]
        public ZendeskApi_v2.Models.Macros.Restriction Restriction { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("updated_at"), JsonConverter(typeof(IsoDateTimeConverter))]
        public DateTimeOffset? UpdatedAt { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }
    }
}

