﻿namespace ZendeskApi_v2.Models.Macros
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models.Tickets;

    public class Result
    {
        [JsonProperty("ticket")]
        public ZendeskApi_v2.Models.Tickets.Ticket Ticket { get; set; }
    }
}

