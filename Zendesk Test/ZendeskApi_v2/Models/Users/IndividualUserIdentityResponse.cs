﻿namespace ZendeskApi_v2.Models.Users
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class IndividualUserIdentityResponse
    {
        [JsonProperty("identity")]
        public UserIdentity Identity { get; set; }
    }
}

