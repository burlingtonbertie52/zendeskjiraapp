﻿namespace ZendeskApi_v2.Models.Users
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class IndividualUserResponse
    {
        [JsonProperty("user")]
        public ZendeskApi_v2.Models.Users.User User { get; set; }
    }
}

