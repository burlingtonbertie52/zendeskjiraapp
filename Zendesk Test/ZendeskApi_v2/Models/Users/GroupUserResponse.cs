﻿namespace ZendeskApi_v2.Models.Users
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models;

    public class GroupUserResponse : GroupResponseBase
    {
        [JsonProperty("users")]
        public IList<User> Users { get; set; }
    }
}

