﻿namespace ZendeskApi_v2.Models.Users
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models;

    public class GroupUserIdentityResponse : GroupResponseBase
    {
        [JsonProperty("identities")]
        public IList<UserIdentity> Identities { get; set; }
    }
}

