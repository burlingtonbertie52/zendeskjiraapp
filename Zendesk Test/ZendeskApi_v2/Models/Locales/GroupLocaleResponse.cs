﻿namespace ZendeskApi_v2.Models.Locales
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models;

    public class GroupLocaleResponse : GroupResponseBase
    {
        [JsonProperty("locales")]
        public IList<Locale> Locales { get; set; }
    }
}

