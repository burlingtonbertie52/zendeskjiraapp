﻿namespace ZendeskApi_v2.Models.Locales
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class IndividualLocaleResponse
    {
        [JsonProperty("locale")]
        public ZendeskApi_v2.Models.Locales.Locale Locale { get; set; }
    }
}

