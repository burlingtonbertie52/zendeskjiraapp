﻿namespace ZendeskApi_v2.Models.Groups
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models;

    public class MultipleGroupResponse : GroupResponseBase
    {
        [JsonProperty("groups")]
        public IList<Group> Groups { get; set; }
    }
}

