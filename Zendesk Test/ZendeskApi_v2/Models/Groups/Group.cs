﻿namespace ZendeskApi_v2.Models.Groups
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using System;
    using System.Runtime.CompilerServices;

    public class Group
    {
        [JsonConverter(typeof(IsoDateTimeConverter)), JsonProperty("created_at")]
        public DateTimeOffset? CreatedAt { get; set; }

        [JsonProperty("deleted")]
        public bool Deleted { get; set; }

        [JsonProperty("id")]
        public long? Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonConverter(typeof(IsoDateTimeConverter)), JsonProperty("updated_at")]
        public DateTimeOffset? UpdatedAt { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }
    }
}

