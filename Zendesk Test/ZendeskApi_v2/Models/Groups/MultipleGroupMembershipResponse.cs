﻿namespace ZendeskApi_v2.Models.Groups
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models;

    public class MultipleGroupMembershipResponse : GroupResponseBase
    {
        [JsonProperty("group_memberships")]
        public IList<GroupMembership> GroupMemberships { get; set; }
    }
}

