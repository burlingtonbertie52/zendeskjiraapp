﻿namespace ZendeskApi_v2.Models.Groups
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class IndividualGroupMembershipResponse
    {
        [JsonProperty("group_membership")]
        public ZendeskApi_v2.Models.Groups.GroupMembership GroupMembership { get; set; }
    }
}

