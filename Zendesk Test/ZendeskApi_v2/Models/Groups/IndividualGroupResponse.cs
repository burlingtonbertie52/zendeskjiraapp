﻿namespace ZendeskApi_v2.Models.Groups
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class IndividualGroupResponse
    {
        [JsonProperty("group")]
        public ZendeskApi_v2.Models.Groups.Group Group { get; set; }
    }
}

