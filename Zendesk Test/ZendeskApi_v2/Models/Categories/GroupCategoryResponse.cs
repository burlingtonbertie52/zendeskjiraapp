﻿namespace ZendeskApi_v2.Models.Categories
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models;

    public class GroupCategoryResponse : GroupResponseBase
    {
        [JsonProperty("categories")]
        public IList<Category> Categories { get; set; }
    }
}

