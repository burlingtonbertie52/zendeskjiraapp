﻿namespace ZendeskApi_v2.Models.Categories
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class IndividualCategoryResponse
    {
        [JsonProperty("category")]
        public ZendeskApi_v2.Models.Categories.Category Category { get; set; }
    }
}

