﻿namespace ZendeskApi_v2.Models.Tickets
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class SatisfactionRating
    {
        [JsonProperty("comment")]
        public string Comment { get; set; }

        [JsonProperty("score")]
        public string Score { get; set; }
    }
}

