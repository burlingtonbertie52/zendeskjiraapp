﻿namespace ZendeskApi_v2.Models.Tickets
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class TimeSpanMetric
    {
        [JsonProperty("business")]
        public int? Business { get; set; }

        [JsonProperty("calendar")]
        public int? Calendar { get; set; }
    }
}

