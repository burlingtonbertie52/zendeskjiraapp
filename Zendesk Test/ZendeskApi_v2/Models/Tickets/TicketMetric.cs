﻿namespace ZendeskApi_v2.Models.Tickets
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using System;
    using System.Runtime.CompilerServices;

    public class TicketMetric : BaseTicket
    {
        [JsonProperty("agent_wait_time_in_minutes")]
        public TimeSpanMetric AgentWaitTimeInMinutes { get; set; }

        [JsonConverter(typeof(IsoDateTimeConverter)), JsonProperty("assigned_at")]
        public DateTimeOffset? AssignedAt { get; set; }

        [JsonProperty("assignee_stations")]
        public long? AssigneeStations { get; set; }

        [JsonProperty("assignee_updated_at"), JsonConverter(typeof(IsoDateTimeConverter))]
        public DateTimeOffset? AssigneeUpdatedAt { get; set; }

        [JsonProperty("first_resolution_time_in_minutes")]
        public TimeSpanMetric FirstResolutionTimeInMinutes { get; set; }

        [JsonProperty("full_resolution_time_in_minutes")]
        public TimeSpanMetric FullResolutionTimeInMinutes { get; set; }

        [JsonProperty("group_stations")]
        public long? GroupStations { get; set; }

        [JsonConverter(typeof(IsoDateTimeConverter)), JsonProperty("initially_assigned_at")]
        public DateTimeOffset? InitiallyAssignedAt { get; set; }

        [JsonConverter(typeof(IsoDateTimeConverter)), JsonProperty("latest_comment_added_at")]
        public DateTimeOffset? LatestCommentAddedAt { get; set; }

        [JsonProperty("reopens")]
        public long? Reopens { get; set; }

        [JsonProperty("replies")]
        public long? Replies { get; set; }

        [JsonProperty("reply_time_in_minutes")]
        public TimeSpanMetric ReplyTimeInMinutes { get; set; }

        [JsonConverter(typeof(IsoDateTimeConverter)), JsonProperty("requester_updated_at")]
        public DateTimeOffset? RequesterUpdatedAt { get; set; }

        [JsonProperty("requester_wait_time_in_minutes")]
        public TimeSpanMetric RequesterWaitTimeInMinutes { get; set; }

        [JsonProperty("solved_at"), JsonConverter(typeof(IsoDateTimeConverter))]
        public DateTimeOffset? SolvedAt { get; set; }

        [JsonConverter(typeof(IsoDateTimeConverter)), JsonProperty("status_updated_at")]
        public DateTimeOffset? StatusUpdatedAt { get; set; }

        [JsonProperty("ticket_id")]
        public long? TicketId { get; set; }
    }
}

