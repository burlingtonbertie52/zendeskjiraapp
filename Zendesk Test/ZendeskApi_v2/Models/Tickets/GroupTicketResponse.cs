﻿namespace ZendeskApi_v2.Models.Tickets
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models;

    public class GroupTicketResponse : GroupResponseBase
    {
        [JsonProperty("tickets")]
        public IList<Ticket> Tickets { get; set; }
    }
}

