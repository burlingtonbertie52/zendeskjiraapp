﻿namespace ZendeskApi_v2.Models.Tickets
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class Source
    {
        [JsonProperty("from")]
        public ZendeskApi_v2.Models.Tickets.From From { get; set; }

        [JsonProperty("rel")]
        public string Rel { get; set; }

        [JsonProperty("to")]
        public ZendeskApi_v2.Models.Tickets.To To { get; set; }
    }
}

