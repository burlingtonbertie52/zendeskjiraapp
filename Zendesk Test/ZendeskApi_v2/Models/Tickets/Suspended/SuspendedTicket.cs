﻿namespace ZendeskApi_v2.Models.Tickets.Suspended
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using System;
    using System.Runtime.CompilerServices;

    public class SuspendedTicket
    {
        [JsonProperty("author")]
        public ZendeskApi_v2.Models.Tickets.Suspended.Author Author { get; set; }

        [JsonProperty("cause")]
        public string Cause { get; set; }

        [JsonProperty("content")]
        public string Content { get; set; }

        [JsonProperty("created_at"), JsonConverter(typeof(IsoDateTimeConverter))]
        public DateTimeOffset? CreatedAt { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("message_id")]
        public string MessageId { get; set; }

        [JsonProperty("subject")]
        public string Subject { get; set; }

        [JsonConverter(typeof(IsoDateTimeConverter)), JsonProperty("updated_at")]
        public DateTimeOffset? UpdatedAt { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("via")]
        public ZendeskApi_v2.Models.Tickets.Suspended.Via Via { get; set; }
    }
}

