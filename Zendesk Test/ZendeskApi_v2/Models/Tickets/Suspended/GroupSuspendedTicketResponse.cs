﻿namespace ZendeskApi_v2.Models.Tickets.Suspended
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class GroupSuspendedTicketResponse
    {
        [JsonProperty("count")]
        public int Count { get; set; }

        [JsonProperty("next_page")]
        public object NextPage { get; set; }

        [JsonProperty("previous_page")]
        public object PreviousPage { get; set; }

        [JsonProperty("suspended_tickets")]
        public IList<SuspendedTicket> SuspendedTickets { get; set; }
    }
}

