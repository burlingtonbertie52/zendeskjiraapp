﻿namespace ZendeskApi_v2.Models.Tickets.Suspended
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class Via
    {
        [JsonProperty("channel")]
        public string Channel { get; set; }

        [JsonProperty("source")]
        public ZendeskApi_v2.Models.Tickets.Suspended.Source Source { get; set; }
    }
}

