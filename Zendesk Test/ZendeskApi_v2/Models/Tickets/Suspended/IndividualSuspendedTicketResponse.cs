﻿namespace ZendeskApi_v2.Models.Tickets.Suspended
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class IndividualSuspendedTicketResponse
    {
        [JsonProperty("suspended_ticket")]
        public ZendeskApi_v2.Models.Tickets.Suspended.SuspendedTicket SuspendedTicket { get; set; }
    }
}

