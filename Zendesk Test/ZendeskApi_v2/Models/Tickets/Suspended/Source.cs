﻿namespace ZendeskApi_v2.Models.Tickets.Suspended
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class Source
    {
        [JsonProperty("from")]
        public ZendeskApi_v2.Models.Tickets.Suspended.From From { get; set; }

        [JsonProperty("rel")]
        public object Rel { get; set; }

        [JsonProperty("to")]
        public ZendeskApi_v2.Models.Tickets.Suspended.To To { get; set; }
    }
}

