﻿namespace ZendeskApi_v2.Models.Tickets.Suspended
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class To
    {
        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}

