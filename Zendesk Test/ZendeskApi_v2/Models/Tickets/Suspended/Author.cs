﻿namespace ZendeskApi_v2.Models.Tickets.Suspended
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class Author
    {
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("id")]
        public object Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}

