﻿namespace ZendeskApi_v2.Models.Tickets
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class BulkUpdate
    {
        [JsonProperty("assignee_id")]
        public long? AssigneeId { get; set; }

        [JsonProperty("collaborators")]
        public IList<string> CollaboratorEmails { get; set; }

        [JsonProperty("comment")]
        public ZendeskApi_v2.Models.Tickets.Comment Comment { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("subject")]
        public string Subject { get; set; }

        [JsonProperty("tags")]
        public IList<string> Tags { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }
}

