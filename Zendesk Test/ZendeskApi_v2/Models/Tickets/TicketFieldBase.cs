﻿namespace ZendeskApi_v2.Models.Tickets
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class TicketFieldBase
    {
        [JsonProperty("active")]
        public bool Active { get; set; }

        [JsonProperty("collapsed_for_agents")]
        public bool CollapsedForAgents { get; set; }

        [JsonProperty("custom_field_options")]
        public IList<ZendeskApi_v2.Models.Tickets.CustomFieldOptions> CustomFieldOptions { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("editable_in_portal")]
        public bool EditableInPortal { get; set; }

        [JsonProperty("position")]
        public int Position { get; set; }

        [JsonProperty("regexp_for_validation")]
        public object RegexpForValidation { get; set; }

        [JsonProperty("required")]
        public bool Required { get; set; }

        [JsonProperty("required_in_portal")]
        public bool RequiredInPortal { get; set; }

        [JsonProperty("tag")]
        public object Tag { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("title_in_portal")]
        public string TitleInPortal { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("visible_in_portal")]
        public bool VisibleInPortal { get; set; }
    }
}

