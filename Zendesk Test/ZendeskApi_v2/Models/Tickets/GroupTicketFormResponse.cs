﻿namespace ZendeskApi_v2.Models.Tickets
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models;

    public class GroupTicketFormResponse : GroupResponseBase
    {
        [JsonProperty("ticket_forms")]
        public IList<TicketForm> TicketForms { get; set; }
    }
}

