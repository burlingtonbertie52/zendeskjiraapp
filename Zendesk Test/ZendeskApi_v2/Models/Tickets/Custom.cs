﻿namespace ZendeskApi_v2.Models.Tickets
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class Custom
    {
        [JsonProperty("time_spent")]
        public string TimeSpent { get; set; }
    }
}

