﻿namespace ZendeskApi_v2.Models.Tickets
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models.Shared;

    public class IndividualTicketResponse
    {
        [JsonProperty("audit")]
        public ZendeskApi_v2.Models.Shared.Audit Audit { get; set; }

        [JsonProperty("ticket")]
        public ZendeskApi_v2.Models.Tickets.Ticket Ticket { get; set; }
    }
}

