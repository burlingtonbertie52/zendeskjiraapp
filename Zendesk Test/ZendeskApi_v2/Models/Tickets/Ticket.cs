﻿namespace ZendeskApi_v2.Models.Tickets
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models.Shared;

    public class Ticket : BaseTicket
    {
        [JsonProperty("assignee_id")]
        public long? AssigneeId { get; set; }

        [JsonProperty("collaborators")]
        public IList<string> CollaboratorEmails { get; set; }

        [JsonProperty("collaborator_ids")]
        public IList<long> CollaboratorIds { get; set; }

        [JsonProperty("comment")]
        public ZendeskApi_v2.Models.Tickets.Comment Comment { get; set; }

        [JsonProperty("custom_fields")]
        public IList<CustomField> CustomFields { get; set; }

        [JsonProperty("description")]
        public string Description { get; internal set; }

        [JsonProperty("due_at"), JsonConverter(typeof(IsoDateTimeConverter))]
        public DateTimeOffset? DueAt { get; set; }

        [JsonProperty("external_id")]
        public object ExternalId { get; set; }

        [JsonProperty("forum_topic_id")]
        public object ForumTopicId { get; set; }

        [JsonProperty("group_id")]
        public long? GroupId { get; set; }

        [JsonProperty("has_incidents")]
        public bool? HasIncidents { get; set; }

        [JsonProperty("organization_id")]
        public long? OrganizationId { get; set; }

        [JsonProperty("priority")]
        public string Priority { get; set; }

        [JsonProperty("problem_id")]
        public object ProblemId { get; set; }

        [JsonProperty("recipient")]
        public string Recipient { get; set; }

        [JsonProperty("requester")]
        public ZendeskApi_v2.Models.Tickets.Requester Requester { get; set; }

        [JsonProperty("requester_id")]
        public long? RequesterId { get; set; }

        [JsonProperty("satisfaction_rating")]
        public ZendeskApi_v2.Models.Tickets.SatisfactionRating SatisfactionRating { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("subject")]
        public string Subject { get; set; }

        [JsonProperty("submitter_id")]
        public long? SubmitterId { get; set; }

        [JsonProperty("tags")]
        public IList<string> Tags { get; set; }

        [JsonProperty("ticket_form_id")]
        public long? TicketFormId { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("via")]
        public ZendeskApi_v2.Models.Shared.Via Via { get; set; }
    }
}

