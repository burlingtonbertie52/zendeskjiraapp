﻿namespace ZendeskApi_v2.Models.Tickets
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class Options
    {
        [JsonProperty("hour_offset")]
        public string HourOffset { get; set; }

        [JsonProperty("timezone")]
        public string Timezone { get; set; }
    }
}

