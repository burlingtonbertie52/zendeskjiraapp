﻿namespace ZendeskApi_v2.Models.Tickets
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class Field
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }
}

