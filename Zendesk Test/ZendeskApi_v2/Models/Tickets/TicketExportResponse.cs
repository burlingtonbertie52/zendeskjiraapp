﻿namespace ZendeskApi_v2.Models.Tickets
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class TicketExportResponse
    {
        [JsonProperty("end_time")]
        public long EndTime { get; set; }

        [JsonProperty("field_headers")]
        public ZendeskApi_v2.Models.Tickets.FieldHeaders FieldHeaders { get; set; }

        [JsonProperty("next_page")]
        public string NextPage { get; set; }

        [JsonProperty("options")]
        public ZendeskApi_v2.Models.Tickets.Options Options { get; set; }

        [JsonProperty("results")]
        public IList<TicketExportResult> Results { get; set; }
    }
}

