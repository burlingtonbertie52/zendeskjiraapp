﻿namespace ZendeskApi_v2.Models.Tickets
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class IndividualTicketMetricResponse
    {
        [JsonProperty("ticket_metric")]
        public ZendeskApi_v2.Models.Tickets.TicketMetric TicketMetric { get; set; }
    }
}

