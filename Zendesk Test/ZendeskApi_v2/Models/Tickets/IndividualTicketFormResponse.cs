﻿namespace ZendeskApi_v2.Models.Tickets
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class IndividualTicketFormResponse
    {
        [JsonProperty("ticket_form")]
        public ZendeskApi_v2.Models.Tickets.TicketForm TicketForm { get; set; }
    }
}

