﻿namespace ZendeskApi_v2.Models.Tickets
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models.Shared;

    public class Comment
    {
        [JsonProperty("attachments")]
        public IList<Attachment> Attachments { get; private set; }

        [JsonProperty("author_id")]
        public long? AuthorId { get; set; }

        [JsonProperty("body")]
        public string Body { get; set; }

        [JsonProperty("created_at"), JsonConverter(typeof(IsoDateTimeConverter))]
        public DateTimeOffset? CreatedAt { get; set; }

        [JsonProperty("html_body")]
        public string HtmlBody { get; private set; }

        [JsonProperty("id")]
        public long? Id { get; set; }

        [JsonProperty("metadata")]
        public ZendeskApi_v2.Models.Shared.MetaData MetaData { get; private set; }

        [JsonProperty("public")]
        public bool Public { get; set; }

        [JsonProperty("uploads")]
        public IList<string> Uploads { get; set; }

        [JsonProperty("via")]
        public ZendeskApi_v2.Models.Shared.Via Via { get; private set; }
    }
}

