﻿namespace ZendeskApi_v2.Models.Tickets
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class IndividualTicketFieldResponse
    {
        [JsonProperty("ticket_field")]
        public ZendeskApi_v2.Models.Tickets.TicketField TicketField { get; set; }
    }
}

