﻿namespace ZendeskApi_v2.Models.Views
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class ViewCount
    {
        [JsonProperty("fresh")]
        public bool Fresh { get; set; }

        [JsonProperty("pretty")]
        public string Pretty { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("value")]
        public long? Value { get; set; }

        [JsonProperty("view_id")]
        public long ViewId { get; set; }
    }
}

