﻿namespace ZendeskApi_v2.Models.Views
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class Conditions
    {
        [JsonProperty("all")]
        public IList<ZendeskApi_v2.Models.Views.All> All { get; set; }

        [JsonProperty("any")]
        public IList<object> Any { get; set; }
    }
}

