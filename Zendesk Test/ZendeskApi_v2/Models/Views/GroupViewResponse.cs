﻿namespace ZendeskApi_v2.Models.Views
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models;

    public class GroupViewResponse : GroupResponseBase
    {
        [JsonProperty("views")]
        public IList<View> Views { get; set; }
    }
}

