﻿namespace ZendeskApi_v2.Models.Views
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class IndividualViewCountResponse
    {
        [JsonProperty("view_count")]
        public ZendeskApi_v2.Models.Views.ViewCount ViewCount { get; set; }
    }
}

