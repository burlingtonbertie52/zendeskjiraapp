﻿namespace ZendeskApi_v2.Models.Views
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class IndividualViewResponse
    {
        [JsonProperty("view")]
        public ZendeskApi_v2.Models.Views.View View { get; set; }
    }
}

