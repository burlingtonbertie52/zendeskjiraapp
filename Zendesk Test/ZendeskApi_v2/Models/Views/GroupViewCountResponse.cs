﻿namespace ZendeskApi_v2.Models.Views
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class GroupViewCountResponse
    {
        [JsonProperty("view_counts")]
        public IList<ViewCount> ViewCounts { get; set; }
    }
}

