﻿namespace ZendeskApi_v2.Models.Views
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class Field
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }
    }
}

