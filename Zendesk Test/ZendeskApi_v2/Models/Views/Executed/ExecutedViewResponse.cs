﻿namespace ZendeskApi_v2.Models.Views.Executed
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models;

    public class ExecutedViewResponse : GroupResponseBase
    {
        [JsonProperty("columns")]
        public IList<Column> Columns { get; set; }

        [JsonProperty("rows")]
        public IList<Row> Rows { get; set; }

        [JsonProperty("users")]
        public IList<ExecutedUser> Users { get; set; }

        [JsonProperty("view")]
        public ZendeskApi_v2.Models.Views.Executed.View View { get; set; }
    }
}

