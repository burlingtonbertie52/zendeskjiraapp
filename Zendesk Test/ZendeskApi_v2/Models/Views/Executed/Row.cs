﻿namespace ZendeskApi_v2.Models.Views.Executed
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class Row
    {
        [JsonProperty("assignee_id")]
        public long AssigneeId { get; set; }

        [JsonProperty("created")]
        public string Created { get; set; }

        [JsonProperty("custom_fields")]
        public IList<CustomField> CustomFields { get; set; }

        [JsonProperty("ticket")]
        public ZendeskApi_v2.Models.Views.Executed.ExecutedTicket ExecutedTicket { get; set; }

        [JsonProperty("fields")]
        public IList<Field> Fields { get; set; }

        [JsonProperty("priority")]
        public string Priority { get; set; }

        [JsonProperty("requester_id")]
        public long RequesterId { get; set; }

        [JsonProperty("score")]
        public int Score { get; set; }

        [JsonProperty("subject")]
        public string Subject { get; set; }

        [JsonProperty("updated")]
        public string Updated { get; set; }
    }
}

