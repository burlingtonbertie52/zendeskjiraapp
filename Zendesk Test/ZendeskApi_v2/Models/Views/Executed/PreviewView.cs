﻿namespace ZendeskApi_v2.Models.Views.Executed
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class PreviewView
    {
        [JsonProperty("all")]
        public IList<ZendeskApi_v2.Models.Views.All> All { get; set; }

        [JsonProperty("output")]
        public PreviewViewOutput Output { get; set; }
    }
}

