﻿namespace ZendeskApi_v2.Models.Views.Executed
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class ExecutedTicket
    {
        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("last_comment")]
        public ZendeskApi_v2.Models.Views.Executed.LastComment LastComment { get; set; }

        [JsonProperty("priority")]
        public string Priority { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("subject")]
        public string Subject { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }
    }
}

