﻿namespace ZendeskApi_v2.Models.Views.Executed
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class PreviewViewOutput
    {
        [JsonProperty("columns")]
        public IList<string> Columns { get; set; }
    }
}

