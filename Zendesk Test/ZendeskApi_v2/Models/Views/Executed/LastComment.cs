﻿namespace ZendeskApi_v2.Models.Views.Executed
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using System;
    using System.Runtime.CompilerServices;

    public class LastComment
    {
        [JsonProperty("author_id")]
        public long AuthorId { get; set; }

        [JsonProperty("body")]
        public string Body { get; set; }

        [JsonProperty("created_at"), JsonConverter(typeof(IsoDateTimeConverter))]
        public DateTimeOffset? CreatedAt { get; set; }
    }
}

