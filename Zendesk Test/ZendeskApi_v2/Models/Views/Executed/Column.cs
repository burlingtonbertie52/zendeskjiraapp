﻿namespace ZendeskApi_v2.Models.Views.Executed
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class Column
    {
        [JsonProperty("id")]
        public object Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }
    }
}

