﻿namespace ZendeskApi_v2.Models.Views.Executed
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class ExecutedUser
    {
        [JsonProperty("id")]
        public long? Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }
    }
}

