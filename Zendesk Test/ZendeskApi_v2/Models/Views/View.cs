﻿namespace ZendeskApi_v2.Models.Views
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using System;
    using System.Runtime.CompilerServices;

    public class View
    {
        [JsonProperty("active")]
        public bool Active { get; set; }

        [JsonProperty("conditions")]
        public ZendeskApi_v2.Models.Views.Conditions Conditions { get; set; }

        [JsonConverter(typeof(IsoDateTimeConverter)), JsonProperty("created_at")]
        public DateTimeOffset? CreatedAt { get; set; }

        [JsonProperty("execution")]
        public ZendeskApi_v2.Models.Views.Execution Execution { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("restriction")]
        public object Restriction { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonConverter(typeof(IsoDateTimeConverter)), JsonProperty("updated_at")]
        public DateTimeOffset? UpdatedAt { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }
    }
}

