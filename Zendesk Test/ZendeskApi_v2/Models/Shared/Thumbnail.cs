﻿namespace ZendeskApi_v2.Models.Shared
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class Thumbnail
    {
        [JsonProperty("content_type")]
        public string ContentType { get; set; }

        [JsonProperty("content_url")]
        public string ContentUrl { get; set; }

        [JsonProperty("file_name")]
        public string FileName { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("size")]
        public int Size { get; set; }
    }
}

