﻿namespace ZendeskApi_v2.Models.Shared
{
    using System;
    using System.Runtime.CompilerServices;

    public class ZenFile
    {
        public string ContentType { get; set; }

        public byte[] FileData { get; set; }

        public string FileName { get; set; }
    }
}

