﻿namespace ZendeskApi_v2.Models.Shared
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models.Tickets;

    public class MetaData
    {
        [JsonProperty("custom")]
        public ZendeskApi_v2.Models.Tickets.Custom Custom { get; set; }

        [JsonProperty("system")]
        public ZendeskApi_v2.Models.Shared.System System { get; set; }
    }
}

