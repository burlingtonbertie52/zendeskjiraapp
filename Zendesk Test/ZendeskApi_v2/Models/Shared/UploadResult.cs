﻿namespace ZendeskApi_v2.Models.Shared
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class UploadResult
    {
        [JsonProperty("upload")]
        public ZendeskApi_v2.Models.Shared.Upload Upload { get; set; }
    }
}

