﻿namespace ZendeskApi_v2.Models.Shared
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class System
    {
        [JsonProperty("ip_address")]
        public string IpAddress { get; set; }
    }
}

