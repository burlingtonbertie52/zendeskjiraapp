﻿namespace ZendeskApi_v2.Models.Shared
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class IndividualAuditResponse
    {
        [JsonProperty("audit")]
        public ZendeskApi_v2.Models.Shared.Audit Audit { get; set; }
    }
}

