﻿namespace ZendeskApi_v2.Models.Shared
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class Event
    {
        [JsonProperty("action")]
        public string Action { get; set; }

        [JsonProperty("agreement_id")]
        public string AgreementId { get; set; }

        [JsonProperty("assignee_id")]
        public string AssigneeId { get; set; }

        [JsonProperty("attachments")]
        public IList<Attachment> Attachments { get; set; }

        [JsonProperty("author_id")]
        public long? AuthorId { get; set; }

        [JsonProperty("body")]
        public string Body { get; set; }

        [JsonProperty("comment_id")]
        public long? CommentId { get; set; }

        [JsonProperty("communication")]
        public long? Communication { get; set; }

        [JsonProperty("data")]
        public string Data { get; set; }

        [JsonProperty("direct_message")]
        public string DirectMessage { get; set; }

        [JsonProperty("page")]
        public ZendeskApi_v2.Models.Shared.FacebookPage FacebookPage { get; set; }

        [JsonProperty("field_name")]
        public string FieldName { get; set; }

        [JsonProperty("formatted_from")]
        public string FormattedFrom { get; set; }

        [JsonProperty("html_body")]
        public string HtmlBody { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("phone_number")]
        public string PhoneNumber { get; set; }

        [JsonProperty("previous_value")]
        public object PreviousValue { get; set; }

        [JsonProperty("public")]
        public bool Public { get; set; }

        [JsonProperty("recipient_id")]
        public string RecipientId { get; set; }

        [JsonProperty("recipients")]
        public IList<string> Recipients { get; set; }

        [JsonProperty("resource")]
        public string Resource { get; set; }

        [JsonProperty("score")]
        public string Score { get; set; }

        [JsonProperty("subject")]
        public string Subject { get; set; }

        [JsonProperty("success")]
        public string Success { get; set; }

        [JsonProperty("ticket_via")]
        public string TicketVia { get; set; }

        [JsonProperty("transcription_visible")]
        public string TranscriptionVisible { get; set; }

        [JsonProperty("trusted")]
        public bool? Trusted { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("value")]
        public object Value { get; set; }

        [JsonProperty("value_reference")]
        public string ValueReference { get; set; }

        [JsonProperty("via")]
        public ZendeskApi_v2.Models.Shared.Via Via { get; set; }
    }
}

