﻿namespace ZendeskApi_v2.Models.Shared
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class JobStatusResponse
    {
        [JsonProperty("job_status")]
        public ZendeskApi_v2.Models.Shared.JobStatus JobStatus { get; set; }
    }
}

