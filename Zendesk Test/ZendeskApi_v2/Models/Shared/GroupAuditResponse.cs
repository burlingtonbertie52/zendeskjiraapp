﻿namespace ZendeskApi_v2.Models.Shared
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models;

    public class GroupAuditResponse : GroupResponseBase
    {
        [JsonProperty("audits")]
        public IList<Audit> Audits { get; set; }
    }
}

