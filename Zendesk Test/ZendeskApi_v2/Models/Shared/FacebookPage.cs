﻿namespace ZendeskApi_v2.Models.Shared
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class FacebookPage
    {
        [JsonProperty("graph_id")]
        public string GraphId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}

