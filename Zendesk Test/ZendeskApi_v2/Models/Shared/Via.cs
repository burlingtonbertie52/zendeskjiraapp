﻿namespace ZendeskApi_v2.Models.Shared
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models.Tickets;

    public class Via
    {
        [JsonProperty("channel")]
        public string Channel { get; set; }

        [JsonProperty("source")]
        public ZendeskApi_v2.Models.Tickets.Source Source { get; set; }
    }
}

