﻿namespace ZendeskApi_v2.Models.Shared
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class Audit
    {
        [JsonProperty("author_id")]
        public long AuthorId { get; set; }

        [JsonProperty("created_at"), JsonConverter(typeof(IsoDateTimeConverter))]
        public DateTimeOffset? CreatedAt { get; set; }

        [JsonProperty("events")]
        public IList<Event> Events { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("metadata")]
        public ZendeskApi_v2.Models.Shared.MetaData MetaData { get; set; }

        [JsonProperty("ticket_id")]
        public string TicketId { get; set; }

        [JsonProperty("via")]
        public ZendeskApi_v2.Models.Shared.Via Via { get; set; }
    }
}

