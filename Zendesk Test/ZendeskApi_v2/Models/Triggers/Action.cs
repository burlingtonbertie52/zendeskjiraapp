﻿namespace ZendeskApi_v2.Models.Triggers
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class Action
    {
        [JsonProperty("field")]
        public string Field { get; set; }

        [JsonProperty("value")]
        public object Value { get; set; }
    }
}

