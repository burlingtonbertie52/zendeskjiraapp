﻿namespace ZendeskApi_v2.Models.Triggers
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class Conditions
    {
        [JsonProperty("all")]
        public IList<ZendeskApi_v2.Models.Triggers.All> All { get; set; }

        [JsonProperty("any")]
        public IList<ZendeskApi_v2.Models.Triggers.All> Any { get; set; }
    }
}

