﻿namespace ZendeskApi_v2.Models.Triggers
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class All
    {
        [JsonProperty("field")]
        public string Field { get; set; }

        [JsonProperty("operator")]
        public string Operator { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }
}

