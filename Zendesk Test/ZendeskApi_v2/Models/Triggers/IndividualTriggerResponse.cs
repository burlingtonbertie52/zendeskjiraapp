﻿namespace ZendeskApi_v2.Models.Triggers
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class IndividualTriggerResponse
    {
        [JsonProperty("trigger")]
        public ZendeskApi_v2.Models.Triggers.Trigger Trigger { get; set; }
    }
}

