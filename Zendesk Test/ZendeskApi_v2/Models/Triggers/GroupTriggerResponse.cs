﻿namespace ZendeskApi_v2.Models.Triggers
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models;

    public class GroupTriggerResponse : GroupResponseBase
    {
        [JsonProperty("triggers")]
        public IList<Trigger> Triggers { get; set; }
    }
}

