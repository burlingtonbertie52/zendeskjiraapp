﻿namespace ZendeskApi_v2.Models.Topics
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models;

    public class GroupTopicCommentResponse : GroupResponseBase
    {
        [JsonProperty("topic_comments")]
        public IList<TopicComment> TopicComments { get; set; }
    }
}

