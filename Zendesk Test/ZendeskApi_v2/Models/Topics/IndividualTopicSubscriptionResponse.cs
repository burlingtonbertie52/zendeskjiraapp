﻿namespace ZendeskApi_v2.Models.Topics
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class IndividualTopicSubscriptionResponse
    {
        [JsonProperty("topic_subscription")]
        public ZendeskApi_v2.Models.Topics.TopicSubscription TopicSubscription { get; set; }
    }
}

