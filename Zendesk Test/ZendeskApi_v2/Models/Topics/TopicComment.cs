﻿namespace ZendeskApi_v2.Models.Topics
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class TopicComment
    {
        [JsonProperty("attachments")]
        public IList<Attachment> Attachments { get; set; }

        [JsonProperty("body")]
        public string Body { get; set; }

        [JsonProperty("created_at"), JsonConverter(typeof(IsoDateTimeConverter))]
        public DateTimeOffset? CreatedAt { get; set; }

        [JsonProperty("id")]
        public long? Id { get; set; }

        [JsonProperty("informative")]
        public bool Informative { get; set; }

        [JsonProperty("topic_id")]
        public long? TopicId { get; set; }

        [JsonProperty("updated_at"), JsonConverter(typeof(IsoDateTimeConverter))]
        public DateTimeOffset? UpdatedAt { get; set; }

        [JsonProperty("uploads")]
        public IList<string> Uploads { get; set; }

        [JsonProperty("user_id")]
        public long? UserId { get; set; }
    }
}

