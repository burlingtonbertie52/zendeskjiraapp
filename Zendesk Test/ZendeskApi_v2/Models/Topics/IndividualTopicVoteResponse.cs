﻿namespace ZendeskApi_v2.Models.Topics
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class IndividualTopicVoteResponse
    {
        [JsonProperty("topic_vote")]
        public ZendeskApi_v2.Models.Topics.TopicVote TopicVote { get; set; }
    }
}

