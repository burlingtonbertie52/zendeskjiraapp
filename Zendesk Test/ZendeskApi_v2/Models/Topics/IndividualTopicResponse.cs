﻿namespace ZendeskApi_v2.Models.Topics
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class IndividualTopicResponse
    {
        [JsonProperty("topic")]
        public ZendeskApi_v2.Models.Topics.Topic Topic { get; set; }
    }
}

