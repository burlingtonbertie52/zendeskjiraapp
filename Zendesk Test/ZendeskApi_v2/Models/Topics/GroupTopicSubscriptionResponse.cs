﻿namespace ZendeskApi_v2.Models.Topics
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models;

    public class GroupTopicSubscriptionResponse : GroupResponseBase
    {
        [JsonProperty("topic_subscriptions")]
        public IList<TopicSubscription> TopicSubscriptions { get; set; }
    }
}

