﻿namespace ZendeskApi_v2.Models.Topics
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models;

    public class GroupTopicResponse : GroupResponseBase
    {
        [JsonProperty("topics")]
        public IList<Topic> Topics { get; set; }
    }
}

