﻿namespace ZendeskApi_v2.Models.Topics
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class IndividualTopicCommentResponse
    {
        [JsonProperty("topic_comment")]
        public ZendeskApi_v2.Models.Topics.TopicComment TopicComment { get; set; }
    }
}

