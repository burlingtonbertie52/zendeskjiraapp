﻿namespace ZendeskApi_v2.Models.Topics
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models;

    public class GroupTopicVoteResponse : GroupResponseBase
    {
        [JsonProperty("topic_votes")]
        public IList<TopicVote> TopicVotes { get; set; }
    }
}

