﻿namespace ZendeskApi_v2.Models.AccountsAndActivities
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using System;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models.Users;

    public class Activity
    {
        [JsonProperty("actor")]
        public ZendeskApi_v2.Models.Users.User Actor { get; set; }

        [JsonConverter(typeof(IsoDateTimeConverter)), JsonProperty("created_at")]
        public DateTimeOffset? CreatedAt { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("updated_at"), JsonConverter(typeof(IsoDateTimeConverter))]
        public DateTimeOffset? UpdatedAt { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("user")]
        public ZendeskApi_v2.Models.Users.User User { get; set; }

        [JsonProperty("verb")]
        public string Verb { get; set; }
    }
}

