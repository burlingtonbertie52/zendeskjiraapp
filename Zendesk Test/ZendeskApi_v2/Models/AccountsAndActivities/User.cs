﻿namespace ZendeskApi_v2.Models.AccountsAndActivities
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class User
    {
        [JsonProperty("tagging")]
        public bool Tagging { get; set; }
    }
}

