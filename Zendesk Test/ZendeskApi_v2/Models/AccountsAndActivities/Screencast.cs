﻿namespace ZendeskApi_v2.Models.AccountsAndActivities
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class Screencast
    {
        [JsonProperty("enabled_for_tickets")]
        public bool EnabledForTickets { get; set; }

        [JsonProperty("host")]
        public object Host { get; set; }

        [JsonProperty("tickets_recorder_id")]
        public object TicketsRecorderId { get; set; }
    }
}

