﻿namespace ZendeskApi_v2.Models.AccountsAndActivities
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class IndividualActivityResponse
    {
        [JsonProperty("activity")]
        public ZendeskApi_v2.Models.AccountsAndActivities.Activity Activity { get; set; }
    }
}

