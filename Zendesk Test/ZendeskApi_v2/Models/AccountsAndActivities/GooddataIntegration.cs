﻿namespace ZendeskApi_v2.Models.AccountsAndActivities
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class GooddataIntegration
    {
        [JsonProperty("enabled")]
        public bool Enabled { get; set; }
    }
}

