﻿namespace ZendeskApi_v2.Models.AccountsAndActivities
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class Lotus
    {
        [JsonProperty("prefer_lotus")]
        public bool PreferLotus { get; set; }

        [JsonProperty("reporting")]
        public bool Reporting { get; set; }
    }
}

