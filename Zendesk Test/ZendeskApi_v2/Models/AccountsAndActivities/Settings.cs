﻿namespace ZendeskApi_v2.Models.AccountsAndActivities
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class Settings
    {
        [JsonProperty("apps")]
        public ZendeskApi_v2.Models.AccountsAndActivities.Apps Apps { get; set; }

        [JsonProperty("branding")]
        public ZendeskApi_v2.Models.AccountsAndActivities.Branding Branding { get; set; }

        [JsonProperty("chat")]
        public ZendeskApi_v2.Models.AccountsAndActivities.Chat Chat { get; set; }

        [JsonProperty("gooddata_integration")]
        public ZendeskApi_v2.Models.AccountsAndActivities.GooddataIntegration GooddataIntegration { get; set; }

        [JsonProperty("lotus")]
        public ZendeskApi_v2.Models.AccountsAndActivities.Lotus Lotus { get; set; }

        [JsonProperty("screencast")]
        public ZendeskApi_v2.Models.AccountsAndActivities.Screencast Screencast { get; set; }

        [JsonProperty("tickets")]
        public ZendeskApi_v2.Models.AccountsAndActivities.Tickets Tickets { get; set; }

        [JsonProperty("twitter")]
        public ZendeskApi_v2.Models.AccountsAndActivities.Twitter Twitter { get; set; }

        [JsonProperty("user")]
        public ZendeskApi_v2.Models.AccountsAndActivities.User User { get; set; }

        [JsonProperty("voice")]
        public ZendeskApi_v2.Models.AccountsAndActivities.Voice Voice { get; set; }
    }
}

