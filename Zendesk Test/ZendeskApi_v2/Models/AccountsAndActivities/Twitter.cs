﻿namespace ZendeskApi_v2.Models.AccountsAndActivities
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class Twitter
    {
        [JsonProperty("shorten_url")]
        public string ShortenUrl { get; set; }
    }
}

