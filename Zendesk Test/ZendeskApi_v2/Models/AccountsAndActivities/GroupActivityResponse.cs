﻿namespace ZendeskApi_v2.Models.AccountsAndActivities
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models;

    public class GroupActivityResponse : GroupResponseBase
    {
        [JsonProperty("activities")]
        public IList<Activity> Activities { get; set; }
    }
}

