﻿namespace ZendeskApi_v2.Models.AccountsAndActivities
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class Apps
    {
        [JsonProperty("create_private")]
        public bool CreatePrivate { get; set; }

        [JsonProperty("create_public")]
        public bool CreatePublic { get; set; }

        [JsonProperty("use")]
        public bool Use { get; set; }
    }
}

