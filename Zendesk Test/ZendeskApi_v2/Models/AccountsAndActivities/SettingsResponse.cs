﻿namespace ZendeskApi_v2.Models.AccountsAndActivities
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class SettingsResponse
    {
        [JsonProperty("settings")]
        public ZendeskApi_v2.Models.AccountsAndActivities.Settings Settings { get; set; }
    }
}

