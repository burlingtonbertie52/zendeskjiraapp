﻿namespace ZendeskApi_v2.Models.AccountsAndActivities
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class Branding
    {
        [JsonProperty("header_color")]
        public string HeaderColor { get; set; }

        [JsonProperty("page_background_color")]
        public string PageBackgroundColor { get; set; }

        [JsonProperty("tab_background_color")]
        public string TabBackgroundColor { get; set; }

        [JsonProperty("text_color")]
        public string TextColor { get; set; }
    }
}

