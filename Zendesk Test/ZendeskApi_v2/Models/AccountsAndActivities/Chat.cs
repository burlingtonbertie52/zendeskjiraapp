﻿namespace ZendeskApi_v2.Models.AccountsAndActivities
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class Chat
    {
        [JsonProperty("enabled")]
        public bool Enabled { get; set; }

        [JsonProperty("maximum_request_count")]
        public object MaximumRequestCount { get; set; }

        [JsonProperty("welcome_message")]
        public string WelcomeMessage { get; set; }
    }
}

