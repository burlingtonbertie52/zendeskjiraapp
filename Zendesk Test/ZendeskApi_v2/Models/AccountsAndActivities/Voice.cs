﻿namespace ZendeskApi_v2.Models.AccountsAndActivities
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class Voice
    {
        [JsonProperty("enabled")]
        public bool Enabled { get; set; }

        [JsonProperty("logging")]
        public bool Logging { get; set; }

        [JsonProperty("maintenance")]
        public bool Maintenance { get; set; }

        [JsonProperty("outbound")]
        public bool Outbound { get; set; }
    }
}

