﻿namespace ZendeskApi_v2.Models.Constants
{
    using System;

    public static class UserIdentityTypes
    {
        public const string Email = "email";
        public const string Facebook = "facebook";
        public const string Google = "google";
        public const string Twitter = "twitter";
    }
}

