﻿namespace ZendeskApi_v2.Models.Constants
{
    using System;

    public static class ForumAccessTypes
    {
        public const string AgentsOnly = "agents only";
        public const string Everybody = "everybody";
        public const string LoggedInUsers = "logged-in users";
    }
}

