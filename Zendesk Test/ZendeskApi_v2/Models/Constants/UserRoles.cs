﻿namespace ZendeskApi_v2.Models.Constants
{
    using System;

    public static class UserRoles
    {
        public const string Admin = "admin";
        public const string Agent = "agent";
        public const string EndUser = "end-user";
    }
}

