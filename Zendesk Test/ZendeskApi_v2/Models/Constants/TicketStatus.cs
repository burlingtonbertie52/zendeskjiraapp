﻿namespace ZendeskApi_v2.Models.Constants
{
    using System;

    public static class TicketStatus
    {
        public const string Closed = "closed";
        public const string Hold = "hold";
        public const string New = "new";
        public const string Open = "open";
        public const string Pending = "pending";
        public const string Solved = "solved";
    }
}

