﻿namespace ZendeskApi_v2.Models.Constants
{
    using System;

    public static class ViaTypes
    {
        public const string Api = "api";
        public const string Chat = "chat";
        public const string Email = "email";
        public const string Facebook = "facebook";
        public const string Forum = "forum";
        public const string Rule = "rule";
        public const string Sms = "sms";
        public const string System = "system";
        public const string Twitter = "twitter";
        public const string Voice = "voice";
        public const string Web = "web";
    }
}

