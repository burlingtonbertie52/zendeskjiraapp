﻿namespace ZendeskApi_v2.Models.Constants
{
    using System;

    public static class ForumTypes
    {
        public const string Articles = "articles";
        public const string Ideas = "ideas";
        public const string Questions = "questions";
    }
}

