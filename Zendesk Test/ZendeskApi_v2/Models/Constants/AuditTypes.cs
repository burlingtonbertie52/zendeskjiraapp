﻿namespace ZendeskApi_v2.Models.Constants
{
    using System;

    public static class AuditTypes
    {
        public const string Cc = "Cc";
        public const string Change = "Change";
        public const string Comment = "Comment";
        public const string CommentPrivacyChange = "CommentPrivacyChange";
        public const string Create = "CreateTicket";
        public const string Error = "Error";
        public const string External = "External";
        public const string FacebookEvent = "FacebookEvent";
        public const string LogMeInTranscript = "LogMeInTranscript";
        public const string Notification = "Notification";
        public const string Push = "Push";
        public const string SatisfactionRating = "SatisfactionRating";
        public const string SMS = "SMS";
        public const string TicketSharingEvent = "TicketSharingEvent";
        public const string Tweet = "Tweet";
        public const string VoiceComment = "VoiceComment";
    }
}

