﻿namespace ZendeskApi_v2.Models.Constants
{
    using System;

    public static class TicketPriorities
    {
        public const string High = "high";
        public const string Low = "low";
        public const string Normal = "normal";
        public const string Urgent = "urgent";
    }
}

