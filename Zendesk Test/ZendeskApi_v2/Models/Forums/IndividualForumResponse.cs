﻿namespace ZendeskApi_v2.Models.Forums
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class IndividualForumResponse
    {
        [JsonProperty("forum")]
        public ZendeskApi_v2.Models.Forums.Forum Forum { get; set; }
    }
}

