﻿namespace ZendeskApi_v2.Models.Forums
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class IndividualForumSubcriptionResponse
    {
        [JsonProperty("forum_subscription")]
        public ZendeskApi_v2.Models.Forums.ForumSubscription ForumSubscription { get; set; }
    }
}

