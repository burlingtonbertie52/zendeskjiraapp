﻿namespace ZendeskApi_v2.Models.Forums
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using System;
    using System.Runtime.CompilerServices;

    public class Forum
    {
        [JsonProperty("access")]
        public string Access { get; set; }

        [JsonProperty("category_id")]
        public long? CategoryId { get; set; }

        [JsonConverter(typeof(IsoDateTimeConverter)), JsonProperty("created_at")]
        public DateTimeOffset? CreatedAt { get; set; }

        [JsonProperty("description:")]
        public string Description { get; set; }

        [JsonProperty("forum_type")]
        public string ForumType { get; set; }

        [JsonProperty("id")]
        public long? Id { get; set; }

        [JsonProperty("locale_id")]
        public long? LocaleId { get; set; }

        [JsonProperty("locked")]
        public bool Locked { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("organization_id")]
        public long? OrganizationId { get; set; }

        [JsonProperty("position")]
        public int Position { get; set; }

        [JsonConverter(typeof(IsoDateTimeConverter)), JsonProperty("updated_at")]
        public DateTimeOffset? UpdatedAt { get; set; }
    }
}

