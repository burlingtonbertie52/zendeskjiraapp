﻿namespace ZendeskApi_v2.Models.Forums
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models;

    public class GroupForumSubcriptionResponse : GroupResponseBase
    {
        [JsonProperty("forum_subscriptions")]
        public IList<ForumSubscription> ForumSubscriptions { get; set; }
    }
}

