﻿namespace ZendeskApi_v2.Models.Forums
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models;

    public class GroupForumResponse : GroupResponseBase
    {
        [JsonProperty("forums")]
        public IList<Forum> Forums { get; set; }
    }
}

