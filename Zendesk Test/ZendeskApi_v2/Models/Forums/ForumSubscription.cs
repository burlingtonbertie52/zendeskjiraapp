﻿namespace ZendeskApi_v2.Models.Forums
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using System;
    using System.Runtime.CompilerServices;

    public class ForumSubscription
    {
        [JsonConverter(typeof(IsoDateTimeConverter)), JsonProperty("created_at")]
        public DateTimeOffset? CreatedAt { get; set; }

        [JsonProperty("forum_id")]
        public long? ForumId { get; set; }

        [JsonProperty("id")]
        public long? Id { get; set; }

        [JsonProperty("user_id")]
        public long? UserId { get; set; }
    }
}

