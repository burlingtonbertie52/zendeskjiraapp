﻿namespace ZendeskApi_v2.Models.Satisfaction
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class IndividualSatisfactionResponse
    {
        [JsonProperty("satisfaction_rating")]
        public ZendeskApi_v2.Models.Satisfaction.SatisfactionRating SatisfactionRating { get; set; }
    }
}

