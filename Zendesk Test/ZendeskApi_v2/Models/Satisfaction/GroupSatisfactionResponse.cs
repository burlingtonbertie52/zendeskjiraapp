﻿namespace ZendeskApi_v2.Models.Satisfaction
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models;

    public class GroupSatisfactionResponse : GroupResponseBase
    {
        [JsonProperty("satisfaction_ratings")]
        public IList<SatisfactionRating> SatisfactionRatings { get; set; }
    }
}

