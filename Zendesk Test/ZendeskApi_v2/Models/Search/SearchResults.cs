﻿namespace ZendeskApi_v2.Models.Search
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class SearchResults
    {
        [JsonProperty("count")]
        public int Count { get; set; }

        [JsonProperty("description")]
        public object Description { get; set; }

        [JsonProperty("error")]
        public object Error { get; set; }

        [JsonProperty("next_page")]
        public string NextPage { get; set; }

        [JsonProperty("prev_page")]
        public object PrevPage { get; set; }

        [JsonProperty("results")]
        public IList<Result> Results { get; set; }
    }
}

