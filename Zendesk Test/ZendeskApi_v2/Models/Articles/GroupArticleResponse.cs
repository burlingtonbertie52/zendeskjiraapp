﻿namespace ZendeskApi_v2.Models.Articles
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models;

    public class GroupArticleResponse : GroupResponseBase
    {
        [JsonProperty("articles")]
        public IList<Article> Articles { get; set; }
    }
}

