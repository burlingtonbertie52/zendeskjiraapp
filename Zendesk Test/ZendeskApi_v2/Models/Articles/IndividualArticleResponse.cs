﻿namespace ZendeskApi_v2.Models.Articles
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class IndividualArticleResponse
    {
        [JsonProperty("article")]
        public Article Arcticle { get; set; }
    }
}

