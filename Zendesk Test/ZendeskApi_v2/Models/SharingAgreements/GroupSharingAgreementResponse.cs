﻿namespace ZendeskApi_v2.Models.SharingAgreements
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models;

    public class GroupSharingAgreementResponse : GroupResponseBase
    {
        [JsonProperty("sharing_agreements")]
        public IList<SharingAgreement> SharingAgreements { get; set; }
    }
}

