﻿namespace ZendeskApi_v2.Models.Tags
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class GroupTagResult
    {
        [JsonProperty("tags")]
        public IList<Tag> Tags { get; set; }
    }
}

