﻿namespace ZendeskApi_v2.Models.Tags
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class Tag
    {
        [JsonProperty("count")]
        public int Count { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}

