﻿namespace ZendeskApi_v2.Models.Requests
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models.Tickets;

    public class IndividualCommentResponse
    {
        [JsonProperty("comment")]
        public ZendeskApi_v2.Models.Tickets.Comment Comment { get; set; }
    }
}

