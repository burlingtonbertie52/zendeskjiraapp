﻿namespace ZendeskApi_v2.Models.Requests
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models;

    public class GroupRequestResponse : GroupResponseBase
    {
        [JsonProperty("requests")]
        public IList<Request> Requests { get; set; }
    }
}

