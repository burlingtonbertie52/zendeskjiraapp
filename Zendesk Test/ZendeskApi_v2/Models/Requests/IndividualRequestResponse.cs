﻿namespace ZendeskApi_v2.Models.Requests
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class IndividualRequestResponse
    {
        [JsonProperty("request")]
        public ZendeskApi_v2.Models.Requests.Request Request { get; set; }
    }
}

