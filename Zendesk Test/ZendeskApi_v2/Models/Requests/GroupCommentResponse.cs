﻿namespace ZendeskApi_v2.Models.Requests
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models;

    public class GroupCommentResponse : GroupResponseBase
    {
        [JsonProperty("comments")]
        public IList<Comment> Comments { get; set; }
    }
}

