﻿namespace ZendeskApi_v2.Models
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class GroupResponseBase
    {
        [JsonProperty("count")]
        public int Count { get; set; }

        [JsonProperty("next_page")]
        public string NextPage { get; set; }

        [JsonProperty("previous_page")]
        public string PreviousPage { get; set; }
    }
}

