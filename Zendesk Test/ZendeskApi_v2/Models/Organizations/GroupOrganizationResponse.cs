﻿namespace ZendeskApi_v2.Models.Organizations
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models;

    public class GroupOrganizationResponse : GroupResponseBase
    {
        [JsonProperty("organizations")]
        public IList<Organization> Organizations { get; set; }
    }
}

