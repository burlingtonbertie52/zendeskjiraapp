﻿namespace ZendeskApi_v2.Models.Organizations
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class Organization
    {
        [JsonConverter(typeof(IsoDateTimeConverter)), JsonProperty("created_at")]
        public DateTimeOffset? CreatedAt { get; set; }

        [JsonProperty("details")]
        public object Details { get; set; }

        [JsonProperty("domain_names")]
        public IList<object> DomainNames { get; set; }

        [JsonProperty("external_id")]
        public object ExternalId { get; set; }

        [JsonProperty("group_id")]
        public object GroupId { get; set; }

        [JsonProperty("id")]
        public long? Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("notes")]
        public object Notes { get; set; }

        [JsonProperty("organization_fields")]
        public IDictionary<string, string> OrganizationFields { get; set; }

        [JsonProperty("shared_comments")]
        public bool SharedComments { get; set; }

        [JsonProperty("shared_tickets")]
        public bool SharedTickets { get; set; }

        [JsonProperty("tags")]
        public IList<string> Tags { get; set; }

        [JsonConverter(typeof(IsoDateTimeConverter)), JsonProperty("updated_at")]
        public DateTimeOffset? UpdatedAt { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }
    }
}

