﻿namespace ZendeskApi_v2.Models.Organizations
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class IndividualOrganizationResponse
    {
        [JsonProperty("organization")]
        public ZendeskApi_v2.Models.Organizations.Organization Organization { get; set; }
    }
}

