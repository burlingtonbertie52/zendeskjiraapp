﻿namespace ZendeskApi_v2.Models.CustomRoles
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models;

    public class CustomRoles : GroupResponseBase
    {
        [JsonProperty("custom_roles")]
        public IList<CustomRole> CustomRoleCollection { get; set; }
    }
}

