﻿namespace ZendeskApi_v2.Models.Sections
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Models;

    public class GroupSectionResponse : GroupResponseBase
    {
        [JsonProperty("sections")]
        public IList<Section> Sections { get; set; }
    }
}

