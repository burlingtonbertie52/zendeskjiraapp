﻿namespace ZendeskApi_v2.Models.Sections
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class Section
    {
        [JsonProperty("category_id")]
        public long? CategoryId { get; set; }

        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("id")]
        public long? Id { get; set; }

        [JsonProperty("locale")]
        public string Locale { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("position")]
        public int Position { get; set; }

        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }

        [JsonProperty("visibility")]
        public string Visibility { get; set; }
    }
}

