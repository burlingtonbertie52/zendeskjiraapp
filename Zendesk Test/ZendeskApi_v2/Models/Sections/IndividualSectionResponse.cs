﻿namespace ZendeskApi_v2.Models.Sections
{
    using Newtonsoft.Json;
    using System;
    using System.Runtime.CompilerServices;

    public class IndividualSectionResponse
    {
        [JsonProperty("section")]
        public ZendeskApi_v2.Models.Sections.Section Section { get; set; }
    }
}

