﻿namespace ZendeskApi_v2
{
    using Newtonsoft.Json;
    using System;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Text.RegularExpressions;

    public class Core : ICore
    {
        protected string ApiToken;
        protected string Password;
        internal IWebProxy Proxy;
        protected string User;
        private const string XOnBehalfOfEmail = "X-On-Behalf-Of";
        protected string ZendeskUrl;

        public Core(string zendeskApiUrl, string user, string password, string apiToken)
        {
            this.User = user;
            this.Password = password;
            this.ZendeskUrl = zendeskApiUrl;
            this.ApiToken = apiToken;
        }

        protected bool GenericBoolPost(string resource, object body = null)
        {
            return (this.RunRequest(resource, "POST", body).HttpStatusCode == HttpStatusCode.OK);
        }

        protected bool GenericBoolPut(string resource, object body = null)
        {
            return (this.RunRequest(resource, "PUT", body).HttpStatusCode == HttpStatusCode.OK);
        }

        protected bool GenericDelete(string resource)
        {
            RequestResult result = this.RunRequest(resource, "DELETE", null);
            return ((result.HttpStatusCode == HttpStatusCode.OK) || (result.HttpStatusCode == HttpStatusCode.NoContent));
        }

        protected T GenericGet<T>(string resource)
        {
            return this.RunRequest<T>(resource, "GET", null);
        }

        protected T GenericPost<T>(string resource, object body = null)
        {
            return this.RunRequest<T>(resource, "POST", body);
        }

        protected T GenericPut<T>(string resource, object body = null)
        {
            return this.RunRequest<T>(resource, "PUT", body);
        }

        protected string GetAuthHeader(string userName, string password)
        {
            string str = Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Format("{0}:{1}", userName, password)));
            return string.Format("Basic {0}", str);
        }

        public T GetByPageUrl<T>(string pageUrl, int perPage = 100)
        {
            if (string.IsNullOrEmpty(pageUrl))
            {
                return JsonConvert.DeserializeObject<T>("");
            }
            string resource = Regex.Split(pageUrl, "api/v2/").Last<string>() + "&per_page=" + perPage;
            return this.RunRequest<T>(resource, "GET", null);
        }

        protected string GetPasswordOrTokenAuthHeader()
        {
            if (!(string.IsNullOrEmpty(this.ApiToken) || (this.ApiToken.Trim().Length < 0)))
            {
                return this.GetAuthHeader(this.User + "/token", this.ApiToken);
            }
            return this.GetAuthHeader(this.User, this.Password);
        }

        public RequestResult RunRequest(string resource, string requestMethod, object body = null)
        {
            RequestResult result2;
            try
            {
                string zendeskUrl = this.ZendeskUrl;
                if (!zendeskUrl.EndsWith("/"))
                {
                    zendeskUrl = zendeskUrl + "/";
                }
                HttpWebRequest request = WebRequest.Create(zendeskUrl + resource) as HttpWebRequest;
                request.ContentType = "application/json";
                if (this.Proxy != null)
                {
                    request.Proxy = this.Proxy;
                }
                request.Headers["Authorization"] = this.GetPasswordOrTokenAuthHeader();
                request.PreAuthenticate = true;
                request.Method = requestMethod;
                request.Accept = "application/json, application/xml, text/json, text/x-json, text/javascript, text/xml";
                request.ContentLength = 0L;
                if (body != null)
                {
                    JsonSerializerSettings settings = new JsonSerializerSettings {
                        NullValueHandling = NullValueHandling.Ignore
                    };
                    string s = JsonConvert.SerializeObject(body, settings);
                    byte[] bytes = Encoding.UTF8.GetBytes(s);
                    request.ContentLength = bytes.Length;
                    Stream requestStream = request.GetRequestStream();
                    requestStream.Write(bytes, 0, bytes.Length);
                    requestStream.Close();
                }
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                string str3 = new StreamReader(response.GetResponseStream()).ReadToEnd();
                result2 = new RequestResult {
                    Content = str3,
                    HttpStatusCode = response.StatusCode
                };
            }
            catch (WebException exception)
            {
                throw new WebException(exception.Message + " " + exception.Response.Headers.ToString(), exception);
            }
            return result2;
        }

        public T RunRequest<T>(string resource, string requestMethod, object body = null)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings {
                NullValueHandling = NullValueHandling.Ignore
            };
            return JsonConvert.DeserializeObject<T>(this.RunRequest(resource, requestMethod, body).Content, settings);
        }
    }
}

