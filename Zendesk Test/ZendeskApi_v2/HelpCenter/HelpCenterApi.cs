﻿namespace ZendeskApi_v2.HelpCenter
{
    using System;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2.Requests.HelpCenter;

    public class HelpCenterApi : IHelpCenterApi
    {
        public HelpCenterApi(string yourZendeskUrl, string user, string password, string apiToken, string locale)
        {
            this.Categories = new ZendeskApi_v2.Requests.HelpCenter.Categories(yourZendeskUrl, user, password, apiToken, locale);
            this.Sections = new ZendeskApi_v2.Requests.HelpCenter.Sections(yourZendeskUrl, user, password, apiToken);
            this.Articles = new ZendeskApi_v2.Requests.HelpCenter.Articles(yourZendeskUrl, user, password, apiToken);
            this.Locale = locale;
        }

        public IArticles Articles { get; set; }

        public ICategories Categories { get; set; }

        public string Locale { get; set; }

        public ISections Sections { get; set; }
    }
}

