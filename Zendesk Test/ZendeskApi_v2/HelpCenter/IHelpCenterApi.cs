﻿namespace ZendeskApi_v2.HelpCenter
{
    using System;
    using ZendeskApi_v2.Requests.HelpCenter;

    public interface IHelpCenterApi
    {
        IArticles Articles { get; }

        ICategories Categories { get; }

        string Locale { get; }

        ISections Sections { get; }
    }
}

