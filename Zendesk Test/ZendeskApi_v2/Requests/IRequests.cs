﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using System.Runtime.InteropServices;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Requests;
    using ZendeskApi_v2.Models.Tickets;

    public interface IRequests : ICore
    {
        IndividualRequestResponse CreateRequest(Request request);
        GroupRequestResponse GetAllCcdRequests();
        GroupRequestResponse GetAllRequests();
        GroupRequestResponse GetAllRequestsForUser(long id);
        GroupRequestResponse GetAllSolvedRequests();
        IndividualRequestResponse GetRequestById(long id);
        GroupCommentResponse GetRequestCommentsById(long id);
        IndividualCommentResponse GetSpecificRequestComment(long requestId, long commentId);
        IndividualRequestResponse UpdateRequest(long id, Comment comment);
        IndividualRequestResponse UpdateRequest(Request request, Comment comment = null);
    }
}

