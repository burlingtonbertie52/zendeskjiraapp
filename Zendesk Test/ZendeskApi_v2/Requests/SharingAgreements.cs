﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.SharingAgreements;

    public class SharingAgreements : Core, ICore, ISharingAgreements
    {
        public SharingAgreements(string yourZendeskUrl, string user, string password, string apiToken) : base(yourZendeskUrl, user, password, apiToken)
        {
        }

        public GroupSharingAgreementResponse GetSharingAgreements()
        {
            return base.GenericGet<GroupSharingAgreementResponse>("sharing_agreements.json");
        }
    }
}

