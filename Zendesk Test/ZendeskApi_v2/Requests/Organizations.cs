﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Organizations;

    public class Organizations : Core, ICore, IOrganizations
    {
        public Organizations(string yourZendeskUrl, string user, string password, string apiToken) : base(yourZendeskUrl, user, password, apiToken)
        {
        }

        public IndividualOrganizationResponse CreateOrganization(Organization organization)
        {
            var body = new {
                organization = organization
            };
            return base.GenericPost<IndividualOrganizationResponse>("organizations.json", body);
        }

        public bool DeleteOrganization(long id)
        {
            return base.GenericDelete(string.Format("organizations/{0}.json", id));
        }

        public IndividualOrganizationResponse GetOrganization(long id)
        {
            return base.GenericGet<IndividualOrganizationResponse>(string.Format("organizations/{0}.json", id));
        }

        public GroupOrganizationResponse GetOrganizations()
        {
            return base.GenericGet<GroupOrganizationResponse>("organizations.json");
        }

        public GroupOrganizationResponse GetOrganizationsStartingWith(string name)
        {
            return base.GenericPost<GroupOrganizationResponse>(string.Format("organizations/autocomplete.json?name={0}", name), null);
        }

        public GroupOrganizationResponse SearchForOrganizations(string searchTerm)
        {
            return base.GenericGet<GroupOrganizationResponse>(string.Format("organizations/search.json?external_id={0}", searchTerm));
        }

        public IndividualOrganizationResponse UpdateOrganization(Organization organization)
        {
            var body = new {
                organization = organization
            };
            return base.GenericPut<IndividualOrganizationResponse>(string.Format("organizations/{0}.json", organization.Id), body);
        }
    }
}

