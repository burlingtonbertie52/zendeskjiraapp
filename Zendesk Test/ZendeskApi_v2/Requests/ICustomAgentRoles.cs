﻿namespace ZendeskApi_v2.Requests
{
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.CustomRoles;

    public interface ICustomAgentRoles : ICore
    {
        ZendeskApi_v2.Models.CustomRoles.CustomRoles GetCustomRoles();
    }
}

