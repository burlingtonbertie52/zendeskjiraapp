﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using System.Runtime.InteropServices;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Search;

    public interface ISearch : ICore
    {
        SearchResults AnonymousSearchFor(string searchTerm, string sortBy = "", string sortOrder = "", int page = 1);
        SearchResults SearchFor(string searchTerm, string sortBy = "", string sortOrder = "", int page = 1);
    }
}

