﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Groups;

    public interface IGroups : ICore
    {
        IndividualGroupResponse CreateGroup(string groupName);
        IndividualGroupMembershipResponse CreateGroupMembership(GroupMembership groupMembership);
        bool DeleteGroup(long id);
        bool DeleteGroupMembership(long groupMembershipId);
        bool DeleteUserGroupMembership(long userId, long groupMembershipId);
        MultipleGroupMembershipResponse GetAssignableGroupMemberships();
        MultipleGroupMembershipResponse GetAssignableGroupMembershipsByGroup(long groupId);
        MultipleGroupResponse GetAssignableGroups();
        IndividualGroupResponse GetGroupById(long id);
        MultipleGroupMembershipResponse GetGroupMemberships();
        MultipleGroupMembershipResponse GetGroupMembershipsByGroup(long groupId);
        IndividualGroupMembershipResponse GetGroupMembershipsByMembershipId(long groupMembershipId);
        MultipleGroupMembershipResponse GetGroupMembershipsByUser(long userId);
        IndividualGroupMembershipResponse GetGroupMembershipsByUserAndMembershipId(long userId, long groupMembershipId);
        MultipleGroupResponse GetGroups();
        MultipleGroupMembershipResponse SetGroupMembershipAsDefault(long userId, long groupMembershipId);
        IndividualGroupResponse UpdateGroup(Group group);
    }
}

