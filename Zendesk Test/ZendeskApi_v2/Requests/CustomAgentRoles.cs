﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.CustomRoles;

    public class CustomAgentRoles : Core, ICore, ICustomAgentRoles
    {
        public CustomAgentRoles(string yourZendeskUrl, string user, string password, string apiToken) : base(yourZendeskUrl, user, password, apiToken)
        {
        }

        public ZendeskApi_v2.Models.CustomRoles.CustomRoles GetCustomRoles()
        {
            return base.GenericGet<ZendeskApi_v2.Models.CustomRoles.CustomRoles>("custom_roles.json");
        }
    }
}

