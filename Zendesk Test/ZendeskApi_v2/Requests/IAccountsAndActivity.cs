﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.AccountsAndActivities;

    public interface IAccountsAndActivity : ICore
    {
        GroupActivityResponse GetActivities();
        IndividualActivityResponse GetActivityById(long activityId);
        SettingsResponse GetSettings();
    }
}

