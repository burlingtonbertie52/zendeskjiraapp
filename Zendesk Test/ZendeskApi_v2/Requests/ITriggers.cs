﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Triggers;

    public interface ITriggers : ICore
    {
        GroupTriggerResponse GetActiveTriggers();
        IndividualTriggerResponse GetTriggerById(long id);
        GroupTriggerResponse GetTriggers();
    }
}

