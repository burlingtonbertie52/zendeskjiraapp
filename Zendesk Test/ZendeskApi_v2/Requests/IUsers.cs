﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using System.Collections.Generic;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Shared;
    using ZendeskApi_v2.Models.Users;

    public interface IUsers : ICore
    {
        IndividualUserIdentityResponse AddUserIdentity(long userId, UserIdentity identity);
        JobStatusResponse BulkCreateUsers(IEnumerable<User> users);
        bool ChangeUsersPassword(long userId, string oldPassword, string newPassword);
        IndividualUserResponse CreateUser(User user);
        bool DeleteUser(long id);
        bool DeleteUserIdentity(long userId, long identityId);
        GroupUserResponse GetAllUsers();
        IndividualUserResponse GetCurrentUser();
        IndividualUserIdentityResponse GetSpecificUserIdentity(long userId, long identityId);
        IndividualUserResponse GetUser(long id);
        GroupUserIdentityResponse GetUserIdentities(long userId);
        GroupUserResponse GetUsersInGroup(long id);
        GroupUserResponse GetUsersInOrganization(long id);
        GroupUserResponse SearchByEmail(string email);
        GroupUserResponse SearchByExternalId(string externalId);
        IndividualUserIdentityResponse SendUserVerificationRequest(long userId, long identityId);
        GroupUserIdentityResponse SetUserIdentityAsPrimary(long userId, long identityId);
        IndividualUserIdentityResponse SetUserIdentityAsVerified(long userId, long identityId);
        bool SetUsersPassword(long userId, string newPassword);
        IndividualUserResponse SuspendUser(long id);
        IndividualUserResponse UpdateUser(User user);
    }
}

