﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Categories;

    public class Categories : Core, ICore, ICategories
    {
        public Categories(string yourZendeskUrl, string user, string password, string apiToken) : base(yourZendeskUrl, user, password, apiToken)
        {
        }

        public IndividualCategoryResponse CreateCategory(Category category)
        {
            var body = new {
                category = category
            };
            return base.GenericPost<IndividualCategoryResponse>(string.Format("categories.json", new object[0]), body);
        }

        public bool DeleteCategory(long id)
        {
            return base.GenericDelete(string.Format("categories/{0}.json", id));
        }

        public GroupCategoryResponse GetCategories()
        {
            return base.GenericGet<GroupCategoryResponse>("categories.json");
        }

        public IndividualCategoryResponse GetCategoryById(long id)
        {
            return base.GenericGet<IndividualCategoryResponse>(string.Format("categories/{0}.json", id));
        }

        public IndividualCategoryResponse UpdateCategory(Category category)
        {
            var body = new {
                category = category
            };
            return base.GenericPut<IndividualCategoryResponse>(string.Format("categories/{0}.json", category.Id), body);
        }
    }
}

