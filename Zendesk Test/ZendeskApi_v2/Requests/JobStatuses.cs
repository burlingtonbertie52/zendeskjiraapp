﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Shared;

    public class JobStatuses : Core, ICore, IJobStatuses
    {
        public JobStatuses(string yourZendeskUrl, string user, string password, string apiToken) : base(yourZendeskUrl, user, password, apiToken)
        {
        }

        public JobStatusResponse GetJobStatus(string id)
        {
            return base.GenericGet<JobStatusResponse>(string.Format("job_statuses/{0}.json", id));
        }
    }
}

