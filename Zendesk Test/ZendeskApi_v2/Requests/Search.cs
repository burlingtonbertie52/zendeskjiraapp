﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using System.Runtime.InteropServices;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Search;

    public class Search : Core, ICore, ISearch
    {
        public Search(string yourZendeskUrl, string user, string password, string apiToken) : base(yourZendeskUrl, user, password, apiToken)
        {
        }

        public SearchResults AnonymousSearchFor(string searchTerm, string sortBy = "", string sortOrder = "", int page = 1)
        {
            string resource = string.Format("portal/search.json?query={0}", searchTerm);
            if (page > 1)
            {
                resource = resource + "&page=" + page;
            }
            if (!string.IsNullOrEmpty(sortBy))
            {
                resource = resource + "&sort_by=" + sortBy;
            }
            if (!string.IsNullOrEmpty(sortOrder))
            {
                resource = resource + "&sort_order=" + sortOrder;
            }
            return base.GenericGet<SearchResults>(resource);
        }

        public SearchResults SearchFor(string searchTerm, string sortBy = "", string sortOrder = "", int page = 1)
        {
            string resource = string.Format("search.json?query={0}", searchTerm);
            if (page > 1)
            {
                resource = resource + "&page=" + page;
            }
            if (!string.IsNullOrEmpty(sortBy))
            {
                resource = resource + "&sort_by=" + sortBy;
            }
            if (!string.IsNullOrEmpty(sortOrder))
            {
                resource = resource + "&sort_order=" + sortOrder;
            }
            return base.GenericGet<SearchResults>(resource);
        }
    }
}

