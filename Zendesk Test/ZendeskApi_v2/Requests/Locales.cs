﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Locales;

    public class Locales : Core, ICore, ILocales
    {
        public Locales(string yourZendeskUrl, string user, string password, string apiToken) : base(yourZendeskUrl, user, password, apiToken)
        {
        }

        public GroupLocaleResponse GetAllLocales()
        {
            return base.GenericGet<GroupLocaleResponse>(string.Format("locales.json", new object[0]));
        }

        public IndividualLocaleResponse GetCurrentLocale()
        {
            return base.GenericGet<IndividualLocaleResponse>(string.Format("locales/current.json", new object[0]));
        }

        public IndividualLocaleResponse GetLocaleById(long id)
        {
            return base.GenericGet<IndividualLocaleResponse>(string.Format("locales/{0}.json", id));
        }

        public GroupLocaleResponse GetLocalesForAgents()
        {
            return base.GenericGet<GroupLocaleResponse>(string.Format("locales/agent.json", new object[0]));
        }
    }
}

