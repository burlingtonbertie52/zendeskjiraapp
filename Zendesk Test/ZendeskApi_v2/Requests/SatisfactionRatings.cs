﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Satisfaction;

    public class SatisfactionRatings : Core, ICore, ISatisfactionRatings
    {
        public SatisfactionRatings(string yourZendeskUrl, string user, string password, string apiToken) : base(yourZendeskUrl, user, password, apiToken)
        {
        }

        public GroupSatisfactionResponse GetReceivedSatisfactionRatings()
        {
            return base.GenericGet<GroupSatisfactionResponse>(string.Format("satisfaction_ratings/received.json", new object[0]));
        }

        public IndividualSatisfactionResponse GetSatisfactionRatingById(long id)
        {
            return base.GenericGet<IndividualSatisfactionResponse>(string.Format("satisfaction_ratings/{0}.json", id));
        }

        public GroupSatisfactionResponse GetSatisfactionRatings()
        {
            return base.GenericGet<GroupSatisfactionResponse>(string.Format("satisfaction_ratings.json", new object[0]));
        }
    }
}

