﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Categories;

    public interface ICategories : ICore
    {
        IndividualCategoryResponse CreateCategory(Category category);
        bool DeleteCategory(long id);
        GroupCategoryResponse GetCategories();
        IndividualCategoryResponse GetCategoryById(long id);
        IndividualCategoryResponse UpdateCategory(Category category);
    }
}

