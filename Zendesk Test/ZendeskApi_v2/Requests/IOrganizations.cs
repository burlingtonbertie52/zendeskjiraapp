﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Organizations;

    public interface IOrganizations : ICore
    {
        IndividualOrganizationResponse CreateOrganization(Organization organization);
        bool DeleteOrganization(long id);
        IndividualOrganizationResponse GetOrganization(long id);
        GroupOrganizationResponse GetOrganizations();
        GroupOrganizationResponse GetOrganizationsStartingWith(string name);
        GroupOrganizationResponse SearchForOrganizations(string searchTerm);
        IndividualOrganizationResponse UpdateOrganization(Organization organization);
    }
}

