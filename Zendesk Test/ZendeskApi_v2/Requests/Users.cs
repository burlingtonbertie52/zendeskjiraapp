﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using System.Collections.Generic;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Shared;
    using ZendeskApi_v2.Models.Users;

    public class Users : Core, ICore, IUsers
    {
        public Users(string yourZendeskUrl, string user, string password, string apiToken) : base(yourZendeskUrl, user, password, apiToken)
        {
        }

        public IndividualUserIdentityResponse AddUserIdentity(long userId, UserIdentity identity)
        {
            var body = new {
                identity = identity
            };
            return base.GenericPost<IndividualUserIdentityResponse>(string.Format("users/{0}/identities.json", userId), body);
        }

        public JobStatusResponse BulkCreateUsers(IEnumerable<User> users)
        {
            var body = new {
                users = users
            };
            return base.GenericPost<JobStatusResponse>("users/create_many.json", body);
        }

        public bool ChangeUsersPassword(long userId, string oldPassword, string newPassword)
        {
            var body = new {
                previous_password = oldPassword,
                password = newPassword
            };
            return base.GenericBoolPost(string.Format("users/{0}/password.json", userId), body);
        }

        public IndividualUserResponse CreateUser(User user)
        {
            var body = new {
                user = user
            };
            return base.GenericPost<IndividualUserResponse>("users.json", body);
        }

        public bool DeleteUser(long id)
        {
            return base.GenericDelete(string.Format("users/{0}.json", id));
        }

        public bool DeleteUserIdentity(long userId, long identityId)
        {
            return base.GenericDelete(string.Format("users/{0}/identities/{1}.json", userId, identityId));
        }

        public GroupUserResponse GetAllUsers()
        {
            return base.GenericGet<GroupUserResponse>("users.json");
        }

        public IndividualUserResponse GetCurrentUser()
        {
            return base.GenericGet<IndividualUserResponse>("users/me.json");
        }

        public IndividualUserIdentityResponse GetSpecificUserIdentity(long userId, long identityId)
        {
            return base.GenericGet<IndividualUserIdentityResponse>(string.Format("users/{0}/identities/{1}.json", userId, identityId));
        }

        public IndividualUserResponse GetUser(long id)
        {
            return base.GenericGet<IndividualUserResponse>(string.Format("users/{0}.json", id));
        }

        public GroupUserIdentityResponse GetUserIdentities(long userId)
        {
            return base.GenericGet<GroupUserIdentityResponse>(string.Format("users/{0}/identities.json", userId));
        }

        public GroupUserResponse GetUsersInGroup(long id)
        {
            return base.GenericGet<GroupUserResponse>(string.Format("groups/{0}/users.json", id));
        }

        public GroupUserResponse GetUsersInOrganization(long id)
        {
            return base.GenericGet<GroupUserResponse>(string.Format("organizations/{0}/users.json", id));
        }

        public GroupUserResponse SearchByEmail(string email)
        {
            return base.GenericGet<GroupUserResponse>(string.Format("users/search.json?query={0}", email));
        }

        public GroupUserResponse SearchByExternalId(string externalId)
        {
            return base.GenericGet<GroupUserResponse>(string.Format("users/search.json?external_id={0}", externalId));
        }

        public IndividualUserIdentityResponse SendUserVerificationRequest(long userId, long identityId)
        {
            return base.GenericPut<IndividualUserIdentityResponse>(string.Format("users/{0}/identities/{1}/request_verification.json", userId, identityId), null);
        }

        public GroupUserIdentityResponse SetUserIdentityAsPrimary(long userId, long identityId)
        {
            return base.GenericPut<GroupUserIdentityResponse>(string.Format("users/{0}/identities/{1}/make_primary.json", userId, identityId), null);
        }

        public IndividualUserIdentityResponse SetUserIdentityAsVerified(long userId, long identityId)
        {
            return base.GenericPut<IndividualUserIdentityResponse>(string.Format("users/{0}/identities/{1}/verify.json", userId, identityId), null);
        }

        public bool SetUsersPassword(long userId, string newPassword)
        {
            var body = new {
                password = newPassword
            };
            return base.GenericBoolPost(string.Format("users/{0}/password.json", userId), body);
        }

        public IndividualUserResponse SuspendUser(long id)
        {
            var body = new {
                user = new { suspended = true }
            };
            return base.GenericPut<IndividualUserResponse>(string.Format("users/{0}.json", id), body);
        }

        public IndividualUserResponse UpdateUser(User user)
        {
            var body = new {
                user = user
            };
            return base.GenericPut<IndividualUserResponse>(string.Format("users/{0}.json", user.Id), body);
        }
    }
}

