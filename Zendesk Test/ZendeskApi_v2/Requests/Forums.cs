﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Forums;

    public class Forums : Core, ICore, IForums
    {
        public Forums(string yourZendeskUrl, string user, string password, string apiToken) : base(yourZendeskUrl, user, password, apiToken)
        {
        }

        public IndividualForumResponse CreateForum(Forum forum)
        {
            var body = new {
                forum = forum
            };
            return base.GenericPost<IndividualForumResponse>("forums.json", body);
        }

        public IndividualForumSubcriptionResponse CreateForumSubscription(ForumSubscription forumSubscription)
        {
            return base.GenericPost<IndividualForumSubcriptionResponse>(string.Format("forum_subscriptions.json", new object[0]), forumSubscription);
        }

        public bool DeleteForum(long id)
        {
            return base.GenericDelete(string.Format("forums/{0}.json", id));
        }

        public bool DeleteForumSubscription(long subscriptionId)
        {
            return base.GenericDelete(string.Format("forum_subscriptions/{0}.json", subscriptionId));
        }

        public IndividualForumResponse GetForumById(long forumId)
        {
            return base.GenericGet<IndividualForumResponse>(string.Format("forums/{0}.json", forumId));
        }

        public GroupForumResponse GetForums()
        {
            return base.GenericGet<GroupForumResponse>("forums.json");
        }

        public GroupForumResponse GetForumsByCategory(long categoryId)
        {
            return base.GenericGet<GroupForumResponse>(string.Format("categories/{0}/forums.json", categoryId));
        }

        public GroupForumSubcriptionResponse GetForumSubscriptions()
        {
            return base.GenericGet<GroupForumSubcriptionResponse>("forum_subscriptions.json");
        }

        public GroupForumSubcriptionResponse GetForumSubscriptionsByForumId(long forumId)
        {
            return base.GenericGet<GroupForumSubcriptionResponse>(string.Format("forums/{0}/subscriptions.json", forumId));
        }

        public IndividualForumSubcriptionResponse GetForumSubscriptionsById(long subscriptionId)
        {
            return base.GenericGet<IndividualForumSubcriptionResponse>(string.Format("forum_subscriptions/{0}.json", subscriptionId));
        }

        public IndividualForumResponse UpdateForum(Forum forum)
        {
            var body = new {
                forum = forum
            };
            return base.GenericPut<IndividualForumResponse>(string.Format("forums/{0}.json", forum.Id), body);
        }
    }
}

