﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using System.Runtime.InteropServices;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Requests;
    using ZendeskApi_v2.Models.Tickets;

    public class Requests : Core, ICore, IRequests
    {
        public Requests(string yourZendeskUrl, string user, string password, string apiToken) : base(yourZendeskUrl, user, password, apiToken)
        {
        }

        public IndividualRequestResponse CreateRequest(Request request)
        {
            var body = new {
                request = request
            };
            return base.GenericPost<IndividualRequestResponse>("requests.json", body);
        }

        public GroupRequestResponse GetAllCcdRequests()
        {
            return base.GenericGet<GroupRequestResponse>("requests/ccd.json");
        }

        public GroupRequestResponse GetAllRequests()
        {
            return base.GenericGet<GroupRequestResponse>("requests.json");
        }

        public GroupRequestResponse GetAllRequestsForUser(long id)
        {
            return base.GenericGet<GroupRequestResponse>(string.Format("users/{0}/requests.json", id));
        }

        public GroupRequestResponse GetAllSolvedRequests()
        {
            return base.GenericGet<GroupRequestResponse>("requests/solved.json");
        }

        public IndividualRequestResponse GetRequestById(long id)
        {
            return base.GenericGet<IndividualRequestResponse>(string.Format("requests/{0}.json", id));
        }

        public GroupCommentResponse GetRequestCommentsById(long id)
        {
            return base.GenericGet<GroupCommentResponse>(string.Format("requests/{0}/comments.json", id));
        }

        public IndividualCommentResponse GetSpecificRequestComment(long requestId, long commentId)
        {
            return base.GenericGet<IndividualCommentResponse>(string.Format("requests/{0}/comments/{1}.json", requestId, commentId));
        }

        public IndividualRequestResponse UpdateRequest(long id, Comment comment)
        {
            var body = new {
                request = new { comment = comment }
            };
            return base.GenericPut<IndividualRequestResponse>(string.Format("requests/{0}.json", id), body);
        }

        public IndividualRequestResponse UpdateRequest(Request request, Comment comment = null)
        {
            if (comment != null)
            {
                request.Comment = comment;
            }
            var body = new {
                request = request
            };
            return base.GenericPut<IndividualRequestResponse>(string.Format("requests/{0}.json", request.Id.Value), body);
        }
    }
}

