﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using System.Collections.Generic;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Extensions;
    using ZendeskApi_v2.Models.Topics;

    public class Topics : Core, ICore, ITopics
    {
        public Topics(string yourZendeskUrl, string user, string password, string apiToken) : base(yourZendeskUrl, user, password, apiToken)
        {
        }

        public IndividualTopicVoteResponse CheckForVote(long topicId)
        {
            return base.GenericGet<IndividualTopicVoteResponse>(string.Format("topics/{0}/vote.json", topicId));
        }

        public IndividualTopicResponse CreateTopic(Topic topic)
        {
            var body = new {
                topic = topic
            };
            return base.GenericPost<IndividualTopicResponse>(string.Format("topics.json", new object[0]), body);
        }

        public IndividualTopicCommentResponse CreateTopicComment(long topicId, TopicComment topicComment)
        {
            var body = new {
                topic_comment = topicComment
            };
            return base.GenericPost<IndividualTopicCommentResponse>(string.Format("topics/{0}/comments.json", topicId), body);
        }

        public IndividualTopicSubscriptionResponse CreateTopicSubscription(long userId, long topicId)
        {
            var body = new {
                user_id = userId,
                topic_id = topicId
            };
            return base.GenericPost<IndividualTopicSubscriptionResponse>(string.Format("topic_subscriptions.json", new object[0]), body);
        }

        public IndividualTopicVoteResponse CreateVote(long topicId)
        {
            return base.GenericPost<IndividualTopicVoteResponse>(string.Format("topics/{0}/vote.json", topicId), null);
        }

        public bool DeleteTopic(long topicId)
        {
            return base.GenericDelete(string.Format("topics/{0}.json", topicId));
        }

        public bool DeleteTopicComment(long topicId, long commentId)
        {
            return base.GenericDelete(string.Format("topics/{0}/comments/{1}.json", topicId, commentId));
        }

        public bool DeleteTopicSubscription(long topicSubscriptionId)
        {
            return base.GenericDelete(string.Format("topic_subscriptions/{0}.json", topicSubscriptionId));
        }

        public bool DeleteVote(long topicId)
        {
            return base.GenericDelete(string.Format("topics/{0}/vote.json", topicId));
        }

        public GroupTopicSubscriptionResponse GetAllTopicSubscriptions()
        {
            return base.GenericGet<GroupTopicSubscriptionResponse>(string.Format("topic_subscriptions.json", new object[0]));
        }

        public GroupTopicResponse GetMultipleTopicsById(IEnumerable<long> topicIds)
        {
            return base.GenericPost<GroupTopicResponse>(string.Format("topics/show_many?ids={0}.json", topicIds.ToCsv()), null);
        }

        public IndividualTopicCommentResponse GetSpecificTopicCommentByTopic(long topicId, long commentId)
        {
            return base.GenericGet<IndividualTopicCommentResponse>(string.Format("topics/{0}/comments/{1}.json", topicId, commentId));
        }

        public IndividualTopicCommentResponse GetSpecificTopicCommentByUser(long userId, long commentId)
        {
            return base.GenericGet<IndividualTopicCommentResponse>(string.Format("users/{0}/topic_comments/{1}.json", userId, commentId));
        }

        public IndividualTopicResponse GetTopicById(long topicId)
        {
            return base.GenericGet<IndividualTopicResponse>(string.Format("topics/{0}.json", topicId));
        }

        public GroupTopicCommentResponse GetTopicCommentsByTopicId(long topicId)
        {
            return base.GenericGet<GroupTopicCommentResponse>(string.Format("topics/{0}/comments.json", topicId));
        }

        public GroupTopicCommentResponse GetTopicCommentsByUserId(long userId)
        {
            return base.GenericGet<GroupTopicCommentResponse>(string.Format("users/{0}/topic_comments.json", userId));
        }

        public GroupTopicResponse GetTopics()
        {
            return base.GenericGet<GroupTopicResponse>("topics.json");
        }

        public GroupTopicResponse GetTopicsByForum(long forumId)
        {
            return base.GenericGet<GroupTopicResponse>(string.Format("forums/{0}/topics.json", forumId));
        }

        public GroupTopicResponse GetTopicsByUser(long userId)
        {
            return base.GenericGet<GroupTopicResponse>(string.Format("users/{0}/topics.json", userId));
        }

        public IndividualTopicSubscriptionResponse GetTopicSubscriptionById(long topicSubscriptionId)
        {
            return base.GenericGet<IndividualTopicSubscriptionResponse>(string.Format("topic_subscriptions/{0}.json", topicSubscriptionId));
        }

        public GroupTopicSubscriptionResponse GetTopicSubscriptionsByTopic(long topicId)
        {
            return base.GenericGet<GroupTopicSubscriptionResponse>(string.Format("topics/{0}/subscriptions.json", topicId));
        }

        public GroupTopicVoteResponse GetTopicVotes(long topicId)
        {
            return base.GenericGet<GroupTopicVoteResponse>(string.Format("topics/{0}/votes.json", topicId));
        }

        public GroupTopicVoteResponse GetTopicVotesByUser(long userId)
        {
            return base.GenericGet<GroupTopicVoteResponse>(string.Format("users/{0}/topic_votes.json", userId));
        }

        public IndividualTopicResponse UpdateTopic(Topic topic)
        {
            var body = new {
                topic = topic
            };
            return base.GenericPut<IndividualTopicResponse>(string.Format("topics/{0}.json", topic.Id), body);
        }

        public IndividualTopicCommentResponse UpdateTopicComment(TopicComment topicComment)
        {
            var body = new {
                topic_comment = topicComment
            };
            return base.GenericPut<IndividualTopicCommentResponse>(string.Format("topics/{0}/comments/{1}.json", topicComment.TopicId, topicComment.Id), body);
        }
    }
}

