﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using System.Collections.Generic;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Topics;

    public interface ITopics : ICore
    {
        IndividualTopicVoteResponse CheckForVote(long topicId);
        IndividualTopicResponse CreateTopic(Topic topic);
        IndividualTopicCommentResponse CreateTopicComment(long topicId, TopicComment topicComment);
        IndividualTopicSubscriptionResponse CreateTopicSubscription(long userId, long topicId);
        IndividualTopicVoteResponse CreateVote(long topicId);
        bool DeleteTopic(long topicId);
        bool DeleteTopicComment(long topicId, long commentId);
        bool DeleteTopicSubscription(long topicSubscriptionId);
        bool DeleteVote(long topicId);
        GroupTopicSubscriptionResponse GetAllTopicSubscriptions();
        GroupTopicResponse GetMultipleTopicsById(IEnumerable<long> topicIds);
        IndividualTopicCommentResponse GetSpecificTopicCommentByTopic(long topicId, long commentId);
        IndividualTopicCommentResponse GetSpecificTopicCommentByUser(long userId, long commentId);
        IndividualTopicResponse GetTopicById(long topicId);
        GroupTopicCommentResponse GetTopicCommentsByTopicId(long topicId);
        GroupTopicCommentResponse GetTopicCommentsByUserId(long userId);
        GroupTopicResponse GetTopics();
        GroupTopicResponse GetTopicsByForum(long forumId);
        GroupTopicResponse GetTopicsByUser(long userId);
        IndividualTopicSubscriptionResponse GetTopicSubscriptionById(long topicSubscriptionId);
        GroupTopicSubscriptionResponse GetTopicSubscriptionsByTopic(long topicId);
        GroupTopicVoteResponse GetTopicVotes(long topicId);
        GroupTopicVoteResponse GetTopicVotesByUser(long userId);
        IndividualTopicResponse UpdateTopic(Topic topic);
        IndividualTopicCommentResponse UpdateTopicComment(TopicComment topicComment);
    }
}

