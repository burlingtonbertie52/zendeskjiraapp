﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Tags;

    public interface ITags : ICore
    {
        TagAutocompleteResponse AutocompleteTags(string name);
        GroupTagResult GetTags();
    }
}

