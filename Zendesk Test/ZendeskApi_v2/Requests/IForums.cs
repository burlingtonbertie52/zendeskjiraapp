﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Forums;

    public interface IForums : ICore
    {
        IndividualForumResponse CreateForum(Forum forum);
        IndividualForumSubcriptionResponse CreateForumSubscription(ForumSubscription forumSubscription);
        bool DeleteForum(long id);
        bool DeleteForumSubscription(long subscriptionId);
        IndividualForumResponse GetForumById(long forumId);
        GroupForumResponse GetForums();
        GroupForumResponse GetForumsByCategory(long categoryId);
        GroupForumSubcriptionResponse GetForumSubscriptions();
        GroupForumSubcriptionResponse GetForumSubscriptionsByForumId(long forumId);
        IndividualForumSubcriptionResponse GetForumSubscriptionsById(long subscriptionId);
        IndividualForumResponse UpdateForum(Forum forum);
    }
}

