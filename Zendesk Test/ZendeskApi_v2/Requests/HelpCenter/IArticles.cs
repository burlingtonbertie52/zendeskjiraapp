﻿namespace ZendeskApi_v2.Requests.HelpCenter
{
    using System;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Articles;

    public interface IArticles : ICore
    {
        IndividualArticleResponse CreateArticle(long sectionId, Article article);
        bool DeleteArticle(long id);
        GroupArticleResponse GetArticles();
        GroupArticleResponse GetArticlesByCategoryId(long categoryId);
        GroupArticleResponse GetArticlesBySectionId(long sectionId);
        GroupArticleResponse GetArticlesByUserId(long userId);
        GroupArticleResponse GetArticlesSinceDateTime(DateTime startTime);
        IndividualArticleResponse UpdateArticle(Article article);
    }
}

