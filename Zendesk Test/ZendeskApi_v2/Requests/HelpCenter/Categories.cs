﻿namespace ZendeskApi_v2.Requests.HelpCenter
{
    using System;
    using System.Runtime.CompilerServices;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.HelpCenter.Categories;

    public class Categories : Core, ICore, ICategories
    {
        public Categories(string yourZendeskUrl, string user, string password, string apiToken, string locale) : base(yourZendeskUrl, user, password, apiToken)
        {
            this.Locale = locale;
        }

        public IndividualCategoryResponse CreateCategory(Category category)
        {
            if (string.IsNullOrEmpty(category.Locale))
            {
                category.Locale = this.Locale;
            }
            var body = new {
                category = category
            };
            return base.GenericPost<IndividualCategoryResponse>("help_center/categories.json", body);
        }

        public bool DeleteCategory(long id)
        {
            return base.GenericDelete(string.Format("help_center/categories/{0}.json", id));
        }

        public GroupCategoryResponse GetCategories()
        {
            return base.GenericGet<GroupCategoryResponse>("help_center/categories.json");
        }

        public IndividualCategoryResponse GetCategoryById(long id)
        {
            return base.GenericGet<IndividualCategoryResponse>(string.Format("help_center/categories/{0}.json", id));
        }

        public IndividualCategoryResponse UpdateCategory(Category category)
        {
            var body = new {
                category = category
            };
            return base.GenericPut<IndividualCategoryResponse>(string.Format("help_center/categories/{0}.json", category.Id), body);
        }

        private string Locale { get; set; }
    }
}

