﻿namespace ZendeskApi_v2.Requests.HelpCenter
{
    using System;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Sections;

    public interface ISections : ICore
    {
        IndividualSectionResponse CreateSection(Section section);
        bool DeleteSection(long id);
        IndividualSectionResponse GetSectionById(long id);
        GroupSectionResponse GetSections();
        GroupSectionResponse GetSectionsByCategoryId(long categoryId);
        IndividualSectionResponse UpdateSection(Section section);
    }
}

