﻿namespace ZendeskApi_v2.Requests.HelpCenter
{
    using System;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Sections;

    public class Sections : Core, ICore, ISections
    {
        public Sections(string zendeskApiUrl, string user, string password, string apiToken) : base(zendeskApiUrl, user, password, apiToken)
        {
        }

        public IndividualSectionResponse CreateSection(Section section)
        {
            var body = new {
                section = section
            };
            return base.GenericPost<IndividualSectionResponse>(string.Format("help_center/sections.json", new object[0]), body);
        }

        public bool DeleteSection(long id)
        {
            return base.GenericDelete(string.Format("help_center/sections/{0}.json", id));
        }

        public IndividualSectionResponse GetSectionById(long id)
        {
            return base.GenericGet<IndividualSectionResponse>(string.Format("help_center/sections/{0}.json", id));
        }

        public GroupSectionResponse GetSections()
        {
            return base.GenericGet<GroupSectionResponse>("help_center/sections.json");
        }

        public GroupSectionResponse GetSectionsByCategoryId(long categoryId)
        {
            return base.GenericGet<GroupSectionResponse>(string.Format("help_center/categories/{0}/sections.json", categoryId));
        }

        public IndividualSectionResponse UpdateSection(Section section)
        {
            var body = new {
                section = section
            };
            return base.GenericPut<IndividualSectionResponse>(string.Format("help_center/sections/{0}.json", section.Id), body);
        }
    }
}

