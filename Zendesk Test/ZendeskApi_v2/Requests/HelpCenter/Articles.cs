﻿namespace ZendeskApi_v2.Requests.HelpCenter
{
    using System;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Extensions;
    using ZendeskApi_v2.Models.Articles;

    public class Articles : Core, ICore, IArticles
    {
        public Articles(string zendeskApiUrl, string user, string password, string apiToken) : base(zendeskApiUrl, user, password, apiToken)
        {
        }

        public IndividualArticleResponse CreateArticle(long sectionId, Article article)
        {
            var body = new {
                article = article
            };
            return base.GenericPost<IndividualArticleResponse>(string.Format("help_center/sections/{0}/articles.json", sectionId), body);
        }

        public bool DeleteArticle(long id)
        {
            return base.GenericDelete(string.Format("help_center/articles/{0}.json", id));
        }

        public GroupArticleResponse GetArticles()
        {
            return base.GenericGet<GroupArticleResponse>("help_center/articles.json");
        }

        public GroupArticleResponse GetArticlesByCategoryId(long categoryId)
        {
            return base.GenericGet<GroupArticleResponse>(string.Format("help_center/categories/{0}/articles.json", categoryId));
        }

        public GroupArticleResponse GetArticlesBySectionId(long sectionId)
        {
            return base.GenericGet<GroupArticleResponse>(string.Format("help_center/sections/{0}/articles.json", sectionId));
        }

        public GroupArticleResponse GetArticlesByUserId(long userId)
        {
            return base.GenericGet<GroupArticleResponse>(string.Format("help_center/users/{0}/articles.json", userId));
        }

        public GroupArticleResponse GetArticlesSinceDateTime(DateTime startTime)
        {
            return base.GenericGet<GroupArticleResponse>(string.Format("help_center/incremental/articles.json?start_time={0}", startTime.GetEpoch()));
        }

        public IndividualArticleResponse UpdateArticle(Article article)
        {
            var body = new {
                article = article
            };
            return base.GenericPut<IndividualArticleResponse>(string.Format("help_center/articles/{0}.json", article.Id), body);
        }
    }
}

