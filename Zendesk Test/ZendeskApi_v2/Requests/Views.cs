﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Extensions;
    using ZendeskApi_v2.Models.Views;
    using ZendeskApi_v2.Models.Views.Executed;

    public class Views : Core, ICore, IViews
    {
        public Views(string yourZendeskUrl, string user, string password, string apiToken) : base(yourZendeskUrl, user, password, apiToken)
        {
        }

        public ExecutedViewResponse ExecuteView(long id, string sortCol = "", bool ascending = true)
        {
            string resource = string.Format("views/{0}/execute.json", id);
            if (!string.IsNullOrEmpty(sortCol))
            {
                resource = resource + string.Format("?sort_by={0}&sort_order={1}", sortCol, ascending ? "" : "desc");
            }
            return base.GenericGet<ExecutedViewResponse>(resource);
        }

        public GroupViewResponse GetActiveViews()
        {
            return base.GenericGet<GroupViewResponse>("views/active.json");
        }

        public GroupViewResponse GetAllViews()
        {
            return base.GenericGet<GroupViewResponse>("views.json");
        }

        public GroupViewResponse GetCompactViews()
        {
            return base.GenericGet<GroupViewResponse>("views/compact.json");
        }

        public IndividualViewResponse GetView(long id)
        {
            return base.GenericGet<IndividualViewResponse>(string.Format("views/{0}.json", id));
        }

        public IndividualViewCountResponse GetViewCount(long viewId)
        {
            return base.GenericGet<IndividualViewCountResponse>(string.Format("views/{0}/count.json", viewId));
        }

        public GroupViewCountResponse GetViewCounts(IEnumerable<long> viewIds)
        {
            return base.GenericGet<GroupViewCountResponse>(string.Format("views/count_many.json?ids={0}", viewIds.ToCsv()));
        }

        public ExecutedViewResponse PreviewView(PreviewViewRequest preview)
        {
            return base.GenericPost<ExecutedViewResponse>("views/preview.json", preview);
        }
    }
}

