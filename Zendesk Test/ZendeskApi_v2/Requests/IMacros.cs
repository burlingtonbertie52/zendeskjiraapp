﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Macros;

    public interface IMacros : ICore
    {
        ApplyMacroResponse ApplyMacro(long macroId);
        ApplyMacroResponse ApplyMacroToTicket(long ticketId, long macroId);
        IndividualMacroResponse CreateMacro(Macro macro);
        bool DeleteMacro(long id);
        GroupMacroResponse GetActiveMacros();
        GroupMacroResponse GetAllMacros();
        IndividualMacroResponse GetMacroById(long id);
        IndividualMacroResponse UpdateMacro(Macro macro);
    }
}

