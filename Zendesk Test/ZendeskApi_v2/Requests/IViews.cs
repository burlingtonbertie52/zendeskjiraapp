﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Views;
    using ZendeskApi_v2.Models.Views.Executed;

    public interface IViews : ICore
    {
        ExecutedViewResponse ExecuteView(long id, string sortCol = "", bool ascending = true);
        GroupViewResponse GetActiveViews();
        GroupViewResponse GetAllViews();
        GroupViewResponse GetCompactViews();
        IndividualViewResponse GetView(long id);
        IndividualViewCountResponse GetViewCount(long viewId);
        GroupViewCountResponse GetViewCounts(IEnumerable<long> viewIds);
        ExecutedViewResponse PreviewView(PreviewViewRequest preview);
    }
}

