﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Macros;

    public class Macros : Core, ICore, IMacros
    {
        public Macros(string yourZendeskUrl, string user, string password, string apiToken) : base(yourZendeskUrl, user, password, apiToken)
        {
        }

        public ApplyMacroResponse ApplyMacro(long macroId)
        {
            return base.GenericGet<ApplyMacroResponse>(string.Format("macros/{0}/apply.json", macroId));
        }

        public ApplyMacroResponse ApplyMacroToTicket(long ticketId, long macroId)
        {
            return base.GenericGet<ApplyMacroResponse>(string.Format("tickets/{0}/macros/{1}/apply.json", ticketId, macroId));
        }

        public IndividualMacroResponse CreateMacro(Macro macro)
        {
            var body = new {
                macro = macro
            };
            return base.GenericPost<IndividualMacroResponse>("macros.json", body);
        }

        public bool DeleteMacro(long id)
        {
            return base.GenericDelete(string.Format("macros/{0}.json", id));
        }

        public GroupMacroResponse GetActiveMacros()
        {
            return base.GenericGet<GroupMacroResponse>(string.Format("macros/active.json", new object[0]));
        }

        public GroupMacroResponse GetAllMacros()
        {
            return base.GenericGet<GroupMacroResponse>(string.Format("macros.json", new object[0]));
        }

        public IndividualMacroResponse GetMacroById(long id)
        {
            return base.GenericGet<IndividualMacroResponse>(string.Format("macros/{0}.json", id));
        }

        public IndividualMacroResponse UpdateMacro(Macro macro)
        {
            var body = new {
                macro = macro
            };
            return base.GenericPut<IndividualMacroResponse>(string.Format("macros/{0}.json", macro.Id), body);
        }
    }
}

