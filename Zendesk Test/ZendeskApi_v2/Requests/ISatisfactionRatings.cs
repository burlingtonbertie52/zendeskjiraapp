﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Satisfaction;

    public interface ISatisfactionRatings : ICore
    {
        GroupSatisfactionResponse GetReceivedSatisfactionRatings();
        IndividualSatisfactionResponse GetSatisfactionRatingById(long id);
        GroupSatisfactionResponse GetSatisfactionRatings();
    }
}

