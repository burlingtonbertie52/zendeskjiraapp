﻿namespace ZendeskApi_v2.Requests
{
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.SharingAgreements;

    public interface ISharingAgreements : ICore
    {
        GroupSharingAgreementResponse GetSharingAgreements();
    }
}

