﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Extensions;
    using ZendeskApi_v2.Models.Shared;

    public class Attachments : Core, ICore, IAttachments
    {
        public Attachments(string yourZendeskUrl, string user, string password, string apiToken) : base(yourZendeskUrl, user, password, apiToken)
        {
        }

        public Upload UploadAttachment(ZenFile file)
        {
            return this.UploadAttachment(file, "", null);
        }

        public Upload UploadAttachment(ZenFile file, int? timeout)
        {
            return this.UploadAttachment(file, "", timeout);
        }

        private Upload UploadAttachment(ZenFile file, string token, int? timeout)
        {
            string zendeskUrl = base.ZendeskUrl;
            if (!zendeskUrl.EndsWith("/"))
            {
                zendeskUrl = zendeskUrl + "/";
            }
            zendeskUrl = zendeskUrl + string.Format("uploads.json?filename={0}", file.FileName);
            if (!string.IsNullOrEmpty(token))
            {
                zendeskUrl = zendeskUrl + string.Format("&token={0}", token);
            }
            WebRequest request = WebRequest.Create(zendeskUrl);
            request.ContentType = file.ContentType;
            request.Method = "POST";
            request.ContentLength = file.FileData.Length;
            request.Headers["Authorization"] = base.GetPasswordOrTokenAuthHeader();
            if (timeout.HasValue)
            {
                request.Timeout = timeout.Value;
            }
            request.PreAuthenticate = true;
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(file.FileData, 0, file.FileData.Length);
            requestStream.Dispose();
            WebResponse response = request.GetResponse();
            requestStream = response.GetResponseStream();
            string json = new StreamReader(requestStream).ReadToEnd();
            requestStream.Dispose();
            response.Close();
            return json.ConvertToObject<UploadResult>().Upload;
        }

        public Upload UploadAttachments(IEnumerable<ZenFile> files)
        {
            if (!files.Any<ZenFile>())
            {
                return null;
            }
            Upload upload = this.UploadAttachment(files.First<ZenFile>());
            if (files.Count<ZenFile>() > 1)
            {
                IEnumerable<ZenFile> enumerable = files.Skip<ZenFile>(1);
                foreach (ZenFile file in enumerable)
                {
                    int? timeout = null;
                    upload = this.UploadAttachment(file, upload.Token, timeout);
                }
            }
            return upload;
        }
    }
}

