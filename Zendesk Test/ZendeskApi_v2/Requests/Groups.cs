﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Groups;

    public class Groups : Core, ICore, IGroups
    {
        public Groups(string yourZendeskUrl, string user, string password, string apiToken) : base(yourZendeskUrl, user, password, apiToken)
        {
        }

        public IndividualGroupResponse CreateGroup(string groupName)
        {
            var body = new {
                group = new { name = groupName }
            };
            return base.GenericPost<IndividualGroupResponse>("groups.json", body);
        }

        public IndividualGroupMembershipResponse CreateGroupMembership(GroupMembership groupMembership)
        {
            var body = new {
                group_membership = groupMembership
            };
            return base.GenericPost<IndividualGroupMembershipResponse>(string.Format("group_memberships.json", new object[0]), body);
        }

        public bool DeleteGroup(long id)
        {
            return base.GenericDelete(string.Format("groups/{0}.json", id));
        }

        public bool DeleteGroupMembership(long groupMembershipId)
        {
            return base.GenericDelete(string.Format("group_memberships/{0}.json", groupMembershipId));
        }

        public bool DeleteUserGroupMembership(long userId, long groupMembershipId)
        {
            return base.GenericDelete(string.Format("users/{0}/group_memberships/{1}.json", userId, groupMembershipId));
        }

        public MultipleGroupMembershipResponse GetAssignableGroupMemberships()
        {
            return base.GenericGet<MultipleGroupMembershipResponse>("group_memberships/assignable.json");
        }

        public MultipleGroupMembershipResponse GetAssignableGroupMembershipsByGroup(long groupId)
        {
            return base.GenericGet<MultipleGroupMembershipResponse>(string.Format("groups/{0}/memberships/assignable.json", groupId));
        }

        public MultipleGroupResponse GetAssignableGroups()
        {
            return base.GenericGet<MultipleGroupResponse>("groups/assignable.json");
        }

        public IndividualGroupResponse GetGroupById(long id)
        {
            return base.GenericGet<IndividualGroupResponse>(string.Format("groups/{0}.json", id));
        }

        public MultipleGroupMembershipResponse GetGroupMemberships()
        {
            return base.GenericGet<MultipleGroupMembershipResponse>("group_memberships.json");
        }

        public MultipleGroupMembershipResponse GetGroupMembershipsByGroup(long groupId)
        {
            return base.GenericGet<MultipleGroupMembershipResponse>(string.Format("groups/{0}/memberships.json", groupId));
        }

        public IndividualGroupMembershipResponse GetGroupMembershipsByMembershipId(long groupMembershipId)
        {
            return base.GenericGet<IndividualGroupMembershipResponse>(string.Format("group_memberships/{0}.json", groupMembershipId));
        }

        public MultipleGroupMembershipResponse GetGroupMembershipsByUser(long userId)
        {
            return base.GenericGet<MultipleGroupMembershipResponse>(string.Format("users/{0}/group_memberships.json", userId));
        }

        public IndividualGroupMembershipResponse GetGroupMembershipsByUserAndMembershipId(long userId, long groupMembershipId)
        {
            return base.GenericGet<IndividualGroupMembershipResponse>(string.Format("users/{0}/group_memberships/{1}.json", userId, groupMembershipId));
        }

        public MultipleGroupResponse GetGroups()
        {
            return base.GenericGet<MultipleGroupResponse>("groups.json");
        }

        public MultipleGroupMembershipResponse SetGroupMembershipAsDefault(long userId, long groupMembershipId)
        {
            return base.GenericPut<MultipleGroupMembershipResponse>(string.Format("users/{0}/group_memberships/{1}/make_default.json", userId, groupMembershipId), null);
        }

        public IndividualGroupResponse UpdateGroup(Group group)
        {
            var body = new {
                group = group
            };
            return base.GenericPut<IndividualGroupResponse>(string.Format("groups/{0}.json", group.Id), body);
        }
    }
}

