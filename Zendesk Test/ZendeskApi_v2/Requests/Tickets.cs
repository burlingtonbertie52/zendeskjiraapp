﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Runtime.InteropServices;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Extensions;
    using ZendeskApi_v2.Models.Requests;
    using ZendeskApi_v2.Models.Shared;
    using ZendeskApi_v2.Models.Tickets;
    using ZendeskApi_v2.Models.Tickets.Suspended;
    using ZendeskApi_v2.Models.Users;

    public class Tickets : Core, ICore, ITickets
    {
        private const string _organizations = "organizations";
        private const string _ticket_forms = "ticket_forms";
        private const string _ticket_metrics = "ticket_metrics";
        private const string _tickets = "tickets";
        private const string _views = "views";

        public Tickets(string yourZendeskUrl, string user, string password, string apiToken) : base(yourZendeskUrl, user, password, apiToken)
        {
        }

        public TicketExportResponse __TestOnly__GetInrementalTicketExport(DateTime startTime)
        {
            return base.GenericGet<TicketExportResponse>("exports/tickets/sample.json?start_time=" + startTime.GetEpoch());
        }

        public GroupTicketResponse AutoCompleteProblems(string text)
        {
            return base.GenericPost<GroupTicketResponse>("problems/autocomplete.json?text=" + text, null);
        }

        public JobStatusResponse BulkUpdate(IEnumerable<long> ids, ZendeskApi_v2.Models.Tickets.BulkUpdate info)
        {
            var body = new {
                ticket = info
            };
            return base.GenericPut<JobStatusResponse>(string.Format("{0}/update_many.json?ids={1}", "tickets", ids.ToCsv()), body);
        }

        public IndividualTicketFormResponse CloneTicketForm(long ticketFormId)
        {
            return base.GenericPost<IndividualTicketFormResponse>(string.Format("{0}/{1}/clone.json", "ticket_forms", ticketFormId), null);
        }

        public IndividualTicketResponse CreateTicket(Ticket ticket)
        {
            var body = new {
                ticket = ticket
            };
            return base.GenericPost<IndividualTicketResponse>("tickets.json", body);
        }

        public IndividualTicketFieldResponse CreateTicketField(TicketField ticketField)
        {
            if (ticketField.CustomFieldOptions != null)
            {
                foreach (CustomFieldOptions options in ticketField.CustomFieldOptions)
                {
                    options.Name = options.Name.Replace(' ', '_');
                    options.Value = options.Value.Replace(' ', '_');
                }
            }
            var body = new {
                ticket_field = ticketField
            };
            return base.GenericPost<IndividualTicketFieldResponse>("ticket_fields.json", body);
        }

        public IndividualTicketFormResponse CreateTicketForm(TicketForm ticketForm)
        {
            var body = new {
                ticket_form = ticketForm
            };
            return base.GenericPost<IndividualTicketFormResponse>("ticket_forms.json", body);
        }

        public bool Delete(long id)
        {
            return base.GenericDelete(string.Format("{0}/{1}.json", "tickets", id));
        }

        public bool DeleteManySuspendedTickets(IEnumerable<long> ids)
        {
            return base.GenericDelete(string.Format("suspended_tickets/destroy_many.json?ids={0}", ids.ToCsv()));
        }

        public bool DeleteMultiple(IEnumerable<long> ids)
        {
            return base.GenericDelete(string.Format("{0}/destroy_many.json?ids={1}", "tickets", ids.ToCsv()));
        }

        public bool DeleteSuspendedTickets(long id)
        {
            return base.GenericDelete(string.Format("suspended_tickets/{0}.json", id));
        }

        public bool DeleteTicketField(long id)
        {
            return base.GenericDelete(string.Format("ticket_fields/{0}.json", id));
        }

        public bool DeleteTicketForm(long id)
        {
            return base.GenericDelete(string.Format("{0}/{1}.json", "ticket_forms", id));
        }

        public GroupTicketMetricResponse GetAllTicketMetrics()
        {
            return base.GenericGet<GroupTicketMetricResponse>("ticket_metrics.json");
        }

        public GroupTicketResponse GetAllTickets()
        {
            return base.GenericGet<GroupTicketResponse>("tickets.json");
        }

        public IndividualAuditResponse GetAuditById(long ticketId, long auditId)
        {
            return base.GenericGet<IndividualAuditResponse>(string.Format("tickets/{0}/audits/{1}.json", ticketId, auditId));
        }

        public GroupAuditResponse GetAudits(long ticketId)
        {
            return base.GenericGet<GroupAuditResponse>(string.Format("tickets/{0}/audits.json", ticketId));
        }

        public GroupUserResponse GetCollaborators(long id)
        {
            return base.GenericGet<GroupUserResponse>(string.Format("{0}/{1}/collaborators.json", "tickets", id));
        }

        public GroupTicketResponse GetIncidents(long id)
        {
            return base.GenericGet<GroupTicketResponse>(string.Format("{0}/{1}/incidents.json", "tickets", id));
        }

        public TicketExportResponse GetInrementalTicketExport(DateTime startTime)
        {
            return base.GenericGet<TicketExportResponse>("exports/tickets.json?start_time=" + startTime.GetEpoch());
        }

        public GroupTicketResponse GetMultipleTickets(IEnumerable<long> ids)
        {
            return base.GenericGet<GroupTicketResponse>(string.Format("{0}/show_many.json?ids={1}", "tickets", ids.ToCsv()));
        }

        public GroupTicketResponse GetProblems()
        {
            return base.GenericGet<GroupTicketResponse>("problems.json");
        }

        public GroupTicketResponse GetRecentTickets()
        {
            return base.GenericGet<GroupTicketResponse>("tickets/recent.json");
        }

        public IndividualSuspendedTicketResponse GetSuspendedTicketById(long id)
        {
            return base.GenericGet<IndividualSuspendedTicketResponse>(string.Format("suspended_tickets/{0}.json", id));
        }

        public GroupSuspendedTicketResponse GetSuspendedTickets()
        {
            return base.GenericGet<GroupSuspendedTicketResponse>(string.Format("suspended_tickets.json", new object[0]));
        }

        public IndividualTicketResponse GetTicket(long id)
        {
            return base.GenericGet<IndividualTicketResponse>(string.Format("{0}/{1}.json", "tickets", id));
        }

        public GroupCommentResponse GetTicketComments(long ticketId)
        {
            return base.GenericGet<GroupCommentResponse>(string.Format("{0}/{1}/comments.json", "tickets", ticketId));
        }

        public IndividualTicketFieldResponse GetTicketFieldById(long id)
        {
            return base.GenericGet<IndividualTicketFieldResponse>(string.Format("ticket_fields/{0}.json", id));
        }

        public GroupTicketFieldResponse GetTicketFields()
        {
            return base.GenericGet<GroupTicketFieldResponse>("ticket_fields.json");
        }

        public IndividualTicketFormResponse GetTicketFormById(long id)
        {
            return base.GenericGet<IndividualTicketFormResponse>(string.Format("{0}/{1}.json", "ticket_forms", id));
        }

        public GroupTicketFormResponse GetTicketForms()
        {
            return base.GenericGet<GroupTicketFormResponse>("ticket_forms.json");
        }

        public IndividualTicketMetricResponse GetTicketMetricsForTicket(long ticket_id)
        {
            return base.GenericGet<IndividualTicketMetricResponse>(string.Format("{0}/{1}/metrics.json", "tickets", ticket_id));
        }

        public GroupTicketResponse GetTicketsByOrganizationID(long id)
        {
            return base.GenericGet<GroupTicketResponse>(string.Format("{0}/{1}/{2}.json", "organizations", id, "tickets"));
        }

        public GroupTicketResponse GetTicketsByOrganizationID(long id, int pageNumber, int itemsPerPage)
        {
            return base.GenericGet<GroupTicketResponse>(string.Format("{0}/{1}/{2}.json?page={3}&per_page={4}", new object[] { "organizations", id, "tickets", pageNumber, itemsPerPage }));
        }

        public GroupTicketResponse GetTicketsByUserID(long userId)
        {
            return base.GenericGet<GroupTicketResponse>(string.Format("users/{0}/tickets/requested.json", userId));
        }

        public GroupTicketResponse GetTicketsByViewID(int id)
        {
            return base.GenericGet<GroupTicketResponse>(string.Format("{0}/{1}/{2}.json", "views", id, "tickets"));
        }

        public GroupTicketResponse GetTicketsWhereUserIsCopied(long userId)
        {
            return base.GenericGet<GroupTicketResponse>(string.Format("users/{0}/tickets/ccd.json", userId));
        }

        public bool MarkAuditAsTrusted(long ticketId, long auditId)
        {
            string resource = string.Format("tickets/{0}/audits/{1}/trust.json", ticketId, auditId);
            return (base.RunRequest(resource, "PUT", null).HttpStatusCode == HttpStatusCode.OK);
        }

        public bool RecoverManySuspendedTickets(IEnumerable<long> ids)
        {
            string resource = string.Format("suspended_tickets/recover_many.json?ids={0}", ids.ToCsv());
            return (base.RunRequest(resource, "PUT", null).HttpStatusCode == HttpStatusCode.OK);
        }

        public bool RecoverSuspendedTicket(long id)
        {
            string resource = string.Format("suspended_tickets/{0}/recover.json", id);
            return (base.RunRequest(resource, "PUT", null).HttpStatusCode == HttpStatusCode.OK);
        }

        public bool ReorderTicketForms(long[] orderedTicketFormIds)
        {
            var body = new {
                ticket_form_ids = orderedTicketFormIds
            };
            return base.GenericPut<bool>(string.Format("{0}/reorder.json", "ticket_forms"), body);
        }

        public IndividualTicketResponse UpdateTicket(Ticket ticket, Comment comment = null)
        {
            if (comment != null)
            {
                ticket.Comment = comment;
            }
            var body = new {
                ticket = ticket
            };
            return base.GenericPut<IndividualTicketResponse>(string.Format("{0}/{1}.json", "tickets", ticket.Id), body);
        }

        public IndividualTicketFieldResponse UpdateTicketField(TicketField ticketField)
        {
            var body = new {
                ticket_field = ticketField
            };
            return base.GenericPut<IndividualTicketFieldResponse>(string.Format("ticket_fields/{0}.json", ticketField.Id), body);
        }

        public IndividualTicketFormResponse UpdateTicketForm(TicketForm ticketForm)
        {
            var body = new {
                ticket_form = ticketForm
            };
            return base.GenericPut<IndividualTicketFormResponse>(string.Format("{0}/{1}.json", "ticket_forms", ticketForm.Id), body);
        }
    }
}

