﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using System.Collections.Generic;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Shared;

    public interface IAttachments : ICore
    {
        Upload UploadAttachment(ZenFile file);
        Upload UploadAttachment(ZenFile file, int? timeout);
        Upload UploadAttachments(IEnumerable<ZenFile> files);
    }
}

