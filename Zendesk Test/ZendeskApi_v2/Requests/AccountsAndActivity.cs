﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.AccountsAndActivities;

    public class AccountsAndActivity : Core, ICore, IAccountsAndActivity
    {
        public AccountsAndActivity(string yourZendeskUrl, string user, string password, string apiToken) : base(yourZendeskUrl, user, password, apiToken)
        {
        }

        public GroupActivityResponse GetActivities()
        {
            return base.GenericGet<GroupActivityResponse>("activities.json");
        }

        public IndividualActivityResponse GetActivityById(long activityId)
        {
            return base.GenericGet<IndividualActivityResponse>(string.Format("activities/{0}.json", activityId));
        }

        public SettingsResponse GetSettings()
        {
            return base.GenericGet<SettingsResponse>("account/settings.json");
        }
    }
}

