﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Locales;

    public interface ILocales : ICore
    {
        GroupLocaleResponse GetAllLocales();
        IndividualLocaleResponse GetCurrentLocale();
        IndividualLocaleResponse GetLocaleById(long id);
        GroupLocaleResponse GetLocalesForAgents();
    }
}

