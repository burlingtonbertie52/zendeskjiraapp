﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Tags;

    public class Tags : Core, ICore, ITags
    {
        public Tags(string yourZendeskUrl, string user, string password, string apiToken) : base(yourZendeskUrl, user, password, apiToken)
        {
        }

        public TagAutocompleteResponse AutocompleteTags(string name)
        {
            return base.GenericPost<TagAutocompleteResponse>(string.Format("autocomplete/tags.json?name={0}", name), null);
        }

        public GroupTagResult GetTags()
        {
            return base.GenericGet<GroupTagResult>("tags.json");
        }
    }
}

