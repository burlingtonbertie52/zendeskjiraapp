﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Triggers;

    public class Triggers : Core, ICore, ITriggers
    {
        public Triggers(string yourZendeskUrl, string user, string password, string apiToken) : base(yourZendeskUrl, user, password, apiToken)
        {
        }

        public GroupTriggerResponse GetActiveTriggers()
        {
            return base.GenericGet<GroupTriggerResponse>(string.Format("triggers/active.json", new object[0]));
        }

        public IndividualTriggerResponse GetTriggerById(long id)
        {
            return base.GenericGet<IndividualTriggerResponse>(string.Format("triggers/{0}.json", id));
        }

        public GroupTriggerResponse GetTriggers()
        {
            return base.GenericGet<GroupTriggerResponse>(string.Format("triggers.json", new object[0]));
        }
    }
}

