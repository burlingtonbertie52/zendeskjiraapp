﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Requests;
    using ZendeskApi_v2.Models.Shared;
    using ZendeskApi_v2.Models.Tickets;
    using ZendeskApi_v2.Models.Tickets.Suspended;
    using ZendeskApi_v2.Models.Users;

    public interface ITickets : ICore
    {
        TicketExportResponse __TestOnly__GetInrementalTicketExport(DateTime startTime);
        GroupTicketResponse AutoCompleteProblems(string text);
        JobStatusResponse BulkUpdate(IEnumerable<long> ids, ZendeskApi_v2.Models.Tickets.BulkUpdate info);
        IndividualTicketFormResponse CloneTicketForm(long ticketFormId);
        IndividualTicketResponse CreateTicket(Ticket ticket);
        IndividualTicketFieldResponse CreateTicketField(TicketField ticketField);
        IndividualTicketFormResponse CreateTicketForm(TicketForm ticketForm);
        bool Delete(long id);
        bool DeleteManySuspendedTickets(IEnumerable<long> ids);
        bool DeleteMultiple(IEnumerable<long> ids);
        bool DeleteSuspendedTickets(long id);
        bool DeleteTicketField(long id);
        bool DeleteTicketForm(long id);
        GroupTicketMetricResponse GetAllTicketMetrics();
        GroupTicketResponse GetAllTickets();
        IndividualAuditResponse GetAuditById(long ticketId, long auditId);
        GroupAuditResponse GetAudits(long ticketId);
        GroupUserResponse GetCollaborators(long id);
        GroupTicketResponse GetIncidents(long id);
        TicketExportResponse GetInrementalTicketExport(DateTime startTime);
        GroupTicketResponse GetMultipleTickets(IEnumerable<long> ids);
        GroupTicketResponse GetProblems();
        GroupTicketResponse GetRecentTickets();
        IndividualSuspendedTicketResponse GetSuspendedTicketById(long id);
        GroupSuspendedTicketResponse GetSuspendedTickets();
        IndividualTicketResponse GetTicket(long id);
        GroupCommentResponse GetTicketComments(long ticketId);
        IndividualTicketFieldResponse GetTicketFieldById(long id);
        GroupTicketFieldResponse GetTicketFields();
        IndividualTicketFormResponse GetTicketFormById(long id);
        GroupTicketFormResponse GetTicketForms();
        IndividualTicketMetricResponse GetTicketMetricsForTicket(long ticket_id);
        GroupTicketResponse GetTicketsByOrganizationID(long id);
        GroupTicketResponse GetTicketsByOrganizationID(long id, int pageNumber, int itemsPerPage);
        GroupTicketResponse GetTicketsByUserID(long userId);
        GroupTicketResponse GetTicketsByViewID(int id);
        GroupTicketResponse GetTicketsWhereUserIsCopied(long userId);
        bool MarkAuditAsTrusted(long ticketId, long auditId);
        bool RecoverManySuspendedTickets(IEnumerable<long> ids);
        bool RecoverSuspendedTicket(long id);
        bool ReorderTicketForms(long[] orderedTicketFormIds);
        IndividualTicketResponse UpdateTicket(Ticket ticket, Comment comment = null);
        IndividualTicketFieldResponse UpdateTicketField(TicketField ticketField);
        IndividualTicketFormResponse UpdateTicketForm(TicketForm ticketForm);
    }
}

