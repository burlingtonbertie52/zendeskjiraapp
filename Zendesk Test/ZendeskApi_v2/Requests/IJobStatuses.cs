﻿namespace ZendeskApi_v2.Requests
{
    using System;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Shared;

    public interface IJobStatuses : ICore
    {
        JobStatusResponse GetJobStatus(string id);
    }
}

