﻿namespace Atlassian.Jira
{
    using Atlassian.Jira.Remote;
    using System;

    public class ProjectComponent : JiraNamedEntity
    {
        private readonly Atlassian.Jira.Remote.RemoteComponent _remoteComponent;

        internal ProjectComponent(Atlassian.Jira.Remote.RemoteComponent remoteComponent) : base(remoteComponent)
        {
            this._remoteComponent = remoteComponent;
        }

        internal Atlassian.Jira.Remote.RemoteComponent RemoteComponent
        {
            get
            {
                return this._remoteComponent;
            }
        }
    }
}

