﻿namespace Atlassian.Jira
{
    using System;
    using System.Net;

    internal class WebClientWrapper : IWebClient
    {
        private readonly WebClient _webClient = new WebClient();

        public void AddQueryString(string key, string value)
        {
            this._webClient.QueryString.Add(key, value);
        }

        public void Download(string url, string fileName)
        {
            this._webClient.DownloadFile(url, fileName);
        }
    }
}

