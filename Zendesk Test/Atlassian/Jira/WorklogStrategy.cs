﻿namespace Atlassian.Jira
{
    using System;

    public enum WorklogStrategy
    {
        AutoAdjustRemainingEstimate,
        RetainRemainingEstimate,
        NewRemainingEstimate
    }
}

