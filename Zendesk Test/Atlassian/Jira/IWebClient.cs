﻿namespace Atlassian.Jira
{
    using System;

    public interface IWebClient
    {
        void AddQueryString(string key, string value);
        void Download(string url, string fileName);
    }
}

