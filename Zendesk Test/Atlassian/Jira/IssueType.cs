﻿namespace Atlassian.Jira
{
    using Atlassian.Jira.Remote;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.InteropServices;

    public class IssueType : JiraNamedEntity
    {
        private bool? _isSubTask;

        internal IssueType(RemoteIssueType remoteIssueType) : base(remoteIssueType)
        {
            this._isSubTask = new bool?(remoteIssueType.subTask);
        }

        internal IssueType(string name) : base(name)
        {
        }

        internal IssueType(Atlassian.Jira.Jira jira, string id) : base(jira, id)
        {
        }

        protected override IEnumerable<JiraNamedEntity> GetEntities(Atlassian.Jira.Jira jira, string projectKey = null)
        {
            if (jira == null)
            {
                throw new ArgumentNullException("jira");
            }
            return jira.GetIssueTypes(projectKey);
        }

        public static bool operator ==(IssueType entity, string name)
        {
            if (entity == null)
            {
                return (name == null);
            }
            if (name == null)
            {
                return false;
            }
            return (entity._name == name);
        }

        public static implicit operator IssueType(string name)
        {
            int num;
            if (name == null)
            {
                return null;
            }
            if (int.TryParse(name, out num))
            {
                return new IssueType(null, name);
            }
            return new IssueType(name);
        }

        public static bool operator !=(IssueType entity, string name)
        {
            if (entity == null)
            {
                return (name != null);
            }
            return ((name == null) || (entity._name != name));
        }

        public bool IsSubTask
        {
            get
            {
                Func<IssueType, bool> predicate = null;
                if (!this._isSubTask.HasValue)
                {
                    if (base.Jira == null)
                    {
                        throw new InvalidOperationException("Unable to retrieve remote issue type information. This is not supported if the issue type has been set by user code.");
                    }
                    if (predicate == null)
                    {
                        predicate = issueType => issueType.Id.Equals(base.Id, StringComparison.OrdinalIgnoreCase);
                    }
                    this._isSubTask = new bool?(base.Jira.GetSubTaskIssueTypes().Any<IssueType>(predicate));
                }
                return this._isSubTask.Value;
            }
        }
    }
}

