﻿namespace Atlassian.Jira
{
    using System;
    using System.Runtime.CompilerServices;

    public class ComparableString
    {
        public ComparableString(string value)
        {
            this.Value = value;
        }

        public override bool Equals(object obj)
        {
            if (obj is ComparableString)
            {
                return this.Value.Equals(((ComparableString) obj).Value);
            }
            if (obj is string)
            {
                return this.Value.Equals((string) obj);
            }
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            if (this.Value == null)
            {
                return 0;
            }
            return this.Value.GetHashCode();
        }

        public static bool operator ==(ComparableString field, DateTime value)
        {
            if (field == null)
            {
                return false;
            }
            return (field.Value == value.ToString("yyyy/MM/dd", Atlassian.Jira.Jira.DefaultCultureInfo));
        }

        public static bool operator ==(ComparableString field, string value)
        {
            if (field == null)
            {
                return (value == null);
            }
            return (field.Value == value);
        }

        public static bool operator >(ComparableString field, DateTime value)
        {
            return (field.Value.CompareTo(value.ToString("yyyy/MM/dd", Atlassian.Jira.Jira.DefaultCultureInfo)) > 0);
        }

        public static bool operator >(ComparableString field, string value)
        {
            return (field.Value.CompareTo(value) > 0);
        }

        public static bool operator >=(ComparableString field, DateTime value)
        {
            return (field.Value.CompareTo(value.ToString("yyyy/MM/dd", Atlassian.Jira.Jira.DefaultCultureInfo)) >= 0);
        }

        public static bool operator >=(ComparableString field, string value)
        {
            return (field.Value.CompareTo(value) >= 0);
        }

        public static implicit operator ComparableString(string value)
        {
            if (value != null)
            {
                return new ComparableString(value);
            }
            return null;
        }

        public static bool operator !=(ComparableString field, DateTime value)
        {
            return ((field == null) || (field.Value != value.ToString("yyyy/MM/dd", Atlassian.Jira.Jira.DefaultCultureInfo)));
        }

        public static bool operator !=(ComparableString field, string value)
        {
            if (field == null)
            {
                return (value != null);
            }
            return (field.Value != value);
        }

        public static bool operator <(ComparableString field, DateTime value)
        {
            return (field.Value.CompareTo(value.ToString("yyyy/MM/dd", Atlassian.Jira.Jira.DefaultCultureInfo)) < 0);
        }

        public static bool operator <(ComparableString field, string value)
        {
            return (field.Value.CompareTo(value) < 0);
        }

        public static bool operator <=(ComparableString field, DateTime value)
        {
            return (field.Value.CompareTo(value.ToString("yyyy/MM/dd", Atlassian.Jira.Jira.DefaultCultureInfo)) <= 0);
        }

        public static bool operator <=(ComparableString field, string value)
        {
            return (field.Value.CompareTo(value) <= 0);
        }

        public override string ToString()
        {
            return this.Value;
        }

        public string Value { get; set; }
    }
}

