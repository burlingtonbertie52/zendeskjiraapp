﻿namespace Atlassian.Jira
{
    using Atlassian.Jira.Remote;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.InteropServices;

    public class JiraNamedEntity
    {
        private string _id;
        private Atlassian.Jira.Jira _jira;
        protected string _name;

        internal JiraNamedEntity(AbstractNamedRemoteEntity remoteEntity)
        {
            this._id = remoteEntity.id;
            this._name = remoteEntity.name;
        }

        internal JiraNamedEntity(string name)
        {
            this._name = name;
        }

        internal JiraNamedEntity(Atlassian.Jira.Jira jira, string id)
        {
            this._jira = jira;
            this._id = id;
        }

        protected virtual IEnumerable<JiraNamedEntity> GetEntities(Atlassian.Jira.Jira jira, string projectKey = null)
        {
            throw new NotImplementedException();
        }

        internal JiraNamedEntity LoadByName(Atlassian.Jira.Jira jira, string projectKey)
        {
            JiraNamedEntity entity = this.GetEntities(jira, projectKey).FirstOrDefault<JiraNamedEntity>(e => e.Name.Equals(this._name, StringComparison.OrdinalIgnoreCase));
            if (entity != null)
            {
                this._id = entity._id;
                this._name = entity._name;
            }
            return this;
        }

        public override string ToString()
        {
            if (!string.IsNullOrEmpty(this._name))
            {
                return this._name;
            }
            return this._id;
        }

        public string Id
        {
            get
            {
                return this._id;
            }
        }

        protected Atlassian.Jira.Jira Jira
        {
            get
            {
                return this._jira;
            }
        }

        public string Name
        {
            get
            {
                Func<JiraNamedEntity, bool> predicate = null;
                if (string.IsNullOrEmpty(this._name))
                {
                    if (predicate == null)
                    {
                        predicate = e => e.Id.Equals(this._id, StringComparison.OrdinalIgnoreCase);
                    }
                    this._name = this.GetEntities(this._jira, null).First<JiraNamedEntity>(predicate).Name;
                }
                return this._name;
            }
        }
    }
}

