﻿namespace Atlassian.Jira
{
    using System;
    using System.Runtime.CompilerServices;

    public class UploadAttachmentInfo
    {
        public UploadAttachmentInfo(string name, byte[] data)
        {
            this.Name = name;
            this.Data = data;
        }

        public byte[] Data { get; set; }

        public string Name { get; set; }
    }
}

