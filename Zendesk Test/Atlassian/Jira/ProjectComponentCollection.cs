﻿namespace Atlassian.Jira
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class ProjectComponentCollection : JiraNamedEntityCollection<ProjectComponent>
    {
        internal ProjectComponentCollection(string fieldName, Atlassian.Jira.Jira jira, string projectKey) : this(fieldName, jira, projectKey, new List<ProjectComponent>())
        {
        }

        internal ProjectComponentCollection(string fieldName, Atlassian.Jira.Jira jira, string projectKey, IList<ProjectComponent> list) : base(fieldName, jira, projectKey, list)
        {
        }

        public void Add(string componentName)
        {
            base.Add(base._jira.GetProjectComponents(base._projectKey).First<ProjectComponent>(v => v.Name.Equals(componentName, StringComparison.OrdinalIgnoreCase)));
        }
    }
}

