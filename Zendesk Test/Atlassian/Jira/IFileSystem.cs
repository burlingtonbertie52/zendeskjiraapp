﻿namespace Atlassian.Jira
{
    using System;

    public interface IFileSystem
    {
        byte[] FileReadAllBytes(string path);
    }
}

