﻿namespace Atlassian.Jira
{
    using System;

    public static class WorkflowActions
    {
        public const string Close = "Close Issue";
        public const string Resolve = "Resolve Issue";
        public const string StartProgress = "Start Progress";
    }
}

