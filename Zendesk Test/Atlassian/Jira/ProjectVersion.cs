﻿namespace Atlassian.Jira
{
    using Atlassian.Jira.Remote;
    using System;

    public class ProjectVersion : JiraNamedEntity
    {
        private readonly Atlassian.Jira.Remote.RemoteVersion _remoteVersion;

        internal ProjectVersion(Atlassian.Jira.Remote.RemoteVersion remoteVersion) : base(remoteVersion)
        {
            this._remoteVersion = remoteVersion;
        }

        public bool IsArchived
        {
            get
            {
                return this._remoteVersion.archived;
            }
        }

        public bool IsReleased
        {
            get
            {
                return this._remoteVersion.released;
            }
        }

        public DateTime? ReleasedDate
        {
            get
            {
                return this._remoteVersion.releaseDate;
            }
        }

        internal Atlassian.Jira.Remote.RemoteVersion RemoteVersion
        {
            get
            {
                return this._remoteVersion;
            }
        }
    }
}

