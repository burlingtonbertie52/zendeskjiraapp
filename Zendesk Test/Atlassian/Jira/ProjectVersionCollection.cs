﻿namespace Atlassian.Jira
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class ProjectVersionCollection : JiraNamedEntityCollection<ProjectVersion>
    {
        internal ProjectVersionCollection(string fieldName, Atlassian.Jira.Jira jira, string projectKey) : this(fieldName, jira, projectKey, new List<ProjectVersion>())
        {
        }

        internal ProjectVersionCollection(string fieldName, Atlassian.Jira.Jira jira, string projectKey, IList<ProjectVersion> list) : base(fieldName, jira, projectKey, list)
        {
        }

        public void Add(string versionName)
        {
            base.Add(base._jira.GetProjectVersions(base._projectKey).First<ProjectVersion>(v => v.Name.Equals(versionName, StringComparison.OrdinalIgnoreCase)));
        }
    }
}

