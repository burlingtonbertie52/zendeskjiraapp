﻿namespace Atlassian.Jira
{
    using Atlassian.Jira.Remote;
    using System;

    public class Attachment
    {
        private readonly string _author;
        private readonly DateTime? _created;
        private readonly string _fileName;
        private readonly long? _fileSize;
        private readonly string _id;
        private readonly Atlassian.Jira.Jira _jira;
        private readonly string _mimeType;
        private readonly IWebClient _webClient;

        internal Attachment(Atlassian.Jira.Jira jira, IWebClient webClient, RemoteAttachment remoteAttachment)
        {
            this._jira = jira;
            this._author = remoteAttachment.author;
            this._created = remoteAttachment.created;
            this._fileName = remoteAttachment.filename;
            this._mimeType = remoteAttachment.mimetype;
            this._fileSize = remoteAttachment.filesize;
            this._id = remoteAttachment.id;
            this._webClient = webClient;
        }

        public void Download(string fullFileName)
        {
            JiraCredentials credentials = this._jira.GetCredentials();
            if (string.IsNullOrEmpty(credentials.UserName) || string.IsNullOrEmpty(credentials.Password))
            {
                throw new InvalidOperationException("Unable to download attachment, user and/or password are missing. You can specify a provider for credentials when constructing the Jira instance.");
            }
            this._webClient.AddQueryString("os_username", Uri.EscapeDataString(credentials.UserName));
            this._webClient.AddQueryString("os_password", Uri.EscapeDataString(credentials.Password));
            string url = string.Format("{0}secure/attachment/{1}/{2}", this._jira.Url.EndsWith("/") ? this._jira.Url : (this._jira.Url + "/"), this.Id, this.FileName);
            this._webClient.Download(url, fullFileName);
        }

        public string Author
        {
            get
            {
                return this._author;
            }
        }

        public DateTime? CreatedDate
        {
            get
            {
                return this._created;
            }
        }

        public string FileName
        {
            get
            {
                return this._fileName;
            }
        }

        public long? FileSize
        {
            get
            {
                return this._fileSize;
            }
        }

        public string Id
        {
            get
            {
                return this._id;
            }
        }

        public string MimeType
        {
            get
            {
                return this._mimeType;
            }
        }
    }
}

