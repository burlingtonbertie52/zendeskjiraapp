﻿namespace Atlassian.Jira
{
    using Atlassian.Jira.Remote;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class Worklog
    {
        private readonly DateTime? _created;
        private readonly string _id;
        private readonly long _timeSpentInSeconds;
        private readonly DateTime? _updated;

        internal Worklog(RemoteWorklog remoteWorklog)
        {
            if (remoteWorklog != null)
            {
                this.Author = remoteWorklog.author;
                this.Comment = remoteWorklog.comment;
                this.StartDate = remoteWorklog.startDate;
                this.TimeSpent = remoteWorklog.timeSpent;
                this._id = remoteWorklog.id;
                this._created = remoteWorklog.created;
                this._timeSpentInSeconds = remoteWorklog.timeSpentInSeconds;
                this._updated = remoteWorklog.updated;
            }
        }

        public Worklog(string timeSpent, DateTime startDate, string comment = null)
        {
            this.TimeSpent = timeSpent;
            this.StartDate = new DateTime?(startDate);
            this.Comment = comment;
        }

        internal RemoteWorklog ToRemote()
        {
            return new RemoteWorklog { author = this.Author, comment = this.Comment, startDate = this.StartDate, timeSpent = this.TimeSpent };
        }

        public string Author { get; set; }

        public string Comment { get; set; }

        public DateTime? CreateDate
        {
            get
            {
                return this._created;
            }
        }

        public string Id
        {
            get
            {
                return this._id;
            }
        }

        public DateTime? StartDate { get; set; }

        public string TimeSpent { get; set; }

        public long TimeSpentInSeconds
        {
            get
            {
                return this._timeSpentInSeconds;
            }
        }

        public DateTime? UpdateDate
        {
            get
            {
                return this._updated;
            }
        }
    }
}

