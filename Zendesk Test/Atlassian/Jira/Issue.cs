﻿namespace Atlassian.Jira
{
    using Atlassian.Jira.Linq;
    using Atlassian.Jira.Remote;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class Issue : IRemoteIssueFieldProvider
    {
        private ProjectVersionCollection _affectsVersions;
        private ProjectComponentCollection _components;
        private DateTime? _createDate;
        private CustomFieldCollection _customFields;
        private DateTime? _dueDate;
        private ProjectVersionCollection _fixVersions;
        private readonly Atlassian.Jira.Jira _jira;
        private ComparableString _key;
        private RemoteIssue _originalIssue;
        private readonly string _parentIssueKey;
        private string _project;
        private IssueStatus _status;
        private DateTime? _updateDate;

        internal Issue(Atlassian.Jira.Jira jira, RemoteIssue remoteIssue, string parentIssueKey = null)
        {
            this._jira = jira;
            this._parentIssueKey = parentIssueKey;
            this.Initialize(remoteIssue);
        }

        public Issue(Atlassian.Jira.Jira jira, string projectKey, string parentIssueKey = null) : this(jira, issue, parentIssueKey)
        {
            RemoteIssue issue = new RemoteIssue {
                project = projectKey
            };
        }

        public void AddAttachment(params UploadAttachmentInfo[] attachments)
        {
            if (string.IsNullOrEmpty(this._originalIssue.key))
            {
                throw new InvalidOperationException("Unable to upload attachments to server, issue has not been created.");
            }
            List<string> content = new List<string>();
            List<string> names = new List<string>();
            foreach (UploadAttachmentInfo info in attachments)
            {
                names.Add(info.Name);
                content.Add(Convert.ToBase64String(info.Data));
            }
            this._jira.WithToken((Action<string>) (token => this._jira.RemoteSoapService.AddBase64EncodedAttachmentsToIssue(token, this._originalIssue.key, names.ToArray(), content.ToArray())));
        }

        public void AddAttachment(params string[] filePaths)
        {
            UploadAttachmentInfo[] attachments = (from f in filePaths select new UploadAttachmentInfo(Path.GetFileName(f), this._jira.FileSystem.FileReadAllBytes(f))).ToArray<UploadAttachmentInfo>();
            this.AddAttachment(attachments);
        }

        public void AddAttachment(string name, byte[] data)
        {
            this.AddAttachment(new UploadAttachmentInfo[] { new UploadAttachmentInfo(name, data) });
        }

        public void AddComment(Comment comment)
        {
            if (string.IsNullOrEmpty(this._originalIssue.key))
            {
                throw new InvalidOperationException("Unable to add comment to issue, issue has not been created.");
            }
            if (string.IsNullOrEmpty(comment.Author))
            {
                throw new InvalidOperationException("Unable to add comment due to missing author field. You can specify a provider for credentials when constructing the Jira instance.");
            }
            this._jira.WithToken((Action<string>) (token => this._jira.RemoteSoapService.AddComment(token, this._originalIssue.key, comment.toRemote())));
        }

        public void AddComment(string comment)
        {
            JiraCredentials credentials = this._jira.GetCredentials();
            Comment comment2 = new Comment {
                Author = credentials.UserName,
                Body = comment
            };
            this.AddComment(comment2);
        }

        public void AddLabels(params string[] labels)
        {
            if (string.IsNullOrEmpty(this._originalIssue.key))
            {
                throw new InvalidOperationException("Unable to add label to issue, issue has not been created.");
            }
            RemoteFieldValue[] valueArray2 = new RemoteFieldValue[1];
            RemoteFieldValue value2 = new RemoteFieldValue {
                id = "labels",
                values = labels
            };
            valueArray2[0] = value2;
            RemoteFieldValue[] remoteFields = valueArray2;
            this.UpdateRemoteFields(remoteFields);
        }

        public Worklog AddWorklog(Worklog worklog, WorklogStrategy worklogStrategy = 0, string newEstimate = null)
        {
            if (string.IsNullOrEmpty(this._originalIssue.key))
            {
                throw new InvalidOperationException("Unable to add worklog to issue, issue has not been saved to server.");
            }
            RemoteWorklog remoteWorklog = worklog.ToRemote();
            this._jira.WithToken(delegate (string token) {
                switch (worklogStrategy)
                {
                    case WorklogStrategy.RetainRemainingEstimate:
                        remoteWorklog = this._jira.RemoteSoapService.AddWorklogAndRetainRemainingEstimate(token, this._originalIssue.key, remoteWorklog);
                        return;

                    case WorklogStrategy.NewRemainingEstimate:
                        remoteWorklog = this._jira.RemoteSoapService.AddWorklogWithNewRemainingEstimate(token, this._originalIssue.key, remoteWorklog, newEstimate);
                        return;
                }
                remoteWorklog = this._jira.RemoteSoapService.AddWorklogAndAutoAdjustRemainingEstimate(token, this._originalIssue.key, remoteWorklog);
            });
            return new Worklog(remoteWorklog);
        }

        public Worklog AddWorklog(string timespent, WorklogStrategy worklogStrategy = 0, string newEstimate = null)
        {
            return this.AddWorklog(new Worklog(timespent, DateTime.Now, null), worklogStrategy, newEstimate);
        }

        RemoteFieldValue[] IRemoteIssueFieldProvider.GetRemoteFields()
        {
            List<RemoteFieldValue> list = new List<RemoteFieldValue>();
            PropertyInfo[] properties = typeof(RemoteIssue).GetProperties();
            PropertyInfo[] infoArray2 = typeof(Issue).GetProperties();
            for (int j = 0; j < infoArray2.Length; j++)
            {
                Func<PropertyInfo, bool> predicate = null;
                PropertyInfo localProperty = infoArray2[j];
                if (typeof(IRemoteIssueFieldProvider).IsAssignableFrom(localProperty.PropertyType))
                {
                    IRemoteIssueFieldProvider provider = localProperty.GetValue(this, null) as IRemoteIssueFieldProvider;
                    if (provider != null)
                    {
                        list.AddRange(provider.GetRemoteFields());
                    }
                }
                else
                {
                    if (predicate == null)
                    {
                        predicate = i => i.Name.Equals(localProperty.Name, StringComparison.OrdinalIgnoreCase);
                    }
                    PropertyInfo property = properties.FirstOrDefault<PropertyInfo>(predicate);
                    if (property != null)
                    {
                        string stringValueForProperty = this.GetStringValueForProperty(this, localProperty);
                        if (this.GetStringValueForProperty(this._originalIssue, property) != stringValueForProperty)
                        {
                            string name = property.Name;
                            RemoteFieldNameAttribute attribute = localProperty.GetCustomAttributes(typeof(RemoteFieldNameAttribute), true).OfType<RemoteFieldNameAttribute>().FirstOrDefault<RemoteFieldNameAttribute>();
                            if (attribute != null)
                            {
                                name = attribute.Name;
                            }
                            RemoteFieldValue item = new RemoteFieldValue {
                                id = name,
                                values = new string[] { stringValueForProperty }
                            };
                            list.Add(item);
                        }
                    }
                }
            }
            return list.ToArray();
        }

        public void DeleteWorklog(Worklog worklog, WorklogStrategy worklogStrategy = 0, string newEstimate = null)
        {
            if (string.IsNullOrEmpty(this._originalIssue.key))
            {
                throw new InvalidOperationException("Unable to delete worklog from issue, issue has not been saved to server.");
            }
            this.Jira.WithToken(delegate (string token, IJiraSoapServiceClient client) {
                switch (worklogStrategy)
                {
                    case WorklogStrategy.AutoAdjustRemainingEstimate:
                        client.DeleteWorklogAndAutoAdjustRemainingEstimate(token, worklog.Id);
                        return;

                    case WorklogStrategy.RetainRemainingEstimate:
                        client.DeleteWorklogAndRetainRemainingEstimate(token, worklog.Id);
                        return;

                    case WorklogStrategy.NewRemainingEstimate:
                        client.DeleteWorklogWithNewRemainingEstimate(token, worklog.Id, newEstimate);
                        return;
                }
            });
        }

        public ReadOnlyCollection<Attachment> GetAttachments()
        {
            if (string.IsNullOrEmpty(this._originalIssue.key))
            {
                throw new InvalidOperationException("Unable to retrieve attachments from server, issue has not been created.");
            }
            return this._jira.WithToken<ReadOnlyCollection<Attachment>>((Func<string, ReadOnlyCollection<Attachment>>) (token => (from a in this._jira.RemoteSoapService.GetAttachmentsFromIssue(token, this._originalIssue.key) select new Attachment(this._jira, new WebClientWrapper(), a)).ToList<Attachment>().AsReadOnly()));
        }

        public IEnumerable<JiraNamedEntity> GetAvailableActions()
        {
            if (string.IsNullOrEmpty(this._originalIssue.key))
            {
                throw new InvalidOperationException("Unable to retrieve actions, issue has not been saved to server.");
            }
            return this._jira.WithToken<IEnumerable<JiraNamedEntity>>((Func<string, IEnumerable<JiraNamedEntity>>) (token => (from a in this._jira.RemoteSoapService.GetAvailableActions(token, this._originalIssue.key) select new JiraNamedEntity(a))));
        }

        public ReadOnlyCollection<Comment> GetComments()
        {
            if (string.IsNullOrEmpty(this._originalIssue.key))
            {
                throw new InvalidOperationException("Unable to retrieve comments from server, issue has not been created.");
            }
            return this._jira.WithToken<ReadOnlyCollection<Comment>>((Func<string, ReadOnlyCollection<Comment>>) (token => (from c in this._jira.RemoteSoapService.GetCommentsFromIssue(token, this._originalIssue.key) select new Comment(c)).ToList<Comment>().AsReadOnly()));
        }

        public DateTime? GetResolutionDate()
        {
            if (string.IsNullOrEmpty(this._originalIssue.key))
            {
                return null;
            }
            return this._jira.WithToken<DateTime?>(delegate (string token) {
                DateTime resolutionDateByKey = this._jira.RemoteSoapService.GetResolutionDateByKey(token, this._originalIssue.key);
                if (resolutionDateByKey.Ticks <= 0L)
                {
                    return null;
                }
                return new DateTime?(resolutionDateByKey);
            });
        }

        private string GetStringValueForProperty(object container, PropertyInfo property)
        {
            object obj2 = property.GetValue(container, null);
            if (property.PropertyType == typeof(DateTime?))
            {
                DateTime? nullable = (DateTime?) obj2;
                if (!nullable.HasValue)
                {
                    return null;
                }
                return nullable.Value.ToString("d/MMM/yy");
            }
            if (typeof(JiraNamedEntity).IsAssignableFrom(property.PropertyType))
            {
                JiraNamedEntity entity = property.GetValue(container, null) as JiraNamedEntity;
                if (entity == null)
                {
                    return null;
                }
                return (entity.Id ?? entity.LoadByName(this._jira, this.Project).Id);
            }
            if (obj2 == null)
            {
                return null;
            }
            return obj2.ToString();
        }

        public ReadOnlyCollection<Worklog> GetWorklogs()
        {
            if (string.IsNullOrEmpty(this._originalIssue.key))
            {
                throw new InvalidOperationException("Unable to retrieve worklog, issue has not been saved to server.");
            }
            return this._jira.WithToken<ReadOnlyCollection<Worklog>>((Func<string, ReadOnlyCollection<Worklog>>) (token => (from w in this._jira.RemoteSoapService.GetWorkLogs(token, this._originalIssue.key) select new Worklog(w)).ToList<Worklog>().AsReadOnly()));
        }

        private void Initialize(RemoteIssue remoteIssue)
        {
            this._originalIssue = remoteIssue;
            this._project = remoteIssue.project;
            this._key = remoteIssue.key;
            this._createDate = remoteIssue.created;
            this._dueDate = remoteIssue.duedate;
            this._updateDate = remoteIssue.updated;
            this.Assignee = remoteIssue.assignee;
            this.Description = remoteIssue.description;
            this.Environment = remoteIssue.environment;
            this.Reporter = remoteIssue.reporter;
            this.Summary = remoteIssue.summary;
            this.Votes = remoteIssue.votes;
            this._status = string.IsNullOrEmpty(remoteIssue.status) ? null : new IssueStatus(this._jira, remoteIssue.status);
            this.Priority = string.IsNullOrEmpty(remoteIssue.priority) ? null : new IssuePriority(this._jira, remoteIssue.priority);
            this.Resolution = string.IsNullOrEmpty(remoteIssue.resolution) ? null : new IssueResolution(this._jira, remoteIssue.resolution);
            this.Type = string.IsNullOrEmpty(remoteIssue.type) ? null : new IssueType(this._jira, remoteIssue.type);
            if (this._originalIssue.affectsVersions != null)
            {
            }
            this._affectsVersions = (CS$<>9__CachedAnonymousMethodDelegate6 != null) ? new ProjectVersionCollection("versions", this._jira, this.Project) : new ProjectVersionCollection("versions", this._jira, this.Project, this._originalIssue.affectsVersions.Select<RemoteVersion, ProjectVersion>(CS$<>9__CachedAnonymousMethodDelegate6).ToList<ProjectVersion>());
            if (this._originalIssue.fixVersions != null)
            {
            }
            this._fixVersions = (CS$<>9__CachedAnonymousMethodDelegate7 != null) ? new ProjectVersionCollection("fixVersions", this._jira, this.Project) : new ProjectVersionCollection("fixVersions", this._jira, this.Project, this._originalIssue.fixVersions.Select<RemoteVersion, ProjectVersion>(CS$<>9__CachedAnonymousMethodDelegate7).ToList<ProjectVersion>());
            if (this._originalIssue.components != null)
            {
            }
            this._components = (CS$<>9__CachedAnonymousMethodDelegate8 != null) ? new ProjectComponentCollection("components", this._jira, this.Project) : new ProjectComponentCollection("components", this._jira, this.Project, this._originalIssue.components.Select<RemoteComponent, ProjectComponent>(CS$<>9__CachedAnonymousMethodDelegate8).ToList<ProjectComponent>());
            this._customFields = (this._originalIssue.customFieldValues == null) ? new CustomFieldCollection(this) : new CustomFieldCollection(this, (from f in this._originalIssue.customFieldValues select new CustomField(f.customfieldId, this) { Values = f.values }).ToList<CustomField>());
        }

        public void Refresh()
        {
            if (string.IsNullOrEmpty(this._originalIssue.key))
            {
                throw new InvalidOperationException("Unable to refresh, issue has not been saved to server.");
            }
            RemoteIssue remoteIssue = this._jira.WithToken<RemoteIssue>((Func<string, RemoteIssue>) (token => this._jira.RemoteSoapService.GetIssuesFromJqlSearch(token, "key = " + this._originalIssue.key, 1).First<RemoteIssue>()));
            this.Initialize(remoteIssue);
        }

        public void SaveChanges()
        {
            if (string.IsNullOrEmpty(this._originalIssue.key))
            {
                RemoteIssue remoteIssue = this.ToRemote();
                this._jira.WithToken(delegate (string token) {
                    if (string.IsNullOrEmpty(this._parentIssueKey))
                    {
                        remoteIssue = this._jira.RemoteSoapService.CreateIssue(token, remoteIssue);
                    }
                    else
                    {
                        remoteIssue = this._jira.RemoteSoapService.CreateIssueWithParent(token, remoteIssue, this._parentIssueKey);
                    }
                });
                this.Initialize(remoteIssue);
            }
            else
            {
                this.UpdateRemoteFields(((IRemoteIssueFieldProvider) this).GetRemoteFields());
            }
        }

        internal RemoteIssue ToRemote()
        {
            RemoteIssue issue = new RemoteIssue {
                assignee = this.Assignee,
                description = this.Description,
                environment = this.Environment,
                project = this.Project,
                reporter = this.Reporter,
                summary = this.Summary,
                votes = this.Votes,
                duedate = this.DueDate
            };
            issue.key = (this.Key != null) ? this.Key.Value : null;
            if (this.Status != null)
            {
                issue.status = this.Status.Id ?? this.Status.LoadByName(this._jira, this.Project).Id;
            }
            if (this.Resolution != null)
            {
                issue.resolution = this.Resolution.Id ?? this.Resolution.LoadByName(this._jira, this.Project).Id;
            }
            if (this.Priority != null)
            {
                issue.priority = this.Priority.Id ?? this.Priority.LoadByName(this._jira, this.Project).Id;
            }
            if (this.Type != null)
            {
                issue.type = this.Type.Id ?? this.Type.LoadByName(this._jira, this.Project).Id;
            }
            if (this.AffectsVersions.Count > 0)
            {
                issue.affectsVersions = (from v in this.AffectsVersions select v.RemoteVersion).ToArray<RemoteVersion>();
            }
            if (this.FixVersions.Count > 0)
            {
                issue.fixVersions = (from v in this.FixVersions select v.RemoteVersion).ToArray<RemoteVersion>();
            }
            if (this.Components.Count > 0)
            {
                issue.components = (from c in this.Components select c.RemoteComponent).ToArray<RemoteComponent>();
            }
            if (this.CustomFields.Count > 0)
            {
                issue.customFieldValues = (from f in this.CustomFields select new RemoteCustomFieldValue { customfieldId = f.Id, values = f.Values }).ToArray<RemoteCustomFieldValue>();
            }
            return issue;
        }

        private void UpdateRemoteFields(RemoteFieldValue[] remoteFields)
        {
            RemoteIssue remoteIssue = this._jira.WithToken<RemoteIssue>((Func<string, RemoteIssue>) (token => this._jira.RemoteSoapService.UpdateIssue(token, this.Key.Value, remoteFields)));
            this.Initialize(remoteIssue);
        }

        public void WorkflowTransition(string actionName)
        {
            if (string.IsNullOrEmpty(this._originalIssue.key))
            {
                throw new InvalidOperationException("Unable to execute workflow transition, issue has not been created.");
            }
            JiraNamedEntity action = this.GetAvailableActions().FirstOrDefault<JiraNamedEntity>(a => a.Name.Equals(actionName, StringComparison.OrdinalIgnoreCase));
            if (action == null)
            {
                throw new InvalidOperationException(string.Format("Worflow action with name '{0}' not found.", actionName));
            }
            this._jira.WithToken(delegate (string token) {
                RemoteIssue remoteIssue = this._jira.RemoteSoapService.ProgressWorkflowAction(token, this._originalIssue.key, action.Id, ((IRemoteIssueFieldProvider) this).GetRemoteFields());
                this.Initialize(remoteIssue);
            });
        }

        [JqlFieldName("AffectedVersion")]
        public ProjectVersionCollection AffectsVersions
        {
            get
            {
                return this._affectsVersions;
            }
        }

        public string Assignee { get; set; }

        [JqlFieldName("component")]
        public ProjectComponentCollection Components
        {
            get
            {
                return this._components;
            }
        }

        public DateTime? Created
        {
            get
            {
                return this._createDate;
            }
        }

        public CustomFieldCollection CustomFields
        {
            get
            {
                return this._customFields;
            }
        }

        [JqlContainsEquality]
        public string Description { get; set; }

        public DateTime? DueDate
        {
            get
            {
                return this._dueDate;
            }
            set
            {
                this._dueDate = value;
            }
        }

        [JqlContainsEquality]
        public string Environment { get; set; }

        [JqlFieldName("FixVersion")]
        public ProjectVersionCollection FixVersions
        {
            get
            {
                return this._fixVersions;
            }
        }

        public ComparableString this[string customFieldName]
        {
            get
            {
                CustomField field = this._customFields[customFieldName];
                if (((field != null) && (field.Values != null)) && (field.Values.Count<string>() > 0))
                {
                    return field.Values[0];
                }
                return null;
            }
            set
            {
                CustomField field = this._customFields[customFieldName];
                if (field != null)
                {
                    field.Values = new string[] { value.Value };
                }
                else
                {
                    this._customFields.Add(customFieldName, new string[] { value.Value });
                }
            }
        }

        public Atlassian.Jira.Jira Jira
        {
            get
            {
                return this._jira;
            }
        }

        public string JiraIdentifier
        {
            get
            {
                if (string.IsNullOrEmpty(this._originalIssue.key))
                {
                    throw new InvalidOperationException("Unable to retrieve JIRA id, issue has not been created.");
                }
                return this._originalIssue.id;
            }
        }

        public ComparableString Key
        {
            get
            {
                return this._key;
            }
        }

        public IssuePriority Priority { get; set; }

        public string Project
        {
            get
            {
                return this._project;
            }
        }

        public string Reporter { get; set; }

        public IssueResolution Resolution { get; set; }

        public IssueStatus Status
        {
            get
            {
                return this._status;
            }
        }

        [JqlContainsEquality]
        public string Summary { get; set; }

        [RemoteFieldName("issuetype")]
        public IssueType Type { get; set; }

        public DateTime? Updated
        {
            get
            {
                return this._updateDate;
            }
        }

        public long? Votes { get; set; }
    }
}

