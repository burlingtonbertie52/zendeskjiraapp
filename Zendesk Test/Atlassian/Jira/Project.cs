﻿namespace Atlassian.Jira
{
    using Atlassian.Jira.Remote;
    using System;

    public class Project : JiraNamedEntity
    {
        private readonly Atlassian.Jira.Remote.RemoteProject _remoteProject;

        internal Project(Atlassian.Jira.Remote.RemoteProject remoteProject) : base(remoteProject)
        {
            this._remoteProject = remoteProject;
        }

        public string Key
        {
            get
            {
                return this._remoteProject.key;
            }
        }

        public string Lead
        {
            get
            {
                return this._remoteProject.lead;
            }
        }

        internal Atlassian.Jira.Remote.RemoteProject RemoteProject
        {
            get
            {
                return this._remoteProject;
            }
        }
    }
}

