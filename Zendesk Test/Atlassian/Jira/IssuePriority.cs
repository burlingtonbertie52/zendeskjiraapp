﻿namespace Atlassian.Jira
{
    using Atlassian.Jira.Remote;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public class IssuePriority : JiraNamedEntity
    {
        internal IssuePriority(AbstractNamedRemoteEntity remoteEntity) : base(remoteEntity)
        {
        }

        internal IssuePriority(string name) : base(name)
        {
        }

        internal IssuePriority(Atlassian.Jira.Jira jira, string id) : base(jira, id)
        {
        }

        protected override IEnumerable<JiraNamedEntity> GetEntities(Atlassian.Jira.Jira jira, string projectKey = null)
        {
            return jira.GetIssuePriorities();
        }

        public static bool operator ==(IssuePriority entity, string name)
        {
            if (entity == null)
            {
                return (name == null);
            }
            if (name == null)
            {
                return false;
            }
            return (entity._name == name);
        }

        public static bool operator >(IssuePriority field, string value)
        {
            throw new NotImplementedException();
        }

        public static bool operator >=(IssuePriority field, string value)
        {
            throw new NotImplementedException();
        }

        public static implicit operator IssuePriority(string name)
        {
            int num;
            if (name == null)
            {
                return null;
            }
            if (int.TryParse(name, out num))
            {
                return new IssuePriority(null, name);
            }
            return new IssuePriority(name);
        }

        public static bool operator !=(IssuePriority entity, string name)
        {
            if (entity == null)
            {
                return (name != null);
            }
            return ((name == null) || (entity._name != name));
        }

        public static bool operator <(IssuePriority field, string value)
        {
            throw new NotImplementedException();
        }

        public static bool operator <=(IssuePriority field, string value)
        {
            throw new NotImplementedException();
        }
    }
}

