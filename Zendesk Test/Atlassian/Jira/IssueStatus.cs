﻿namespace Atlassian.Jira
{
    using Atlassian.Jira.Remote;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public class IssueStatus : JiraNamedEntity
    {
        internal IssueStatus(AbstractNamedRemoteEntity remoteEntity) : base(remoteEntity)
        {
        }

        internal IssueStatus(string name) : base(name)
        {
        }

        internal IssueStatus(Atlassian.Jira.Jira jira, string id) : base(jira, id)
        {
        }

        protected override IEnumerable<JiraNamedEntity> GetEntities(Atlassian.Jira.Jira jira, string projectKey = null)
        {
            return jira.GetIssueStatuses();
        }

        public static bool operator ==(IssueStatus entity, string name)
        {
            if (entity == null)
            {
                return (name == null);
            }
            if (name == null)
            {
                return false;
            }
            return (entity._name == name);
        }

        public static implicit operator IssueStatus(string name)
        {
            int num;
            if (name == null)
            {
                return null;
            }
            if (int.TryParse(name, out num))
            {
                return new IssueStatus(null, name);
            }
            return new IssueStatus(name);
        }

        public static bool operator !=(IssueStatus entity, string name)
        {
            if (entity == null)
            {
                return (name != null);
            }
            return ((name == null) || (entity._name != name));
        }
    }
}

