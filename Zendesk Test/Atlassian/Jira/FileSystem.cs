﻿namespace Atlassian.Jira
{
    using System;
    using System.IO;

    internal class FileSystem : IFileSystem
    {
        public byte[] FileReadAllBytes(string path)
        {
            return File.ReadAllBytes(path);
        }
    }
}

