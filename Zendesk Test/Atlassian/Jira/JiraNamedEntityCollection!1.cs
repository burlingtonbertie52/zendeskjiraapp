﻿namespace Atlassian.Jira
{
    using Atlassian.Jira.Remote;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;

    public class JiraNamedEntityCollection<T> : ReadOnlyCollection<T>, IRemoteIssueFieldProvider where T: JiraNamedEntity
    {
        protected readonly string _fieldName;
        protected readonly Atlassian.Jira.Jira _jira;
        private List<T> _newElements;
        protected readonly string _projectKey;

        internal JiraNamedEntityCollection(string fieldName, Atlassian.Jira.Jira jira, string projectKey, IList<T> list) : base(list)
        {
            this._newElements = new List<T>();
            this._fieldName = fieldName;
            this._jira = jira;
            this._projectKey = projectKey;
        }

        public void Add(T element)
        {
            base.Items.Add(element);
            this._newElements.Add(element);
        }

        RemoteFieldValue[] IRemoteIssueFieldProvider.GetRemoteFields()
        {
            List<RemoteFieldValue> list = new List<RemoteFieldValue>();
            if (this._newElements.Count > 0)
            {
                RemoteFieldValue item = new RemoteFieldValue {
                    id = this._fieldName,
                    values = (from e in this._newElements select e.Id).ToArray<string>()
                };
                list.Add(item);
            }
            return list.ToArray();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.Items.GetHashCode();
        }

        public static bool operator ==(JiraNamedEntityCollection<T> list, string value)
        {
            if (list != null)
            {
                return list.Any<T>(v => (v.Name == value));
            }
            return (value == null);
        }

        public static bool operator !=(JiraNamedEntityCollection<T> list, string value)
        {
            if (list != null)
            {
                return !list.Any<T>(v => (v.Name == value));
            }
            return (value == null);
        }
    }
}

