﻿namespace Atlassian.Jira
{
    using System;
    using System.Runtime.InteropServices;

    public class JiraCredentials
    {
        private readonly string _password;
        private readonly string _username;

        public JiraCredentials(string username, string password = null)
        {
            this._username = username;
            this._password = password;
        }

        public string Password
        {
            get
            {
                return this._password;
            }
        }

        public string UserName
        {
            get
            {
                return this._username;
            }
        }
    }
}

