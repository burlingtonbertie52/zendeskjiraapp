﻿namespace Atlassian.Jira
{
    using Atlassian.Jira.Remote;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public class IssueResolution : JiraNamedEntity
    {
        internal IssueResolution(AbstractNamedRemoteEntity remoteEntity) : base(remoteEntity)
        {
        }

        internal IssueResolution(string name) : base(name)
        {
        }

        internal IssueResolution(Atlassian.Jira.Jira jira, string id) : base(jira, id)
        {
        }

        protected override IEnumerable<JiraNamedEntity> GetEntities(Atlassian.Jira.Jira jira, string projectKey = null)
        {
            return jira.GetIssueResolutions();
        }

        public static bool operator ==(IssueResolution entity, string name)
        {
            if (entity == null)
            {
                return (name == null);
            }
            if (name == null)
            {
                return false;
            }
            return (entity._name == name);
        }

        public static implicit operator IssueResolution(string name)
        {
            int num;
            if (name == null)
            {
                return null;
            }
            if (int.TryParse(name, out num))
            {
                return new IssueResolution(null, name);
            }
            return new IssueResolution(name);
        }

        public static bool operator !=(IssueResolution entity, string name)
        {
            if (entity == null)
            {
                return (name != null);
            }
            return ((name == null) || (entity._name != name));
        }
    }
}

