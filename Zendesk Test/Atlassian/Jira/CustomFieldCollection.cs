﻿namespace Atlassian.Jira
{
    using Atlassian.Jira.Remote;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;

    public class CustomFieldCollection : ReadOnlyCollection<CustomField>, IRemoteIssueFieldProvider
    {
        private Func<string, string> _getFieldIdProvider;
        private readonly Issue _issue;

        internal CustomFieldCollection(Issue issue) : this(issue, new List<CustomField>())
        {
        }

        internal CustomFieldCollection(Issue issue, IList<CustomField> list) : base(list)
        {
            this._issue = issue;
            this.ForEdit();
        }

        public CustomFieldCollection Add(string fieldName, string fieldValue)
        {
            this.Add(fieldName, new string[] { fieldValue });
            return this;
        }

        public CustomFieldCollection Add(string fieldName, string[] fieldValues)
        {
            string id = this._getFieldIdProvider(fieldName);
            CustomField item = new CustomField(id, fieldName, this._issue) {
                Values = fieldValues
            };
            base.Items.Add(item);
            return this;
        }

        RemoteFieldValue[] IRemoteIssueFieldProvider.GetRemoteFields()
        {
            return (from f in base.Items select new RemoteFieldValue { id = f.Id, values = f.Values }).ToArray<RemoteFieldValue>();
        }

        public CustomFieldCollection ForAction(string actionId)
        {
            this._getFieldIdProvider = delegate (string name) {
                JiraNamedEntity entity = this._issue.Jira.GetFieldsForAction(this._issue, actionId).FirstOrDefault<JiraNamedEntity>(f => f.Name.Equals(name, StringComparison.OrdinalIgnoreCase));
                if (entity == null)
                {
                    throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, "Could not find custom field with name '{0}' and action with id '{1}' on the JIRA server. ", new object[] { name, actionId }));
                }
                return entity.Id;
            };
            return this;
        }

        public CustomFieldCollection ForEdit()
        {
            this._getFieldIdProvider = delegate (string fieldName) {
                JiraNamedEntity entity = this._issue.Jira.GetFieldsForEdit(this._issue).FirstOrDefault<JiraNamedEntity>(f => f.Name.Equals(fieldName, StringComparison.OrdinalIgnoreCase));
                if (entity == null)
                {
                    throw new InvalidOperationException(string.Format("Could not find custom field with name '{0}' on the JIRA server. Make sure this field is available when editing this issue. For more information see JRA-6857", fieldName));
                }
                return entity.Id;
            };
            return this;
        }

        public CustomField this[string fieldName]
        {
            get
            {
                string fieldId = this._getFieldIdProvider(fieldName);
                return base.Items.FirstOrDefault<CustomField>(f => (f.Id == fieldId));
            }
        }
    }
}

