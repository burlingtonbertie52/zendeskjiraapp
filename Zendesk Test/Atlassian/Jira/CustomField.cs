﻿namespace Atlassian.Jira
{
    using System;
    using System.Linq;
    using System.Runtime.CompilerServices;

    public class CustomField
    {
        private readonly string _id;
        private readonly Issue _issue;
        private string _name;

        internal CustomField(string id, Issue issue)
        {
            this._id = id;
            this._issue = issue;
        }

        internal CustomField(string id, string name, Issue issue) : this(id, issue)
        {
            this._name = name;
        }

        public string Id
        {
            get
            {
                return this._id;
            }
        }

        public string Name
        {
            get
            {
                Func<JiraNamedEntity, bool> predicate = null;
                if (string.IsNullOrEmpty(this._name))
                {
                    if (predicate == null)
                    {
                        predicate = f => f.Id == this._id;
                    }
                    this._name = this._issue.Jira.GetFieldsForEdit(this._issue).First<JiraNamedEntity>(predicate).Name;
                }
                return this._name;
            }
        }

        public string[] Values { get; set; }
    }
}

