﻿namespace Atlassian.Jira
{
    using Atlassian.Jira.Linq;
    using Atlassian.Jira.Remote;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.ServiceModel;
    using Util.DoubleKeyDictionary;

    public class Jira
    {
        private Dictionary<string, IEnumerable<ProjectComponent>> _cachedComponents;
        private IEnumerable<JiraNamedEntity> _cachedCustomFields;
        private Util.DoubleKeyDictionary.DoubleKeyDictionary<string, string, IEnumerable<JiraNamedEntity>> _cachedFieldsForAction;
        private Dictionary<string, IEnumerable<JiraNamedEntity>> _cachedFieldsForEdit;
        private IEnumerable<JiraNamedEntity> _cachedFilters;
        private Dictionary<string, IEnumerable<IssueType>> _cachedIssueTypes;
        private IEnumerable<IssuePriority> _cachedPriorities;
        private IEnumerable<Project> _cachedProjects;
        private IEnumerable<IssueResolution> _cachedResolutions;
        private IEnumerable<IssueStatus> _cachedStatuses;
        private Dictionary<string, IEnumerable<IssueType>> _cachedSubTaskIssueTypes;
        private Dictionary<string, IEnumerable<ProjectVersion>> _cachedVersions;
        private Func<JiraCredentials> _credentialsProvider;
        private readonly IFileSystem _fileSystem;
        private readonly bool _isAnonymous;
        private readonly IJiraSoapServiceClient _jiraSoapService;
        private readonly JiraQueryProvider _provider;
        private string _token;
        private const string ALL_PROJECTS_KEY = "[ALL_PROJECTS]";
        internal const string DEFAULT_DATE_FORMAT = "yyyy/MM/dd";
        private const int DEFAULT_MAX_ISSUES_PER_REQUEST = 20;
        internal static CultureInfo DefaultCultureInfo = CultureInfo.GetCultureInfo("en-us");
        private const string REMOTE_AUTH_EXCEPTION_STRING = "com.atlassian.jira.rpc.exception.RemoteAuthenticationException";

        public Jira(string url) : this(new JqlExpressionVisitor(), new JiraSoapServiceClientWrapper(url), new Atlassian.Jira.FileSystem())
        {
        }

        public Jira(IJqlExpressionVisitor translator, IJiraSoapServiceClient jiraSoapService, IFileSystem fileSystem)
        {
            this._token = string.Empty;
            this._cachedVersions = new Dictionary<string, IEnumerable<ProjectVersion>>();
            this._cachedComponents = new Dictionary<string, IEnumerable<ProjectComponent>>();
            this._cachedIssueTypes = new Dictionary<string, IEnumerable<IssueType>>();
            this._cachedSubTaskIssueTypes = new Dictionary<string, IEnumerable<IssueType>>();
            this._cachedFieldsForEdit = new Dictionary<string, IEnumerable<JiraNamedEntity>>();
            this._cachedFieldsForAction = new Util.DoubleKeyDictionary.DoubleKeyDictionary<string, string, IEnumerable<JiraNamedEntity>>();
            this._isAnonymous = true;
            this._jiraSoapService = jiraSoapService;
            this._fileSystem = fileSystem;
            this.MaxIssuesPerRequest = 20;
            this.Debug = false;
            this._provider = new JiraQueryProvider(translator, this);
        }

        public Jira(string url, string token, Func<JiraCredentials> credentialsProvider = null) : this(new JqlExpressionVisitor(), new JiraSoapServiceClientWrapper(url), new Atlassian.Jira.FileSystem(), token, credentialsProvider)
        {
        }

        public Jira(string url, string username, string password) : this(url, null, func)
        {
            Func<JiraCredentials> func = null;
            if (func == null)
            {
                func = () => new JiraCredentials(username, password);
            }
        }

        public Jira(IJqlExpressionVisitor translator, IJiraSoapServiceClient jiraSoapService, IFileSystem fileSystem, string accessToken, Func<JiraCredentials> credentialsProvider = null) : this(translator, jiraSoapService, fileSystem)
        {
            this._token = accessToken;
            this._credentialsProvider = credentialsProvider;
            this._isAnonymous = false;
        }

        public Issue CreateIssue(string project, string parentIssueKey = null)
        {
            return new Issue(this, project, parentIssueKey);
        }

        public void DeleteIssue(Issue issue)
        {
            if ((issue.Key == null) || string.IsNullOrEmpty(issue.Key.ToString()))
            {
                throw new InvalidOperationException("Unable to delete issue, it has not been created.");
            }
            this.WithToken((Action<string>) (token => this._jiraSoapService.DeleteIssue(token, issue.Key.ToString())));
        }

        public string GetAccessToken()
        {
            JiraCredentials credentials = this.GetCredentials();
            return this._jiraSoapService.Login(credentials.UserName, credentials.Password);
        }

        internal JiraCredentials GetCredentials()
        {
            if (this._credentialsProvider == null)
            {
                throw new InvalidOperationException("Unable to get user and password, credentials provider is not set.");
            }
            JiraCredentials credentials = this._credentialsProvider();
            if (credentials == null)
            {
                throw new InvalidOperationException("Unable to get user and password, credentials provider returned null.");
            }
            return credentials;
        }

        public IEnumerable<JiraNamedEntity> GetCustomFields()
        {
            Action<string> action = null;
            if (this._cachedCustomFields == null)
            {
                if (action == null)
                {
                    action = delegate (string token) {
                        this._cachedCustomFields = from f in this._jiraSoapService.GetCustomFields(token) select new JiraNamedEntity(f);
                    };
                }
                this.WithToken(action);
            }
            return this._cachedCustomFields;
        }

        internal IEnumerable<JiraNamedEntity> GetFieldsForAction(Issue issue, string actionId)
        {
            Action<string, IJiraSoapServiceClient> action = null;
            if (issue.Key == null)
            {
                issue = this.GetOneIssueFromProject(issue.Project);
            }
            if (!this._cachedFieldsForAction.ContainsKey(issue.Project, actionId))
            {
                if (action == null)
                {
                    action = delegate (string token, IJiraSoapServiceClient service) {
                        this._cachedFieldsForAction.Add(issue.Project, actionId, from f in this._jiraSoapService.GetFieldsForAction(token, issue.Key.Value, actionId) select new JiraNamedEntity(f));
                    };
                }
                this.WithToken(action);
            }
            return this._cachedFieldsForAction[issue.Project, actionId];
        }

        internal IEnumerable<JiraNamedEntity> GetFieldsForEdit(Issue issue)
        {
            Action<string> action = null;
            if (issue.Key == null)
            {
                issue = this.GetOneIssueFromProject(issue.Project);
            }
            if (!this._cachedFieldsForEdit.ContainsKey(issue.Project))
            {
                if (action == null)
                {
                    action = delegate (string token) {
                        this._cachedFieldsForEdit.Add(issue.Project, from f in this._jiraSoapService.GetFieldsForEdit(token, issue.Key.Value) select new JiraNamedEntity(f));
                    };
                }
                this.WithToken(action);
            }
            return this._cachedFieldsForEdit[issue.Project];
        }

        public IEnumerable<JiraNamedEntity> GetFilters()
        {
            Action<string> action = null;
            if (this._cachedFilters == null)
            {
                if (action == null)
                {
                    action = delegate (string token) {
                        this._cachedFilters = from f in this._jiraSoapService.GetFavouriteFilters(token) select new JiraNamedEntity(f);
                    };
                }
                this.WithToken(action);
            }
            return this._cachedFilters;
        }

        public Issue GetIssue(string key)
        {
            return (from i in this.Issues
                where i.Key == key
                select i).First<Issue>();
        }

        public IEnumerable<IssuePriority> GetIssuePriorities()
        {
            Action<string> action = null;
            if (this._cachedPriorities == null)
            {
                if (action == null)
                {
                    action = delegate (string token) {
                        this._cachedPriorities = from p in this._jiraSoapService.GetPriorities(token) select new IssuePriority(p);
                    };
                }
                this.WithToken(action);
            }
            return this._cachedPriorities;
        }

        public IEnumerable<IssueResolution> GetIssueResolutions()
        {
            Action<string> action = null;
            if (this._cachedResolutions == null)
            {
                if (action == null)
                {
                    action = delegate (string token) {
                        this._cachedResolutions = from r in this._jiraSoapService.GetResolutions(token) select new IssueResolution(r);
                    };
                }
                this.WithToken(action);
            }
            return this._cachedResolutions;
        }

        public IEnumerable<Issue> GetIssuesFromFilter(string filterName, int start = 0, int? maxResults = new int?())
        {
            JiraNamedEntity filter = this.GetFilters().FirstOrDefault<JiraNamedEntity>(f => f.Name.Equals(filterName, StringComparison.OrdinalIgnoreCase));
            if (filter == null)
            {
                throw new InvalidOperationException(string.Format("Filter with name '{0}' was not found", filterName));
            }
            return this.WithToken<IEnumerable<Issue>>(delegate (string token) {
                int? nullable1 = maxResults;
                return from i in this._jiraSoapService.GetIssuesFromFilterWithLimit(token, filter.Id, start, nullable1.HasValue ? nullable1.GetValueOrDefault() : this.MaxIssuesPerRequest) select new Issue(this, i, null);
            });
        }

        public IEnumerable<Issue> GetIssuesFromJql(string jql)
        {
            return this.GetIssuesFromJql(jql, null);
        }

        public IEnumerable<Issue> GetIssuesFromJql(string jql, int? maxIssues)
        {
            if (this.Debug)
            {
                Console.WriteLine("JQL: " + jql);
            }
            IList<Issue> issues = new List<Issue>();
            this.WithToken(delegate (string t) {
                int? nullable1 = maxIssues;
                foreach (RemoteIssue issue in this._jiraSoapService.GetIssuesFromJqlSearch(t, jql, nullable1.HasValue ? nullable1.GetValueOrDefault() : this.MaxIssuesPerRequest))
                {
                    issues.Add(new Issue(this, issue, null));
                }
            });
            return issues;
        }

        public IEnumerable<IssueStatus> GetIssueStatuses()
        {
            Action<string> action = null;
            if (this._cachedStatuses == null)
            {
                if (action == null)
                {
                    action = delegate (string token) {
                        this._cachedStatuses = from s in this._jiraSoapService.GetStatuses(token) select new IssueStatus(s);
                    };
                }
                this.WithToken(action);
            }
            return this._cachedStatuses;
        }

        public IEnumerable<IssueType> GetIssueTypes(string projectKey = null)
        {
            Func<Project, bool> predicate = null;
            Action<string> action = null;
            string projectId = null;
            if (projectKey != null)
            {
                if (predicate == null)
                {
                    predicate = p => p.Key.Equals(projectKey, StringComparison.OrdinalIgnoreCase);
                }
                Project project = this.GetProjects().FirstOrDefault<Project>(predicate);
                if (project != null)
                {
                    projectId = project.Id;
                }
            }
            projectKey = projectKey ?? "[ALL_PROJECTS]";
            if (!this._cachedIssueTypes.ContainsKey(projectKey))
            {
                if (action == null)
                {
                    action = delegate (string token) {
                        this._cachedIssueTypes.Add(projectKey, from remoteIssueType in this._jiraSoapService.GetIssueTypes(token, projectId) select new IssueType(remoteIssueType));
                    };
                }
                this.WithToken(action);
            }
            return this._cachedIssueTypes[projectKey];
        }

        private Issue GetOneIssueFromProject(string projectKey)
        {
            Issue issue = this.GetIssuesFromJql(string.Format("project = \"{0}\"", projectKey), 1).FirstOrDefault<Issue>();
            if (issue == null)
            {
                throw new InvalidOperationException("Project must contain at least one issue to be able to retrieve issue fields.");
            }
            return issue;
        }

        public IEnumerable<ProjectComponent> GetProjectComponents(string projectKey)
        {
            Action<string> action = null;
            if (!this._cachedComponents.ContainsKey(projectKey))
            {
                if (action == null)
                {
                    action = delegate (string token) {
                        this._cachedComponents.Add(projectKey, from c in this._jiraSoapService.GetComponents(token, projectKey) select new ProjectComponent(c));
                    };
                }
                this.WithToken(action);
            }
            return this._cachedComponents[projectKey];
        }

        public IEnumerable<Project> GetProjects()
        {
            Action<string> action = null;
            if (this._cachedProjects == null)
            {
                if (action == null)
                {
                    action = delegate (string token) {
                        this._cachedProjects = from p in this._jiraSoapService.GetProjects(token) select new Project(p);
                    };
                }
                this.WithToken(action);
            }
            return this._cachedProjects;
        }

        public IEnumerable<ProjectVersion> GetProjectVersions(string projectKey)
        {
            Action<string> action = null;
            if (!this._cachedVersions.ContainsKey(projectKey))
            {
                if (action == null)
                {
                    action = delegate (string token) {
                        this._cachedVersions.Add(projectKey, from v in this._jiraSoapService.GetVersions(token, projectKey) select new ProjectVersion(v));
                    };
                }
                this.WithToken(action);
            }
            return this._cachedVersions[projectKey];
        }

        public IEnumerable<IssueType> GetSubTaskIssueTypes()
        {
            Action<string> action = null;
            if (!this._cachedSubTaskIssueTypes.ContainsKey("[ALL_PROJECTS]"))
            {
                if (action == null)
                {
                    action = delegate (string token) {
                        this._cachedSubTaskIssueTypes.Add("[ALL_PROJECTS]", from remoteIssueType in this._jiraSoapService.GetSubTaskIssueTypes(token) select new IssueType(remoteIssueType));
                    };
                }
                this.WithToken(action);
            }
            return this._cachedSubTaskIssueTypes["[ALL_PROJECTS]"];
        }

        public void WithToken(Action<string> action)
        {
            this.WithToken<object>(delegate (string t) {
                action(t);
                return null;
            });
        }

        public void WithToken(Action<string, IJiraSoapServiceClient> action)
        {
            this.WithToken<object>(delegate (string token, IJiraSoapServiceClient client) {
                action(token, client);
                return null;
            });
        }

        public TResult WithToken<TResult>(Func<string, TResult> function)
        {
            return this.WithToken<TResult>((Func<string, IJiraSoapServiceClient, TResult>) ((token, client) => function(token)));
        }

        public TResult WithToken<TResult>(Func<string, IJiraSoapServiceClient, TResult> function)
        {
            if (!this._isAnonymous && string.IsNullOrEmpty(this._token))
            {
                this._token = this.GetAccessToken();
            }
            try
            {
                return function(this._token, this.RemoteSoapService);
            }
            catch (FaultException exception)
            {
                if (this._isAnonymous || (exception.Message.IndexOf("com.atlassian.jira.rpc.exception.RemoteAuthenticationException", StringComparison.OrdinalIgnoreCase) < 0))
                {
                    throw;
                }
                this._token = this.GetAccessToken();
                return function(this._token, this.RemoteSoapService);
            }
        }

        public bool Debug { get; set; }

        internal IFileSystem FileSystem
        {
            get
            {
                return this._fileSystem;
            }
        }

        public JiraQueryable<Issue> Issues
        {
            get
            {
                return new JiraQueryable<Issue>(this._provider);
            }
        }

        public int MaxIssuesPerRequest { get; set; }

        internal IJiraSoapServiceClient RemoteSoapService
        {
            get
            {
                return this._jiraSoapService;
            }
        }

        public string Url
        {
            get
            {
                return this._jiraSoapService.Url;
            }
        }
    }
}

