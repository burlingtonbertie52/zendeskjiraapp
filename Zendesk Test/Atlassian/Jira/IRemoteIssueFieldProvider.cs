﻿namespace Atlassian.Jira
{
    using Atlassian.Jira.Remote;

    public interface IRemoteIssueFieldProvider
    {
        RemoteFieldValue[] GetRemoteFields();
    }
}

