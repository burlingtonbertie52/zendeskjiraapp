﻿namespace Atlassian.Jira
{
    using System;

    public class LiteralMatch
    {
        private readonly string _value;

        public LiteralMatch(string value)
        {
            this._value = value;
        }

        public static bool operator ==(ComparableString comparable, LiteralMatch literal)
        {
            if (comparable == null)
            {
                return (literal == null);
            }
            return (comparable.Value == literal._value);
        }

        public static bool operator !=(ComparableString comparable, LiteralMatch literal)
        {
            if (comparable == null)
            {
                return (literal != null);
            }
            return (comparable.Value != literal._value);
        }

        public override string ToString()
        {
            return this._value;
        }
    }
}

