﻿namespace Atlassian.Jira
{
    using Atlassian.Jira.Remote;
    using System;
    using System.Runtime.CompilerServices;

    public class JiraConstant : JiraNamedEntity
    {
        internal JiraConstant(AbstractRemoteConstant remoteConstant) : base(remoteConstant)
        {
            this.Description = remoteConstant.description;
        }

        public string Description { get; set; }
    }
}

