﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [MessageContract(WrapperName="getUserResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class getUserResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteUser getUserReturn;

        public getUserResponse()
        {
        }

        public getUserResponse(RemoteUser getUserReturn)
        {
            this.getUserReturn = getUserReturn;
        }
    }
}

