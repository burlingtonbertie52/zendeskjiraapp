﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [GeneratedCode("System.ServiceModel", "3.0.0.0"), DebuggerStepThrough, MessageContract(WrapperName="getStatusesResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true)]
    public class getStatusesResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteStatus[] getStatusesReturn;

        public getStatusesResponse()
        {
        }

        public getStatusesResponse(RemoteStatus[] getStatusesReturn)
        {
            this.getStatusesReturn = getStatusesReturn;
        }
    }
}

