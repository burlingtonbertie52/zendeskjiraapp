﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [GeneratedCode("System.ServiceModel", "3.0.0.0"), DebuggerStepThrough, MessageContract(WrapperName="createIssueResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true)]
    public class createIssueResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteIssue createIssueReturn;

        public createIssueResponse()
        {
        }

        public createIssueResponse(RemoteIssue createIssueReturn)
        {
            this.createIssueReturn = createIssueReturn;
        }
    }
}

