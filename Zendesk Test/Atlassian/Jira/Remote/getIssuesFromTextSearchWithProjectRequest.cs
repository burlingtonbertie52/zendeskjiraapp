﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, MessageContract(WrapperName="getIssuesFromTextSearchWithProject", WrapperNamespace="http://soap.rpc.jira.atlassian.com", IsWrapped=true), GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class getIssuesFromTextSearchWithProjectRequest
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public string in0;
        [MessageBodyMember(Namespace="", Order=1)]
        public string[] in1;
        [MessageBodyMember(Namespace="", Order=2)]
        public string in2;
        [MessageBodyMember(Namespace="", Order=3)]
        public int in3;

        public getIssuesFromTextSearchWithProjectRequest()
        {
        }

        public getIssuesFromTextSearchWithProjectRequest(string in0, string[] in1, string in2, int in3)
        {
            this.in0 = in0;
            this.in1 = in1;
            this.in2 = in2;
            this.in3 = in3;
        }
    }
}

