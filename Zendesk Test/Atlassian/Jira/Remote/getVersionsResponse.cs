﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [GeneratedCode("System.ServiceModel", "3.0.0.0"), DebuggerStepThrough, MessageContract(WrapperName="getVersionsResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true)]
    public class getVersionsResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteVersion[] getVersionsReturn;

        public getVersionsResponse()
        {
        }

        public getVersionsResponse(RemoteVersion[] getVersionsReturn)
        {
            this.getVersionsReturn = getVersionsReturn;
        }
    }
}

