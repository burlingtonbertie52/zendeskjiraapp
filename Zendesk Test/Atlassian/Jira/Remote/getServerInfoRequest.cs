﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="getServerInfo", WrapperNamespace="http://soap.rpc.jira.atlassian.com", IsWrapped=true)]
    public class getServerInfoRequest
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public string in0;

        public getServerInfoRequest()
        {
        }

        public getServerInfoRequest(string in0)
        {
            this.in0 = in0;
        }
    }
}

