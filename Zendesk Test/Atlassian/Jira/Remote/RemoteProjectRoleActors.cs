﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Xml.Serialization;

    [Serializable, DebuggerStepThrough, DesignerCategory("code"), GeneratedCode("svcutil", "3.0.4506.2152"), SoapType(Namespace="http://beans.soap.rpc.jira.atlassian.com")]
    public class RemoteProjectRoleActors : RemoteRoleActors
    {
        private RemoteProject projectField;

        [SoapElement(IsNullable=true)]
        public RemoteProject project
        {
            get
            {
                return this.projectField;
            }
            set
            {
                this.projectField = value;
            }
        }
    }
}

