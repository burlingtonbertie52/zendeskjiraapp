﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="createPermissionSchemeResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), DebuggerStepThrough]
    public class createPermissionSchemeResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemotePermissionScheme createPermissionSchemeReturn;

        public createPermissionSchemeResponse()
        {
        }

        public createPermissionSchemeResponse(RemotePermissionScheme createPermissionSchemeReturn)
        {
            this.createPermissionSchemeReturn = createPermissionSchemeReturn;
        }
    }
}

