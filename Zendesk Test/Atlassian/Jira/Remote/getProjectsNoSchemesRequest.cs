﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, MessageContract(WrapperName="getProjectsNoSchemes", WrapperNamespace="http://soap.rpc.jira.atlassian.com", IsWrapped=true), GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class getProjectsNoSchemesRequest
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public string in0;

        public getProjectsNoSchemesRequest()
        {
        }

        public getProjectsNoSchemesRequest(string in0)
        {
            this.in0 = in0;
        }
    }
}

