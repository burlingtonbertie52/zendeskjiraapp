﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [GeneratedCode("System.ServiceModel", "3.0.0.0"), DebuggerStepThrough, MessageContract(WrapperName="getConfiguration", WrapperNamespace="http://soap.rpc.jira.atlassian.com", IsWrapped=true)]
    public class getConfigurationRequest
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public string in0;

        public getConfigurationRequest()
        {
        }

        public getConfigurationRequest(string in0)
        {
            this.in0 = in0;
        }
    }
}

