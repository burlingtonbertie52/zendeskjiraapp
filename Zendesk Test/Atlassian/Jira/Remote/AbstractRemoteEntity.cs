﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Xml.Serialization;

    [Serializable, SoapInclude(typeof(RemoteComponent)), SoapInclude(typeof(RemoteStatus)), SoapType(Namespace="http://beans.soap.rpc.jira.atlassian.com"), SoapInclude(typeof(RemoteIssue)), SoapInclude(typeof(AbstractNamedRemoteEntity)), SoapInclude(typeof(RemoteSecurityLevel)), SoapInclude(typeof(RemoteFilter)), SoapInclude(typeof(RemoteField)), SoapInclude(typeof(RemoteProject)), SoapInclude(typeof(AbstractRemoteConstant)), SoapInclude(typeof(RemoteAttachment)), SoapInclude(typeof(RemoteResolution)), SoapInclude(typeof(RemotePriority)), SoapInclude(typeof(RemoteIssueType)), SoapInclude(typeof(RemoteNamedObject)), SoapInclude(typeof(RemoteVersion)), GeneratedCode("svcutil", "3.0.4506.2152"), DebuggerStepThrough, DesignerCategory("code")]
    public abstract class AbstractRemoteEntity
    {
        private string idField;

        protected AbstractRemoteEntity()
        {
        }

        [SoapElement(IsNullable=true)]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }
}

