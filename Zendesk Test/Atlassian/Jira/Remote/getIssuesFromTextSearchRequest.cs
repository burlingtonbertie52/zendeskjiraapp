﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [MessageContract(WrapperName="getIssuesFromTextSearch", WrapperNamespace="http://soap.rpc.jira.atlassian.com", IsWrapped=true), DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class getIssuesFromTextSearchRequest
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public string in0;
        [MessageBodyMember(Namespace="", Order=1)]
        public string in1;

        public getIssuesFromTextSearchRequest()
        {
        }

        public getIssuesFromTextSearchRequest(string in0, string in1)
        {
            this.in0 = in0;
            this.in1 = in1;
        }
    }
}

