﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [MessageContract(WrapperName="getProjectRoleActors", WrapperNamespace="http://soap.rpc.jira.atlassian.com", IsWrapped=true), DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class getProjectRoleActorsRequest
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public string in0;
        [MessageBodyMember(Namespace="", Order=1)]
        public RemoteProjectRole in1;
        [MessageBodyMember(Namespace="", Order=2)]
        public RemoteProject in2;

        public getProjectRoleActorsRequest()
        {
        }

        public getProjectRoleActorsRequest(string in0, RemoteProjectRole in1, RemoteProject in2)
        {
            this.in0 = in0;
            this.in1 = in1;
            this.in2 = in2;
        }
    }
}

