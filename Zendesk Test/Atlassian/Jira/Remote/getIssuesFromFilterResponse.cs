﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [GeneratedCode("System.ServiceModel", "3.0.0.0"), DebuggerStepThrough, MessageContract(WrapperName="getIssuesFromFilterResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true)]
    public class getIssuesFromFilterResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteIssue[] getIssuesFromFilterReturn;

        public getIssuesFromFilterResponse()
        {
        }

        public getIssuesFromFilterResponse(RemoteIssue[] getIssuesFromFilterReturn)
        {
            this.getIssuesFromFilterReturn = getIssuesFromFilterReturn;
        }
    }
}

