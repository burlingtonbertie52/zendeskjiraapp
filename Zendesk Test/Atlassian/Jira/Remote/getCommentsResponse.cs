﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [MessageContract(WrapperName="getCommentsResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class getCommentsResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteComment[] getCommentsReturn;

        public getCommentsResponse()
        {
        }

        public getCommentsResponse(RemoteComment[] getCommentsReturn)
        {
            this.getCommentsReturn = getCommentsReturn;
        }
    }
}

