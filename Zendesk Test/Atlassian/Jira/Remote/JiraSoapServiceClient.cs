﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.ServiceModel;
    using System.ServiceModel.Channels;

    [GeneratedCode("System.ServiceModel", "3.0.0.0"), DebuggerStepThrough]
    public class JiraSoapServiceClient : ClientBase<JiraSoapService>, JiraSoapService
    {
        public JiraSoapServiceClient()
        {
        }

        public JiraSoapServiceClient(string endpointConfigurationName) : base(endpointConfigurationName)
        {
        }

        public JiraSoapServiceClient(Binding binding, EndpointAddress remoteAddress) : base(binding, remoteAddress)
        {
        }

        public JiraSoapServiceClient(string endpointConfigurationName, EndpointAddress remoteAddress) : base(endpointConfigurationName, remoteAddress)
        {
        }

        public JiraSoapServiceClient(string endpointConfigurationName, string remoteAddress) : base(endpointConfigurationName, remoteAddress)
        {
        }

        public void addActorsToProjectRole(string in0, string[] in1, RemoteProjectRole in2, RemoteProject in3, string in4)
        {
            addActorsToProjectRoleRequest request = new addActorsToProjectRoleRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2,
                in3 = in3,
                in4 = in4
            };
            ((JiraSoapService) this).addActorsToProjectRole(request);
        }

        public bool addAttachmentsToIssue(string in0, string in1, string[] in2, sbyte[] in3)
        {
            addAttachmentsToIssueRequest request = new addAttachmentsToIssueRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2,
                in3 = in3
            };
            return ((JiraSoapService) this).addAttachmentsToIssue(request).addAttachmentsToIssueReturn;
        }

        public bool addBase64EncodedAttachmentsToIssue(string in0, string in1, string[] in2, string[] in3)
        {
            addBase64EncodedAttachmentsToIssueRequest request = new addBase64EncodedAttachmentsToIssueRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2,
                in3 = in3
            };
            return ((JiraSoapService) this).addBase64EncodedAttachmentsToIssue(request).addBase64EncodedAttachmentsToIssueReturn;
        }

        public void addComment(string in0, string in1, RemoteComment in2)
        {
            addCommentRequest request = new addCommentRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2
            };
            ((JiraSoapService) this).addComment(request);
        }

        public void addDefaultActorsToProjectRole(string in0, string[] in1, RemoteProjectRole in2, string in3)
        {
            addDefaultActorsToProjectRoleRequest request = new addDefaultActorsToProjectRoleRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2,
                in3 = in3
            };
            ((JiraSoapService) this).addDefaultActorsToProjectRole(request);
        }

        public RemotePermissionScheme addPermissionTo(string in0, RemotePermissionScheme in1, RemotePermission in2, RemoteEntity in3)
        {
            addPermissionToRequest request = new addPermissionToRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2,
                in3 = in3
            };
            return ((JiraSoapService) this).addPermissionTo(request).addPermissionToReturn;
        }

        public void addUserToGroup(string in0, RemoteGroup in1, RemoteUser in2)
        {
            addUserToGroupRequest request = new addUserToGroupRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2
            };
            ((JiraSoapService) this).addUserToGroup(request);
        }

        public RemoteVersion addVersion(string in0, string in1, RemoteVersion in2)
        {
            addVersionRequest request = new addVersionRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2
            };
            return ((JiraSoapService) this).addVersion(request).addVersionReturn;
        }

        public RemoteWorklog addWorklogAndAutoAdjustRemainingEstimate(string in0, string in1, RemoteWorklog in2)
        {
            addWorklogAndAutoAdjustRemainingEstimateRequest request = new addWorklogAndAutoAdjustRemainingEstimateRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2
            };
            return ((JiraSoapService) this).addWorklogAndAutoAdjustRemainingEstimate(request).addWorklogAndAutoAdjustRemainingEstimateReturn;
        }

        public RemoteWorklog addWorklogAndRetainRemainingEstimate(string in0, string in1, RemoteWorklog in2)
        {
            addWorklogAndRetainRemainingEstimateRequest request = new addWorklogAndRetainRemainingEstimateRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2
            };
            return ((JiraSoapService) this).addWorklogAndRetainRemainingEstimate(request).addWorklogAndRetainRemainingEstimateReturn;
        }

        public RemoteWorklog addWorklogWithNewRemainingEstimate(string in0, string in1, RemoteWorklog in2, string in3)
        {
            addWorklogWithNewRemainingEstimateRequest request = new addWorklogWithNewRemainingEstimateRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2,
                in3 = in3
            };
            return ((JiraSoapService) this).addWorklogWithNewRemainingEstimate(request).addWorklogWithNewRemainingEstimateReturn;
        }

        public void archiveVersion(string in0, string in1, string in2, bool in3)
        {
            archiveVersionRequest request = new archiveVersionRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2,
                in3 = in3
            };
            ((JiraSoapService) this).archiveVersion(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        addActorsToProjectRoleResponse JiraSoapService.addActorsToProjectRole(addActorsToProjectRoleRequest request)
        {
            return base.Channel.addActorsToProjectRole(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        addAttachmentsToIssueResponse JiraSoapService.addAttachmentsToIssue(addAttachmentsToIssueRequest request)
        {
            return base.Channel.addAttachmentsToIssue(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        addBase64EncodedAttachmentsToIssueResponse JiraSoapService.addBase64EncodedAttachmentsToIssue(addBase64EncodedAttachmentsToIssueRequest request)
        {
            return base.Channel.addBase64EncodedAttachmentsToIssue(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        addCommentResponse JiraSoapService.addComment(addCommentRequest request)
        {
            return base.Channel.addComment(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        addDefaultActorsToProjectRoleResponse JiraSoapService.addDefaultActorsToProjectRole(addDefaultActorsToProjectRoleRequest request)
        {
            return base.Channel.addDefaultActorsToProjectRole(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        addPermissionToResponse JiraSoapService.addPermissionTo(addPermissionToRequest request)
        {
            return base.Channel.addPermissionTo(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        addUserToGroupResponse JiraSoapService.addUserToGroup(addUserToGroupRequest request)
        {
            return base.Channel.addUserToGroup(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        addVersionResponse JiraSoapService.addVersion(addVersionRequest request)
        {
            return base.Channel.addVersion(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        addWorklogAndAutoAdjustRemainingEstimateResponse JiraSoapService.addWorklogAndAutoAdjustRemainingEstimate(addWorklogAndAutoAdjustRemainingEstimateRequest request)
        {
            return base.Channel.addWorklogAndAutoAdjustRemainingEstimate(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        addWorklogAndRetainRemainingEstimateResponse JiraSoapService.addWorklogAndRetainRemainingEstimate(addWorklogAndRetainRemainingEstimateRequest request)
        {
            return base.Channel.addWorklogAndRetainRemainingEstimate(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        addWorklogWithNewRemainingEstimateResponse JiraSoapService.addWorklogWithNewRemainingEstimate(addWorklogWithNewRemainingEstimateRequest request)
        {
            return base.Channel.addWorklogWithNewRemainingEstimate(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        archiveVersionResponse JiraSoapService.archiveVersion(archiveVersionRequest request)
        {
            return base.Channel.archiveVersion(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        createGroupResponse JiraSoapService.createGroup(createGroupRequest request)
        {
            return base.Channel.createGroup(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        createIssueResponse JiraSoapService.createIssue(createIssueRequest request)
        {
            return base.Channel.createIssue(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        createIssueWithParentResponse JiraSoapService.createIssueWithParent(createIssueWithParentRequest request)
        {
            return base.Channel.createIssueWithParent(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        createIssueWithSecurityLevelResponse JiraSoapService.createIssueWithSecurityLevel(createIssueWithSecurityLevelRequest request)
        {
            return base.Channel.createIssueWithSecurityLevel(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        createPermissionSchemeResponse JiraSoapService.createPermissionScheme(createPermissionSchemeRequest request)
        {
            return base.Channel.createPermissionScheme(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        createProjectResponse JiraSoapService.createProject(createProjectRequest request)
        {
            return base.Channel.createProject(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        createProjectFromObjectResponse JiraSoapService.createProjectFromObject(createProjectFromObjectRequest request)
        {
            return base.Channel.createProjectFromObject(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        createProjectRoleResponse JiraSoapService.createProjectRole(createProjectRoleRequest request)
        {
            return base.Channel.createProjectRole(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        createUserResponse JiraSoapService.createUser(createUserRequest request)
        {
            return base.Channel.createUser(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        deleteGroupResponse JiraSoapService.deleteGroup(deleteGroupRequest request)
        {
            return base.Channel.deleteGroup(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        deleteIssueResponse JiraSoapService.deleteIssue(deleteIssueRequest request)
        {
            return base.Channel.deleteIssue(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        deletePermissionFromResponse JiraSoapService.deletePermissionFrom(deletePermissionFromRequest request)
        {
            return base.Channel.deletePermissionFrom(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        deletePermissionSchemeResponse JiraSoapService.deletePermissionScheme(deletePermissionSchemeRequest request)
        {
            return base.Channel.deletePermissionScheme(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        deleteProjectResponse JiraSoapService.deleteProject(deleteProjectRequest request)
        {
            return base.Channel.deleteProject(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        deleteProjectAvatarResponse JiraSoapService.deleteProjectAvatar(deleteProjectAvatarRequest request)
        {
            return base.Channel.deleteProjectAvatar(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        deleteProjectRoleResponse JiraSoapService.deleteProjectRole(deleteProjectRoleRequest request)
        {
            return base.Channel.deleteProjectRole(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        deleteUserResponse JiraSoapService.deleteUser(deleteUserRequest request)
        {
            return base.Channel.deleteUser(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        deleteWorklogAndAutoAdjustRemainingEstimateResponse JiraSoapService.deleteWorklogAndAutoAdjustRemainingEstimate(deleteWorklogAndAutoAdjustRemainingEstimateRequest request)
        {
            return base.Channel.deleteWorklogAndAutoAdjustRemainingEstimate(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        deleteWorklogAndRetainRemainingEstimateResponse JiraSoapService.deleteWorklogAndRetainRemainingEstimate(deleteWorklogAndRetainRemainingEstimateRequest request)
        {
            return base.Channel.deleteWorklogAndRetainRemainingEstimate(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        deleteWorklogWithNewRemainingEstimateResponse JiraSoapService.deleteWorklogWithNewRemainingEstimate(deleteWorklogWithNewRemainingEstimateRequest request)
        {
            return base.Channel.deleteWorklogWithNewRemainingEstimate(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        editCommentResponse JiraSoapService.editComment(editCommentRequest request)
        {
            return base.Channel.editComment(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getAllPermissionsResponse JiraSoapService.getAllPermissions(getAllPermissionsRequest request)
        {
            return base.Channel.getAllPermissions(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getAssociatedNotificationSchemesResponse JiraSoapService.getAssociatedNotificationSchemes(getAssociatedNotificationSchemesRequest request)
        {
            return base.Channel.getAssociatedNotificationSchemes(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getAssociatedPermissionSchemesResponse JiraSoapService.getAssociatedPermissionSchemes(getAssociatedPermissionSchemesRequest request)
        {
            return base.Channel.getAssociatedPermissionSchemes(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getAttachmentsFromIssueResponse JiraSoapService.getAttachmentsFromIssue(getAttachmentsFromIssueRequest request)
        {
            return base.Channel.getAttachmentsFromIssue(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getAvailableActionsResponse JiraSoapService.getAvailableActions(getAvailableActionsRequest request)
        {
            return base.Channel.getAvailableActions(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getCommentResponse JiraSoapService.getComment(getCommentRequest request)
        {
            return base.Channel.getComment(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getCommentsResponse JiraSoapService.getComments(getCommentsRequest request)
        {
            return base.Channel.getComments(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getComponentsResponse JiraSoapService.getComponents(getComponentsRequest request)
        {
            return base.Channel.getComponents(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getConfigurationResponse JiraSoapService.getConfiguration(getConfigurationRequest request)
        {
            return base.Channel.getConfiguration(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getCustomFieldsResponse JiraSoapService.getCustomFields(getCustomFieldsRequest request)
        {
            return base.Channel.getCustomFields(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getDefaultRoleActorsResponse JiraSoapService.getDefaultRoleActors(getDefaultRoleActorsRequest request)
        {
            return base.Channel.getDefaultRoleActors(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getFavouriteFiltersResponse JiraSoapService.getFavouriteFilters(getFavouriteFiltersRequest request)
        {
            return base.Channel.getFavouriteFilters(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getFieldsForActionResponse JiraSoapService.getFieldsForAction(getFieldsForActionRequest request)
        {
            return base.Channel.getFieldsForAction(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getFieldsForEditResponse JiraSoapService.getFieldsForEdit(getFieldsForEditRequest request)
        {
            return base.Channel.getFieldsForEdit(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getGroupResponse JiraSoapService.getGroup(getGroupRequest request)
        {
            return base.Channel.getGroup(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getIssueResponse JiraSoapService.getIssue(getIssueRequest request)
        {
            return base.Channel.getIssue(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getIssueByIdResponse JiraSoapService.getIssueById(getIssueByIdRequest request)
        {
            return base.Channel.getIssueById(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getIssueCountForFilterResponse JiraSoapService.getIssueCountForFilter(getIssueCountForFilterRequest request)
        {
            return base.Channel.getIssueCountForFilter(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getIssuesFromFilterResponse JiraSoapService.getIssuesFromFilter(getIssuesFromFilterRequest request)
        {
            return base.Channel.getIssuesFromFilter(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getIssuesFromFilterWithLimitResponse JiraSoapService.getIssuesFromFilterWithLimit(getIssuesFromFilterWithLimitRequest request)
        {
            return base.Channel.getIssuesFromFilterWithLimit(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getIssuesFromJqlSearchResponse JiraSoapService.getIssuesFromJqlSearch(getIssuesFromJqlSearchRequest request)
        {
            return base.Channel.getIssuesFromJqlSearch(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getIssuesFromTextSearchResponse JiraSoapService.getIssuesFromTextSearch(getIssuesFromTextSearchRequest request)
        {
            return base.Channel.getIssuesFromTextSearch(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getIssuesFromTextSearchWithLimitResponse JiraSoapService.getIssuesFromTextSearchWithLimit(getIssuesFromTextSearchWithLimitRequest request)
        {
            return base.Channel.getIssuesFromTextSearchWithLimit(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getIssuesFromTextSearchWithProjectResponse JiraSoapService.getIssuesFromTextSearchWithProject(getIssuesFromTextSearchWithProjectRequest request)
        {
            return base.Channel.getIssuesFromTextSearchWithProject(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getIssueTypesResponse JiraSoapService.getIssueTypes(getIssueTypesRequest request)
        {
            return base.Channel.getIssueTypes(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getIssueTypesForProjectResponse JiraSoapService.getIssueTypesForProject(getIssueTypesForProjectRequest request)
        {
            return base.Channel.getIssueTypesForProject(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getNotificationSchemesResponse JiraSoapService.getNotificationSchemes(getNotificationSchemesRequest request)
        {
            return base.Channel.getNotificationSchemes(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getPermissionSchemesResponse JiraSoapService.getPermissionSchemes(getPermissionSchemesRequest request)
        {
            return base.Channel.getPermissionSchemes(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getPrioritiesResponse JiraSoapService.getPriorities(getPrioritiesRequest request)
        {
            return base.Channel.getPriorities(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getProjectAvatarResponse JiraSoapService.getProjectAvatar(getProjectAvatarRequest request)
        {
            return base.Channel.getProjectAvatar(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getProjectAvatarsResponse JiraSoapService.getProjectAvatars(getProjectAvatarsRequest request)
        {
            return base.Channel.getProjectAvatars(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getProjectByIdResponse JiraSoapService.getProjectById(getProjectByIdRequest request)
        {
            return base.Channel.getProjectById(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getProjectByKeyResponse JiraSoapService.getProjectByKey(getProjectByKeyRequest request)
        {
            return base.Channel.getProjectByKey(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getProjectRoleResponse JiraSoapService.getProjectRole(getProjectRoleRequest request)
        {
            return base.Channel.getProjectRole(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getProjectRoleActorsResponse JiraSoapService.getProjectRoleActors(getProjectRoleActorsRequest request)
        {
            return base.Channel.getProjectRoleActors(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getProjectRolesResponse JiraSoapService.getProjectRoles(getProjectRolesRequest request)
        {
            return base.Channel.getProjectRoles(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getProjectsNoSchemesResponse JiraSoapService.getProjectsNoSchemes(getProjectsNoSchemesRequest request)
        {
            return base.Channel.getProjectsNoSchemes(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getProjectWithSchemesByIdResponse JiraSoapService.getProjectWithSchemesById(getProjectWithSchemesByIdRequest request)
        {
            return base.Channel.getProjectWithSchemesById(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getResolutionDateByIdResponse JiraSoapService.getResolutionDateById(getResolutionDateByIdRequest request)
        {
            return base.Channel.getResolutionDateById(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getResolutionDateByKeyResponse JiraSoapService.getResolutionDateByKey(getResolutionDateByKeyRequest request)
        {
            return base.Channel.getResolutionDateByKey(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getResolutionsResponse JiraSoapService.getResolutions(getResolutionsRequest request)
        {
            return base.Channel.getResolutions(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getSavedFiltersResponse JiraSoapService.getSavedFilters(getSavedFiltersRequest request)
        {
            return base.Channel.getSavedFilters(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getSecurityLevelResponse JiraSoapService.getSecurityLevel(getSecurityLevelRequest request)
        {
            return base.Channel.getSecurityLevel(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getSecurityLevelsResponse JiraSoapService.getSecurityLevels(getSecurityLevelsRequest request)
        {
            return base.Channel.getSecurityLevels(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getSecuritySchemesResponse JiraSoapService.getSecuritySchemes(getSecuritySchemesRequest request)
        {
            return base.Channel.getSecuritySchemes(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getServerInfoResponse JiraSoapService.getServerInfo(getServerInfoRequest request)
        {
            return base.Channel.getServerInfo(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getStatusesResponse JiraSoapService.getStatuses(getStatusesRequest request)
        {
            return base.Channel.getStatuses(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getSubTaskIssueTypesResponse JiraSoapService.getSubTaskIssueTypes(getSubTaskIssueTypesRequest request)
        {
            return base.Channel.getSubTaskIssueTypes(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getSubTaskIssueTypesForProjectResponse JiraSoapService.getSubTaskIssueTypesForProject(getSubTaskIssueTypesForProjectRequest request)
        {
            return base.Channel.getSubTaskIssueTypesForProject(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getUserResponse JiraSoapService.getUser(getUserRequest request)
        {
            return base.Channel.getUser(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getVersionsResponse JiraSoapService.getVersions(getVersionsRequest request)
        {
            return base.Channel.getVersions(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        getWorklogsResponse JiraSoapService.getWorklogs(getWorklogsRequest request)
        {
            return base.Channel.getWorklogs(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        hasPermissionToCreateWorklogResponse JiraSoapService.hasPermissionToCreateWorklog(hasPermissionToCreateWorklogRequest request)
        {
            return base.Channel.hasPermissionToCreateWorklog(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        hasPermissionToDeleteWorklogResponse JiraSoapService.hasPermissionToDeleteWorklog(hasPermissionToDeleteWorklogRequest request)
        {
            return base.Channel.hasPermissionToDeleteWorklog(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        hasPermissionToEditCommentResponse JiraSoapService.hasPermissionToEditComment(hasPermissionToEditCommentRequest request)
        {
            return base.Channel.hasPermissionToEditComment(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        hasPermissionToUpdateWorklogResponse JiraSoapService.hasPermissionToUpdateWorklog(hasPermissionToUpdateWorklogRequest request)
        {
            return base.Channel.hasPermissionToUpdateWorklog(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        isProjectRoleNameUniqueResponse JiraSoapService.isProjectRoleNameUnique(isProjectRoleNameUniqueRequest request)
        {
            return base.Channel.isProjectRoleNameUnique(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        loginResponse JiraSoapService.login(loginRequest request)
        {
            return base.Channel.login(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        logoutResponse JiraSoapService.logout(logoutRequest request)
        {
            return base.Channel.logout(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        progressWorkflowActionResponse JiraSoapService.progressWorkflowAction(progressWorkflowActionRequest request)
        {
            return base.Channel.progressWorkflowAction(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        refreshCustomFieldsResponse JiraSoapService.refreshCustomFields(refreshCustomFieldsRequest request)
        {
            return base.Channel.refreshCustomFields(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        releaseVersionResponse JiraSoapService.releaseVersion(releaseVersionRequest request)
        {
            return base.Channel.releaseVersion(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        removeActorsFromProjectRoleResponse JiraSoapService.removeActorsFromProjectRole(removeActorsFromProjectRoleRequest request)
        {
            return base.Channel.removeActorsFromProjectRole(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        removeAllRoleActorsByNameAndTypeResponse JiraSoapService.removeAllRoleActorsByNameAndType(removeAllRoleActorsByNameAndTypeRequest request)
        {
            return base.Channel.removeAllRoleActorsByNameAndType(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        removeAllRoleActorsByProjectResponse JiraSoapService.removeAllRoleActorsByProject(removeAllRoleActorsByProjectRequest request)
        {
            return base.Channel.removeAllRoleActorsByProject(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        removeDefaultActorsFromProjectRoleResponse JiraSoapService.removeDefaultActorsFromProjectRole(removeDefaultActorsFromProjectRoleRequest request)
        {
            return base.Channel.removeDefaultActorsFromProjectRole(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        removeUserFromGroupResponse JiraSoapService.removeUserFromGroup(removeUserFromGroupRequest request)
        {
            return base.Channel.removeUserFromGroup(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        setNewProjectAvatarResponse JiraSoapService.setNewProjectAvatar(setNewProjectAvatarRequest request)
        {
            return base.Channel.setNewProjectAvatar(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        setProjectAvatarResponse JiraSoapService.setProjectAvatar(setProjectAvatarRequest request)
        {
            return base.Channel.setProjectAvatar(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        updateGroupResponse JiraSoapService.updateGroup(updateGroupRequest request)
        {
            return base.Channel.updateGroup(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        updateIssueResponse JiraSoapService.updateIssue(updateIssueRequest request)
        {
            return base.Channel.updateIssue(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        updateProjectResponse JiraSoapService.updateProject(updateProjectRequest request)
        {
            return base.Channel.updateProject(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        updateProjectRoleResponse JiraSoapService.updateProjectRole(updateProjectRoleRequest request)
        {
            return base.Channel.updateProjectRole(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        updateWorklogAndAutoAdjustRemainingEstimateResponse JiraSoapService.updateWorklogAndAutoAdjustRemainingEstimate(updateWorklogAndAutoAdjustRemainingEstimateRequest request)
        {
            return base.Channel.updateWorklogAndAutoAdjustRemainingEstimate(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        updateWorklogAndRetainRemainingEstimateResponse JiraSoapService.updateWorklogAndRetainRemainingEstimate(updateWorklogAndRetainRemainingEstimateRequest request)
        {
            return base.Channel.updateWorklogAndRetainRemainingEstimate(request);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        updateWorklogWithNewRemainingEstimateResponse JiraSoapService.updateWorklogWithNewRemainingEstimate(updateWorklogWithNewRemainingEstimateRequest request)
        {
            return base.Channel.updateWorklogWithNewRemainingEstimate(request);
        }

        public RemoteGroup createGroup(string in0, string in1, RemoteUser in2)
        {
            createGroupRequest request = new createGroupRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2
            };
            return ((JiraSoapService) this).createGroup(request).createGroupReturn;
        }

        public RemoteIssue createIssue(string in0, RemoteIssue in1)
        {
            createIssueRequest request = new createIssueRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).createIssue(request).createIssueReturn;
        }

        public RemoteIssue createIssueWithParent(string in0, RemoteIssue in1, string in2)
        {
            createIssueWithParentRequest request = new createIssueWithParentRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2
            };
            return ((JiraSoapService) this).createIssueWithParent(request).createIssueWithParentReturn;
        }

        public RemoteIssue createIssueWithSecurityLevel(string in0, RemoteIssue in1, long in2)
        {
            createIssueWithSecurityLevelRequest request = new createIssueWithSecurityLevelRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2
            };
            return ((JiraSoapService) this).createIssueWithSecurityLevel(request).createIssueWithSecurityLevelReturn;
        }

        public RemotePermissionScheme createPermissionScheme(string in0, string in1, string in2)
        {
            createPermissionSchemeRequest request = new createPermissionSchemeRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2
            };
            return ((JiraSoapService) this).createPermissionScheme(request).createPermissionSchemeReturn;
        }

        public RemoteProject createProject(string in0, string in1, string in2, string in3, string in4, string in5, RemotePermissionScheme in6, RemoteScheme in7, RemoteScheme in8)
        {
            createProjectRequest request = new createProjectRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2,
                in3 = in3,
                in4 = in4,
                in5 = in5,
                in6 = in6,
                in7 = in7,
                in8 = in8
            };
            return ((JiraSoapService) this).createProject(request).createProjectReturn;
        }

        public RemoteProject createProjectFromObject(string in0, RemoteProject in1)
        {
            createProjectFromObjectRequest request = new createProjectFromObjectRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).createProjectFromObject(request).createProjectFromObjectReturn;
        }

        public RemoteProjectRole createProjectRole(string in0, RemoteProjectRole in1)
        {
            createProjectRoleRequest request = new createProjectRoleRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).createProjectRole(request).createProjectRoleReturn;
        }

        public RemoteUser createUser(string in0, string in1, string in2, string in3, string in4)
        {
            createUserRequest request = new createUserRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2,
                in3 = in3,
                in4 = in4
            };
            return ((JiraSoapService) this).createUser(request).createUserReturn;
        }

        public void deleteGroup(string in0, string in1, string in2)
        {
            deleteGroupRequest request = new deleteGroupRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2
            };
            ((JiraSoapService) this).deleteGroup(request);
        }

        public void deleteIssue(string in0, string in1)
        {
            deleteIssueRequest request = new deleteIssueRequest {
                in0 = in0,
                in1 = in1
            };
            ((JiraSoapService) this).deleteIssue(request);
        }

        public RemotePermissionScheme deletePermissionFrom(string in0, RemotePermissionScheme in1, RemotePermission in2, RemoteEntity in3)
        {
            deletePermissionFromRequest request = new deletePermissionFromRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2,
                in3 = in3
            };
            return ((JiraSoapService) this).deletePermissionFrom(request).deletePermissionFromReturn;
        }

        public void deletePermissionScheme(string in0, string in1)
        {
            deletePermissionSchemeRequest request = new deletePermissionSchemeRequest {
                in0 = in0,
                in1 = in1
            };
            ((JiraSoapService) this).deletePermissionScheme(request);
        }

        public void deleteProject(string in0, string in1)
        {
            deleteProjectRequest request = new deleteProjectRequest {
                in0 = in0,
                in1 = in1
            };
            ((JiraSoapService) this).deleteProject(request);
        }

        public void deleteProjectAvatar(string in0, long in1)
        {
            deleteProjectAvatarRequest request = new deleteProjectAvatarRequest {
                in0 = in0,
                in1 = in1
            };
            ((JiraSoapService) this).deleteProjectAvatar(request);
        }

        public void deleteProjectRole(string in0, RemoteProjectRole in1, bool in2)
        {
            deleteProjectRoleRequest request = new deleteProjectRoleRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2
            };
            ((JiraSoapService) this).deleteProjectRole(request);
        }

        public void deleteUser(string in0, string in1)
        {
            deleteUserRequest request = new deleteUserRequest {
                in0 = in0,
                in1 = in1
            };
            ((JiraSoapService) this).deleteUser(request);
        }

        public void deleteWorklogAndAutoAdjustRemainingEstimate(string in0, string in1)
        {
            deleteWorklogAndAutoAdjustRemainingEstimateRequest request = new deleteWorklogAndAutoAdjustRemainingEstimateRequest {
                in0 = in0,
                in1 = in1
            };
            ((JiraSoapService) this).deleteWorklogAndAutoAdjustRemainingEstimate(request);
        }

        public void deleteWorklogAndRetainRemainingEstimate(string in0, string in1)
        {
            deleteWorklogAndRetainRemainingEstimateRequest request = new deleteWorklogAndRetainRemainingEstimateRequest {
                in0 = in0,
                in1 = in1
            };
            ((JiraSoapService) this).deleteWorklogAndRetainRemainingEstimate(request);
        }

        public void deleteWorklogWithNewRemainingEstimate(string in0, string in1, string in2)
        {
            deleteWorklogWithNewRemainingEstimateRequest request = new deleteWorklogWithNewRemainingEstimateRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2
            };
            ((JiraSoapService) this).deleteWorklogWithNewRemainingEstimate(request);
        }

        public RemoteComment editComment(string in0, RemoteComment in1)
        {
            editCommentRequest request = new editCommentRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).editComment(request).editCommentReturn;
        }

        public RemotePermission[] getAllPermissions(string in0)
        {
            getAllPermissionsRequest request = new getAllPermissionsRequest {
                in0 = in0
            };
            return ((JiraSoapService) this).getAllPermissions(request).getAllPermissionsReturn;
        }

        public RemoteScheme[] getAssociatedNotificationSchemes(string in0, RemoteProjectRole in1)
        {
            getAssociatedNotificationSchemesRequest request = new getAssociatedNotificationSchemesRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).getAssociatedNotificationSchemes(request).getAssociatedNotificationSchemesReturn;
        }

        public RemoteScheme[] getAssociatedPermissionSchemes(string in0, RemoteProjectRole in1)
        {
            getAssociatedPermissionSchemesRequest request = new getAssociatedPermissionSchemesRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).getAssociatedPermissionSchemes(request).getAssociatedPermissionSchemesReturn;
        }

        public RemoteAttachment[] getAttachmentsFromIssue(string in0, string in1)
        {
            getAttachmentsFromIssueRequest request = new getAttachmentsFromIssueRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).getAttachmentsFromIssue(request).getAttachmentsFromIssueReturn;
        }

        public RemoteNamedObject[] getAvailableActions(string in0, string in1)
        {
            getAvailableActionsRequest request = new getAvailableActionsRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).getAvailableActions(request).getAvailableActionsReturn;
        }

        public RemoteComment getComment(string in0, long in1)
        {
            getCommentRequest request = new getCommentRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).getComment(request).getCommentReturn;
        }

        public RemoteComment[] getComments(string in0, string in1)
        {
            getCommentsRequest request = new getCommentsRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).getComments(request).getCommentsReturn;
        }

        public RemoteComponent[] getComponents(string in0, string in1)
        {
            getComponentsRequest request = new getComponentsRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).getComponents(request).getComponentsReturn;
        }

        public RemoteConfiguration getConfiguration(string in0)
        {
            getConfigurationRequest request = new getConfigurationRequest {
                in0 = in0
            };
            return ((JiraSoapService) this).getConfiguration(request).getConfigurationReturn;
        }

        public RemoteField[] getCustomFields(string in0)
        {
            getCustomFieldsRequest request = new getCustomFieldsRequest {
                in0 = in0
            };
            return ((JiraSoapService) this).getCustomFields(request).getCustomFieldsReturn;
        }

        public RemoteRoleActors getDefaultRoleActors(string in0, RemoteProjectRole in1)
        {
            getDefaultRoleActorsRequest request = new getDefaultRoleActorsRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).getDefaultRoleActors(request).getDefaultRoleActorsReturn;
        }

        public RemoteFilter[] getFavouriteFilters(string in0)
        {
            getFavouriteFiltersRequest request = new getFavouriteFiltersRequest {
                in0 = in0
            };
            return ((JiraSoapService) this).getFavouriteFilters(request).getFavouriteFiltersReturn;
        }

        public RemoteField[] getFieldsForAction(string in0, string in1, string in2)
        {
            getFieldsForActionRequest request = new getFieldsForActionRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2
            };
            return ((JiraSoapService) this).getFieldsForAction(request).getFieldsForActionReturn;
        }

        public RemoteField[] getFieldsForEdit(string in0, string in1)
        {
            getFieldsForEditRequest request = new getFieldsForEditRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).getFieldsForEdit(request).getFieldsForEditReturn;
        }

        public RemoteGroup getGroup(string in0, string in1)
        {
            getGroupRequest request = new getGroupRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).getGroup(request).getGroupReturn;
        }

        public RemoteIssue getIssue(string in0, string in1)
        {
            getIssueRequest request = new getIssueRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).getIssue(request).getIssueReturn;
        }

        public RemoteIssue getIssueById(string in0, string in1)
        {
            getIssueByIdRequest request = new getIssueByIdRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).getIssueById(request).getIssueByIdReturn;
        }

        public long getIssueCountForFilter(string in0, string in1)
        {
            getIssueCountForFilterRequest request = new getIssueCountForFilterRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).getIssueCountForFilter(request).getIssueCountForFilterReturn;
        }

        public RemoteIssue[] getIssuesFromFilter(string in0, string in1)
        {
            getIssuesFromFilterRequest request = new getIssuesFromFilterRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).getIssuesFromFilter(request).getIssuesFromFilterReturn;
        }

        public RemoteIssue[] getIssuesFromFilterWithLimit(string in0, string in1, int in2, int in3)
        {
            getIssuesFromFilterWithLimitRequest request = new getIssuesFromFilterWithLimitRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2,
                in3 = in3
            };
            return ((JiraSoapService) this).getIssuesFromFilterWithLimit(request).getIssuesFromFilterWithLimitReturn;
        }

        public RemoteIssue[] getIssuesFromJqlSearch(string in0, string in1, int in2)
        {
            getIssuesFromJqlSearchRequest request = new getIssuesFromJqlSearchRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2
            };
            return ((JiraSoapService) this).getIssuesFromJqlSearch(request).getIssuesFromJqlSearchReturn;
        }

        public RemoteIssue[] getIssuesFromTextSearch(string in0, string in1)
        {
            getIssuesFromTextSearchRequest request = new getIssuesFromTextSearchRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).getIssuesFromTextSearch(request).getIssuesFromTextSearchReturn;
        }

        public RemoteIssue[] getIssuesFromTextSearchWithLimit(string in0, string in1, int in2, int in3)
        {
            getIssuesFromTextSearchWithLimitRequest request = new getIssuesFromTextSearchWithLimitRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2,
                in3 = in3
            };
            return ((JiraSoapService) this).getIssuesFromTextSearchWithLimit(request).getIssuesFromTextSearchWithLimitReturn;
        }

        public RemoteIssue[] getIssuesFromTextSearchWithProject(string in0, string[] in1, string in2, int in3)
        {
            getIssuesFromTextSearchWithProjectRequest request = new getIssuesFromTextSearchWithProjectRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2,
                in3 = in3
            };
            return ((JiraSoapService) this).getIssuesFromTextSearchWithProject(request).getIssuesFromTextSearchWithProjectReturn;
        }

        public RemoteIssueType[] getIssueTypes(string in0)
        {
            getIssueTypesRequest request = new getIssueTypesRequest {
                in0 = in0
            };
            return ((JiraSoapService) this).getIssueTypes(request).getIssueTypesReturn;
        }

        public RemoteIssueType[] getIssueTypesForProject(string in0, string in1)
        {
            getIssueTypesForProjectRequest request = new getIssueTypesForProjectRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).getIssueTypesForProject(request).getIssueTypesForProjectReturn;
        }

        public RemoteScheme[] getNotificationSchemes(string in0)
        {
            getNotificationSchemesRequest request = new getNotificationSchemesRequest {
                in0 = in0
            };
            return ((JiraSoapService) this).getNotificationSchemes(request).getNotificationSchemesReturn;
        }

        public RemotePermissionScheme[] getPermissionSchemes(string in0)
        {
            getPermissionSchemesRequest request = new getPermissionSchemesRequest {
                in0 = in0
            };
            return ((JiraSoapService) this).getPermissionSchemes(request).getPermissionSchemesReturn;
        }

        public RemotePriority[] getPriorities(string in0)
        {
            getPrioritiesRequest request = new getPrioritiesRequest {
                in0 = in0
            };
            return ((JiraSoapService) this).getPriorities(request).getPrioritiesReturn;
        }

        public RemoteAvatar getProjectAvatar(string in0, string in1)
        {
            getProjectAvatarRequest request = new getProjectAvatarRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).getProjectAvatar(request).getProjectAvatarReturn;
        }

        public RemoteAvatar[] getProjectAvatars(string in0, string in1, bool in2)
        {
            getProjectAvatarsRequest request = new getProjectAvatarsRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2
            };
            return ((JiraSoapService) this).getProjectAvatars(request).getProjectAvatarsReturn;
        }

        public RemoteProject getProjectById(string in0, long in1)
        {
            getProjectByIdRequest request = new getProjectByIdRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).getProjectById(request).getProjectByIdReturn;
        }

        public RemoteProject getProjectByKey(string in0, string in1)
        {
            getProjectByKeyRequest request = new getProjectByKeyRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).getProjectByKey(request).getProjectByKeyReturn;
        }

        public RemoteProjectRole getProjectRole(string in0, long in1)
        {
            getProjectRoleRequest request = new getProjectRoleRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).getProjectRole(request).getProjectRoleReturn;
        }

        public RemoteProjectRoleActors getProjectRoleActors(string in0, RemoteProjectRole in1, RemoteProject in2)
        {
            getProjectRoleActorsRequest request = new getProjectRoleActorsRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2
            };
            return ((JiraSoapService) this).getProjectRoleActors(request).getProjectRoleActorsReturn;
        }

        public RemoteProjectRole[] getProjectRoles(string in0)
        {
            getProjectRolesRequest request = new getProjectRolesRequest {
                in0 = in0
            };
            return ((JiraSoapService) this).getProjectRoles(request).getProjectRolesReturn;
        }

        public RemoteProject[] getProjectsNoSchemes(string in0)
        {
            getProjectsNoSchemesRequest request = new getProjectsNoSchemesRequest {
                in0 = in0
            };
            return ((JiraSoapService) this).getProjectsNoSchemes(request).getProjectsNoSchemesReturn;
        }

        public RemoteProject getProjectWithSchemesById(string in0, long in1)
        {
            getProjectWithSchemesByIdRequest request = new getProjectWithSchemesByIdRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).getProjectWithSchemesById(request).getProjectWithSchemesByIdReturn;
        }

        public DateTime getResolutionDateById(string in0, long in1)
        {
            getResolutionDateByIdRequest request = new getResolutionDateByIdRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).getResolutionDateById(request).getResolutionDateByIdReturn;
        }

        public DateTime getResolutionDateByKey(string in0, string in1)
        {
            getResolutionDateByKeyRequest request = new getResolutionDateByKeyRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).getResolutionDateByKey(request).getResolutionDateByKeyReturn;
        }

        public RemoteResolution[] getResolutions(string in0)
        {
            getResolutionsRequest request = new getResolutionsRequest {
                in0 = in0
            };
            return ((JiraSoapService) this).getResolutions(request).getResolutionsReturn;
        }

        public RemoteFilter[] getSavedFilters(string in0)
        {
            getSavedFiltersRequest request = new getSavedFiltersRequest {
                in0 = in0
            };
            return ((JiraSoapService) this).getSavedFilters(request).getSavedFiltersReturn;
        }

        public RemoteSecurityLevel getSecurityLevel(string in0, string in1)
        {
            getSecurityLevelRequest request = new getSecurityLevelRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).getSecurityLevel(request).getSecurityLevelReturn;
        }

        public RemoteSecurityLevel[] getSecurityLevels(string in0, string in1)
        {
            getSecurityLevelsRequest request = new getSecurityLevelsRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).getSecurityLevels(request).getSecurityLevelsReturn;
        }

        public RemoteScheme[] getSecuritySchemes(string in0)
        {
            getSecuritySchemesRequest request = new getSecuritySchemesRequest {
                in0 = in0
            };
            return ((JiraSoapService) this).getSecuritySchemes(request).getSecuritySchemesReturn;
        }

        public RemoteServerInfo getServerInfo(string in0)
        {
            getServerInfoRequest request = new getServerInfoRequest {
                in0 = in0
            };
            return ((JiraSoapService) this).getServerInfo(request).getServerInfoReturn;
        }

        public RemoteStatus[] getStatuses(string in0)
        {
            getStatusesRequest request = new getStatusesRequest {
                in0 = in0
            };
            return ((JiraSoapService) this).getStatuses(request).getStatusesReturn;
        }

        public RemoteIssueType[] getSubTaskIssueTypes(string in0)
        {
            getSubTaskIssueTypesRequest request = new getSubTaskIssueTypesRequest {
                in0 = in0
            };
            return ((JiraSoapService) this).getSubTaskIssueTypes(request).getSubTaskIssueTypesReturn;
        }

        public RemoteIssueType[] getSubTaskIssueTypesForProject(string in0, string in1)
        {
            getSubTaskIssueTypesForProjectRequest request = new getSubTaskIssueTypesForProjectRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).getSubTaskIssueTypesForProject(request).getSubTaskIssueTypesForProjectReturn;
        }

        public RemoteUser getUser(string in0, string in1)
        {
            getUserRequest request = new getUserRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).getUser(request).getUserReturn;
        }

        public RemoteVersion[] getVersions(string in0, string in1)
        {
            getVersionsRequest request = new getVersionsRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).getVersions(request).getVersionsReturn;
        }

        public RemoteWorklog[] getWorklogs(string in0, string in1)
        {
            getWorklogsRequest request = new getWorklogsRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).getWorklogs(request).getWorklogsReturn;
        }

        public bool hasPermissionToCreateWorklog(string in0, string in1)
        {
            hasPermissionToCreateWorklogRequest request = new hasPermissionToCreateWorklogRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).hasPermissionToCreateWorklog(request).hasPermissionToCreateWorklogReturn;
        }

        public bool hasPermissionToDeleteWorklog(string in0, string in1)
        {
            hasPermissionToDeleteWorklogRequest request = new hasPermissionToDeleteWorklogRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).hasPermissionToDeleteWorklog(request).hasPermissionToDeleteWorklogReturn;
        }

        public bool hasPermissionToEditComment(string in0, RemoteComment in1)
        {
            hasPermissionToEditCommentRequest request = new hasPermissionToEditCommentRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).hasPermissionToEditComment(request).hasPermissionToEditCommentReturn;
        }

        public bool hasPermissionToUpdateWorklog(string in0, string in1)
        {
            hasPermissionToUpdateWorklogRequest request = new hasPermissionToUpdateWorklogRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).hasPermissionToUpdateWorklog(request).hasPermissionToUpdateWorklogReturn;
        }

        public bool isProjectRoleNameUnique(string in0, string in1)
        {
            isProjectRoleNameUniqueRequest request = new isProjectRoleNameUniqueRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).isProjectRoleNameUnique(request).isProjectRoleNameUniqueReturn;
        }

        public string login(string in0, string in1)
        {
            loginRequest request = new loginRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).login(request).loginReturn;
        }

        public bool logout(string in0)
        {
            logoutRequest request = new logoutRequest {
                in0 = in0
            };
            return ((JiraSoapService) this).logout(request).logoutReturn;
        }

        public RemoteIssue progressWorkflowAction(string in0, string in1, string in2, RemoteFieldValue[] in3)
        {
            progressWorkflowActionRequest request = new progressWorkflowActionRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2,
                in3 = in3
            };
            return ((JiraSoapService) this).progressWorkflowAction(request).progressWorkflowActionReturn;
        }

        public void refreshCustomFields(string in0)
        {
            refreshCustomFieldsRequest request = new refreshCustomFieldsRequest {
                in0 = in0
            };
            ((JiraSoapService) this).refreshCustomFields(request);
        }

        public void releaseVersion(string in0, string in1, RemoteVersion in2)
        {
            releaseVersionRequest request = new releaseVersionRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2
            };
            ((JiraSoapService) this).releaseVersion(request);
        }

        public void removeActorsFromProjectRole(string in0, string[] in1, RemoteProjectRole in2, RemoteProject in3, string in4)
        {
            removeActorsFromProjectRoleRequest request = new removeActorsFromProjectRoleRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2,
                in3 = in3,
                in4 = in4
            };
            ((JiraSoapService) this).removeActorsFromProjectRole(request);
        }

        public void removeAllRoleActorsByNameAndType(string in0, string in1, string in2)
        {
            removeAllRoleActorsByNameAndTypeRequest request = new removeAllRoleActorsByNameAndTypeRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2
            };
            ((JiraSoapService) this).removeAllRoleActorsByNameAndType(request);
        }

        public void removeAllRoleActorsByProject(string in0, RemoteProject in1)
        {
            removeAllRoleActorsByProjectRequest request = new removeAllRoleActorsByProjectRequest {
                in0 = in0,
                in1 = in1
            };
            ((JiraSoapService) this).removeAllRoleActorsByProject(request);
        }

        public void removeDefaultActorsFromProjectRole(string in0, string[] in1, RemoteProjectRole in2, string in3)
        {
            removeDefaultActorsFromProjectRoleRequest request = new removeDefaultActorsFromProjectRoleRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2,
                in3 = in3
            };
            ((JiraSoapService) this).removeDefaultActorsFromProjectRole(request);
        }

        public void removeUserFromGroup(string in0, RemoteGroup in1, RemoteUser in2)
        {
            removeUserFromGroupRequest request = new removeUserFromGroupRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2
            };
            ((JiraSoapService) this).removeUserFromGroup(request);
        }

        public void setNewProjectAvatar(string in0, string in1, string in2, string in3)
        {
            setNewProjectAvatarRequest request = new setNewProjectAvatarRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2,
                in3 = in3
            };
            ((JiraSoapService) this).setNewProjectAvatar(request);
        }

        public void setProjectAvatar(string in0, string in1, long in2)
        {
            setProjectAvatarRequest request = new setProjectAvatarRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2
            };
            ((JiraSoapService) this).setProjectAvatar(request);
        }

        public RemoteGroup updateGroup(string in0, RemoteGroup in1)
        {
            updateGroupRequest request = new updateGroupRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).updateGroup(request).updateGroupReturn;
        }

        public RemoteIssue updateIssue(string in0, string in1, RemoteFieldValue[] in2)
        {
            updateIssueRequest request = new updateIssueRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2
            };
            return ((JiraSoapService) this).updateIssue(request).updateIssueReturn;
        }

        public RemoteProject updateProject(string in0, RemoteProject in1)
        {
            updateProjectRequest request = new updateProjectRequest {
                in0 = in0,
                in1 = in1
            };
            return ((JiraSoapService) this).updateProject(request).updateProjectReturn;
        }

        public void updateProjectRole(string in0, RemoteProjectRole in1)
        {
            updateProjectRoleRequest request = new updateProjectRoleRequest {
                in0 = in0,
                in1 = in1
            };
            ((JiraSoapService) this).updateProjectRole(request);
        }

        public void updateWorklogAndAutoAdjustRemainingEstimate(string in0, RemoteWorklog in1)
        {
            updateWorklogAndAutoAdjustRemainingEstimateRequest request = new updateWorklogAndAutoAdjustRemainingEstimateRequest {
                in0 = in0,
                in1 = in1
            };
            ((JiraSoapService) this).updateWorklogAndAutoAdjustRemainingEstimate(request);
        }

        public void updateWorklogAndRetainRemainingEstimate(string in0, RemoteWorklog in1)
        {
            updateWorklogAndRetainRemainingEstimateRequest request = new updateWorklogAndRetainRemainingEstimateRequest {
                in0 = in0,
                in1 = in1
            };
            ((JiraSoapService) this).updateWorklogAndRetainRemainingEstimate(request);
        }

        public void updateWorklogWithNewRemainingEstimate(string in0, RemoteWorklog in1, string in2)
        {
            updateWorklogWithNewRemainingEstimateRequest request = new updateWorklogWithNewRemainingEstimateRequest {
                in0 = in0,
                in1 = in1,
                in2 = in2
            };
            ((JiraSoapService) this).updateWorklogWithNewRemainingEstimate(request);
        }
    }
}

