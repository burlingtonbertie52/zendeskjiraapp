﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [GeneratedCode("System.ServiceModel", "3.0.0.0"), DebuggerStepThrough, MessageContract(WrapperName="addBase64EncodedAttachmentsToIssueResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true)]
    public class addBase64EncodedAttachmentsToIssueResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public bool addBase64EncodedAttachmentsToIssueReturn;

        public addBase64EncodedAttachmentsToIssueResponse()
        {
        }

        public addBase64EncodedAttachmentsToIssueResponse(bool addBase64EncodedAttachmentsToIssueReturn)
        {
            this.addBase64EncodedAttachmentsToIssueReturn = addBase64EncodedAttachmentsToIssueReturn;
        }
    }
}

