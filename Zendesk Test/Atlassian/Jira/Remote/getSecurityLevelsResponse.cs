﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [MessageContract(WrapperName="getSecurityLevelsResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class getSecurityLevelsResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteSecurityLevel[] getSecurityLevelsReturn;

        public getSecurityLevelsResponse()
        {
        }

        public getSecurityLevelsResponse(RemoteSecurityLevel[] getSecurityLevelsReturn)
        {
            this.getSecurityLevelsReturn = getSecurityLevelsReturn;
        }
    }
}

