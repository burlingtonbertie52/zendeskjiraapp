﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="getProjectRoleResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true)]
    public class getProjectRoleResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteProjectRole getProjectRoleReturn;

        public getProjectRoleResponse()
        {
        }

        public getProjectRoleResponse(RemoteProjectRole getProjectRoleReturn)
        {
            this.getProjectRoleReturn = getProjectRoleReturn;
        }
    }
}

