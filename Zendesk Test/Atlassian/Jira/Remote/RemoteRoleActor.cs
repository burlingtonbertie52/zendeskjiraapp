﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Xml.Serialization;

    [Serializable, DesignerCategory("code"), SoapType(Namespace="http://beans.soap.rpc.jira.atlassian.com"), GeneratedCode("svcutil", "3.0.4506.2152"), DebuggerStepThrough]
    public class RemoteRoleActor
    {
        private string descriptorField;
        private string parameterField;
        private RemoteProjectRole projectRoleField;
        private string typeField;
        private RemoteUser[] usersField;

        [SoapElement(IsNullable=true)]
        public string descriptor
        {
            get
            {
                return this.descriptorField;
            }
            set
            {
                this.descriptorField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string parameter
        {
            get
            {
                return this.parameterField;
            }
            set
            {
                this.parameterField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public RemoteProjectRole projectRole
        {
            get
            {
                return this.projectRoleField;
            }
            set
            {
                this.projectRoleField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public RemoteUser[] users
        {
            get
            {
                return this.usersField;
            }
            set
            {
                this.usersField = value;
            }
        }
    }
}

