﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="getPermissionSchemesResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true)]
    public class getPermissionSchemesResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemotePermissionScheme[] getPermissionSchemesReturn;

        public getPermissionSchemesResponse()
        {
        }

        public getPermissionSchemesResponse(RemotePermissionScheme[] getPermissionSchemesReturn)
        {
            this.getPermissionSchemesReturn = getPermissionSchemesReturn;
        }
    }
}

