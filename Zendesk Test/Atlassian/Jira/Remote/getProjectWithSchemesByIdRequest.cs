﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, MessageContract(WrapperName="getProjectWithSchemesById", WrapperNamespace="http://soap.rpc.jira.atlassian.com", IsWrapped=true), GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class getProjectWithSchemesByIdRequest
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public string in0;
        [MessageBodyMember(Namespace="", Order=1)]
        public long in1;

        public getProjectWithSchemesByIdRequest()
        {
        }

        public getProjectWithSchemesByIdRequest(string in0, long in1)
        {
            this.in0 = in0;
            this.in1 = in1;
        }
    }
}

