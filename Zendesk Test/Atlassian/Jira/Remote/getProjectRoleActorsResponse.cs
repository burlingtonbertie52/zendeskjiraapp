﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, MessageContract(WrapperName="getProjectRoleActorsResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class getProjectRoleActorsResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteProjectRoleActors getProjectRoleActorsReturn;

        public getProjectRoleActorsResponse()
        {
        }

        public getProjectRoleActorsResponse(RemoteProjectRoleActors getProjectRoleActorsReturn)
        {
            this.getProjectRoleActorsReturn = getProjectRoleActorsReturn;
        }
    }
}

