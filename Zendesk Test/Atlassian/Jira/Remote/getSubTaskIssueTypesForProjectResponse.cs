﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, MessageContract(WrapperName="getSubTaskIssueTypesForProjectResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class getSubTaskIssueTypesForProjectResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteIssueType[] getSubTaskIssueTypesForProjectReturn;

        public getSubTaskIssueTypesForProjectResponse()
        {
        }

        public getSubTaskIssueTypesForProjectResponse(RemoteIssueType[] getSubTaskIssueTypesForProjectReturn)
        {
            this.getSubTaskIssueTypesForProjectReturn = getSubTaskIssueTypesForProjectReturn;
        }
    }
}

