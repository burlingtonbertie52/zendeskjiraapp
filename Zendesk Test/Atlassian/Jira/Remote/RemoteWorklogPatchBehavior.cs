﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.IO;
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using System.ServiceModel.Description;
    using System.ServiceModel.Dispatcher;
    using System.Xml;

    internal class RemoteWorklogPatchBehavior : IEndpointBehavior
    {
        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(new RemoteWorklogMessageInspector());
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
        }

        public void Validate(ServiceEndpoint endpoint)
        {
        }

        private class RemoteWorklogMessageInspector : IClientMessageInspector
        {
            private static string _correlationState = "worklog";

            public void AfterReceiveReply(ref Message reply, object correlationState)
            {
                if (((correlationState != null) && object.Equals(correlationState, _correlationState)) && !reply.ToString().Contains("<soapenv:Fault>"))
                {
                    MemoryStream output = new MemoryStream();
                    XmlWriter writer = XmlWriter.Create(output);
                    reply.WriteMessage(writer);
                    writer.Flush();
                    output.Position = 0L;
                    XmlDocument doc = new XmlDocument();
                    doc.Load(output);
                    this.UpdateMessage(doc);
                    output.SetLength(0L);
                    writer = XmlWriter.Create(output);
                    doc.WriteTo(writer);
                    writer.Flush();
                    output.Position = 0L;
                    XmlReader envelopeReader = XmlReader.Create(output);
                    reply = Message.CreateMessage(envelopeReader, 0x7fffffff, reply.Version);
                }
            }

            public object BeforeSendRequest(ref Message request, IClientChannel channel)
            {
                string str = request.ToString();
                if ((!str.Contains("addWorklogAndAutoAdjustRemainingEstimate") && !str.Contains("addWorklogAndRetainRemainingEstimate")) && (!str.Contains("addWorklogWithNewRemainingEstimate") && !str.Contains("getWorklogs")))
                {
                    return null;
                }
                return _correlationState;
            }

            private void UpdateMessage(XmlDocument doc)
            {
                XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
                nsmgr.AddNamespace("soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
                foreach (XmlElement element in doc.SelectNodes("//soapenv:Body/multiRef", nsmgr))
                {
                    element.SetAttribute("xsi:type", "ns2:RemoteWorklog");
                    element.SetAttribute("xmlns:ns2", "http://beans.soap.rpc.jira.atlassian.com");
                }
            }
        }
    }
}

