﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="getIssueTypesResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true)]
    public class getIssueTypesResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteIssueType[] getIssueTypesReturn;

        public getIssueTypesResponse()
        {
        }

        public getIssueTypesResponse(RemoteIssueType[] getIssueTypesReturn)
        {
            this.getIssueTypesReturn = getIssueTypesReturn;
        }
    }
}

