﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, MessageContract(WrapperName="getComponentsResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class getComponentsResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteComponent[] getComponentsReturn;

        public getComponentsResponse()
        {
        }

        public getComponentsResponse(RemoteComponent[] getComponentsReturn)
        {
            this.getComponentsReturn = getComponentsReturn;
        }
    }
}

