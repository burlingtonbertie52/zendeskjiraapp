﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="updateProjectResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true)]
    public class updateProjectResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteProject updateProjectReturn;

        public updateProjectResponse()
        {
        }

        public updateProjectResponse(RemoteProject updateProjectReturn)
        {
            this.updateProjectReturn = updateProjectReturn;
        }
    }
}

