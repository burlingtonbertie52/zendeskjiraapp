﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="getCustomFieldsResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true)]
    public class getCustomFieldsResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteField[] getCustomFieldsReturn;

        public getCustomFieldsResponse()
        {
        }

        public getCustomFieldsResponse(RemoteField[] getCustomFieldsReturn)
        {
            this.getCustomFieldsReturn = getCustomFieldsReturn;
        }
    }
}

