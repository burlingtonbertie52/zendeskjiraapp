﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, MessageContract(WrapperName="removeUserFromGroup", WrapperNamespace="http://soap.rpc.jira.atlassian.com", IsWrapped=true), GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class removeUserFromGroupRequest
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public string in0;
        [MessageBodyMember(Namespace="", Order=1)]
        public RemoteGroup in1;
        [MessageBodyMember(Namespace="", Order=2)]
        public RemoteUser in2;

        public removeUserFromGroupRequest()
        {
        }

        public removeUserFromGroupRequest(string in0, RemoteGroup in1, RemoteUser in2)
        {
            this.in0 = in0;
            this.in1 = in1;
            this.in2 = in2;
        }
    }
}

