﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="createProject", WrapperNamespace="http://soap.rpc.jira.atlassian.com", IsWrapped=true), DebuggerStepThrough]
    public class createProjectRequest
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public string in0;
        [MessageBodyMember(Namespace="", Order=1)]
        public string in1;
        [MessageBodyMember(Namespace="", Order=2)]
        public string in2;
        [MessageBodyMember(Namespace="", Order=3)]
        public string in3;
        [MessageBodyMember(Namespace="", Order=4)]
        public string in4;
        [MessageBodyMember(Namespace="", Order=5)]
        public string in5;
        [MessageBodyMember(Namespace="", Order=6)]
        public RemotePermissionScheme in6;
        [MessageBodyMember(Namespace="", Order=7)]
        public RemoteScheme in7;
        [MessageBodyMember(Namespace="", Order=8)]
        public RemoteScheme in8;

        public createProjectRequest()
        {
        }

        public createProjectRequest(string in0, string in1, string in2, string in3, string in4, string in5, RemotePermissionScheme in6, RemoteScheme in7, RemoteScheme in8)
        {
            this.in0 = in0;
            this.in1 = in1;
            this.in2 = in2;
            this.in3 = in3;
            this.in4 = in4;
            this.in5 = in5;
            this.in6 = in6;
            this.in7 = in7;
            this.in8 = in8;
        }
    }
}

