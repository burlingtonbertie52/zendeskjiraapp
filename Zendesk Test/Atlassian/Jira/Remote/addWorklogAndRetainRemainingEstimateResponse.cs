﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [MessageContract(WrapperName="addWorklogAndRetainRemainingEstimateResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class addWorklogAndRetainRemainingEstimateResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteWorklog addWorklogAndRetainRemainingEstimateReturn;

        public addWorklogAndRetainRemainingEstimateResponse()
        {
        }

        public addWorklogAndRetainRemainingEstimateResponse(RemoteWorklog addWorklogAndRetainRemainingEstimateReturn)
        {
            this.addWorklogAndRetainRemainingEstimateReturn = addWorklogAndRetainRemainingEstimateReturn;
        }
    }
}

