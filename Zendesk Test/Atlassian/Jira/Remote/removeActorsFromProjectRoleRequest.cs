﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="removeActorsFromProjectRole", WrapperNamespace="http://soap.rpc.jira.atlassian.com", IsWrapped=true)]
    public class removeActorsFromProjectRoleRequest
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public string in0;
        [MessageBodyMember(Namespace="", Order=1)]
        public string[] in1;
        [MessageBodyMember(Namespace="", Order=2)]
        public RemoteProjectRole in2;
        [MessageBodyMember(Namespace="", Order=3)]
        public RemoteProject in3;
        [MessageBodyMember(Namespace="", Order=4)]
        public string in4;

        public removeActorsFromProjectRoleRequest()
        {
        }

        public removeActorsFromProjectRoleRequest(string in0, string[] in1, RemoteProjectRole in2, RemoteProject in3, string in4)
        {
            this.in0 = in0;
            this.in1 = in1;
            this.in2 = in2;
            this.in3 = in3;
            this.in4 = in4;
        }
    }
}

