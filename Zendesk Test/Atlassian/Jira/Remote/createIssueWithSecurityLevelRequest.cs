﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, MessageContract(WrapperName="createIssueWithSecurityLevel", WrapperNamespace="http://soap.rpc.jira.atlassian.com", IsWrapped=true), GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class createIssueWithSecurityLevelRequest
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public string in0;
        [MessageBodyMember(Namespace="", Order=1)]
        public RemoteIssue in1;
        [MessageBodyMember(Namespace="", Order=2)]
        public long in2;

        public createIssueWithSecurityLevelRequest()
        {
        }

        public createIssueWithSecurityLevelRequest(string in0, RemoteIssue in1, long in2)
        {
            this.in0 = in0;
            this.in1 = in1;
            this.in2 = in2;
        }
    }
}

