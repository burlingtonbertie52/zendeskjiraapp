﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [MessageContract(WrapperName="getResolutionDateByKeyResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class getResolutionDateByKeyResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public DateTime getResolutionDateByKeyReturn;

        public getResolutionDateByKeyResponse()
        {
        }

        public getResolutionDateByKeyResponse(DateTime getResolutionDateByKeyReturn)
        {
            this.getResolutionDateByKeyReturn = getResolutionDateByKeyReturn;
        }
    }
}

