﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [MessageContract(WrapperName="getIssuesFromJqlSearchResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), GeneratedCode("System.ServiceModel", "3.0.0.0"), DebuggerStepThrough]
    public class getIssuesFromJqlSearchResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteIssue[] getIssuesFromJqlSearchReturn;

        public getIssuesFromJqlSearchResponse()
        {
        }

        public getIssuesFromJqlSearchResponse(RemoteIssue[] getIssuesFromJqlSearchReturn)
        {
            this.getIssuesFromJqlSearchReturn = getIssuesFromJqlSearchReturn;
        }
    }
}

