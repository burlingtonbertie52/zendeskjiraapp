﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="hasPermissionToEditComment", WrapperNamespace="http://soap.rpc.jira.atlassian.com", IsWrapped=true), DebuggerStepThrough]
    public class hasPermissionToEditCommentRequest
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public string in0;
        [MessageBodyMember(Namespace="", Order=1)]
        public RemoteComment in1;

        public hasPermissionToEditCommentRequest()
        {
        }

        public hasPermissionToEditCommentRequest(string in0, RemoteComment in1)
        {
            this.in0 = in0;
            this.in1 = in1;
        }
    }
}

