﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [MessageContract(WrapperName="getAvailableActionsResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class getAvailableActionsResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteNamedObject[] getAvailableActionsReturn;

        public getAvailableActionsResponse()
        {
        }

        public getAvailableActionsResponse(RemoteNamedObject[] getAvailableActionsReturn)
        {
            this.getAvailableActionsReturn = getAvailableActionsReturn;
        }
    }
}

