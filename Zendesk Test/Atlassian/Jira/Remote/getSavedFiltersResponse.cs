﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [MessageContract(WrapperName="getSavedFiltersResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class getSavedFiltersResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteFilter[] getSavedFiltersReturn;

        public getSavedFiltersResponse()
        {
        }

        public getSavedFiltersResponse(RemoteFilter[] getSavedFiltersReturn)
        {
            this.getSavedFiltersReturn = getSavedFiltersReturn;
        }
    }
}

