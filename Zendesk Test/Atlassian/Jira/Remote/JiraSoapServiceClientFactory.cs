﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.ServiceModel;
    using System.Xml;

    public static class JiraSoapServiceClientFactory
    {
        public static JiraSoapServiceClient Create(string jiraBaseUrl)
        {
            if (!jiraBaseUrl.EndsWith("/"))
            {
                jiraBaseUrl = jiraBaseUrl + "/";
            }
            Uri uri = new Uri(jiraBaseUrl + "rpc/soap/jirasoapservice-v2");
            BasicHttpBinding binding = null;
            if (uri.Scheme == "https")
            {
                binding = new BasicHttpBinding(BasicHttpSecurityMode.Transport);
            }
            else
            {
                binding = new BasicHttpBinding(BasicHttpSecurityMode.None);
            }
            binding.TransferMode = TransferMode.Buffered;
            binding.UseDefaultWebProxy = true;
            binding.MaxReceivedMessageSize = 0x7fffffffL;
            binding.SendTimeout = new TimeSpan(0, 10, 0);
            binding.ReceiveTimeout = new TimeSpan(0, 10, 0);
            XmlDictionaryReaderQuotas quotas = new XmlDictionaryReaderQuotas {
                MaxStringContentLength = 0x7fffffff,
                MaxNameTableCharCount = 0x7fffffff
            };
            binding.ReaderQuotas = quotas;
            binding.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName;
            EndpointAddress remoteAddress = new EndpointAddress(uri, new AddressHeader[0]);
            JiraSoapServiceClient client = new JiraSoapServiceClient(binding, remoteAddress);
            client.Endpoint.Behaviors.Add(new RemoteWorklogPatchBehavior());
            return client;
        }
    }
}

