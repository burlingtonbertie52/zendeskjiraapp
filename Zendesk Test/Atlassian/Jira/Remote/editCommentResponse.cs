﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [MessageContract(WrapperName="editCommentResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class editCommentResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteComment editCommentReturn;

        public editCommentResponse()
        {
        }

        public editCommentResponse(RemoteComment editCommentReturn)
        {
            this.editCommentReturn = editCommentReturn;
        }
    }
}

