﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [GeneratedCode("System.ServiceModel", "3.0.0.0"), DebuggerStepThrough, MessageContract(WrapperName="getResolutionDateByIdResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true)]
    public class getResolutionDateByIdResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public DateTime getResolutionDateByIdReturn;

        public getResolutionDateByIdResponse()
        {
        }

        public getResolutionDateByIdResponse(DateTime getResolutionDateByIdReturn)
        {
            this.getResolutionDateByIdReturn = getResolutionDateByIdReturn;
        }
    }
}

