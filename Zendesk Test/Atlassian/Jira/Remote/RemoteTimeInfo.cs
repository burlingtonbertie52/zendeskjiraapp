﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Xml.Serialization;

    [Serializable, SoapType(Namespace="http://beans.soap.rpc.jira.atlassian.com"), GeneratedCode("svcutil", "3.0.4506.2152"), DebuggerStepThrough, DesignerCategory("code")]
    public class RemoteTimeInfo
    {
        private string serverTimeField;
        private string timeZoneIdField;

        [SoapElement(IsNullable=true)]
        public string serverTime
        {
            get
            {
                return this.serverTimeField;
            }
            set
            {
                this.serverTimeField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string timeZoneId
        {
            get
            {
                return this.timeZoneIdField;
            }
            set
            {
                this.timeZoneIdField = value;
            }
        }
    }
}

