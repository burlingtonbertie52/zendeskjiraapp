﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="getIssueCountForFilterResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), DebuggerStepThrough]
    public class getIssueCountForFilterResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public long getIssueCountForFilterReturn;

        public getIssueCountForFilterResponse()
        {
        }

        public getIssueCountForFilterResponse(long getIssueCountForFilterReturn)
        {
            this.getIssueCountForFilterReturn = getIssueCountForFilterReturn;
        }
    }
}

