﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Xml.Serialization;

    [Serializable, SoapType(Namespace="http://beans.soap.rpc.jira.atlassian.com"), DebuggerStepThrough, DesignerCategory("code"), GeneratedCode("svcutil", "3.0.4506.2152")]
    public class RemoteIssueType : AbstractRemoteConstant
    {
        private bool subTaskField;

        public bool subTask
        {
            get
            {
                return this.subTaskField;
            }
            set
            {
                this.subTaskField = value;
            }
        }
    }
}

