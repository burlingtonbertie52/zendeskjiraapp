﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="createIssueWithParentResponse", WrapperNamespace="https://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true)]
    public class createIssueWithParentResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteIssue createIssueWithParentReturn;

        public createIssueWithParentResponse()
        {
        }

        public createIssueWithParentResponse(RemoteIssue createIssueWithParentReturn)
        {
            this.createIssueWithParentReturn = createIssueWithParentReturn;
        }
    }
}

