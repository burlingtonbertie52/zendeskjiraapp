﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="removeDefaultActorsFromProjectRole", WrapperNamespace="http://soap.rpc.jira.atlassian.com", IsWrapped=true)]
    public class removeDefaultActorsFromProjectRoleRequest
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public string in0;
        [MessageBodyMember(Namespace="", Order=1)]
        public string[] in1;
        [MessageBodyMember(Namespace="", Order=2)]
        public RemoteProjectRole in2;
        [MessageBodyMember(Namespace="", Order=3)]
        public string in3;

        public removeDefaultActorsFromProjectRoleRequest()
        {
        }

        public removeDefaultActorsFromProjectRoleRequest(string in0, string[] in1, RemoteProjectRole in2, string in3)
        {
            this.in0 = in0;
            this.in1 = in1;
            this.in2 = in2;
            this.in3 = in3;
        }
    }
}

