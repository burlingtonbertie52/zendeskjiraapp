﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="getStatuses", WrapperNamespace="http://soap.rpc.jira.atlassian.com", IsWrapped=true)]
    public class getStatusesRequest
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public string in0;

        public getStatusesRequest()
        {
        }

        public getStatusesRequest(string in0)
        {
            this.in0 = in0;
        }
    }
}

