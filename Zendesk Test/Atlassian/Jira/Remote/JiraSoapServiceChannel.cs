﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.ServiceModel;
    using System.ServiceModel.Channels;

    [GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public interface JiraSoapServiceChannel : IExtensibleObject<IContextChannel>, IClientChannel, IContextChannel, IChannel, ICommunicationObject, IDisposable, JiraSoapService
    {
    }
}

