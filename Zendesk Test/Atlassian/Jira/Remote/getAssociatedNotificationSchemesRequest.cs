﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [MessageContract(WrapperName="getAssociatedNotificationSchemes", WrapperNamespace="http://soap.rpc.jira.atlassian.com", IsWrapped=true), GeneratedCode("System.ServiceModel", "3.0.0.0"), DebuggerStepThrough]
    public class getAssociatedNotificationSchemesRequest
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public string in0;
        [MessageBodyMember(Namespace="", Order=1)]
        public RemoteProjectRole in1;

        public getAssociatedNotificationSchemesRequest()
        {
        }

        public getAssociatedNotificationSchemesRequest(string in0, RemoteProjectRole in1)
        {
            this.in0 = in0;
            this.in1 = in1;
        }
    }
}

