﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="getSecuritySchemesResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), DebuggerStepThrough]
    public class getSecuritySchemesResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteScheme[] getSecuritySchemesReturn;

        public getSecuritySchemesResponse()
        {
        }

        public getSecuritySchemesResponse(RemoteScheme[] getSecuritySchemesReturn)
        {
            this.getSecuritySchemesReturn = getSecuritySchemesReturn;
        }
    }
}

