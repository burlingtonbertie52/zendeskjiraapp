﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [MessageContract(WrapperName="createProjectRoleResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class createProjectRoleResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteProjectRole createProjectRoleReturn;

        public createProjectRoleResponse()
        {
        }

        public createProjectRoleResponse(RemoteProjectRole createProjectRoleReturn)
        {
            this.createProjectRoleReturn = createProjectRoleReturn;
        }
    }
}

