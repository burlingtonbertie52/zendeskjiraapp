﻿namespace Atlassian.Jira.Remote
{
    using Atlassian.Jira;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public static class ExtensionMethods
    {
        public static ProjectComponent ToLocal(this RemoteComponent remoteComponent)
        {
            return new ProjectComponent(remoteComponent);
        }

        public static ProjectVersion ToLocal(this RemoteVersion remoteVersion)
        {
            return new ProjectVersion(remoteVersion);
        }

        public static Issue ToLocal(this RemoteIssue remoteIssue, Atlassian.Jira.Jira jira = null)
        {
            return new Issue(jira, remoteIssue, null);
        }

        public static Attachment ToLocal(this RemoteAttachment remoteAttachment, Atlassian.Jira.Jira jira, IWebClient webClient)
        {
            return new Attachment(jira, webClient, remoteAttachment);
        }

        public static RemoteIssue ToRemote(this Issue issue)
        {
            return issue.ToRemote();
        }
    }
}

