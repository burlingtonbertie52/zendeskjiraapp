﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="getIssuesFromFilterWithLimit", WrapperNamespace="http://soap.rpc.jira.atlassian.com", IsWrapped=true), DebuggerStepThrough]
    public class getIssuesFromFilterWithLimitRequest
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public string in0;
        [MessageBodyMember(Namespace="", Order=1)]
        public string in1;
        [MessageBodyMember(Namespace="", Order=2)]
        public int in2;
        [MessageBodyMember(Namespace="", Order=3)]
        public int in3;

        public getIssuesFromFilterWithLimitRequest()
        {
        }

        public getIssuesFromFilterWithLimitRequest(string in0, string in1, int in2, int in3)
        {
            this.in0 = in0;
            this.in1 = in1;
            this.in2 = in2;
            this.in3 = in3;
        }
    }
}

