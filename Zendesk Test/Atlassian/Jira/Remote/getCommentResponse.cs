﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="getCommentResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), DebuggerStepThrough]
    public class getCommentResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteComment getCommentReturn;

        public getCommentResponse()
        {
        }

        public getCommentResponse(RemoteComment getCommentReturn)
        {
            this.getCommentReturn = getCommentReturn;
        }
    }
}

