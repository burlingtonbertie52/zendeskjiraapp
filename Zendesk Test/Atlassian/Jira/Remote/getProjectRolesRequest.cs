﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [MessageContract(WrapperName="getProjectRoles", WrapperNamespace="http://soap.rpc.jira.atlassian.com", IsWrapped=true), DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class getProjectRolesRequest
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public string in0;

        public getProjectRolesRequest()
        {
        }

        public getProjectRolesRequest(string in0)
        {
            this.in0 = in0;
        }
    }
}

