﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="getIssuesFromTextSearchResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true)]
    public class getIssuesFromTextSearchResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteIssue[] getIssuesFromTextSearchReturn;

        public getIssuesFromTextSearchResponse()
        {
        }

        public getIssuesFromTextSearchResponse(RemoteIssue[] getIssuesFromTextSearchReturn)
        {
            this.getIssuesFromTextSearchReturn = getIssuesFromTextSearchReturn;
        }
    }
}

