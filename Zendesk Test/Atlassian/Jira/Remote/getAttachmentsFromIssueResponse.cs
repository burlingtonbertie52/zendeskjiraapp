﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="getAttachmentsFromIssueResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true)]
    public class getAttachmentsFromIssueResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteAttachment[] getAttachmentsFromIssueReturn;

        public getAttachmentsFromIssueResponse()
        {
        }

        public getAttachmentsFromIssueResponse(RemoteAttachment[] getAttachmentsFromIssueReturn)
        {
            this.getAttachmentsFromIssueReturn = getAttachmentsFromIssueReturn;
        }
    }
}

