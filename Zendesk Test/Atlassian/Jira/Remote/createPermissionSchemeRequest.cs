﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [MessageContract(WrapperName="createPermissionScheme", WrapperNamespace="http://soap.rpc.jira.atlassian.com", IsWrapped=true), DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class createPermissionSchemeRequest
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public string in0;
        [MessageBodyMember(Namespace="", Order=1)]
        public string in1;
        [MessageBodyMember(Namespace="", Order=2)]
        public string in2;

        public createPermissionSchemeRequest()
        {
        }

        public createPermissionSchemeRequest(string in0, string in1, string in2)
        {
            this.in0 = in0;
            this.in1 = in1;
            this.in2 = in2;
        }
    }
}

