﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, MessageContract(WrapperName="updateIssueResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class updateIssueResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteIssue updateIssueReturn;

        public updateIssueResponse()
        {
        }

        public updateIssueResponse(RemoteIssue updateIssueReturn)
        {
            this.updateIssueReturn = updateIssueReturn;
        }
    }
}

