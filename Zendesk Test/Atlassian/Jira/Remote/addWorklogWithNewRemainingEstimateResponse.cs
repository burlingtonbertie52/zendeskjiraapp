﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [MessageContract(WrapperName="addWorklogWithNewRemainingEstimateResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class addWorklogWithNewRemainingEstimateResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteWorklog addWorklogWithNewRemainingEstimateReturn;

        public addWorklogWithNewRemainingEstimateResponse()
        {
        }

        public addWorklogWithNewRemainingEstimateResponse(RemoteWorklog addWorklogWithNewRemainingEstimateReturn)
        {
            this.addWorklogWithNewRemainingEstimateReturn = addWorklogWithNewRemainingEstimateReturn;
        }
    }
}

