﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Xml.Serialization;

    [Serializable, DebuggerStepThrough, GeneratedCode("svcutil", "3.0.4506.2152"), SoapType(Namespace="http://beans.soap.rpc.jira.atlassian.com"), DesignerCategory("code")]
    public class RemoteResolution : AbstractRemoteConstant
    {
    }
}

