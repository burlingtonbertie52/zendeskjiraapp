﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [GeneratedCode("System.ServiceModel", "3.0.0.0"), DebuggerStepThrough, MessageContract(WrapperName="updateGroupResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true)]
    public class updateGroupResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteGroup updateGroupReturn;

        public updateGroupResponse()
        {
        }

        public updateGroupResponse(RemoteGroup updateGroupReturn)
        {
            this.updateGroupReturn = updateGroupReturn;
        }
    }
}

