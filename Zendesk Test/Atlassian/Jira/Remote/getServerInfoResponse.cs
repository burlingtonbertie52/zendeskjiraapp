﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [MessageContract(WrapperName="getServerInfoResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), GeneratedCode("System.ServiceModel", "3.0.0.0"), DebuggerStepThrough]
    public class getServerInfoResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteServerInfo getServerInfoReturn;

        public getServerInfoResponse()
        {
        }

        public getServerInfoResponse(RemoteServerInfo getServerInfoReturn)
        {
            this.getServerInfoReturn = getServerInfoReturn;
        }
    }
}

