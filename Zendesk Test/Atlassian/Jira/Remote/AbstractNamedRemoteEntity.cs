﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Xml.Serialization;

    [Serializable, SoapInclude(typeof(RemoteProject)), DebuggerStepThrough, DesignerCategory("code"), SoapInclude(typeof(RemoteIssueType)), SoapInclude(typeof(RemoteNamedObject)), SoapType(Namespace="http://beans.soap.rpc.jira.atlassian.com"), SoapInclude(typeof(RemoteFilter)), SoapInclude(typeof(RemoteField)), SoapInclude(typeof(AbstractRemoteConstant)), SoapInclude(typeof(RemoteStatus)), SoapInclude(typeof(RemoteResolution)), SoapInclude(typeof(RemotePriority)), SoapInclude(typeof(RemoteSecurityLevel)), SoapInclude(typeof(RemoteComponent)), SoapInclude(typeof(RemoteVersion)), GeneratedCode("svcutil", "3.0.4506.2152")]
    public abstract class AbstractNamedRemoteEntity : AbstractRemoteEntity
    {
        private string nameField;

        protected AbstractNamedRemoteEntity()
        {
        }

        [SoapElement(IsNullable=true)]
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }
    }
}

