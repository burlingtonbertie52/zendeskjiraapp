﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Xml.Serialization;

    [Serializable, DebuggerStepThrough, SoapType(Namespace="http://beans.soap.rpc.jira.atlassian.com"), GeneratedCode("svcutil", "3.0.4506.2152"), DesignerCategory("code")]
    public class RemoteVersion : AbstractNamedRemoteEntity
    {
        private bool archivedField;
        private DateTime? releaseDateField;
        private bool releasedField;
        private long? sequenceField;

        public bool archived
        {
            get
            {
                return this.archivedField;
            }
            set
            {
                this.archivedField = value;
            }
        }

        public bool released
        {
            get
            {
                return this.releasedField;
            }
            set
            {
                this.releasedField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public DateTime? releaseDate
        {
            get
            {
                return this.releaseDateField;
            }
            set
            {
                this.releaseDateField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public long? sequence
        {
            get
            {
                return this.sequenceField;
            }
            set
            {
                this.sequenceField = value;
            }
        }
    }
}

