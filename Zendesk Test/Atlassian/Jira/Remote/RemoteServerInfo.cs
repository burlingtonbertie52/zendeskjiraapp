﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Xml.Serialization;

    [Serializable, DebuggerStepThrough, SoapType(Namespace="http://beans.soap.rpc.jira.atlassian.com"), DesignerCategory("code"), GeneratedCode("svcutil", "3.0.4506.2152")]
    public class RemoteServerInfo
    {
        private string baseUrlField;
        private DateTime? buildDateField;
        private string buildNumberField;
        private string editionField;
        private RemoteTimeInfo serverTimeField;
        private string versionField;

        [SoapElement(IsNullable=true)]
        public string baseUrl
        {
            get
            {
                return this.baseUrlField;
            }
            set
            {
                this.baseUrlField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public DateTime? buildDate
        {
            get
            {
                return this.buildDateField;
            }
            set
            {
                this.buildDateField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string buildNumber
        {
            get
            {
                return this.buildNumberField;
            }
            set
            {
                this.buildNumberField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string edition
        {
            get
            {
                return this.editionField;
            }
            set
            {
                this.editionField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public RemoteTimeInfo serverTime
        {
            get
            {
                return this.serverTimeField;
            }
            set
            {
                this.serverTimeField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
            }
        }
    }
}

