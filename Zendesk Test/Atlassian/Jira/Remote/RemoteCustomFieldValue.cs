﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Xml.Serialization;

    [Serializable, DebuggerStepThrough, SoapType(Namespace="http://beans.soap.rpc.jira.atlassian.com"), GeneratedCode("svcutil", "3.0.4506.2152"), DesignerCategory("code")]
    public class RemoteCustomFieldValue
    {
        private string customfieldIdField;
        private string keyField;
        private string[] valuesField;

        [SoapElement(IsNullable=true)]
        public string customfieldId
        {
            get
            {
                return this.customfieldIdField;
            }
            set
            {
                this.customfieldIdField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string key
        {
            get
            {
                return this.keyField;
            }
            set
            {
                this.keyField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string[] values
        {
            get
            {
                return this.valuesField;
            }
            set
            {
                this.valuesField = value;
            }
        }
    }
}

