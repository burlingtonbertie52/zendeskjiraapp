﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="getProjectByKeyResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), DebuggerStepThrough]
    public class getProjectByKeyResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteProject getProjectByKeyReturn;

        public getProjectByKeyResponse()
        {
        }

        public getProjectByKeyResponse(RemoteProject getProjectByKeyReturn)
        {
            this.getProjectByKeyReturn = getProjectByKeyReturn;
        }
    }
}

