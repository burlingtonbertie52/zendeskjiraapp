﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [MessageContract(WrapperName="addPermissionToResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class addPermissionToResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemotePermissionScheme addPermissionToReturn;

        public addPermissionToResponse()
        {
        }

        public addPermissionToResponse(RemotePermissionScheme addPermissionToReturn)
        {
            this.addPermissionToReturn = addPermissionToReturn;
        }
    }
}

