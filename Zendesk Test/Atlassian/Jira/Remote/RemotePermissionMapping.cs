﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Xml.Serialization;

    [Serializable, GeneratedCode("svcutil", "3.0.4506.2152"), DebuggerStepThrough, DesignerCategory("code"), SoapType(Namespace="http://beans.soap.rpc.jira.atlassian.com")]
    public class RemotePermissionMapping
    {
        private RemotePermission permissionField;
        private RemoteEntity[] remoteEntitiesField;

        [SoapElement(IsNullable=true)]
        public RemotePermission permission
        {
            get
            {
                return this.permissionField;
            }
            set
            {
                this.permissionField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public RemoteEntity[] remoteEntities
        {
            get
            {
                return this.remoteEntitiesField;
            }
            set
            {
                this.remoteEntitiesField = value;
            }
        }
    }
}

