﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [MessageContract(WrapperName="logoutResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class logoutResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public bool logoutReturn;

        public logoutResponse()
        {
        }

        public logoutResponse(bool logoutReturn)
        {
            this.logoutReturn = logoutReturn;
        }
    }
}

