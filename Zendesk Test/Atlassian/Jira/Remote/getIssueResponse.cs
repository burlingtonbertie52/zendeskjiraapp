﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [MessageContract(WrapperName="getIssueResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class getIssueResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteIssue getIssueReturn;

        public getIssueResponse()
        {
        }

        public getIssueResponse(RemoteIssue getIssueReturn)
        {
            this.getIssueReturn = getIssueReturn;
        }
    }
}

