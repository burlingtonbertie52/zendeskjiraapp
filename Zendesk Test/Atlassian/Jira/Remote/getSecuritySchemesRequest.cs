﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, MessageContract(WrapperName="getSecuritySchemes", WrapperNamespace="http://soap.rpc.jira.atlassian.com", IsWrapped=true), GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class getSecuritySchemesRequest
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public string in0;

        public getSecuritySchemesRequest()
        {
        }

        public getSecuritySchemesRequest(string in0)
        {
            this.in0 = in0;
        }
    }
}

