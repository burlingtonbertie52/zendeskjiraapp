﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.Runtime.CompilerServices;

    [AttributeUsage(AttributeTargets.Property)]
    internal class RemoteFieldNameAttribute : Attribute
    {
        public RemoteFieldNameAttribute(string remoteFieldName)
        {
            this.Name = remoteFieldName;
        }

        public string Name { get; set; }
    }
}

