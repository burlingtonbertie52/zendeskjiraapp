﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [GeneratedCode("System.ServiceModel", "3.0.0.0"), DebuggerStepThrough, MessageContract(WrapperName="updateIssue", WrapperNamespace="http://soap.rpc.jira.atlassian.com", IsWrapped=true)]
    public class updateIssueRequest
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public string in0;
        [MessageBodyMember(Namespace="", Order=1)]
        public string in1;
        [MessageBodyMember(Namespace="", Order=2)]
        public RemoteFieldValue[] in2;

        public updateIssueRequest()
        {
        }

        public updateIssueRequest(string in0, string in1, RemoteFieldValue[] in2)
        {
            this.in0 = in0;
            this.in1 = in1;
            this.in2 = in2;
        }
    }
}

