﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [GeneratedCode("System.ServiceModel", "3.0.0.0"), DebuggerStepThrough, MessageContract(WrapperName="addAttachmentsToIssueResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true)]
    public class addAttachmentsToIssueResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public bool addAttachmentsToIssueReturn;

        public addAttachmentsToIssueResponse()
        {
        }

        public addAttachmentsToIssueResponse(bool addAttachmentsToIssueReturn)
        {
            this.addAttachmentsToIssueReturn = addAttachmentsToIssueReturn;
        }
    }
}

