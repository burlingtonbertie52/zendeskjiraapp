﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [GeneratedCode("System.ServiceModel", "3.0.0.0"), DebuggerStepThrough, MessageContract(WrapperName="getResolutionsResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true)]
    public class getResolutionsResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteResolution[] getResolutionsReturn;

        public getResolutionsResponse()
        {
        }

        public getResolutionsResponse(RemoteResolution[] getResolutionsReturn)
        {
            this.getResolutionsReturn = getResolutionsReturn;
        }
    }
}

