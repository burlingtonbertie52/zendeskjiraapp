﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="getFavouriteFiltersResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true)]
    public class getFavouriteFiltersResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteFilter[] getFavouriteFiltersReturn;

        public getFavouriteFiltersResponse()
        {
        }

        public getFavouriteFiltersResponse(RemoteFilter[] getFavouriteFiltersReturn)
        {
            this.getFavouriteFiltersReturn = getFavouriteFiltersReturn;
        }
    }
}

