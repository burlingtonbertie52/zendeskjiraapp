﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, MessageContract(WrapperName="getAssociatedPermissionSchemesResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class getAssociatedPermissionSchemesResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteScheme[] getAssociatedPermissionSchemesReturn;

        public getAssociatedPermissionSchemesResponse()
        {
        }

        public getAssociatedPermissionSchemesResponse(RemoteScheme[] getAssociatedPermissionSchemesReturn)
        {
            this.getAssociatedPermissionSchemesReturn = getAssociatedPermissionSchemesReturn;
        }
    }
}

