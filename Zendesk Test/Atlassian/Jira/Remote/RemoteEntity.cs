﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Xml.Serialization;

    [Serializable, SoapInclude(typeof(RemoteGroup)), SoapInclude(typeof(RemoteUser)), SoapType(Namespace="http://beans.soap.rpc.jira.atlassian.com"), DebuggerStepThrough, DesignerCategory("code"), GeneratedCode("svcutil", "3.0.4506.2152")]
    public class RemoteEntity
    {
    }
}

