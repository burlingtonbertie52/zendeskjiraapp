﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="getProjectAvatarsResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true)]
    public class getProjectAvatarsResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteAvatar[] getProjectAvatarsReturn;

        public getProjectAvatarsResponse()
        {
        }

        public getProjectAvatarsResponse(RemoteAvatar[] getProjectAvatarsReturn)
        {
            this.getProjectAvatarsReturn = getProjectAvatarsReturn;
        }
    }
}

