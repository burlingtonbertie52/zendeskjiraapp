﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [MessageContract(WrapperName="deletePermissionFrom", WrapperNamespace="http://soap.rpc.jira.atlassian.com", IsWrapped=true), GeneratedCode("System.ServiceModel", "3.0.0.0"), DebuggerStepThrough]
    public class deletePermissionFromRequest
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public string in0;
        [MessageBodyMember(Namespace="", Order=1)]
        public RemotePermissionScheme in1;
        [MessageBodyMember(Namespace="", Order=2)]
        public RemotePermission in2;
        [MessageBodyMember(Namespace="", Order=3)]
        public RemoteEntity in3;

        public deletePermissionFromRequest()
        {
        }

        public deletePermissionFromRequest(string in0, RemotePermissionScheme in1, RemotePermission in2, RemoteEntity in3)
        {
            this.in0 = in0;
            this.in1 = in1;
            this.in2 = in2;
            this.in3 = in3;
        }
    }
}

