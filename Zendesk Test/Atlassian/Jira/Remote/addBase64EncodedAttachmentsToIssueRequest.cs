﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [GeneratedCode("System.ServiceModel", "3.0.0.0"), DebuggerStepThrough, MessageContract(WrapperName="addBase64EncodedAttachmentsToIssue", WrapperNamespace="http://soap.rpc.jira.atlassian.com", IsWrapped=true)]
    public class addBase64EncodedAttachmentsToIssueRequest
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public string in0;
        [MessageBodyMember(Namespace="", Order=1)]
        public string in1;
        [MessageBodyMember(Namespace="", Order=2)]
        public string[] in2;
        [MessageBodyMember(Namespace="", Order=3)]
        public string[] in3;

        public addBase64EncodedAttachmentsToIssueRequest()
        {
        }

        public addBase64EncodedAttachmentsToIssueRequest(string in0, string in1, string[] in2, string[] in3)
        {
            this.in0 = in0;
            this.in1 = in1;
            this.in2 = in2;
            this.in3 = in3;
        }
    }
}

