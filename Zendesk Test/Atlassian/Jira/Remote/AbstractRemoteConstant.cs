﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Xml.Serialization;

    [Serializable, DebuggerStepThrough, SoapType(Namespace="http://beans.soap.rpc.jira.atlassian.com"), SoapInclude(typeof(RemoteStatus)), SoapInclude(typeof(RemoteResolution)), SoapInclude(typeof(RemotePriority)), SoapInclude(typeof(RemoteIssueType)), DesignerCategory("code"), GeneratedCode("svcutil", "3.0.4506.2152")]
    public abstract class AbstractRemoteConstant : AbstractNamedRemoteEntity
    {
        private string descriptionField;
        private string iconField;

        protected AbstractRemoteConstant()
        {
        }

        [SoapElement(IsNullable=true)]
        public string description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string icon
        {
            get
            {
                return this.iconField;
            }
            set
            {
                this.iconField = value;
            }
        }
    }
}

