﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Xml.Serialization;

    [Serializable, SoapType(Namespace="http://beans.soap.rpc.jira.atlassian.com"), DebuggerStepThrough, DesignerCategory("code"), GeneratedCode("svcutil", "3.0.4506.2152")]
    public class RemoteSecurityLevel : AbstractNamedRemoteEntity
    {
        private string descriptionField;

        [SoapElement(IsNullable=true)]
        public string description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }
    }
}

