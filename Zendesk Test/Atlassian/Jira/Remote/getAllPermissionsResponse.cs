﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [MessageContract(WrapperName="getAllPermissionsResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), GeneratedCode("System.ServiceModel", "3.0.0.0"), DebuggerStepThrough]
    public class getAllPermissionsResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemotePermission[] getAllPermissionsReturn;

        public getAllPermissionsResponse()
        {
        }

        public getAllPermissionsResponse(RemotePermission[] getAllPermissionsReturn)
        {
            this.getAllPermissionsReturn = getAllPermissionsReturn;
        }
    }
}

