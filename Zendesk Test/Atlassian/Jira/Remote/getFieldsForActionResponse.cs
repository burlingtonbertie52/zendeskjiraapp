﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [MessageContract(WrapperName="getFieldsForActionResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class getFieldsForActionResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteField[] getFieldsForActionReturn;

        public getFieldsForActionResponse()
        {
        }

        public getFieldsForActionResponse(RemoteField[] getFieldsForActionReturn)
        {
            this.getFieldsForActionReturn = getFieldsForActionReturn;
        }
    }
}

