﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [GeneratedCode("System.ServiceModel", "3.0.0.0"), DebuggerStepThrough, MessageContract(WrapperName="getAssociatedNotificationSchemesResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true)]
    public class getAssociatedNotificationSchemesResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteScheme[] getAssociatedNotificationSchemesReturn;

        public getAssociatedNotificationSchemesResponse()
        {
        }

        public getAssociatedNotificationSchemesResponse(RemoteScheme[] getAssociatedNotificationSchemesReturn)
        {
            this.getAssociatedNotificationSchemesReturn = getAssociatedNotificationSchemesReturn;
        }
    }
}

