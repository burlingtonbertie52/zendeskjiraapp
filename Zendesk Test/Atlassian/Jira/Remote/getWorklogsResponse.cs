﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, MessageContract(WrapperName="getWorklogsResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class getWorklogsResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteWorklog[] getWorklogsReturn;

        public getWorklogsResponse()
        {
        }

        public getWorklogsResponse(RemoteWorklog[] getWorklogsReturn)
        {
            this.getWorklogsReturn = getWorklogsReturn;
        }
    }
}

