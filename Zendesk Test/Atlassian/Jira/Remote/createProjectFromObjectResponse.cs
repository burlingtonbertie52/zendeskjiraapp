﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, MessageContract(WrapperName="createProjectFromObjectResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class createProjectFromObjectResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteProject createProjectFromObjectReturn;

        public createProjectFromObjectResponse()
        {
        }

        public createProjectFromObjectResponse(RemoteProject createProjectFromObjectReturn)
        {
            this.createProjectFromObjectReturn = createProjectFromObjectReturn;
        }
    }
}

