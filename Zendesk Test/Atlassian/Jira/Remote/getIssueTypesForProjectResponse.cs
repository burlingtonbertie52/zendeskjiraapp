﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="getIssueTypesForProjectResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true)]
    public class getIssueTypesForProjectResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteIssueType[] getIssueTypesForProjectReturn;

        public getIssueTypesForProjectResponse()
        {
        }

        public getIssueTypesForProjectResponse(RemoteIssueType[] getIssueTypesForProjectReturn)
        {
            this.getIssueTypesForProjectReturn = getIssueTypesForProjectReturn;
        }
    }
}

