﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Xml.Serialization;

    [Serializable, DesignerCategory("code"), DebuggerStepThrough, SoapType(Namespace="http://beans.soap.rpc.jira.atlassian.com"), GeneratedCode("svcutil", "3.0.4506.2152")]
    public class RemotePriority : AbstractRemoteConstant
    {
        private string colorField;

        [SoapElement(IsNullable=true)]
        public string color
        {
            get
            {
                return this.colorField;
            }
            set
            {
                this.colorField = value;
            }
        }
    }
}

