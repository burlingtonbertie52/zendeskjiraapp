﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [MessageContract(WrapperName="getProjectWithSchemesByIdResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class getProjectWithSchemesByIdResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteProject getProjectWithSchemesByIdReturn;

        public getProjectWithSchemesByIdResponse()
        {
        }

        public getProjectWithSchemesByIdResponse(RemoteProject getProjectWithSchemesByIdReturn)
        {
            this.getProjectWithSchemesByIdReturn = getProjectWithSchemesByIdReturn;
        }
    }
}

