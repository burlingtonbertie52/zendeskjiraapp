﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [GeneratedCode("System.ServiceModel", "3.0.0.0"), DebuggerStepThrough, MessageContract(WrapperName="refreshCustomFields", WrapperNamespace="http://soap.rpc.jira.atlassian.com", IsWrapped=true)]
    public class refreshCustomFieldsRequest
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public string in0;

        public refreshCustomFieldsRequest()
        {
        }

        public refreshCustomFieldsRequest(string in0)
        {
            this.in0 = in0;
        }
    }
}

