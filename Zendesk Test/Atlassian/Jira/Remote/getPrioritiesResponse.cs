﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, MessageContract(WrapperName="getPrioritiesResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class getPrioritiesResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemotePriority[] getPrioritiesReturn;

        public getPrioritiesResponse()
        {
        }

        public getPrioritiesResponse(RemotePriority[] getPrioritiesReturn)
        {
            this.getPrioritiesReturn = getPrioritiesReturn;
        }
    }
}

