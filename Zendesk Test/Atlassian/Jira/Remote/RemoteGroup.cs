﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Xml.Serialization;

    [Serializable, DesignerCategory("code"), GeneratedCode("svcutil", "3.0.4506.2152"), SoapType(Namespace="http://beans.soap.rpc.jira.atlassian.com"), DebuggerStepThrough]
    public class RemoteGroup : RemoteEntity
    {
        private string nameField;
        private RemoteUser[] usersField;

        [SoapElement(IsNullable=true)]
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public RemoteUser[] users
        {
            get
            {
                return this.usersField;
            }
            set
            {
                this.usersField = value;
            }
        }
    }
}

