﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="getResolutionDateById", WrapperNamespace="http://soap.rpc.jira.atlassian.com", IsWrapped=true)]
    public class getResolutionDateByIdRequest
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public string in0;
        [MessageBodyMember(Namespace="", Order=1)]
        public long in1;

        public getResolutionDateByIdRequest()
        {
        }

        public getResolutionDateByIdRequest(string in0, long in1)
        {
            this.in0 = in0;
            this.in1 = in1;
        }
    }
}

