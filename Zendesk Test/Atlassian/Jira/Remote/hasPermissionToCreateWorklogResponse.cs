﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="hasPermissionToCreateWorklogResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true)]
    public class hasPermissionToCreateWorklogResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public bool hasPermissionToCreateWorklogReturn;

        public hasPermissionToCreateWorklogResponse()
        {
        }

        public hasPermissionToCreateWorklogResponse(bool hasPermissionToCreateWorklogReturn)
        {
            this.hasPermissionToCreateWorklogReturn = hasPermissionToCreateWorklogReturn;
        }
    }
}

