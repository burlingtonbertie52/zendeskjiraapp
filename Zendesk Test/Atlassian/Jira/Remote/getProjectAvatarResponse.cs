﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [MessageContract(WrapperName="getProjectAvatarResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class getProjectAvatarResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteAvatar getProjectAvatarReturn;

        public getProjectAvatarResponse()
        {
        }

        public getProjectAvatarResponse(RemoteAvatar getProjectAvatarReturn)
        {
            this.getProjectAvatarReturn = getProjectAvatarReturn;
        }
    }
}

