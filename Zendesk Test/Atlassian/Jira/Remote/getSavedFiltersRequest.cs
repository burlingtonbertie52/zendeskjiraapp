﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="getSavedFilters", WrapperNamespace="http://soap.rpc.jira.atlassian.com", IsWrapped=true)]
    public class getSavedFiltersRequest
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public string in0;

        public getSavedFiltersRequest()
        {
        }

        public getSavedFiltersRequest(string in0)
        {
            this.in0 = in0;
        }
    }
}

