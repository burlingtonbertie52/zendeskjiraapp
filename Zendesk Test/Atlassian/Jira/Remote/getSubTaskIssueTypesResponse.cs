﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [MessageContract(WrapperName="getSubTaskIssueTypesResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class getSubTaskIssueTypesResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteIssueType[] getSubTaskIssueTypesReturn;

        public getSubTaskIssueTypesResponse()
        {
        }

        public getSubTaskIssueTypesResponse(RemoteIssueType[] getSubTaskIssueTypesReturn)
        {
            this.getSubTaskIssueTypesReturn = getSubTaskIssueTypesReturn;
        }
    }
}

