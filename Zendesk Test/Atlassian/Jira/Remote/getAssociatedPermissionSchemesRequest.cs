﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="getAssociatedPermissionSchemes", WrapperNamespace="http://soap.rpc.jira.atlassian.com", IsWrapped=true), DebuggerStepThrough]
    public class getAssociatedPermissionSchemesRequest
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public string in0;
        [MessageBodyMember(Namespace="", Order=1)]
        public RemoteProjectRole in1;

        public getAssociatedPermissionSchemesRequest()
        {
        }

        public getAssociatedPermissionSchemesRequest(string in0, RemoteProjectRole in1)
        {
            this.in0 = in0;
            this.in1 = in1;
        }
    }
}

