﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [MessageContract(WrapperName="progressWorkflowActionResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class progressWorkflowActionResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteIssue progressWorkflowActionReturn;

        public progressWorkflowActionResponse()
        {
        }

        public progressWorkflowActionResponse(RemoteIssue progressWorkflowActionReturn)
        {
            this.progressWorkflowActionReturn = progressWorkflowActionReturn;
        }
    }
}

