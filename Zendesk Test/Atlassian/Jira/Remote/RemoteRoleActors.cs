﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Xml.Serialization;

    [Serializable, DesignerCategory("code"), GeneratedCode("svcutil", "3.0.4506.2152"), SoapInclude(typeof(RemoteProjectRoleActors)), SoapType(Namespace="http://beans.soap.rpc.jira.atlassian.com"), DebuggerStepThrough]
    public class RemoteRoleActors
    {
        private RemoteProjectRole projectRoleField;
        private RemoteRoleActor[] roleActorsField;
        private RemoteUser[] usersField;

        [SoapElement(IsNullable=true)]
        public RemoteProjectRole projectRole
        {
            get
            {
                return this.projectRoleField;
            }
            set
            {
                this.projectRoleField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public RemoteRoleActor[] roleActors
        {
            get
            {
                return this.roleActorsField;
            }
            set
            {
                this.roleActorsField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public RemoteUser[] users
        {
            get
            {
                return this.usersField;
            }
            set
            {
                this.usersField = value;
            }
        }
    }
}

