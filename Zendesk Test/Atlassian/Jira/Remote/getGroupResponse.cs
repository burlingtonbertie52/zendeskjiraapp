﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, MessageContract(WrapperName="getGroupResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class getGroupResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteGroup getGroupReturn;

        public getGroupResponse()
        {
        }

        public getGroupResponse(RemoteGroup getGroupReturn)
        {
            this.getGroupReturn = getGroupReturn;
        }
    }
}

