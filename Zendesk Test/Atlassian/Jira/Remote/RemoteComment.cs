﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Xml.Serialization;

    [Serializable, GeneratedCode("svcutil", "3.0.4506.2152"), DebuggerStepThrough, DesignerCategory("code"), SoapType(Namespace="http://beans.soap.rpc.jira.atlassian.com")]
    public class RemoteComment
    {
        private string authorField;
        private string bodyField;
        private DateTime? createdField;
        private string groupLevelField;
        private string idField;
        private string roleLevelField;
        private string updateAuthorField;
        private DateTime? updatedField;

        [SoapElement(IsNullable=true)]
        public string author
        {
            get
            {
                return this.authorField;
            }
            set
            {
                this.authorField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string body
        {
            get
            {
                return this.bodyField;
            }
            set
            {
                this.bodyField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public DateTime? created
        {
            get
            {
                return this.createdField;
            }
            set
            {
                this.createdField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string groupLevel
        {
            get
            {
                return this.groupLevelField;
            }
            set
            {
                this.groupLevelField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string roleLevel
        {
            get
            {
                return this.roleLevelField;
            }
            set
            {
                this.roleLevelField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string updateAuthor
        {
            get
            {
                return this.updateAuthorField;
            }
            set
            {
                this.updateAuthorField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public DateTime? updated
        {
            get
            {
                return this.updatedField;
            }
            set
            {
                this.updatedField = value;
            }
        }
    }
}

