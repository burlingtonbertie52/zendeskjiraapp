﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Xml.Serialization;

    [Serializable, GeneratedCode("svcutil", "3.0.4506.2152"), DebuggerStepThrough, DesignerCategory("code"), SoapType(Namespace="http://beans.soap.rpc.jira.atlassian.com")]
    public class RemoteIssue : AbstractRemoteEntity
    {
        private RemoteVersion[] affectsVersionsField;
        private string assigneeField;
        private string[] attachmentNamesField;
        private RemoteComponent[] componentsField;
        private DateTime? createdField;
        private RemoteCustomFieldValue[] customFieldValuesField;
        private string descriptionField;
        private DateTime? duedateField;
        private string environmentField;
        private RemoteVersion[] fixVersionsField;
        private string keyField;
        private string priorityField;
        private string projectField;
        private string reporterField;
        private string resolutionField;
        private string statusField;
        private string summaryField;
        private string typeField;
        private DateTime? updatedField;
        private long? votesField;

        [SoapElement(IsNullable=true)]
        public RemoteVersion[] affectsVersions
        {
            get
            {
                return this.affectsVersionsField;
            }
            set
            {
                this.affectsVersionsField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string assignee
        {
            get
            {
                return this.assigneeField;
            }
            set
            {
                this.assigneeField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string[] attachmentNames
        {
            get
            {
                return this.attachmentNamesField;
            }
            set
            {
                this.attachmentNamesField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public RemoteComponent[] components
        {
            get
            {
                return this.componentsField;
            }
            set
            {
                this.componentsField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public DateTime? created
        {
            get
            {
                return this.createdField;
            }
            set
            {
                this.createdField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public RemoteCustomFieldValue[] customFieldValues
        {
            get
            {
                return this.customFieldValuesField;
            }
            set
            {
                this.customFieldValuesField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public DateTime? duedate
        {
            get
            {
                return this.duedateField;
            }
            set
            {
                this.duedateField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string environment
        {
            get
            {
                return this.environmentField;
            }
            set
            {
                this.environmentField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public RemoteVersion[] fixVersions
        {
            get
            {
                return this.fixVersionsField;
            }
            set
            {
                this.fixVersionsField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string key
        {
            get
            {
                return this.keyField;
            }
            set
            {
                this.keyField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string priority
        {
            get
            {
                return this.priorityField;
            }
            set
            {
                this.priorityField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string project
        {
            get
            {
                return this.projectField;
            }
            set
            {
                this.projectField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string reporter
        {
            get
            {
                return this.reporterField;
            }
            set
            {
                this.reporterField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string resolution
        {
            get
            {
                return this.resolutionField;
            }
            set
            {
                this.resolutionField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string summary
        {
            get
            {
                return this.summaryField;
            }
            set
            {
                this.summaryField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public DateTime? updated
        {
            get
            {
                return this.updatedField;
            }
            set
            {
                this.updatedField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public long? votes
        {
            get
            {
                return this.votesField;
            }
            set
            {
                this.votesField = value;
            }
        }
    }
}

