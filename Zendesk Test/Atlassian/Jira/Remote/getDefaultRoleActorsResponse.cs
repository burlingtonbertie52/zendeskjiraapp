﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="getDefaultRoleActorsResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), DebuggerStepThrough]
    public class getDefaultRoleActorsResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteRoleActors getDefaultRoleActorsReturn;

        public getDefaultRoleActorsResponse()
        {
        }

        public getDefaultRoleActorsResponse(RemoteRoleActors getDefaultRoleActorsReturn)
        {
            this.getDefaultRoleActorsReturn = getDefaultRoleActorsReturn;
        }
    }
}

