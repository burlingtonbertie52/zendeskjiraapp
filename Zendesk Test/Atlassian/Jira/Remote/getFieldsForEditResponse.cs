﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="getFieldsForEditResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true)]
    public class getFieldsForEditResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteField[] getFieldsForEditReturn;

        public getFieldsForEditResponse()
        {
        }

        public getFieldsForEditResponse(RemoteField[] getFieldsForEditReturn)
        {
            this.getFieldsForEditReturn = getFieldsForEditReturn;
        }
    }
}

