﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Xml.Serialization;

    [Serializable, DesignerCategory("code"), DebuggerStepThrough, SoapType(Namespace="http://beans.soap.rpc.jira.atlassian.com"), GeneratedCode("svcutil", "3.0.4506.2152")]
    public class RemoteAttachment : AbstractRemoteEntity
    {
        private string authorField;
        private DateTime? createdField;
        private string filenameField;
        private long? filesizeField;
        private string mimetypeField;

        [SoapElement(IsNullable=true)]
        public string author
        {
            get
            {
                return this.authorField;
            }
            set
            {
                this.authorField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public DateTime? created
        {
            get
            {
                return this.createdField;
            }
            set
            {
                this.createdField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string filename
        {
            get
            {
                return this.filenameField;
            }
            set
            {
                this.filenameField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public long? filesize
        {
            get
            {
                return this.filesizeField;
            }
            set
            {
                this.filesizeField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string mimetype
        {
            get
            {
                return this.mimetypeField;
            }
            set
            {
                this.mimetypeField = value;
            }
        }
    }
}

