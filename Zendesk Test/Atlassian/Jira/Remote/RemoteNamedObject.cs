﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Xml.Serialization;

    [Serializable, SoapType(Namespace="http://beans.soap.rpc.jira.atlassian.com"), GeneratedCode("svcutil", "3.0.4506.2152"), DebuggerStepThrough, DesignerCategory("code")]
    public class RemoteNamedObject : AbstractNamedRemoteEntity
    {
    }
}

