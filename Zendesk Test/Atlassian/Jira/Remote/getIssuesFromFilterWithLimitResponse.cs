﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="getIssuesFromFilterWithLimitResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true)]
    public class getIssuesFromFilterWithLimitResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteIssue[] getIssuesFromFilterWithLimitReturn;

        public getIssuesFromFilterWithLimitResponse()
        {
        }

        public getIssuesFromFilterWithLimitResponse(RemoteIssue[] getIssuesFromFilterWithLimitReturn)
        {
            this.getIssuesFromFilterWithLimitReturn = getIssuesFromFilterWithLimitReturn;
        }
    }
}

