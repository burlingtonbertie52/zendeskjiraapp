﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [GeneratedCode("System.ServiceModel", "3.0.0.0"), DebuggerStepThrough, MessageContract(WrapperName="loginResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true)]
    public class loginResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public string loginReturn;

        public loginResponse()
        {
        }

        public loginResponse(string loginReturn)
        {
            this.loginReturn = loginReturn;
        }
    }
}

