﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [MessageContract(WrapperName="deletePermissionFromResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class deletePermissionFromResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemotePermissionScheme deletePermissionFromReturn;

        public deletePermissionFromResponse()
        {
        }

        public deletePermissionFromResponse(RemotePermissionScheme deletePermissionFromReturn)
        {
            this.deletePermissionFromReturn = deletePermissionFromReturn;
        }
    }
}

