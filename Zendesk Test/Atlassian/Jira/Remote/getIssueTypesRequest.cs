﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="getIssueTypes", WrapperNamespace="http://soap.rpc.jira.atlassian.com", IsWrapped=true), DebuggerStepThrough]
    public class getIssueTypesRequest
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public string in0;

        public getIssueTypesRequest()
        {
        }

        public getIssueTypesRequest(string in0)
        {
            this.in0 = in0;
        }
    }
}

