﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Xml.Serialization;

    [Serializable, GeneratedCode("svcutil", "3.0.4506.2152"), DebuggerStepThrough, DesignerCategory("code"), SoapType(Namespace="http://beans.soap.rpc.jira.atlassian.com")]
    public class RemoteProject : AbstractNamedRemoteEntity
    {
        private string descriptionField;
        private RemoteScheme issueSecuritySchemeField;
        private string keyField;
        private string leadField;
        private RemoteScheme notificationSchemeField;
        private RemotePermissionScheme permissionSchemeField;
        private string projectUrlField;
        private string urlField;

        [SoapElement(IsNullable=true)]
        public string description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public RemoteScheme issueSecurityScheme
        {
            get
            {
                return this.issueSecuritySchemeField;
            }
            set
            {
                this.issueSecuritySchemeField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string key
        {
            get
            {
                return this.keyField;
            }
            set
            {
                this.keyField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string lead
        {
            get
            {
                return this.leadField;
            }
            set
            {
                this.leadField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public RemoteScheme notificationScheme
        {
            get
            {
                return this.notificationSchemeField;
            }
            set
            {
                this.notificationSchemeField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public RemotePermissionScheme permissionScheme
        {
            get
            {
                return this.permissionSchemeField;
            }
            set
            {
                this.permissionSchemeField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string projectUrl
        {
            get
            {
                return this.projectUrlField;
            }
            set
            {
                this.projectUrlField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string url
        {
            get
            {
                return this.urlField;
            }
            set
            {
                this.urlField = value;
            }
        }
    }
}

