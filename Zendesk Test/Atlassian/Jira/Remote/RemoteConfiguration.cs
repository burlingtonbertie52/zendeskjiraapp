﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Xml.Serialization;

    [Serializable, GeneratedCode("svcutil", "3.0.4506.2152"), DesignerCategory("code"), SoapType(Namespace="http://beans.soap.rpc.jira.atlassian.com"), DebuggerStepThrough]
    public class RemoteConfiguration
    {
        private bool allowAttachmentsField;
        private bool allowExternalUserManagmentField;
        private bool allowIssueLinkingField;
        private bool allowSubTasksField;
        private bool allowTimeTrackingField;
        private bool allowUnassignedIssuesField;
        private bool allowVotingField;
        private bool allowWatchingField;
        private int timeTrackingDaysPerWeekField;
        private int timeTrackingHoursPerDayField;

        public bool allowAttachments
        {
            get
            {
                return this.allowAttachmentsField;
            }
            set
            {
                this.allowAttachmentsField = value;
            }
        }

        public bool allowExternalUserManagment
        {
            get
            {
                return this.allowExternalUserManagmentField;
            }
            set
            {
                this.allowExternalUserManagmentField = value;
            }
        }

        public bool allowIssueLinking
        {
            get
            {
                return this.allowIssueLinkingField;
            }
            set
            {
                this.allowIssueLinkingField = value;
            }
        }

        public bool allowSubTasks
        {
            get
            {
                return this.allowSubTasksField;
            }
            set
            {
                this.allowSubTasksField = value;
            }
        }

        public bool allowTimeTracking
        {
            get
            {
                return this.allowTimeTrackingField;
            }
            set
            {
                this.allowTimeTrackingField = value;
            }
        }

        public bool allowUnassignedIssues
        {
            get
            {
                return this.allowUnassignedIssuesField;
            }
            set
            {
                this.allowUnassignedIssuesField = value;
            }
        }

        public bool allowVoting
        {
            get
            {
                return this.allowVotingField;
            }
            set
            {
                this.allowVotingField = value;
            }
        }

        public bool allowWatching
        {
            get
            {
                return this.allowWatchingField;
            }
            set
            {
                this.allowWatchingField = value;
            }
        }

        public int timeTrackingDaysPerWeek
        {
            get
            {
                return this.timeTrackingDaysPerWeekField;
            }
            set
            {
                this.timeTrackingDaysPerWeekField = value;
            }
        }

        public int timeTrackingHoursPerDay
        {
            get
            {
                return this.timeTrackingHoursPerDayField;
            }
            set
            {
                this.timeTrackingHoursPerDayField = value;
            }
        }
    }
}

