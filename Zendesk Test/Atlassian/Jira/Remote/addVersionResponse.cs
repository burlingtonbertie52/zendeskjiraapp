﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="addVersionResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true)]
    public class addVersionResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteVersion addVersionReturn;

        public addVersionResponse()
        {
        }

        public addVersionResponse(RemoteVersion addVersionReturn)
        {
            this.addVersionReturn = addVersionReturn;
        }
    }
}

