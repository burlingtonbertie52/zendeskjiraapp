﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="getProjectsNoSchemesResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true)]
    public class getProjectsNoSchemesResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteProject[] getProjectsNoSchemesReturn;

        public getProjectsNoSchemesResponse()
        {
        }

        public getProjectsNoSchemesResponse(RemoteProject[] getProjectsNoSchemesReturn)
        {
            this.getProjectsNoSchemesReturn = getProjectsNoSchemesReturn;
        }
    }
}

