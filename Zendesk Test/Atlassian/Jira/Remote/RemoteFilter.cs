﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Xml.Serialization;

    [Serializable, SoapType(Namespace="http://beans.soap.rpc.jira.atlassian.com"), DebuggerStepThrough, DesignerCategory("code"), GeneratedCode("svcutil", "3.0.4506.2152")]
    public class RemoteFilter : AbstractNamedRemoteEntity
    {
        private string authorField;
        private string descriptionField;
        private string projectField;
        private string xmlField;

        [SoapElement(IsNullable=true)]
        public string author
        {
            get
            {
                return this.authorField;
            }
            set
            {
                this.authorField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string project
        {
            get
            {
                return this.projectField;
            }
            set
            {
                this.projectField = value;
            }
        }

        [SoapElement(IsNullable=true)]
        public string xml
        {
            get
            {
                return this.xmlField;
            }
            set
            {
                this.xmlField = value;
            }
        }
    }
}

