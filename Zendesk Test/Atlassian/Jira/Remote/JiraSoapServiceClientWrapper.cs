﻿namespace Atlassian.Jira.Remote
{
    using System;

    internal class JiraSoapServiceClientWrapper : IJiraSoapServiceClient
    {
        private readonly JiraSoapServiceClient _client;
        private readonly string _url;

        public JiraSoapServiceClientWrapper(string jiraBaseUrl)
        {
            this._client = JiraSoapServiceClientFactory.Create(jiraBaseUrl);
            this._url = jiraBaseUrl.EndsWith("/") ? jiraBaseUrl : (jiraBaseUrl = jiraBaseUrl + "/");
        }

        public void AddActorsToProjectRole(string in0, string[] in1, RemoteProjectRole in2, RemoteProject in3, string in4)
        {
            this._client.addActorsToProjectRole(in0, in1, in2, in3, in4);
        }

        public bool AddBase64EncodedAttachmentsToIssue(string token, string key, string[] fileNames, string[] base64EncodedAttachmentData)
        {
            return this._client.addBase64EncodedAttachmentsToIssue(token, key, fileNames, base64EncodedAttachmentData);
        }

        public void AddComment(string token, string key, RemoteComment comment)
        {
            this._client.addComment(token, key, comment);
        }

        public void AddDefaultActorsToProjectRole(string in0, string[] in1, RemoteProjectRole in2, string in3)
        {
            this._client.addDefaultActorsToProjectRole(in0, in1, in2, in3);
        }

        public RemotePermissionScheme AddPermissionTo(string in0, RemotePermissionScheme in1, RemotePermission in2, RemoteEntity in3)
        {
            return this._client.addPermissionTo(in0, in1, in2, in3);
        }

        public void AddUserToGroup(string in0, RemoteGroup in1, RemoteUser in2)
        {
            this._client.addUserToGroup(in0, in1, in2);
        }

        public RemoteVersion AddVersion(string in0, string in1, RemoteVersion in2)
        {
            return this._client.addVersion(in0, in1, in2);
        }

        public RemoteWorklog AddWorklogAndAutoAdjustRemainingEstimate(string token, string key, RemoteWorklog worklog)
        {
            return this._client.addWorklogAndAutoAdjustRemainingEstimate(token, key, worklog);
        }

        public RemoteWorklog AddWorklogAndRetainRemainingEstimate(string token, string key, RemoteWorklog worklog)
        {
            return this._client.addWorklogAndRetainRemainingEstimate(token, key, worklog);
        }

        public RemoteWorklog AddWorklogWithNewRemainingEstimate(string token, string key, RemoteWorklog worklog, string newRemainingEstimate)
        {
            return this._client.addWorklogWithNewRemainingEstimate(token, key, worklog, newRemainingEstimate);
        }

        public void ArchiveVersion(string in0, string in1, string in2, bool in3)
        {
            this._client.archiveVersion(in0, in1, in2, in3);
        }

        public RemoteGroup CreateGroup(string in0, string in1, RemoteUser in2)
        {
            return this._client.createGroup(in0, in1, in2);
        }

        public RemoteIssue CreateIssue(string token, RemoteIssue newIssue)
        {
            return this._client.createIssue(token, newIssue);
        }

        public RemoteIssue CreateIssueWithParent(string token, RemoteIssue newIssue, string parentIssueKey)
        {
            return this._client.createIssueWithParent(token, newIssue, parentIssueKey);
        }

        public RemoteIssue CreateIssueWithSecurityLevel(string in0, RemoteIssue in1, long in2)
        {
            return this._client.createIssueWithSecurityLevel(in0, in1, in2);
        }

        public RemotePermissionScheme CreatePermissionScheme(string in0, string in1, string in2)
        {
            return this._client.createPermissionScheme(in0, in1, in2);
        }

        public RemoteProject CreateProject(string in0, string in1, string in2, string in3, string in4, string in5, RemotePermissionScheme in6, RemoteScheme in7, RemoteScheme in8)
        {
            return this._client.createProject(in0, in1, in2, in3, in4, in5, in6, in7, in8);
        }

        public RemoteProject CreateProjectFromObject(string in0, RemoteProject in1)
        {
            return this._client.createProjectFromObject(in0, in1);
        }

        public RemoteProjectRole CreateProjectRole(string in0, RemoteProjectRole in1)
        {
            return this._client.createProjectRole(in0, in1);
        }

        public RemoteUser CreateUser(string in0, string in1, string in2, string in3, string in4)
        {
            return this._client.createUser(in0, in1, in2, in3, in4);
        }

        public void DeleteGroup(string in0, string in1, string in2)
        {
            this._client.deleteGroup(in0, in1, in2);
        }

        public void DeleteIssue(string in0, string in1)
        {
            this._client.deleteIssue(in0, in1);
        }

        public RemotePermissionScheme DeletePermissionFrom(string in0, RemotePermissionScheme in1, RemotePermission in2, RemoteEntity in3)
        {
            return this._client.deletePermissionFrom(in0, in1, in2, in3);
        }

        public void DeletePermissionScheme(string in0, string in1)
        {
            this._client.deletePermissionScheme(in0, in1);
        }

        public void DeleteProject(string in0, string in1)
        {
            this._client.deleteProject(in0, in1);
        }

        public void DeleteProjectAvatar(string in0, long in1)
        {
            this._client.deleteProjectAvatar(in0, in1);
        }

        public void DeleteProjectRole(string in0, RemoteProjectRole in1, bool in2)
        {
            this._client.deleteProjectRole(in0, in1, in2);
        }

        public void DeleteUser(string in0, string in1)
        {
            this._client.deleteUser(in0, in1);
        }

        public void DeleteWorklogAndAutoAdjustRemainingEstimate(string in0, string in1)
        {
            this._client.deleteWorklogAndAutoAdjustRemainingEstimate(in0, in1);
        }

        public void DeleteWorklogAndRetainRemainingEstimate(string in0, string in1)
        {
            this._client.deleteWorklogAndRetainRemainingEstimate(in0, in1);
        }

        public void DeleteWorklogWithNewRemainingEstimate(string in0, string in1, string in2)
        {
            this._client.deleteWorklogWithNewRemainingEstimate(in0, in1, in2);
        }

        public RemoteComment EditComment(string in0, RemoteComment in1)
        {
            return this._client.editComment(in0, in1);
        }

        public RemotePermission[] GetAllPermissions(string in0)
        {
            return this._client.getAllPermissions(in0);
        }

        public RemoteScheme[] GetAssociatedNotificationSchemes(string in0, RemoteProjectRole in1)
        {
            return this._client.getAssociatedNotificationSchemes(in0, in1);
        }

        public RemoteScheme[] GetAssociatedPermissionSchemes(string in0, RemoteProjectRole in1)
        {
            return this._client.getAssociatedPermissionSchemes(in0, in1);
        }

        public RemoteAttachment[] GetAttachmentsFromIssue(string token, string key)
        {
            return this._client.getAttachmentsFromIssue(token, key);
        }

        public RemoteNamedObject[] GetAvailableActions(string token, string issueKey)
        {
            return this._client.getAvailableActions(token, issueKey);
        }

        public RemoteComment GetComment(string in0, long in1)
        {
            return this._client.getComment(in0, in1);
        }

        public RemoteComment[] GetComments(string in0, string in1)
        {
            return this._client.getComments(in0, in1);
        }

        public RemoteComment[] GetCommentsFromIssue(string token, string key)
        {
            return this._client.getComments(token, key);
        }

        public RemoteComponent[] GetComponents(string token, string projectKey)
        {
            return this._client.getComponents(token, projectKey);
        }

        public RemoteConfiguration GetConfiguration(string in0)
        {
            return this._client.getConfiguration(in0);
        }

        public RemoteField[] GetCustomFields(string token)
        {
            return this._client.getCustomFields(token);
        }

        public RemoteRoleActors GetDefaultRoleActors(string in0, RemoteProjectRole in1)
        {
            return this._client.getDefaultRoleActors(in0, in1);
        }

        public RemoteFilter[] GetFavouriteFilters(string token)
        {
            return this._client.getFavouriteFilters(token);
        }

        public RemoteField[] GetFieldsForAction(string in0, string in1, string in2)
        {
            return this._client.getFieldsForAction(in0, in1, in2);
        }

        public RemoteField[] GetFieldsForEdit(string token, string key)
        {
            return this._client.getFieldsForEdit(token, key);
        }

        public RemoteGroup GetGroup(string in0, string in1)
        {
            return this._client.getGroup(in0, in1);
        }

        public RemoteIssue GetIssue(string in0, string in1)
        {
            return this._client.getIssue(in0, in1);
        }

        public RemoteIssue GetIssueById(string in0, string in1)
        {
            return this._client.getIssueById(in0, in1);
        }

        public long GetIssueCountForFilter(string in0, string in1)
        {
            return this._client.getIssueCountForFilter(in0, in1);
        }

        public RemoteIssue[] GetIssuesFromFilterWithLimit(string token, string filterId, int offset, int maxResults)
        {
            return this._client.getIssuesFromFilterWithLimit(token, filterId, offset, maxResults);
        }

        public RemoteIssue[] GetIssuesFromJqlSearch(string token, string jqlSearch, int maxNumResults)
        {
            return this._client.getIssuesFromJqlSearch(token, jqlSearch, maxNumResults);
        }

        public RemoteIssue[] GetIssuesFromTextSearchWithLimit(string in0, string in1, int in2, int in3)
        {
            return this._client.getIssuesFromTextSearchWithLimit(in0, in1, in2, in3);
        }

        public RemoteIssue[] GetIssuesFromTextSearchWithProject(string in0, string[] in1, string in2, int in3)
        {
            return this._client.getIssuesFromTextSearchWithProject(in0, in1, in2, in3);
        }

        public RemoteIssueType[] GetIssueTypes(string in0)
        {
            return this._client.getIssueTypes(in0);
        }

        public RemoteIssueType[] GetIssueTypes(string token, string projectId)
        {
            if (string.IsNullOrEmpty(projectId))
            {
                return this._client.getIssueTypes(token);
            }
            return this._client.getIssueTypesForProject(token, projectId);
        }

        public RemoteIssueType[] GetIssueTypesForProject(string in0, string in1)
        {
            return this._client.getIssueTypesForProject(in0, in1);
        }

        public RemoteScheme[] GetNotificationSchemes(string in0)
        {
            return this._client.getNotificationSchemes(in0);
        }

        public RemotePermissionScheme[] GetPermissionSchemes(string in0)
        {
            return this._client.getPermissionSchemes(in0);
        }

        public RemotePriority[] GetPriorities(string token)
        {
            return this._client.getPriorities(token);
        }

        public RemoteAvatar GetProjectAvatar(string in0, string in1)
        {
            return this._client.getProjectAvatar(in0, in1);
        }

        public RemoteAvatar[] GetProjectAvatars(string in0, string in1, bool in2)
        {
            return this._client.getProjectAvatars(in0, in1, in2);
        }

        public RemoteProject GetProjectById(string in0, long in1)
        {
            return this._client.getProjectById(in0, in1);
        }

        public RemoteProject GetProjectByKey(string in0, string in1)
        {
            return this._client.getProjectByKey(in0, in1);
        }

        public RemoteProjectRole GetProjectRole(string in0, long in1)
        {
            return this._client.getProjectRole(in0, in1);
        }

        public RemoteProjectRoleActors GetProjectRoleActors(string in0, RemoteProjectRole in1, RemoteProject in2)
        {
            return this._client.getProjectRoleActors(in0, in1, in2);
        }

        public RemoteProjectRole[] GetProjectRoles(string in0)
        {
            return this._client.getProjectRoles(in0);
        }

        public RemoteProject[] GetProjects(string token)
        {
            return this._client.getProjectsNoSchemes(token);
        }

        public RemoteProject[] GetProjectsNoSchemes(string in0)
        {
            return this._client.getProjectsNoSchemes(in0);
        }

        public RemoteProject GetProjectWithSchemesById(string in0, long in1)
        {
            return this._client.getProjectWithSchemesById(in0, in1);
        }

        public DateTime GetResolutionDateById(string in0, long in1)
        {
            return this._client.getResolutionDateById(in0, in1);
        }

        public DateTime GetResolutionDateByKey(string in0, string in1)
        {
            return this._client.getResolutionDateByKey(in0, in1);
        }

        public RemoteResolution[] GetResolutions(string token)
        {
            return this._client.getResolutions(token);
        }

        public RemoteSecurityLevel GetSecurityLevel(string in0, string in1)
        {
            return this._client.getSecurityLevel(in0, in1);
        }

        public RemoteSecurityLevel[] GetSecurityLevels(string in0, string in1)
        {
            return this._client.getSecurityLevels(in0, in1);
        }

        public RemoteScheme[] GetSecuritySchemes(string in0)
        {
            return this._client.getSecuritySchemes(in0);
        }

        public RemoteServerInfo GetServerInfo(string in0)
        {
            return this._client.getServerInfo(in0);
        }

        public RemoteStatus[] GetStatuses(string token)
        {
            return this._client.getStatuses(token);
        }

        public RemoteIssueType[] GetSubTaskIssueTypes(string in0)
        {
            return this._client.getSubTaskIssueTypes(in0);
        }

        public RemoteIssueType[] GetSubTaskIssueTypesForProject(string in0, string in1)
        {
            return this._client.getSubTaskIssueTypesForProject(in0, in1);
        }

        public RemoteUser GetUser(string in0, string in1)
        {
            return this._client.getUser(in0, in1);
        }

        public RemoteVersion[] GetVersions(string token, string projectKey)
        {
            return this._client.getVersions(token, projectKey);
        }

        public RemoteWorklog[] GetWorklogs(string in0, string in1)
        {
            return this._client.getWorklogs(in0, in1);
        }

        public RemoteWorklog[] GetWorkLogs(string token, string key)
        {
            return this._client.getWorklogs(token, key);
        }

        public bool HasPermissionToCreateWorklog(string in0, string in1)
        {
            return this._client.hasPermissionToCreateWorklog(in0, in1);
        }

        public bool HasPermissionToDeleteWorklog(string in0, string in1)
        {
            return this._client.hasPermissionToDeleteWorklog(in0, in1);
        }

        public bool HasPermissionToEditComment(string in0, RemoteComment in1)
        {
            return this._client.hasPermissionToEditComment(in0, in1);
        }

        public bool HasPermissionToUpdateWorklog(string in0, string in1)
        {
            return this._client.hasPermissionToUpdateWorklog(in0, in1);
        }

        public bool IsProjectRoleNameUnique(string in0, string in1)
        {
            return this._client.isProjectRoleNameUnique(in0, in1);
        }

        public string Login(string username, string password)
        {
            return this._client.login(username, password);
        }

        public bool Logout(string in0)
        {
            return this._client.logout(in0);
        }

        public RemoteIssue ProgressWorkflowAction(string token, string issueKey, string actionId, RemoteFieldValue[] remoteFieldValues)
        {
            return this._client.progressWorkflowAction(token, issueKey, actionId, remoteFieldValues);
        }

        public void RefreshCustomFields(string in0)
        {
            this._client.refreshCustomFields(in0);
        }

        public void ReleaseVersion(string in0, string in1, RemoteVersion in2)
        {
            this._client.releaseVersion(in0, in1, in2);
        }

        public void RemoveActorsFromProjectRole(string in0, string[] in1, RemoteProjectRole in2, RemoteProject in3, string in4)
        {
            this._client.removeActorsFromProjectRole(in0, in1, in2, in3, in4);
        }

        public void RemoveAllRoleActorsByNameAndType(string in0, string in1, string in2)
        {
            this._client.removeAllRoleActorsByNameAndType(in0, in1, in2);
        }

        public void RemoveAllRoleActorsByProject(string in0, RemoteProject in1)
        {
            this._client.removeAllRoleActorsByProject(in0, in1);
        }

        public void RemoveDefaultActorsFromProjectRole(string in0, string[] in1, RemoteProjectRole in2, string in3)
        {
            this._client.removeDefaultActorsFromProjectRole(in0, in1, in2, in3);
        }

        public void RemoveUserFromGroup(string in0, RemoteGroup in1, RemoteUser in2)
        {
            this._client.removeUserFromGroup(in0, in1, in2);
        }

        public void SetNewProjectAvatar(string in0, string in1, string in2, string in3)
        {
            this._client.setNewProjectAvatar(in0, in1, in2, in3);
        }

        public void SetProjectAvatar(string in0, string in1, long in2)
        {
            this._client.setProjectAvatar(in0, in1, in2);
        }

        public RemoteGroup UpdateGroup(string in0, RemoteGroup in1)
        {
            return this._client.updateGroup(in0, in1);
        }

        public RemoteIssue UpdateIssue(string token, string key, RemoteFieldValue[] fields)
        {
            return this._client.updateIssue(token, key, fields);
        }

        public RemoteProject UpdateProject(string in0, RemoteProject in1)
        {
            return this._client.updateProject(in0, in1);
        }

        public void UpdateProjectRole(string in0, RemoteProjectRole in1)
        {
            this._client.updateProjectRole(in0, in1);
        }

        public void UpdateWorklogAndAutoAdjustRemainingEstimate(string in0, RemoteWorklog in1)
        {
            this._client.updateWorklogAndAutoAdjustRemainingEstimate(in0, in1);
        }

        public void UpdateWorklogAndRetainRemainingEstimate(string in0, RemoteWorklog in1)
        {
            this._client.updateWorklogAndRetainRemainingEstimate(in0, in1);
        }

        public void UpdateWorklogWithNewRemainingEstimate(string in0, RemoteWorklog in1, string in2)
        {
            this._client.updateWorklogWithNewRemainingEstimate(in0, in1, in2);
        }

        public string Url
        {
            get
            {
                return this._url;
            }
        }
    }
}

