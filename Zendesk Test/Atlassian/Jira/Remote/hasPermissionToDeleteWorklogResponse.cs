﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [DebuggerStepThrough, MessageContract(WrapperName="hasPermissionToDeleteWorklogResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), GeneratedCode("System.ServiceModel", "3.0.0.0")]
    public class hasPermissionToDeleteWorklogResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public bool hasPermissionToDeleteWorklogReturn;

        public hasPermissionToDeleteWorklogResponse()
        {
        }

        public hasPermissionToDeleteWorklogResponse(bool hasPermissionToDeleteWorklogReturn)
        {
            this.hasPermissionToDeleteWorklogReturn = hasPermissionToDeleteWorklogReturn;
        }
    }
}

