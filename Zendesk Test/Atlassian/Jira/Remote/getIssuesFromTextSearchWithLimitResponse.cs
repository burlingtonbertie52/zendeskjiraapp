﻿namespace Atlassian.Jira.Remote
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.ServiceModel;

    [GeneratedCode("System.ServiceModel", "3.0.0.0"), MessageContract(WrapperName="getIssuesFromTextSearchWithLimitResponse", WrapperNamespace="http://jira.atlassian.com/rpc/soap/jirasoapservice-v2", IsWrapped=true), DebuggerStepThrough]
    public class getIssuesFromTextSearchWithLimitResponse
    {
        [MessageBodyMember(Namespace="", Order=0)]
        public RemoteIssue[] getIssuesFromTextSearchWithLimitReturn;

        public getIssuesFromTextSearchWithLimitResponse()
        {
        }

        public getIssuesFromTextSearchWithLimitResponse(RemoteIssue[] getIssuesFromTextSearchWithLimitReturn)
        {
            this.getIssuesFromTextSearchWithLimitReturn = getIssuesFromTextSearchWithLimitReturn;
        }
    }
}

