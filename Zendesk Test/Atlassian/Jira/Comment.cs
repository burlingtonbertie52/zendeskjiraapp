﻿namespace Atlassian.Jira
{
    using Atlassian.Jira.Remote;
    using System;

    public class Comment
    {
        private readonly RemoteComment _remoteComment;

        public Comment() : this(new RemoteComment())
        {
        }

        internal Comment(RemoteComment remoteComment)
        {
            this._remoteComment = remoteComment;
        }

        internal RemoteComment toRemote()
        {
            return this._remoteComment;
        }

        public string Author
        {
            get
            {
                return this._remoteComment.author;
            }
            set
            {
                this._remoteComment.author = value;
            }
        }

        public string Body
        {
            get
            {
                return this._remoteComment.body;
            }
            set
            {
                this._remoteComment.body = value;
            }
        }

        public DateTime? CreatedDate
        {
            get
            {
                return this._remoteComment.created;
            }
        }

        public string GroupLevel
        {
            get
            {
                return this._remoteComment.groupLevel;
            }
            set
            {
                this._remoteComment.groupLevel = value;
            }
        }

        public string Id
        {
            get
            {
                return this._remoteComment.id;
            }
        }

        public string RoleLevel
        {
            get
            {
                return this._remoteComment.roleLevel;
            }
            set
            {
                this._remoteComment.roleLevel = value;
            }
        }

        public string UpdateAuthor
        {
            get
            {
                return this._remoteComment.updateAuthor;
            }
        }

        public DateTime? UpdatedDate
        {
            get
            {
                return this._remoteComment.updated;
            }
        }
    }
}

