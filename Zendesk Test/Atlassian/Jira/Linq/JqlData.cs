﻿namespace Atlassian.Jira.Linq
{
    using System;
    using System.Runtime.CompilerServices;

    public class JqlData
    {
        public string Expression { get; set; }

        public int? NumberOfResults { get; set; }
    }
}

