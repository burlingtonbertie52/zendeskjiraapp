﻿namespace Atlassian.Jira.Linq
{
    using Atlassian.Jira;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Runtime.InteropServices;
    using System.Text;

    public class JqlExpressionVisitor : ExpressionVisitor, IJqlExpressionVisitor
    {
        private StringBuilder _jqlOrderBy;
        private StringBuilder _jqlWhere;
        private int? _numberOfResults;

        private string GetFieldNameFromBinaryExpression(BinaryExpression expression)
        {
            PropertyInfo propertyInfo = null;
            if (this.TryGetPropertyInfoFromBinaryExpression(expression, out propertyInfo))
            {
                object[] customAttributes = propertyInfo.GetCustomAttributes(typeof(JqlFieldNameAttribute), true);
                if (customAttributes.Count<object>() > 0)
                {
                    return ((JqlFieldNameAttribute) customAttributes[0]).Name;
                }
                return propertyInfo.Name;
            }
            MethodCallExpression left = expression.Left as MethodCallExpression;
            if (left == null)
            {
                throw new NotSupportedException(string.Format("Operator '{0}' can only be applied on properties and property indexers.", expression.NodeType));
            }
            return string.Format("\"{0}\"", ((ConstantExpression) left.Arguments[0]).Value);
        }

        private object GetFieldValueFromBinaryExpression(BinaryExpression expression)
        {
            if (expression.Right.NodeType == ExpressionType.Constant)
            {
                return ((ConstantExpression) expression.Right).Value;
            }
            if (expression.Right.NodeType != ExpressionType.New)
            {
                throw new NotSupportedException(string.Format("Operator '{0}' can only be used with constant values.", expression.NodeType));
            }
            NewExpression right = (NewExpression) expression.Right;
            List<object> list = new List<object>();
            foreach (ConstantExpression expression3 in right.Arguments)
            {
                list.Add(expression3.Value);
            }
            return right.Constructor.Invoke(list.ToArray());
        }

        public JqlData Process(Expression expression)
        {
            expression = ExpressionEvaluator.PartialEval(expression);
            this._jqlWhere = new StringBuilder();
            this._jqlOrderBy = new StringBuilder();
            this.Visit(expression);
            return new JqlData { Expression = this.Jql, NumberOfResults = this._numberOfResults };
        }

        private void ProcessConstant(object value)
        {
            Type type = value.GetType();
            if (((type == typeof(string)) || (type == typeof(ComparableString))) || ((type == typeof(LiteralDateTime)) || (type == typeof(LiteralMatch))))
            {
                this._jqlWhere.Append(string.Format("\"{0}\"", value));
            }
            else if (type == typeof(DateTime))
            {
                string str = ((DateTime) value).ToString("yyyy/MM/dd", Atlassian.Jira.Jira.DefaultCultureInfo);
                this._jqlWhere.Append(string.Format("\"{0}\"", str));
            }
            else
            {
                this._jqlWhere.Append(value);
            }
        }

        private void ProcessEqualityOperator(BinaryExpression expression, bool equal)
        {
            if (expression.Left is MemberExpression)
            {
                this.ProcessMemberEqualityOperator(expression, equal);
            }
            else if (expression.Left is MethodCallExpression)
            {
                this.ProcessIndexedMemberEqualityOperator(expression, equal);
            }
        }

        private void ProcessGreaterAndLessThanOperator(BinaryExpression expression, string operatorString)
        {
            string fieldNameFromBinaryExpression = this.GetFieldNameFromBinaryExpression(expression);
            object fieldValueFromBinaryExpression = this.GetFieldValueFromBinaryExpression(expression);
            this._jqlWhere.Append(fieldNameFromBinaryExpression);
            this._jqlWhere.Append(string.Format(" {0} ", operatorString));
            this.ProcessConstant(fieldValueFromBinaryExpression);
        }

        private void ProcessIndexedMemberEqualityOperator(BinaryExpression expression, bool equal)
        {
            string str2;
            string fieldNameFromBinaryExpression = this.GetFieldNameFromBinaryExpression(expression);
            object fieldValueFromBinaryExpression = this.GetFieldValueFromBinaryExpression(expression);
            this._jqlWhere.Append(fieldNameFromBinaryExpression);
            if (fieldValueFromBinaryExpression is string)
            {
                str2 = equal ? "~" : "!~";
            }
            else
            {
                str2 = equal ? "=" : "!=";
            }
            this._jqlWhere.Append(string.Format(" {0} ", str2));
            this.ProcessConstant(fieldValueFromBinaryExpression);
        }

        private void ProcessMemberEqualityOperator(BinaryExpression expression, bool equal)
        {
            string fieldNameFromBinaryExpression = this.GetFieldNameFromBinaryExpression(expression);
            object fieldValueFromBinaryExpression = this.GetFieldValueFromBinaryExpression(expression);
            this._jqlWhere.Append(fieldNameFromBinaryExpression);
            if ((fieldValueFromBinaryExpression == null) || fieldValueFromBinaryExpression.Equals(""))
            {
                this._jqlWhere.Append(" ");
                this._jqlWhere.Append(equal ? "is" : "is not");
                this._jqlWhere.Append(" ");
                this._jqlWhere.Append((fieldValueFromBinaryExpression == null) ? "null" : "empty");
            }
            else
            {
                string str2 = string.Empty;
                PropertyInfo propertyInfo = null;
                if (this.TryGetPropertyInfoFromBinaryExpression(expression, out propertyInfo) && (propertyInfo.GetCustomAttributes(typeof(JqlContainsEqualityAttribute), true).Count<object>() > 0))
                {
                    str2 = equal ? "~" : "!~";
                }
                else
                {
                    str2 = equal ? "=" : "!=";
                }
                this._jqlWhere.Append(string.Format(" {0} ", str2));
                this.ProcessConstant(fieldValueFromBinaryExpression);
            }
        }

        private void ProcessOrderBy(MethodCallExpression node)
        {
            bool flag = this._jqlOrderBy.Length == 0;
            if (flag)
            {
                this._jqlOrderBy.Append(" order by ");
            }
            MemberExpression body = ((LambdaExpression) ((UnaryExpression) node.Arguments[1]).Operand).Body as MemberExpression;
            if (body != null)
            {
                if (flag)
                {
                    this._jqlOrderBy.Append(body.Member.Name);
                }
                else
                {
                    this._jqlOrderBy.Insert(10, body.Member.Name + ", ");
                }
            }
            if ((node.Method.Name == "OrderByDescending") || (node.Method.Name == "ThenByDescending"))
            {
                this._jqlOrderBy.Append(" desc");
            }
        }

        private void ProcessTake(MethodCallExpression node)
        {
            this._numberOfResults = new int?(int.Parse(((ConstantExpression) node.Arguments[1]).Value.ToString()));
        }

        private void ProcessUnionOperator(BinaryExpression expression, string operatorString)
        {
            this._jqlWhere.Append("(");
            this.Visit(expression.Left);
            this._jqlWhere.Append(" " + operatorString + " ");
            this.Visit(expression.Right);
            this._jqlWhere.Append(")");
        }

        private bool TryGetPropertyInfoFromBinaryExpression(BinaryExpression expression, out PropertyInfo propertyInfo)
        {
            MemberExpression left = expression.Left as MemberExpression;
            if (left != null)
            {
                propertyInfo = left.Member as PropertyInfo;
                if (propertyInfo != null)
                {
                    return true;
                }
            }
            propertyInfo = null;
            return false;
        }

        protected override Expression VisitBinary(BinaryExpression node)
        {
            switch (node.NodeType)
            {
                case ExpressionType.Equal:
                    this.ProcessEqualityOperator(node, true);
                    return node;

                case ExpressionType.GreaterThan:
                    this.ProcessGreaterAndLessThanOperator(node, ">");
                    return node;

                case ExpressionType.GreaterThanOrEqual:
                    this.ProcessGreaterAndLessThanOperator(node, ">=");
                    return node;

                case ExpressionType.LessThan:
                    this.ProcessGreaterAndLessThanOperator(node, "<");
                    return node;

                case ExpressionType.LessThanOrEqual:
                    this.ProcessGreaterAndLessThanOperator(node, "<=");
                    return node;

                case ExpressionType.AndAlso:
                    this.ProcessUnionOperator(node, "and");
                    return node;

                case ExpressionType.NotEqual:
                    this.ProcessEqualityOperator(node, false);
                    return node;

                case ExpressionType.OrElse:
                    this.ProcessUnionOperator(node, "or");
                    return node;
            }
            throw new NotSupportedException(string.Format("Expression type '{0}' is not supported.", node.NodeType));
        }

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            if (((node.Method.Name == "OrderBy") || (node.Method.Name == "OrderByDescending")) || ((node.Method.Name == "ThenBy") || (node.Method.Name == "ThenByDescending")))
            {
                this.ProcessOrderBy(node);
            }
            else if (node.Method.Name == "Take")
            {
                this.ProcessTake(node);
            }
            return base.VisitMethodCall(node);
        }

        public string Jql
        {
            get
            {
                return (this._jqlWhere.ToString() + this._jqlOrderBy.ToString());
            }
        }

        public int? NumberOfResults
        {
            get
            {
                return this._numberOfResults;
            }
        }
    }
}

