﻿namespace Atlassian.Jira.Linq
{
    using System;
    using System.Runtime.CompilerServices;

    [AttributeUsage(AttributeTargets.Property)]
    internal class JqlFieldNameAttribute : Attribute
    {
        public JqlFieldNameAttribute(string name)
        {
            this.Name = name;
        }

        public string Name { get; set; }
    }
}

