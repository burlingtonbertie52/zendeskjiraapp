﻿namespace Atlassian.Jira.Linq
{
    using Atlassian.Jira;
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    public class JiraQueryProvider : IQueryProvider
    {
        private readonly Atlassian.Jira.Jira _jiraServer;
        private readonly IJqlExpressionVisitor _translator;

        public JiraQueryProvider(IJqlExpressionVisitor translator, Atlassian.Jira.Jira jiraInstance)
        {
            this._translator = translator;
            this._jiraServer = jiraInstance;
        }

        public IQueryable CreateQuery(Expression expression)
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> CreateQuery<T>(Expression expression)
        {
            return new JiraQueryable<T>(this, expression);
        }

        public object Execute(Expression expression)
        {
            return this.Execute(expression, true);
        }

        public T Execute<T>(Expression expression)
        {
            bool isEnumerable = typeof(T).Name == "IEnumerable`1";
            return (T) this.Execute(expression, isEnumerable);
        }

        private object Execute(Expression expression, bool isEnumerable)
        {
            JqlData data = this._translator.Process(expression);
            IQueryable<Issue> queryableIssues = this._jiraServer.GetIssuesFromJql(data.Expression, data.NumberOfResults).AsQueryable<Issue>();
            if (isEnumerable)
            {
                return queryableIssues;
            }
            Expression expression2 = new ExpressionTreeModifier(queryableIssues).Visit(expression);
            return queryableIssues.Provider.Execute(expression2);
        }
    }
}

