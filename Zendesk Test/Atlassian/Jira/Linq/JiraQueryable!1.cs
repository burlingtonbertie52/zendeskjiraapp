﻿namespace Atlassian.Jira.Linq
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    public class JiraQueryable<T> : IOrderedQueryable<T>, IQueryable<T>, IOrderedQueryable, IEnumerable<T>, IQueryable, IEnumerable
    {
        private readonly System.Linq.Expressions.Expression _expression;
        private readonly JiraQueryProvider _provider;

        public JiraQueryable(JiraQueryProvider provider)
        {
            this._provider = provider;
            this._expression = System.Linq.Expressions.Expression.Constant(this);
        }

        public JiraQueryable(JiraQueryProvider provider, System.Linq.Expressions.Expression expression)
        {
            this._provider = provider;
            this._expression = expression;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return ((IEnumerable<T>) this._provider.Execute(this.Expression)).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable) this._provider.Execute(this.Expression)).GetEnumerator();
        }

        public Type ElementType
        {
            get
            {
                return typeof(T);
            }
        }

        public System.Linq.Expressions.Expression Expression
        {
            get
            {
                return this._expression;
            }
        }

        public IQueryProvider Provider
        {
            get
            {
                return this._provider;
            }
        }
    }
}

