﻿namespace Atlassian.Jira.Linq
{
    using System;

    public class JiraOperators
    {
        public const string AND = "and";
        public const string CONTAINS = "~";
        public const string EQUALS = "=";
        public const string GREATERTHAN = ">";
        public const string GREATERTHANOREQUALS = ">=";
        public const string IS = "is";
        public const string ISNOT = "is not";
        public const string LESSTHAN = "<";
        public const string LESSTHANOREQUALS = "<=";
        public const string NOTCONTAINS = "!~";
        public const string NOTEQUALS = "!=";
        public const string OR = "or";
    }
}

