﻿namespace Atlassian.Jira.Linq
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    internal class ExpressionTreeModifier : ExpressionVisitor
    {
        private readonly IQueryable<Issue> _queryableIssues;

        public ExpressionTreeModifier(IQueryable<Issue> queryableIssues)
        {
            this._queryableIssues = queryableIssues;
        }

        protected override Expression VisitConstant(ConstantExpression node)
        {
            if (node.Type == typeof(JiraQueryable<Issue>))
            {
                return Expression.Constant(this._queryableIssues);
            }
            return node;
        }

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            if (((!(node.Method.Name == "Where") && !(node.Method.Name == "Take")) && (!(node.Method.Name == "OrderBy") && !(node.Method.Name == "OrderByDescending"))) && (!(node.Method.Name == "ThenBy") && !(node.Method.Name == "ThenByDescending")))
            {
                return base.VisitMethodCall(node);
            }
            return Expression.Constant(this._queryableIssues);
        }
    }
}

