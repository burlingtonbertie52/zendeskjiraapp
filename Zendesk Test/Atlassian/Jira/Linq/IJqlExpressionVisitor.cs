﻿namespace Atlassian.Jira.Linq
{
    using System.Linq.Expressions;

    public interface IJqlExpressionVisitor
    {
        JqlData Process(Expression expression);
    }
}

