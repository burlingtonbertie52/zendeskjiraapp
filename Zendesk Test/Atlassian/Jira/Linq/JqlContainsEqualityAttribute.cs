﻿namespace Atlassian.Jira.Linq
{
    using System;

    [AttributeUsage(AttributeTargets.Property)]
    internal class JqlContainsEqualityAttribute : Attribute
    {
    }
}

