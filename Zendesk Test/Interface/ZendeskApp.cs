﻿namespace Interface
{
    using Atlassian.Jira;
    using Engine;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using ZendeskApi_v2;
    using ZendeskApi_v2.Models.Search;
    using ZendeskApi_v2.Models.Tickets;

    public class ZendeskApp
    {
        private ZendeskApi client;
        private const int ConsoleHeight = 50;
        private Atlassian.Jira.Jira jiraServer;
        private const int Maxwidth = 160;
      //  private Dictionary<Result, Issue> ticketData;
        private XmlOptions xmlOptions;

        
        private long userid=0L;

        public ZendeskApp()
        {
           // SetupConsole();
            this.CheckForOptionsFile();
            this.SetupClients();
            //Console.Clear();
            //long userid = 0L;
            try
            {
                userid = this.SearchForUserId(this.xmlOptions.Email);
            }
            catch
            {
                //Console.WriteLine("No user found.");
                //Console.ReadKey();
                Environment.Exit(0);
            }
            //while (true)
            //{
            //    this.MapTicketAndJiraIssues(userid);
            //    //this.PrintTickets();
            //   // Console.ReadKey();
            //  //  SetupConsole();
            //}
        }

        private void CheckForOptionsFile()
        {
            try
            {
                this.xmlOptions = XmlOptionsSerializer.Deserialize("options.xml");
            }
            catch (FileNotFoundException)
            {
                char ch;
               // bool flag;
                Console.WriteLine("Options file not found. Create it? (Y) = Yes, (N) = No");
                goto Label_00C8;
            Label_002A:
                ch = Console.ReadKey().KeyChar;
                if (ch.ToString().ToLower() == "y")
                {
                    Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
                    this.CreateOptionsFile();
                    return;
                }
                if (ch.ToString().ToLower() == "n")
                {
                    Environment.Exit(0);
                }
                Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
                Console.Write(" ");
                Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
            Label_00C8:
               // flag = true;
                goto Label_002A;
            }
        }

        private void CreateOptionsFile()
        {
            Console.WriteLine("Enter your email address : ");
            string email = Console.ReadLine();
            XmlOptions options = new XmlOptions("https://redgatesupport.zendesk.com/", email, "cptJbrkZB9X9hU0XxiJ47cAxYu30ZFLpnWb1fWz8", "https://jira.red-gate.com/");
            XmlOptionsSerializer.Serialize(options);
            this.xmlOptions = options;
        }

        public  Dictionary<Result, Issue>  MapTicketAndJiraIssues()
        {
            Dictionary<Result, Issue> ticketData = new Dictionary<Result, Issue>();
            SearchResults results = this.client.Search.SearchFor("status:hold+assignee_id:" + userid, "", "", 1);
            string key = string.Empty;
            //Console.Write("Loading data.");
            foreach (Result result in results.Results)
            {
             //   Console.Write(".");
                IEnumerable<object> source = from customf in result.CustomFields
                    where customf.Id == 0x163969cL
                    select customf.Value;
                key = (source.ElementAt<object>(0) != null) ? source.ElementAt<object>(0).ToString() : "N/A";
                try
                {
                    Issue issue = this.jiraServer.GetIssue(key);
                    ticketData.Add(result, issue);
                }
                catch (Exception)
                {
                    ticketData.Add(result, null);
                }
            }
           // Console.Clear();

            return ticketData;


        }

      

        private long SearchForUserId(string user)
        {
            return (long) this.client.Search.SearchFor("email:" + user + ", organization_id:21636362", "", "", 1).Results[0].Id;
        }

        private void SetupClients()
        {
            this.client = new ZendeskApi(this.xmlOptions.Url, this.xmlOptions.Email, "", this.xmlOptions.Apitoken, "en-us");
            this.jiraServer = new Atlassian.Jira.Jira(this.xmlOptions.JiraUrl);
        }

        private static void SetupConsole()
        {
            Console.Clear();
            Console.Title = "On Hold Ticket Checker";
            Console.SetWindowSize(0xa1, 50);
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
        }
    }
}

