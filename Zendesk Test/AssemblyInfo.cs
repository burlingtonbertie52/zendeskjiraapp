﻿// Assembly Zendesk Test, Version 1.0.0.0

//m[assembly: System.Reflection.AssemblyAlgorithmId(0)]
[assembly: System.Reflection.AssemblyCompany("")]
[assembly: System.Reflection.AssemblyProduct("Zendesk Test")]
[assembly: System.Runtime.CompilerServices.RuntimeCompatibility(WrapNonExceptionThrows=true)]

[assembly: System.Reflection.AssemblyTitle("Zendesk Test")]
[assembly: System.Reflection.AssemblyDescription("")]
[assembly: System.Reflection.AssemblyConfiguration("")]
[assembly: System.Runtime.InteropServices.ComVisible(false)]
[assembly: System.Reflection.AssemblyCopyright("Copyright \x00a9  2015")]
[assembly: System.Reflection.AssemblyTrademark("")]
[assembly: System.Reflection.AssemblyFileVersion("1.0.0.0")]
[assembly: System.Runtime.CompilerServices.CompilationRelaxations(8)]
[assembly: System.Runtime.InteropServices.Guid("fa8f207f-6628-48bf-9141-41675438f8f2")]
[assembly: System.Diagnostics.Debuggable(System.Diagnostics.DebuggableAttribute.DebuggingModes.DisableOptimizations | System.Diagnostics.DebuggableAttribute.DebuggingModes.EnableEditAndContinue | System.Diagnostics.DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints | System.Diagnostics.DebuggableAttribute.DebuggingModes.Default)]
[assembly: System.CLSCompliant(true)]


