﻿namespace Engine
{
    using System;
    using System.Runtime.CompilerServices;

    public class XmlOptions
    {
        public XmlOptions()
        {
        }

        public XmlOptions(string url, string email, string apitoken, string jiraUrl)
        {
            this.Url = url;
            this.Email = email;
            this.Apitoken = apitoken;
            this.JiraUrl = jiraUrl;
        }

        public string Apitoken { get; set; }

        public string Email { get; set; }

        public string JiraUrl { get; set; }

        public string Url { get; set; }
    }
}

