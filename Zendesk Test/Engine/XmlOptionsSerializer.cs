﻿namespace Engine
{
    using System;
    using System.IO;
    using System.Xml.Serialization;

    public static class XmlOptionsSerializer
    {
        public static string lastErrorMessage;

        public static XmlOptions Deserialize(string filename)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(XmlOptions));
            FileStream stream = new FileStream(filename, FileMode.Open);
            return (XmlOptions) serializer.Deserialize(stream);
        }

        public static bool Serialize(XmlOptions options)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(XmlOptions));
            StreamWriter writer = new StreamWriter("options.xml");
            try
            {
                serializer.Serialize((TextWriter) writer, options);
                return true;
            }
            catch (Exception exception)
            {
                lastErrorMessage = exception.Message;
                return false;
            }
        }
    }
}

