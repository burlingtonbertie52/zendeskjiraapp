﻿namespace Newtonsoft.Json.Linq
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Utilities;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Dynamic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Threading;

    public class JObject : JContainer, IEnumerable, IDictionary<string, JToken>, ICollection<KeyValuePair<string, JToken>>, IEnumerable<KeyValuePair<string, JToken>>, INotifyPropertyChanged, ICustomTypeDescriptor, INotifyPropertyChanging
    {
        private readonly JPropertyKeyedCollection _properties;

        public event PropertyChangedEventHandler PropertyChanged;

        public event PropertyChangingEventHandler PropertyChanging;

        public JObject()
        {
            this._properties = new JPropertyKeyedCollection();
        }

        public JObject(JObject other) : base(other)
        {
            this._properties = new JPropertyKeyedCollection();
        }

        public JObject(params object[] content) : this(content)
        {
        }

        public JObject(object content)
        {
            this._properties = new JPropertyKeyedCollection();
            this.Add(content);
        }

        public void Add(string propertyName, JToken value)
        {
            this.Add(new JProperty(propertyName, value));
        }

        internal override JToken CloneToken()
        {
            return new JObject(this);
        }

        internal override bool DeepEquals(JToken node)
        {
            JObject obj2 = node as JObject;
            if (obj2 == null)
            {
                return false;
            }
            return this._properties.Compare(obj2._properties);
        }

        public static JObject FromObject(object o)
        {
            return FromObject(o, new JsonSerializer());
        }

        public static JObject FromObject(object o, JsonSerializer jsonSerializer)
        {
            JToken token = JToken.FromObjectInternal(o, jsonSerializer);
            if ((token != null) && (token.Type != JTokenType.Object))
            {
                throw new ArgumentException("Object serialized to {0}. JObject instance expected.".FormatWith(CultureInfo.InvariantCulture, token.Type));
            }
            return (JObject) token;
        }

        internal override int GetDeepHashCode()
        {
            return base.ContentsHashCode();
        }

        public IEnumerator<KeyValuePair<string, JToken>> GetEnumerator()
        {
            foreach (JProperty iteratorVariable0 in this._properties)
            {
                yield return new KeyValuePair<string, JToken>(iteratorVariable0.Name, iteratorVariable0.Value);
            }
        }

        protected override DynamicMetaObject GetMetaObject(Expression parameter)
        {
            return new DynamicProxyMetaObject<JObject>(parameter, this, new JObjectDynamicProxy(), true);
        }

        private static System.Type GetTokenPropertyType(JToken token)
        {
            if (!(token is JValue))
            {
                return token.GetType();
            }
            JValue value2 = (JValue) token;
            if (value2.Value == null)
            {
                return typeof(object);
            }
            return value2.Value.GetType();
        }

        public JToken GetValue(string propertyName)
        {
            return this.GetValue(propertyName, StringComparison.Ordinal);
        }

        public JToken GetValue(string propertyName, StringComparison comparison)
        {
            if (propertyName != null)
            {
                JProperty property = this.Property(propertyName);
                if (property != null)
                {
                    return property.Value;
                }
                if (comparison != StringComparison.Ordinal)
                {
                    foreach (JProperty property2 in this._properties)
                    {
                        if (string.Equals(property2.Name, propertyName, comparison))
                        {
                            return property2.Value;
                        }
                    }
                }
            }
            return null;
        }

        internal override void InsertItem(int index, JToken item, bool skipParentCheck)
        {
            if ((item == null) || (item.Type != JTokenType.Comment))
            {
                base.InsertItem(index, item, skipParentCheck);
            }
        }

        internal void InternalPropertyChanged(JProperty childProperty)
        {
            this.OnPropertyChanged(childProperty.Name);
            if (base._listChanged != null)
            {
                this.OnListChanged(new ListChangedEventArgs(ListChangedType.ItemChanged, base.IndexOfItem(childProperty)));
            }
            if (base._collectionChanged != null)
            {
                this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, childProperty, childProperty, base.IndexOfItem(childProperty)));
            }
        }

        internal void InternalPropertyChanging(JProperty childProperty)
        {
            this.OnPropertyChanging(childProperty.Name);
        }

        public static JObject Load(JsonReader reader)
        {
            ValidationUtils.ArgumentNotNull(reader, "reader");
            if ((reader.TokenType == JsonToken.None) && !reader.Read())
            {
                throw JsonReaderException.Create(reader, "Error reading JObject from JsonReader.");
            }
            while (reader.TokenType == JsonToken.Comment)
            {
                reader.Read();
            }
            if (reader.TokenType != JsonToken.StartObject)
            {
                throw JsonReaderException.Create(reader, "Error reading JObject from JsonReader. Current JsonReader item is not an object: {0}".FormatWith(CultureInfo.InvariantCulture, reader.TokenType));
            }
            JObject obj2 = new JObject();
            obj2.SetLineInfo(reader as IJsonLineInfo);
            obj2.ReadTokenFrom(reader);
            return obj2;
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        protected virtual void OnPropertyChanging(string propertyName)
        {
            if (this.PropertyChanging != null)
            {
                this.PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        public static JObject Parse(string json)
        {
            JsonReader reader = new JsonTextReader(new StringReader(json));
            JObject obj2 = Load(reader);
            if (reader.Read() && (reader.TokenType != JsonToken.Comment))
            {
                throw JsonReaderException.Create(reader, "Additional text found in JSON string after parsing content.");
            }
            return obj2;
        }

        public IEnumerable<JProperty> Properties()
        {
            return this._properties.Cast<JProperty>();
        }

        public JProperty Property(string name)
        {
            JToken token;
            if (name == null)
            {
                return null;
            }
            this._properties.TryGetValue(name, out token);
            return (JProperty) token;
        }

        public JEnumerable<JToken> PropertyValues()
        {
            return new JEnumerable<JToken>(from p in this.Properties() select p.Value);
        }

        public bool Remove(string propertyName)
        {
            JProperty property = this.Property(propertyName);
            if (property == null)
            {
                return false;
            }
            property.Remove();
            return true;
        }

        void ICollection<KeyValuePair<string, JToken>>.Add(KeyValuePair<string, JToken> item)
        {
            this.Add(new JProperty(item.Key, item.Value));
        }

        void ICollection<KeyValuePair<string, JToken>>.Clear()
        {
            base.RemoveAll();
        }

        bool ICollection<KeyValuePair<string, JToken>>.Contains(KeyValuePair<string, JToken> item)
        {
            JProperty property = this.Property(item.Key);
            if (property == null)
            {
                return false;
            }
            return (property.Value == item.Value);
        }

        void ICollection<KeyValuePair<string, JToken>>.CopyTo(KeyValuePair<string, JToken>[] array, int arrayIndex)
        {
            if (array == null)
            {
                throw new ArgumentNullException("array");
            }
            if (arrayIndex < 0)
            {
                throw new ArgumentOutOfRangeException("arrayIndex", "arrayIndex is less than 0.");
            }
            if ((arrayIndex >= array.Length) && (arrayIndex != 0))
            {
                throw new ArgumentException("arrayIndex is equal to or greater than the length of array.");
            }
            if (base.Count > (array.Length - arrayIndex))
            {
                throw new ArgumentException("The number of elements in the source JObject is greater than the available space from arrayIndex to the end of the destination array.");
            }
            int num = 0;
            foreach (JProperty property in this._properties)
            {
                array[arrayIndex + num] = new KeyValuePair<string, JToken>(property.Name, property.Value);
                num++;
            }
        }

        bool ICollection<KeyValuePair<string, JToken>>.Remove(KeyValuePair<string, JToken> item)
        {
            if (!((ICollection<KeyValuePair<string, JToken>>) this).Contains(item))
            {
                return false;
            }
            this.Remove(item.Key);
            return true;
        }

        bool IDictionary<string, JToken>.ContainsKey(string key)
        {
            return this._properties.Contains(key);
        }

        AttributeCollection ICustomTypeDescriptor.GetAttributes()
        {
            return AttributeCollection.Empty;
        }

        string ICustomTypeDescriptor.GetClassName()
        {
            return null;
        }

        string ICustomTypeDescriptor.GetComponentName()
        {
            return null;
        }

        TypeConverter ICustomTypeDescriptor.GetConverter()
        {
            return new TypeConverter();
        }

        EventDescriptor ICustomTypeDescriptor.GetDefaultEvent()
        {
            return null;
        }

        PropertyDescriptor ICustomTypeDescriptor.GetDefaultProperty()
        {
            return null;
        }

        object ICustomTypeDescriptor.GetEditor(System.Type editorBaseType)
        {
            return null;
        }

        EventDescriptorCollection ICustomTypeDescriptor.GetEvents()
        {
            return EventDescriptorCollection.Empty;
        }

        EventDescriptorCollection ICustomTypeDescriptor.GetEvents(Attribute[] attributes)
        {
            return EventDescriptorCollection.Empty;
        }

        PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties()
        {
            return ((ICustomTypeDescriptor) this).GetProperties(null);
        }

        PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties(Attribute[] attributes)
        {
            PropertyDescriptorCollection descriptors = new PropertyDescriptorCollection(null);
            foreach (KeyValuePair<string, JToken> pair in this)
            {
                descriptors.Add(new JPropertyDescriptor(pair.Key, GetTokenPropertyType(pair.Value)));
            }
            return descriptors;
        }

        object ICustomTypeDescriptor.GetPropertyOwner(PropertyDescriptor pd)
        {
            return null;
        }

        public bool TryGetValue(string propertyName, out JToken value)
        {
            JProperty property = this.Property(propertyName);
            if (property == null)
            {
                value = null;
                return false;
            }
            value = property.Value;
            return true;
        }

        public bool TryGetValue(string propertyName, StringComparison comparison, out JToken value)
        {
            value = this.GetValue(propertyName, comparison);
            return (value != null);
        }

        internal override void ValidateToken(JToken o, JToken existing)
        {
            ValidationUtils.ArgumentNotNull(o, "o");
            if (o.Type != JTokenType.Property)
            {
                throw new ArgumentException("Can not add {0} to {1}.".FormatWith(CultureInfo.InvariantCulture, o.GetType(), base.GetType()));
            }
            JProperty property = (JProperty) o;
            if (existing != null)
            {
                JProperty property2 = (JProperty) existing;
                if (property.Name == property2.Name)
                {
                    return;
                }
            }
            if (this._properties.TryGetValue(property.Name, out existing))
            {
                throw new ArgumentException("Can not add property {0} to {1}. Property with the same name already exists on object.".FormatWith(CultureInfo.InvariantCulture, property.Name, base.GetType()));
            }
        }

        public override void WriteTo(JsonWriter writer, params JsonConverter[] converters)
        {
            writer.WriteStartObject();
            for (int i = 0; i < this._properties.Count; i++)
            {
                this._properties[i].WriteTo(writer, converters);
            }
            writer.WriteEndObject();
        }

        protected override IList<JToken> ChildrenTokens
        {
            get
            {
                return this._properties;
            }
        }

        public override JToken this[object key]
        {
            get
            {
                ValidationUtils.ArgumentNotNull(key, "o");
                string str = key as string;
                if (str == null)
                {
                    throw new ArgumentException("Accessed JObject values with invalid key value: {0}. Object property name expected.".FormatWith(CultureInfo.InvariantCulture, MiscellaneousUtils.ToString(key)));
                }
                return this[str];
            }
            set
            {
                ValidationUtils.ArgumentNotNull(key, "o");
                string str = key as string;
                if (str == null)
                {
                    throw new ArgumentException("Set JObject values with invalid key value: {0}. Object property name expected.".FormatWith(CultureInfo.InvariantCulture, MiscellaneousUtils.ToString(key)));
                }
                this[str] = value;
            }
        }

        public JToken this[string propertyName]
        {
            get
            {
                ValidationUtils.ArgumentNotNull(propertyName, "propertyName");
                JProperty property = this.Property(propertyName);
                if (property == null)
                {
                    return null;
                }
                return property.Value;
            }
            set
            {
                JProperty property = this.Property(propertyName);
                if (property != null)
                {
                    property.Value = value;
                }
                else
                {
                    this.OnPropertyChanging(propertyName);
                    this.Add(new JProperty(propertyName, value));
                    this.OnPropertyChanged(propertyName);
                }
            }
        }

        bool ICollection<KeyValuePair<string, JToken>>.IsReadOnly
        {
            get
            {
                return false;
            }
        }

        ICollection<string> IDictionary<string, JToken>.Keys
        {
            get
            {
                return this._properties.Keys;
            }
        }

        ICollection<JToken> IDictionary<string, JToken>.Values
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override JTokenType Type
        {
            get
            {
                return JTokenType.Object;
            }
        }


        private class JObjectDynamicProxy : DynamicProxy<JObject>
        {
            public override IEnumerable<string> GetDynamicMemberNames(JObject instance)
            {
                return (from p in instance.Properties() select p.Name);
            }

            public override bool TryGetMember(JObject instance, GetMemberBinder binder, out object result)
            {
                result = instance[binder.Name];
                return true;
            }

            public override bool TrySetMember(JObject instance, SetMemberBinder binder, object value)
            {
                JToken token = value as JToken;
                if (token == null)
                {
                    token = new JValue(value);
                }
                instance[binder.Name] = token;
                return true;
            }
        }
    }
}

