﻿namespace Newtonsoft.Json.Linq
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Utilities;
    using System;
    using System.Collections.Generic;
    using System.Dynamic;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Runtime.InteropServices;

    public class JValue : JToken, IEquatable<JValue>, IComparable<JValue>, IFormattable, IComparable
    {
        private object _value;
        private JTokenType _valueType;

        public JValue(JValue other) : this(other.Value, other.Type)
        {
        }

        public JValue(bool value) : this(value, JTokenType.Boolean)
        {
        }

        public JValue(char value) : this(value, JTokenType.String)
        {
        }

        public JValue(DateTime value) : this(value, JTokenType.Date)
        {
        }

        public JValue(double value) : this(value, JTokenType.Float)
        {
        }

        public JValue(Guid value) : this(value, JTokenType.Guid)
        {
        }

        public JValue(long value) : this(value, JTokenType.Integer)
        {
        }

        public JValue(object value) : this(value, GetValueType(null, value))
        {
        }

        public JValue(float value) : this(value, JTokenType.Float)
        {
        }

        public JValue(string value) : this(value, JTokenType.String)
        {
        }

        public JValue(TimeSpan value) : this(value, JTokenType.TimeSpan)
        {
        }

        [CLSCompliant(false)]
        public JValue(ulong value) : this(value, JTokenType.Integer)
        {
        }

        public JValue(Uri value) : this(value, (value != null) ? JTokenType.Uri : JTokenType.Null)
        {
        }

        internal JValue(object value, JTokenType type)
        {
            this._value = value;
            this._valueType = type;
        }

        internal override JToken CloneToken()
        {
            return new JValue(this);
        }

        private static int Compare(JTokenType valueType, object objA, object objB)
        {
            DateTime time;
            DateTime dateTime;
            if ((objA == null) && (objB == null))
            {
                return 0;
            }
            if ((objA != null) && (objB == null))
            {
                return 1;
            }
            if ((objA == null) && (objB != null))
            {
                return -1;
            }
            switch (valueType)
            {
                case JTokenType.Comment:
                case JTokenType.String:
                case JTokenType.Raw:
                {
                    string strA = Convert.ToString(objA, CultureInfo.InvariantCulture);
                    string strB = Convert.ToString(objB, CultureInfo.InvariantCulture);
                    return string.CompareOrdinal(strA, strB);
                }
                case JTokenType.Integer:
                    if (((objA is ulong) || (objB is ulong)) || ((objA is decimal) || (objB is decimal)))
                    {
                        return Convert.ToDecimal(objA, CultureInfo.InvariantCulture).CompareTo(Convert.ToDecimal(objB, CultureInfo.InvariantCulture));
                    }
                    if (((objA is float) || (objB is float)) || ((objA is double) || (objB is double)))
                    {
                        return CompareFloat(objA, objB);
                    }
                    return Convert.ToInt64(objA, CultureInfo.InvariantCulture).CompareTo(Convert.ToInt64(objB, CultureInfo.InvariantCulture));

                case JTokenType.Float:
                    return CompareFloat(objA, objB);

                case JTokenType.Boolean:
                {
                    bool flag = Convert.ToBoolean(objA, CultureInfo.InvariantCulture);
                    bool flag2 = Convert.ToBoolean(objB, CultureInfo.InvariantCulture);
                    return flag.CompareTo(flag2);
                }
                case JTokenType.Date:
                {
                    if (!(objA is DateTime))
                    {
                        DateTimeOffset offset2;
                        DateTimeOffset offset = (DateTimeOffset) objA;
                        if (objB is DateTimeOffset)
                        {
                            offset2 = (DateTimeOffset) objB;
                        }
                        else
                        {
                            offset2 = new DateTimeOffset(Convert.ToDateTime(objB, CultureInfo.InvariantCulture));
                        }
                        return offset.CompareTo(offset2);
                    }
                    time = (DateTime) objA;
                    if (!(objB is DateTimeOffset))
                    {
                        dateTime = Convert.ToDateTime(objB, CultureInfo.InvariantCulture);
                        break;
                    }
                    DateTimeOffset offset3 = (DateTimeOffset) objB;
                    dateTime = offset3.DateTime;
                    break;
                }
                case JTokenType.Bytes:
                {
                    if (!(objB is byte[]))
                    {
                        throw new ArgumentException("Object must be of type byte[].");
                    }
                    byte[] buffer = objA as byte[];
                    byte[] buffer2 = objB as byte[];
                    if (buffer == null)
                    {
                        return -1;
                    }
                    if (buffer2 == null)
                    {
                        return 1;
                    }
                    return MiscellaneousUtils.ByteArrayCompare(buffer, buffer2);
                }
                case JTokenType.Guid:
                {
                    if (!(objB is Guid))
                    {
                        throw new ArgumentException("Object must be of type Guid.");
                    }
                    Guid guid = (Guid) objA;
                    Guid guid2 = (Guid) objB;
                    return guid.CompareTo(guid2);
                }
                case JTokenType.Uri:
                {
                    if (!(objB is Uri))
                    {
                        throw new ArgumentException("Object must be of type Uri.");
                    }
                    Uri uri = (Uri) objA;
                    Uri uri2 = (Uri) objB;
                    return Comparer<string>.Default.Compare(uri.ToString(), uri2.ToString());
                }
                case JTokenType.TimeSpan:
                {
                    if (!(objB is TimeSpan))
                    {
                        throw new ArgumentException("Object must be of type TimeSpan.");
                    }
                    TimeSpan span = (TimeSpan) objA;
                    TimeSpan span2 = (TimeSpan) objB;
                    return span.CompareTo(span2);
                }
                default:
                    throw MiscellaneousUtils.CreateArgumentOutOfRangeException("valueType", valueType, "Unexpected value type: {0}".FormatWith(CultureInfo.InvariantCulture, valueType));
            }
            return time.CompareTo(dateTime);
        }

        private static int CompareFloat(object objA, object objB)
        {
            double num = Convert.ToDouble(objA, CultureInfo.InvariantCulture);
            double num2 = Convert.ToDouble(objB, CultureInfo.InvariantCulture);
            if (MathUtils.ApproxEquals(num, num2))
            {
                return 0;
            }
            return num.CompareTo(num2);
        }

        public int CompareTo(JValue obj)
        {
            if (obj == null)
            {
                return 1;
            }
            return Compare(this._valueType, this._value, obj._value);
        }

        public static JValue CreateComment(string value)
        {
            return new JValue(value, JTokenType.Comment);
        }

        public static JValue CreateString(string value)
        {
            return new JValue(value, JTokenType.String);
        }

        internal override bool DeepEquals(JToken node)
        {
            JValue value2 = node as JValue;
            if (value2 == null)
            {
                return false;
            }
            return ((value2 == this) || ValuesEquals(this, value2));
        }

        public bool Equals(JValue other)
        {
            if (other == null)
            {
                return false;
            }
            return ValuesEquals(this, other);
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            JValue other = obj as JValue;
            if (other != null)
            {
                return this.Equals(other);
            }
            return base.Equals(obj);
        }

        internal override int GetDeepHashCode()
        {
            int num = (this._value != null) ? this._value.GetHashCode() : 0;
            return (this._valueType.GetHashCode() ^ num);
        }

        public override int GetHashCode()
        {
            if (this._value == null)
            {
                return 0;
            }
            return this._value.GetHashCode();
        }

        protected override DynamicMetaObject GetMetaObject(Expression parameter)
        {
            return new DynamicProxyMetaObject<JValue>(parameter, this, new JValueDynamicProxy(), true);
        }

        private static JTokenType GetStringValueType(JTokenType? current)
        {
            if (!current.HasValue)
            {
                return JTokenType.String;
            }
            JTokenType type = current.Value;
            if (((type != JTokenType.Comment) && (type != JTokenType.String)) && (type != JTokenType.Raw))
            {
                return JTokenType.String;
            }
            return current.Value;
        }

        private static JTokenType GetValueType(JTokenType? current, object value)
        {
            if (value == null)
            {
                return JTokenType.Null;
            }
            if (value == DBNull.Value)
            {
                return JTokenType.Null;
            }
            if (value is string)
            {
                return GetStringValueType(current);
            }
            if ((((value is long) || (value is int)) || ((value is short) || (value is sbyte))) || (((value is ulong) || (value is uint)) || ((value is ushort) || (value is byte))))
            {
                return JTokenType.Integer;
            }
            if (value is Enum)
            {
                return JTokenType.Integer;
            }
            if (((value is double) || (value is float)) || (value is decimal))
            {
                return JTokenType.Float;
            }
            if (value is DateTime)
            {
                return JTokenType.Date;
            }
            if (value is DateTimeOffset)
            {
                return JTokenType.Date;
            }
            if (value is byte[])
            {
                return JTokenType.Bytes;
            }
            if (value is bool)
            {
                return JTokenType.Boolean;
            }
            if (value is Guid)
            {
                return JTokenType.Guid;
            }
            if (value is Uri)
            {
                return JTokenType.Uri;
            }
            if (!(value is TimeSpan))
            {
                throw new ArgumentException("Could not determine JSON object type for type {0}.".FormatWith(CultureInfo.InvariantCulture, value.GetType()));
            }
            return JTokenType.TimeSpan;
        }

        private static bool Operation(ExpressionType operation, object objA, object objB, out object result)
        {
            if (((objA is string) || (objB is string)) && ((operation == ExpressionType.Add) || (operation == ExpressionType.AddAssign)))
            {
                result = ((objA != null) ? objA.ToString() : null) + ((objB != null) ? objB.ToString() : null);
                return true;
            }
            if (((objA is ulong) || (objB is ulong)) || ((objA is decimal) || (objB is decimal)))
            {
                if ((objA == null) || (objB == null))
                {
                    result = null;
                    return true;
                }
                decimal num = Convert.ToDecimal(objA, CultureInfo.InvariantCulture);
                decimal num2 = Convert.ToDecimal(objB, CultureInfo.InvariantCulture);
                switch (operation)
                {
                    case ExpressionType.Add:
                    case ExpressionType.AddAssign:
                        result = num + num2;
                        return true;

                    case ExpressionType.Divide:
                    case ExpressionType.DivideAssign:
                        result = num / num2;
                        return true;

                    case ExpressionType.Multiply:
                    case ExpressionType.MultiplyAssign:
                        result = num * num2;
                        return true;

                    case ExpressionType.SubtractAssign:
                    case ExpressionType.Subtract:
                        result = num - num2;
                        return true;
                }
            }
            else if (((objA is float) || (objB is float)) || ((objA is double) || (objB is double)))
            {
                if ((objA == null) || (objB == null))
                {
                    result = null;
                    return true;
                }
                double num3 = Convert.ToDouble(objA, CultureInfo.InvariantCulture);
                double num4 = Convert.ToDouble(objB, CultureInfo.InvariantCulture);
                switch (operation)
                {
                    case ExpressionType.Add:
                    case ExpressionType.AddAssign:
                        result = num3 + num4;
                        return true;

                    case ExpressionType.Divide:
                    case ExpressionType.DivideAssign:
                        result = num3 / num4;
                        return true;

                    case ExpressionType.Multiply:
                    case ExpressionType.MultiplyAssign:
                        result = num3 * num4;
                        return true;

                    case ExpressionType.SubtractAssign:
                    case ExpressionType.Subtract:
                        result = num3 - num4;
                        return true;
                }
            }
            else if (((((objA is int) || (objA is uint)) || ((objA is long) || (objA is short))) || (((objA is ushort) || (objA is sbyte)) || ((objA is byte) || (objB is int)))) || ((((objB is uint) || (objB is long)) || ((objB is short) || (objB is ushort))) || ((objB is sbyte) || (objB is byte))))
            {
                if ((objA == null) || (objB == null))
                {
                    result = null;
                    return true;
                }
                long num5 = Convert.ToInt64(objA, CultureInfo.InvariantCulture);
                long num6 = Convert.ToInt64(objB, CultureInfo.InvariantCulture);
                switch (operation)
                {
                    case ExpressionType.Add:
                    case ExpressionType.AddAssign:
                        result = num5 + num6;
                        return true;

                    case ExpressionType.Divide:
                    case ExpressionType.DivideAssign:
                        result = num5 / num6;
                        return true;

                    case ExpressionType.Multiply:
                    case ExpressionType.MultiplyAssign:
                        result = num5 * num6;
                        return true;

                    case ExpressionType.SubtractAssign:
                    case ExpressionType.Subtract:
                        result = num5 - num6;
                        return true;
                }
            }
            result = null;
            return false;
        }

        int IComparable.CompareTo(object obj)
        {
            if (obj == null)
            {
                return 1;
            }
            object objB = (obj is JValue) ? ((JValue) obj).Value : obj;
            return Compare(this._valueType, this._value, objB);
        }

        public override string ToString()
        {
            if (this._value == null)
            {
                return string.Empty;
            }
            return this._value.ToString();
        }

        public string ToString(IFormatProvider formatProvider)
        {
            return this.ToString(null, formatProvider);
        }

        public string ToString(string format)
        {
            return this.ToString(format, CultureInfo.CurrentCulture);
        }

        public string ToString(string format, IFormatProvider formatProvider)
        {
            if (this._value == null)
            {
                return string.Empty;
            }
            IFormattable formattable = this._value as IFormattable;
            if (formattable != null)
            {
                return formattable.ToString(format, formatProvider);
            }
            return this._value.ToString();
        }

        private static bool ValuesEquals(JValue v1, JValue v2)
        {
            return ((v1 == v2) || ((v1._valueType == v2._valueType) && (Compare(v1._valueType, v1._value, v2._value) == 0)));
        }

        public override void WriteTo(JsonWriter writer, params JsonConverter[] converters)
        {
            if (((converters != null) && (converters.Length > 0)) && (this._value != null))
            {
                JsonConverter matchingConverter = JsonSerializer.GetMatchingConverter(converters, this._value.GetType());
                if (matchingConverter != null)
                {
                    matchingConverter.WriteJson(writer, this._value, new JsonSerializer());
                    return;
                }
            }
            switch (this._valueType)
            {
                case JTokenType.Comment:
                    writer.WriteComment((this._value != null) ? this._value.ToString() : null);
                    return;

                case JTokenType.Integer:
                    writer.WriteValue(Convert.ToInt64(this._value, CultureInfo.InvariantCulture));
                    return;

                case JTokenType.Float:
                    if (!(this._value is decimal))
                    {
                        if (this._value is double)
                        {
                            writer.WriteValue((double) this._value);
                            return;
                        }
                        if (this._value is float)
                        {
                            writer.WriteValue((float) this._value);
                            return;
                        }
                        writer.WriteValue(Convert.ToDouble(this._value, CultureInfo.InvariantCulture));
                        return;
                    }
                    writer.WriteValue((decimal) this._value);
                    return;

                case JTokenType.String:
                    writer.WriteValue((this._value != null) ? this._value.ToString() : null);
                    return;

                case JTokenType.Boolean:
                    writer.WriteValue(Convert.ToBoolean(this._value, CultureInfo.InvariantCulture));
                    return;

                case JTokenType.Null:
                    writer.WriteNull();
                    return;

                case JTokenType.Undefined:
                    writer.WriteUndefined();
                    return;

                case JTokenType.Date:
                    if (!(this._value is DateTimeOffset))
                    {
                        writer.WriteValue(Convert.ToDateTime(this._value, CultureInfo.InvariantCulture));
                        return;
                    }
                    writer.WriteValue((DateTimeOffset) this._value);
                    return;

                case JTokenType.Raw:
                    writer.WriteRawValue((this._value != null) ? this._value.ToString() : null);
                    return;

                case JTokenType.Bytes:
                    writer.WriteValue((byte[]) this._value);
                    return;

                case JTokenType.Guid:
                case JTokenType.Uri:
                case JTokenType.TimeSpan:
                    writer.WriteValue((this._value != null) ? this._value.ToString() : null);
                    return;
            }
            throw MiscellaneousUtils.CreateArgumentOutOfRangeException("TokenType", this._valueType, "Unexpected token type.");
        }

        public override bool HasValues
        {
            get
            {
                return false;
            }
        }

        public override JTokenType Type
        {
            get
            {
                return this._valueType;
            }
        }

        public object Value
        {
            get
            {
                return this._value;
            }
            set
            {
                System.Type type = (this._value != null) ? this._value.GetType() : null;
                System.Type type2 = (value != null) ? value.GetType() : null;
                if (type != type2)
                {
                    this._valueType = GetValueType(new JTokenType?(this._valueType), value);
                }
                this._value = value;
            }
        }

        private class JValueDynamicProxy : DynamicProxy<JValue>
        {
            public override bool TryBinaryOperation(JValue instance, BinaryOperationBinder binder, object arg, out object result)
            {
                object objB = (arg is JValue) ? ((JValue) arg).Value : arg;
                switch (binder.Operation)
                {
                    case ExpressionType.Multiply:
                    case ExpressionType.Divide:
                    case ExpressionType.Add:
                    case ExpressionType.AddAssign:
                    case ExpressionType.DivideAssign:
                    case ExpressionType.Subtract:
                    case ExpressionType.MultiplyAssign:
                    case ExpressionType.SubtractAssign:
                        if (JValue.Operation(binder.Operation, instance.Value, objB, out result))
                        {
                            result = new JValue(result);
                            return true;
                        }
                        break;

                    case ExpressionType.NotEqual:
                        result = JValue.Compare(instance.Type, instance.Value, objB) != 0;
                        return true;

                    case ExpressionType.Equal:
                        result = JValue.Compare(instance.Type, instance.Value, objB) == 0;
                        return true;

                    case ExpressionType.GreaterThan:
                        result = JValue.Compare(instance.Type, instance.Value, objB) > 0;
                        return true;

                    case ExpressionType.GreaterThanOrEqual:
                        result = JValue.Compare(instance.Type, instance.Value, objB) >= 0;
                        return true;

                    case ExpressionType.LessThan:
                        result = JValue.Compare(instance.Type, instance.Value, objB) < 0;
                        return true;

                    case ExpressionType.LessThanOrEqual:
                        result = JValue.Compare(instance.Type, instance.Value, objB) <= 0;
                        return true;
                }
                result = null;
                return false;
            }

            public override bool TryConvert(JValue instance, ConvertBinder binder, out object result)
            {
                if (binder.Type == typeof(JValue))
                {
                    result = instance;
                    return true;
                }
                if (instance.Value == null)
                {
                    result = null;
                    return ReflectionUtils.IsNullable(binder.Type);
                }
                result = ConvertUtils.Convert(instance.Value, CultureInfo.InvariantCulture, binder.Type);
                return true;
            }
        }
    }
}

