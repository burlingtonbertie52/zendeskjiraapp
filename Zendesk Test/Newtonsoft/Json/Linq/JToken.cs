﻿namespace Newtonsoft.Json.Linq
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Utilities;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Dynamic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Threading;

    public abstract class JToken : IEnumerable, IEnumerable<JToken>, IJEnumerable<JToken>, ICloneable, IDynamicMetaObjectProvider, IJsonLineInfo
    {
        private static JTokenEqualityComparer _equalityComparer;
        private int? _lineNumber;
        private int? _linePosition;
        private JToken _next;
        private JContainer _parent;
        private JToken _previous;
        private static readonly JTokenType[] BooleanTypes = new JTokenType[] { JTokenType.Integer, JTokenType.Float, JTokenType.String, JTokenType.Comment, JTokenType.Raw, JTokenType.Boolean };
        private static readonly JTokenType[] BytesTypes = new JTokenType[] { JTokenType.Bytes, JTokenType.String, JTokenType.Comment, JTokenType.Raw };
        private static readonly JTokenType[] CharTypes = new JTokenType[] { JTokenType.Integer, JTokenType.Float, JTokenType.String, JTokenType.Comment, JTokenType.Raw };
        private static readonly JTokenType[] DateTimeTypes = new JTokenType[] { JTokenType.Date, JTokenType.String, JTokenType.Comment, JTokenType.Raw };
        private static readonly JTokenType[] GuidTypes = new JTokenType[] { JTokenType.String, JTokenType.Comment, JTokenType.Raw, JTokenType.Guid };
        private static readonly JTokenType[] NumberTypes = new JTokenType[] { JTokenType.Integer, JTokenType.Float, JTokenType.String, JTokenType.Comment, JTokenType.Raw, JTokenType.Boolean };
        private static readonly JTokenType[] StringTypes = new JTokenType[] { JTokenType.Date, JTokenType.Integer, JTokenType.Float, JTokenType.String, JTokenType.Comment, JTokenType.Raw, JTokenType.Boolean, JTokenType.Bytes, JTokenType.Guid, JTokenType.TimeSpan, JTokenType.Uri };
        private static readonly JTokenType[] TimeSpanTypes = new JTokenType[] { JTokenType.String, JTokenType.Comment, JTokenType.Raw, JTokenType.TimeSpan };
        private static readonly JTokenType[] UriTypes = new JTokenType[] { JTokenType.String, JTokenType.Comment, JTokenType.Raw, JTokenType.Uri };

        internal JToken()
        {
        }

        public void AddAfterSelf(object content)
        {
            if (this._parent == null)
            {
                throw new InvalidOperationException("The parent is missing.");
            }
            int num = this._parent.IndexOfItem(this);
            this._parent.AddInternal(num + 1, content, false);
        }

        public void AddBeforeSelf(object content)
        {
            if (this._parent == null)
            {
                throw new InvalidOperationException("The parent is missing.");
            }
            int index = this._parent.IndexOfItem(this);
            this._parent.AddInternal(index, content, false);
        }

        public IEnumerable<JToken> AfterSelf()
        {
            if (this.Parent == null)
            {
                goto Label_0073;
            }
            JToken next = this.Next;
        Label_PostSwitchInIterator:;
            if (next != null)
            {
                yield return next;
                next = next.Next;
                goto Label_PostSwitchInIterator;
            }
        Label_0073:;
        }

        public IEnumerable<JToken> Ancestors()
        {
            JToken parent = this.Parent;
            while (true)
            {
                if (parent == null)
                {
                    yield break;
                }
                yield return parent;
                parent = parent.Parent;
            }
        }

        public IEnumerable<JToken> BeforeSelf()
        {
            JToken first = this.Parent.First;
            while (true)
            {
                if (first == this)
                {
                    yield break;
                }
                yield return first;
                first = first.Next;
            }
        }

        public virtual JEnumerable<JToken> Children()
        {
            return JEnumerable<JToken>.Empty;
        }

        public JEnumerable<T> Children<T>() where T: JToken
        {
            return new JEnumerable<T>(this.Children().OfType<T>());
        }

        internal abstract JToken CloneToken();
        public JsonReader CreateReader()
        {
            return new JTokenReader(this);
        }

        public JToken DeepClone()
        {
            return this.CloneToken();
        }

        internal abstract bool DeepEquals(JToken node);
        public static bool DeepEquals(JToken t1, JToken t2)
        {
            return ((t1 == t2) || (((t1 != null) && (t2 != null)) && t1.DeepEquals(t2)));
        }

        private static JValue EnsureValue(JToken value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }
            if (value is JProperty)
            {
                value = ((JProperty) value).Value;
            }
            return (value as JValue);
        }

        public static JToken FromObject(object o)
        {
            return FromObjectInternal(o, new JsonSerializer());
        }

        public static JToken FromObject(object o, JsonSerializer jsonSerializer)
        {
            return FromObjectInternal(o, jsonSerializer);
        }

        internal static JToken FromObjectInternal(object o, JsonSerializer jsonSerializer)
        {
            ValidationUtils.ArgumentNotNull(o, "o");
            ValidationUtils.ArgumentNotNull(jsonSerializer, "jsonSerializer");
            using (JTokenWriter writer = new JTokenWriter())
            {
                jsonSerializer.Serialize(writer, o);
                return writer.Token;
            }
        }

        internal abstract int GetDeepHashCode();
        protected virtual DynamicMetaObject GetMetaObject(Expression parameter)
        {
            return new DynamicProxyMetaObject<JToken>(parameter, this, new DynamicProxy<JToken>(), true);
        }

        private static string GetType(JToken token)
        {
            ValidationUtils.ArgumentNotNull(token, "token");
            if (token is JProperty)
            {
                token = ((JProperty) token).Value;
            }
            return token.Type.ToString();
        }

        public static JToken Load(JsonReader reader)
        {
            return ReadFrom(reader);
        }

        bool IJsonLineInfo.HasLineInfo()
        {
            return (this._lineNumber.HasValue && this._linePosition.HasValue);
        }

        public static explicit operator bool(JToken value)
        {
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, BooleanTypes, false))
            {
                throw new ArgumentException("Can not convert {0} to Boolean.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            return Convert.ToBoolean(o.Value, CultureInfo.InvariantCulture);
        }

        public static explicit operator byte(JToken value)
        {
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, NumberTypes, false))
            {
                throw new ArgumentException("Can not convert {0} to Byte.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            return Convert.ToByte(o.Value, CultureInfo.InvariantCulture);
        }

        [CLSCompliant(false)]
        public static explicit operator char(JToken value)
        {
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, CharTypes, false))
            {
                throw new ArgumentException("Can not convert {0} to Char.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            return Convert.ToChar(o.Value, CultureInfo.InvariantCulture);
        }

        public static explicit operator DateTime(JToken value)
        {
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, DateTimeTypes, false))
            {
                throw new ArgumentException("Can not convert {0} to DateTime.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            return Convert.ToDateTime(o.Value, CultureInfo.InvariantCulture);
        }

        public static explicit operator DateTimeOffset(JToken value)
        {
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, DateTimeTypes, false))
            {
                throw new ArgumentException("Can not convert {0} to DateTimeOffset.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            if (o.Value is DateTimeOffset)
            {
                return (DateTimeOffset) o.Value;
            }
            if (o.Value is string)
            {
                return DateTimeOffset.Parse((string) o.Value);
            }
            return new DateTimeOffset(Convert.ToDateTime(o.Value, CultureInfo.InvariantCulture));
        }

        public static explicit operator decimal(JToken value)
        {
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, NumberTypes, false))
            {
                throw new ArgumentException("Can not convert {0} to Decimal.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            return Convert.ToDecimal(o.Value, CultureInfo.InvariantCulture);
        }

        public static explicit operator double(JToken value)
        {
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, NumberTypes, false))
            {
                throw new ArgumentException("Can not convert {0} to Double.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            return Convert.ToDouble(o.Value, CultureInfo.InvariantCulture);
        }

        public static explicit operator Guid(JToken value)
        {
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, GuidTypes, false))
            {
                throw new ArgumentException("Can not convert {0} to Guid.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            if (o.Value is Guid)
            {
                return (Guid) o.Value;
            }
            return new Guid(Convert.ToString(o.Value, CultureInfo.InvariantCulture));
        }

        public static explicit operator short(JToken value)
        {
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, NumberTypes, false))
            {
                throw new ArgumentException("Can not convert {0} to Int16.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            return Convert.ToInt16(o.Value, CultureInfo.InvariantCulture);
        }

        public static explicit operator int(JToken value)
        {
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, NumberTypes, false))
            {
                throw new ArgumentException("Can not convert {0} to Int32.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            return Convert.ToInt32(o.Value, CultureInfo.InvariantCulture);
        }

        public static explicit operator long(JToken value)
        {
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, NumberTypes, false))
            {
                throw new ArgumentException("Can not convert {0} to Int64.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            return Convert.ToInt64(o.Value, CultureInfo.InvariantCulture);
        }

        public static explicit operator float(JToken value)
        {
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, NumberTypes, false))
            {
                throw new ArgumentException("Can not convert {0} to Single.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            return Convert.ToSingle(o.Value, CultureInfo.InvariantCulture);
        }

        public static explicit operator string(JToken value)
        {
            if (value == null)
            {
                return null;
            }
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, StringTypes, true))
            {
                throw new ArgumentException("Can not convert {0} to String.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            if (o.Value == null)
            {
                return null;
            }
            if (o.Value is byte[])
            {
                return Convert.ToBase64String((byte[]) o.Value);
            }
            return Convert.ToString(o.Value, CultureInfo.InvariantCulture);
        }

        public static explicit operator byte[](JToken value)
        {
            if (value == null)
            {
                return null;
            }
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, BytesTypes, false))
            {
                throw new ArgumentException("Can not convert {0} to byte array.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            if (o.Value is string)
            {
                return Convert.FromBase64String(Convert.ToString(o.Value, CultureInfo.InvariantCulture));
            }
            return (byte[]) o.Value;
        }

        [CLSCompliant(false)]
        public static explicit operator uint(JToken value)
        {
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, NumberTypes, false))
            {
                throw new ArgumentException("Can not convert {0} to UInt32.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            return Convert.ToUInt32(o.Value, CultureInfo.InvariantCulture);
        }

        public static explicit operator TimeSpan(JToken value)
        {
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, TimeSpanTypes, false))
            {
                throw new ArgumentException("Can not convert {0} to TimeSpan.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            if (o.Value is TimeSpan)
            {
                return (TimeSpan) o.Value;
            }
            return ConvertUtils.ParseTimeSpan(Convert.ToString(o.Value, CultureInfo.InvariantCulture));
        }

        [CLSCompliant(false)]
        public static explicit operator ulong?(JToken value)
        {
            if (value == null)
            {
                return null;
            }
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, NumberTypes, true))
            {
                throw new ArgumentException("Can not convert {0} to UInt64.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            if (o.Value == null)
            {
                return null;
            }
            return new ulong?(Convert.ToUInt64(o.Value, CultureInfo.InvariantCulture));
        }

        [CLSCompliant(false)]
        public static explicit operator ushort(JToken value)
        {
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, NumberTypes, false))
            {
                throw new ArgumentException("Can not convert {0} to UInt16.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            return Convert.ToUInt16(o.Value, CultureInfo.InvariantCulture);
        }

        public static explicit operator Guid?(JToken value)
        {
            if (value == null)
            {
                return null;
            }
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, GuidTypes, true))
            {
                throw new ArgumentException("Can not convert {0} to Guid.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            if (o.Value == null)
            {
                return null;
            }
            return new Guid?((o.Value is Guid) ? ((Guid) o.Value) : new Guid(Convert.ToString(o.Value, CultureInfo.InvariantCulture)));
        }

        public static explicit operator TimeSpan?(JToken value)
        {
            if (value == null)
            {
                return null;
            }
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, TimeSpanTypes, true))
            {
                throw new ArgumentException("Can not convert {0} to TimeSpan.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            if (o.Value == null)
            {
                return null;
            }
            return new TimeSpan?((o.Value is TimeSpan) ? ((TimeSpan) o.Value) : ConvertUtils.ParseTimeSpan(Convert.ToString(o.Value, CultureInfo.InvariantCulture)));
        }

        [CLSCompliant(false)]
        public static explicit operator ulong(JToken value)
        {
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, NumberTypes, false))
            {
                throw new ArgumentException("Can not convert {0} to UInt64.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            return Convert.ToUInt64(o.Value, CultureInfo.InvariantCulture);
        }

        [CLSCompliant(false)]
        public static explicit operator uint?(JToken value)
        {
            if (value == null)
            {
                return null;
            }
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, NumberTypes, true))
            {
                throw new ArgumentException("Can not convert {0} to UInt32.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            if (o.Value == null)
            {
                return null;
            }
            return new uint?(Convert.ToUInt32(o.Value, CultureInfo.InvariantCulture));
        }

        public static explicit operator bool?(JToken value)
        {
            if (value == null)
            {
                return null;
            }
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, BooleanTypes, true))
            {
                throw new ArgumentException("Can not convert {0} to Boolean.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            if (o.Value == null)
            {
                return null;
            }
            return new bool?(Convert.ToBoolean(o.Value, CultureInfo.InvariantCulture));
        }

        public static explicit operator byte?(JToken value)
        {
            if (value == null)
            {
                return null;
            }
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, NumberTypes, true))
            {
                throw new ArgumentException("Can not convert {0} to Byte.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            if (o.Value == null)
            {
                return null;
            }
            return new byte?(Convert.ToByte(o.Value, CultureInfo.InvariantCulture));
        }

        public static explicit operator char?(JToken value)
        {
            if (value == null)
            {
                return null;
            }
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, CharTypes, true))
            {
                throw new ArgumentException("Can not convert {0} to Char.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            if (o.Value == null)
            {
                return null;
            }
            return new char?(Convert.ToChar(o.Value, CultureInfo.InvariantCulture));
        }

        public static explicit operator DateTime?(JToken value)
        {
            if (value == null)
            {
                return null;
            }
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, DateTimeTypes, true))
            {
                throw new ArgumentException("Can not convert {0} to DateTime.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            if (o.Value == null)
            {
                return null;
            }
            return new DateTime?(Convert.ToDateTime(o.Value, CultureInfo.InvariantCulture));
        }

        public static explicit operator DateTimeOffset?(JToken value)
        {
            if (value == null)
            {
                return null;
            }
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, DateTimeTypes, true))
            {
                throw new ArgumentException("Can not convert {0} to DateTimeOffset.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            if (o.Value == null)
            {
                return null;
            }
            if (o.Value is DateTimeOffset)
            {
                return (DateTimeOffset?) o.Value;
            }
            if (o.Value is string)
            {
                return new DateTimeOffset?(DateTimeOffset.Parse((string) o.Value));
            }
            return new DateTimeOffset(Convert.ToDateTime(o.Value, CultureInfo.InvariantCulture));
        }

        public static explicit operator decimal?(JToken value)
        {
            if (value == null)
            {
                return null;
            }
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, NumberTypes, true))
            {
                throw new ArgumentException("Can not convert {0} to Decimal.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            if (o.Value == null)
            {
                return null;
            }
            return new decimal?(Convert.ToDecimal(o.Value, CultureInfo.InvariantCulture));
        }

        public static explicit operator short?(JToken value)
        {
            if (value == null)
            {
                return null;
            }
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, NumberTypes, true))
            {
                throw new ArgumentException("Can not convert {0} to Int16.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            if (o.Value == null)
            {
                return null;
            }
            return new short?(Convert.ToInt16(o.Value, CultureInfo.InvariantCulture));
        }

        public static explicit operator long?(JToken value)
        {
            if (value == null)
            {
                return null;
            }
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, NumberTypes, true))
            {
                throw new ArgumentException("Can not convert {0} to Int64.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            if (o.Value == null)
            {
                return null;
            }
            return new long?(Convert.ToInt64(o.Value, CultureInfo.InvariantCulture));
        }

        public static explicit operator Uri(JToken value)
        {
            if (value == null)
            {
                return null;
            }
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, UriTypes, true))
            {
                throw new ArgumentException("Can not convert {0} to Uri.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            if (o.Value == null)
            {
                return null;
            }
            if (o.Value is Uri)
            {
                return (Uri) o.Value;
            }
            return new Uri(Convert.ToString(o.Value, CultureInfo.InvariantCulture));
        }

        public static explicit operator double?(JToken value)
        {
            if (value == null)
            {
                return null;
            }
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, NumberTypes, true))
            {
                throw new ArgumentException("Can not convert {0} to Double.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            if (o.Value == null)
            {
                return null;
            }
            return new double?(Convert.ToDouble(o.Value, CultureInfo.InvariantCulture));
        }

        public static explicit operator int?(JToken value)
        {
            if (value == null)
            {
                return null;
            }
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, NumberTypes, true))
            {
                throw new ArgumentException("Can not convert {0} to Int32.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            if (o.Value == null)
            {
                return null;
            }
            return new int?(Convert.ToInt32(o.Value, CultureInfo.InvariantCulture));
        }

        public static explicit operator float?(JToken value)
        {
            if (value == null)
            {
                return null;
            }
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, NumberTypes, true))
            {
                throw new ArgumentException("Can not convert {0} to Single.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            if (o.Value == null)
            {
                return null;
            }
            return new float?(Convert.ToSingle(o.Value, CultureInfo.InvariantCulture));
        }

        [CLSCompliant(false)]
        public static explicit operator ushort?(JToken value)
        {
            if (value == null)
            {
                return null;
            }
            JValue o = EnsureValue(value);
            if ((o == null) || !ValidateToken(o, NumberTypes, true))
            {
                throw new ArgumentException("Can not convert {0} to UInt16.".FormatWith(CultureInfo.InvariantCulture, GetType(value)));
            }
            if (o.Value == null)
            {
                return null;
            }
            return new ushort?(Convert.ToUInt16(o.Value, CultureInfo.InvariantCulture));
        }

        public static implicit operator JToken(bool value)
        {
            return new JValue(value);
        }

        public static implicit operator JToken(DateTime value)
        {
            return new JValue(value);
        }

        public static implicit operator JToken(DateTimeOffset value)
        {
            return new JValue(value);
        }

        public static implicit operator JToken(decimal value)
        {
            return new JValue(value);
        }

        public static implicit operator JToken(double value)
        {
            return new JValue(value);
        }

        public static implicit operator JToken(Guid value)
        {
            return new JValue(value);
        }

        [CLSCompliant(false)]
        public static implicit operator JToken(short value)
        {
            return new JValue((long) value);
        }

        public static implicit operator JToken(int value)
        {
            return new JValue((long) value);
        }

        public static implicit operator JToken(long value)
        {
            return new JValue(value);
        }

        public static implicit operator JToken(float value)
        {
            return new JValue(value);
        }

        public static implicit operator JToken(TimeSpan value)
        {
            return new JValue(value);
        }

        [CLSCompliant(false)]
        public static implicit operator JToken(ulong value)
        {
            return new JValue(value);
        }

        public static implicit operator JToken(string value)
        {
            return new JValue(value);
        }

        public static implicit operator JToken(bool? value)
        {
            return new JValue(value);
        }

        [CLSCompliant(false)]
        public static implicit operator JToken(uint value)
        {
            return new JValue((long) value);
        }

        public static implicit operator JToken(Uri value)
        {
            return new JValue(value);
        }

        public static implicit operator JToken(DateTime? value)
        {
            return new JValue(value);
        }

        public static implicit operator JToken(DateTimeOffset? value)
        {
            return new JValue(value);
        }

        public static implicit operator JToken(decimal? value)
        {
            return new JValue(value);
        }

        public static implicit operator JToken(double? value)
        {
            return new JValue(value);
        }

        public static implicit operator JToken(Guid? value)
        {
            return new JValue(value);
        }

        [CLSCompliant(false)]
        public static implicit operator JToken(short? value)
        {
            return new JValue(value);
        }

        public static implicit operator JToken(int? value)
        {
            return new JValue(value);
        }

        public static implicit operator JToken(long? value)
        {
            return new JValue(value);
        }

        public static implicit operator JToken(float? value)
        {
            return new JValue(value);
        }

        public static implicit operator JToken(TimeSpan? value)
        {
            return new JValue(value);
        }

        [CLSCompliant(false)]
        public static implicit operator JToken(ushort? value)
        {
            return new JValue(value);
        }

        [CLSCompliant(false)]
        public static implicit operator JToken(ulong? value)
        {
            return new JValue(value);
        }

        [CLSCompliant(false)]
        public static implicit operator JToken(ushort value)
        {
            return new JValue((long) value);
        }

        [CLSCompliant(false)]
        public static implicit operator JToken(uint? value)
        {
            return new JValue(value);
        }

        public static implicit operator JToken(byte[] value)
        {
            return new JValue(value);
        }

        public static JToken Parse(string json)
        {
            JsonReader reader = new JsonTextReader(new StringReader(json));
            JToken token = Load(reader);
            if (reader.Read() && (reader.TokenType != JsonToken.Comment))
            {
                throw JsonReaderException.Create(reader, "Additional text found in JSON string after parsing content.");
            }
            return token;
        }

        public static JToken ReadFrom(JsonReader reader)
        {
            ValidationUtils.ArgumentNotNull(reader, "reader");
            if ((reader.TokenType == JsonToken.None) && !reader.Read())
            {
                throw JsonReaderException.Create(reader, "Error reading JToken from JsonReader.");
            }
            if (reader.TokenType == JsonToken.StartObject)
            {
                return JObject.Load(reader);
            }
            if (reader.TokenType == JsonToken.StartArray)
            {
                return JArray.Load(reader);
            }
            if (reader.TokenType == JsonToken.PropertyName)
            {
                return JProperty.Load(reader);
            }
            if (reader.TokenType == JsonToken.StartConstructor)
            {
                return JConstructor.Load(reader);
            }
            if (JsonReader.IsStartToken(reader.TokenType))
            {
                throw JsonReaderException.Create(reader, "Error reading JToken from JsonReader. Unexpected token: {0}".FormatWith(CultureInfo.InvariantCulture, reader.TokenType));
            }
            return new JValue(reader.Value);
        }

        public void Remove()
        {
            if (this._parent == null)
            {
                throw new InvalidOperationException("The parent is missing.");
            }
            this._parent.RemoveItem(this);
        }

        public void Replace(JToken value)
        {
            if (this._parent == null)
            {
                throw new InvalidOperationException("The parent is missing.");
            }
            this._parent.ReplaceItem(this, value);
        }

        public JToken SelectToken(string path)
        {
            return this.SelectToken(path, false);
        }

        public JToken SelectToken(string path, bool errorWhenNoMatch)
        {
            JPath path2 = new JPath(path);
            return path2.Evaluate(this, errorWhenNoMatch);
        }

        internal void SetLineInfo(IJsonLineInfo lineInfo)
        {
            if ((lineInfo != null) && lineInfo.HasLineInfo())
            {
                this.SetLineInfo(lineInfo.LineNumber, lineInfo.LinePosition);
            }
        }

        internal void SetLineInfo(int lineNumber, int linePosition)
        {
            this._lineNumber = new int?(lineNumber);
            this._linePosition = new int?(linePosition);
        }

        IEnumerator<JToken> IEnumerable<JToken>.GetEnumerator()
        {
            return this.Children().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<JToken>) this).GetEnumerator();
        }

        DynamicMetaObject IDynamicMetaObjectProvider.GetMetaObject(Expression parameter)
        {
            return this.GetMetaObject(parameter);
        }

        object ICloneable.Clone()
        {
            return this.DeepClone();
        }

        public T ToObject<T>()
        {
            return (T) this.ToObject(typeof(T));
        }

        public T ToObject<T>(JsonSerializer jsonSerializer)
        {
            return (T) this.ToObject(typeof(T), jsonSerializer);
        }

        public object ToObject(System.Type objectType)
        {
            return this.ToObject(objectType, false);
        }

        public object ToObject(System.Type objectType, JsonSerializer jsonSerializer)
        {
            ValidationUtils.ArgumentNotNull(jsonSerializer, "jsonSerializer");
            using (JTokenReader reader = new JTokenReader(this))
            {
                return jsonSerializer.Deserialize(reader, objectType);
            }
        }

        private object ToObject(System.Type objectType, bool isNullable)
        {
            switch (ConvertUtils.GetTypeCode(objectType))
            {
                case TypeCode.Object:
                    if (isNullable || !ReflectionUtils.IsNullableType(objectType))
                    {
                        break;
                    }
                    return this.ToObject(Nullable.GetUnderlyingType(objectType), true);

                case TypeCode.Boolean:
                    if (!isNullable)
                    {
                        return (bool) this;
                    }
                    return (bool?) this;

                case TypeCode.Char:
                    if (!isNullable)
                    {
                        return (char) this;
                    }
                    return (char?) this;

                case TypeCode.SByte:
                {
                    if (!isNullable)
                    {
                        return (sbyte) ((short) this);
                    }
                    short? nullable = (short?) this;
                    return (nullable.HasValue ? new sbyte?((sbyte) nullable.GetValueOrDefault()) : null);
                }
                case TypeCode.Byte:
                    if (!isNullable)
                    {
                        return (byte) this;
                    }
                    return (byte?) this;

                case TypeCode.Int16:
                    if (!isNullable)
                    {
                        return (short) this;
                    }
                    return (short?) this;

                case TypeCode.UInt16:
                    if (!isNullable)
                    {
                        return (ushort) this;
                    }
                    return (ushort?) this;

                case TypeCode.Int32:
                    if (!isNullable)
                    {
                        return (int) this;
                    }
                    return (int?) this;

                case TypeCode.UInt32:
                    if (!isNullable)
                    {
                        return (uint) this;
                    }
                    return (uint?) this;

                case TypeCode.Int64:
                    if (!isNullable)
                    {
                        return (long) this;
                    }
                    return (long?) this;

                case TypeCode.UInt64:
                    if (!isNullable)
                    {
                        return (ulong) this;
                    }
                    return (ulong?) this;

                case TypeCode.Single:
                    if (!isNullable)
                    {
                        return (float) this;
                    }
                    return (float?) this;

                case TypeCode.Double:
                    if (!isNullable)
                    {
                        return (double) this;
                    }
                    return (double?) this;

                case TypeCode.Decimal:
                    if (!isNullable)
                    {
                        return (decimal) this;
                    }
                    return (decimal?) this;

                case TypeCode.DateTime:
                    if (!isNullable)
                    {
                        return (DateTime) this;
                    }
                    return (DateTime?) this;

                case TypeCode.String:
                    return (string) this;
            }
            if (objectType == typeof(DateTimeOffset))
            {
                if (isNullable)
                {
                    return (DateTimeOffset?) this;
                }
                return (DateTimeOffset) this;
            }
            if (objectType == typeof(Guid))
            {
                if (isNullable)
                {
                    return (Guid?) this;
                }
                return (Guid) this;
            }
            if (objectType == typeof(Uri))
            {
                return (Uri) this;
            }
            if (!(objectType == typeof(TimeSpan)))
            {
                return this.ToObject(objectType, new JsonSerializer());
            }
            if (isNullable)
            {
                return (TimeSpan?) this;
            }
            return (TimeSpan) this;
        }

        public override string ToString()
        {
            return this.ToString(Formatting.Indented, new JsonConverter[0]);
        }

        public string ToString(Formatting formatting, params JsonConverter[] converters)
        {
            using (StringWriter writer = new StringWriter(CultureInfo.InvariantCulture))
            {
                JsonTextWriter writer2 = new JsonTextWriter(writer) {
                    Formatting = formatting
                };
                this.WriteTo(writer2, converters);
                return writer.ToString();
            }
        }

        private static bool ValidateToken(JToken o, JTokenType[] validTypes, bool nullable)
        {
            if (Array.IndexOf<JTokenType>(validTypes, o.Type) == -1)
            {
                if (!nullable)
                {
                    return false;
                }
                if (o.Type != JTokenType.Null)
                {
                    return (o.Type == JTokenType.Undefined);
                }
            }
            return true;
        }

        public virtual T Value<T>(object key)
        {
            JToken token = this[key];
            return token.Convert<JToken, T>();
        }

        public virtual IEnumerable<T> Values<T>()
        {
            throw new InvalidOperationException("Cannot access child value on {0}.".FormatWith(CultureInfo.InvariantCulture, base.GetType()));
        }

        public abstract void WriteTo(JsonWriter writer, params JsonConverter[] converters);

        public static JTokenEqualityComparer EqualityComparer
        {
            get
            {
                if (_equalityComparer == null)
                {
                    _equalityComparer = new JTokenEqualityComparer();
                }
                return _equalityComparer;
            }
        }

        public virtual JToken First
        {
            get
            {
                throw new InvalidOperationException("Cannot access child value on {0}.".FormatWith(CultureInfo.InvariantCulture, base.GetType()));
            }
        }

        public abstract bool HasValues { get; }

        public virtual JToken this[object key]
        {
            get
            {
                throw new InvalidOperationException("Cannot access child value on {0}.".FormatWith(CultureInfo.InvariantCulture, base.GetType()));
            }
            set
            {
                throw new InvalidOperationException("Cannot set child value on {0}.".FormatWith(CultureInfo.InvariantCulture, base.GetType()));
            }
        }

        public virtual JToken Last
        {
            get
            {
                throw new InvalidOperationException("Cannot access child value on {0}.".FormatWith(CultureInfo.InvariantCulture, base.GetType()));
            }
        }

        int IJsonLineInfo.LineNumber
        {
            get
            {
                int? nullable = this._lineNumber;
                if (!nullable.HasValue)
                {
                    return 0;
                }
                return nullable.GetValueOrDefault();
            }
        }

        int IJsonLineInfo.LinePosition
        {
            get
            {
                int? nullable = this._linePosition;
                if (!nullable.HasValue)
                {
                    return 0;
                }
                return nullable.GetValueOrDefault();
            }
        }

        IJEnumerable<JToken> IJEnumerable<JToken>.this[object key]
        {
            get
            {
                return this[key];
            }
        }

        public JToken Next
        {
            get
            {
                return this._next;
            }
            internal set
            {
                this._next = value;
            }
        }

        public JContainer Parent
        {
            [DebuggerStepThrough]
            get
            {
                return this._parent;
            }
            internal set
            {
                this._parent = value;
            }
        }

        public JToken Previous
        {
            get
            {
                return this._previous;
            }
            internal set
            {
                this._previous = value;
            }
        }

        public JToken Root
        {
            get
            {
                JContainer parent = this.Parent;
                if (parent != null)
                {
                    while (parent.Parent != null)
                    {
                        parent = parent.Parent;
                    }
                    return parent;
                }
                return this;
            }
        }

        public abstract JTokenType Type { get; }



    }
}

