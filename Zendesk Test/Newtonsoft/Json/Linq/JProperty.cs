﻿namespace Newtonsoft.Json.Linq
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Utilities;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;

    public class JProperty : JContainer
    {
        private readonly List<JToken> _content;
        private readonly string _name;

        public JProperty(JProperty other) : base(other)
        {
            this._content = new List<JToken>();
            this._name = other.Name;
        }

        internal JProperty(string name)
        {
            this._content = new List<JToken>();
            ValidationUtils.ArgumentNotNull(name, "name");
            this._name = name;
        }

        public JProperty(string name, params object[] content) : this(name, content)
        {
        }

        public JProperty(string name, object content)
        {
            this._content = new List<JToken>();
            ValidationUtils.ArgumentNotNull(name, "name");
            this._name = name;
            this.Value = base.IsMultiContent(content) ? new JArray(content) : base.CreateFromContent(content);
        }

        internal override void ClearItems()
        {
            throw new JsonException("Cannot add or remove items from {0}.".FormatWith(CultureInfo.InvariantCulture, typeof(JProperty)));
        }

        internal override JToken CloneToken()
        {
            return new JProperty(this);
        }

        internal override bool ContainsItem(JToken item)
        {
            return (this.Value == item);
        }

        internal override bool DeepEquals(JToken node)
        {
            JProperty container = node as JProperty;
            return (((container != null) && (this._name == container.Name)) && base.ContentsEqual(container));
        }

        internal override int GetDeepHashCode()
        {
            return (this._name.GetHashCode() ^ ((this.Value != null) ? this.Value.GetDeepHashCode() : 0));
        }

        internal override JToken GetItem(int index)
        {
            if (index != 0)
            {
                throw new ArgumentOutOfRangeException();
            }
            return this.Value;
        }

        internal override void InsertItem(int index, JToken item, bool skipParentCheck)
        {
            if (this.Value != null)
            {
                throw new JsonException("{0} cannot have multiple values.".FormatWith(CultureInfo.InvariantCulture, typeof(JProperty)));
            }
            base.InsertItem(0, item, false);
        }

        public static JProperty Load(JsonReader reader)
        {
            if ((reader.TokenType == JsonToken.None) && !reader.Read())
            {
                throw JsonReaderException.Create(reader, "Error reading JProperty from JsonReader.");
            }
            while (reader.TokenType == JsonToken.Comment)
            {
                reader.Read();
            }
            if (reader.TokenType != JsonToken.PropertyName)
            {
                throw JsonReaderException.Create(reader, "Error reading JProperty from JsonReader. Current JsonReader item is not a property: {0}".FormatWith(CultureInfo.InvariantCulture, reader.TokenType));
            }
            JProperty property = new JProperty((string) reader.Value);
            property.SetLineInfo(reader as IJsonLineInfo);
            property.ReadTokenFrom(reader);
            return property;
        }

        internal override bool RemoveItem(JToken item)
        {
            throw new JsonException("Cannot add or remove items from {0}.".FormatWith(CultureInfo.InvariantCulture, typeof(JProperty)));
        }

        internal override void RemoveItemAt(int index)
        {
            throw new JsonException("Cannot add or remove items from {0}.".FormatWith(CultureInfo.InvariantCulture, typeof(JProperty)));
        }

        internal override void SetItem(int index, JToken item)
        {
            if (index != 0)
            {
                throw new ArgumentOutOfRangeException();
            }
            if (!JContainer.IsTokenUnchanged(this.Value, item))
            {
                if (base.Parent != null)
                {
                    ((JObject) base.Parent).InternalPropertyChanging(this);
                }
                base.SetItem(0, item);
                if (base.Parent != null)
                {
                    ((JObject) base.Parent).InternalPropertyChanged(this);
                }
            }
        }

        public override void WriteTo(JsonWriter writer, params JsonConverter[] converters)
        {
            writer.WritePropertyName(this._name);
            JToken token = this.Value;
            if (token != null)
            {
                token.WriteTo(writer, converters);
            }
            else
            {
                writer.WriteNull();
            }
        }

        protected override IList<JToken> ChildrenTokens
        {
            get
            {
                return this._content;
            }
        }

        public string Name
        {
            [DebuggerStepThrough]
            get
            {
                return this._name;
            }
        }

        public override JTokenType Type
        {
            [DebuggerStepThrough]
            get
            {
                return JTokenType.Property;
            }
        }

        public JToken Value
        {
            [DebuggerStepThrough]
            get
            {
                if (this._content.Count <= 0)
                {
                    return null;
                }
                return this._content[0];
            }
            set
            {
                base.CheckReentrancy();
                JToken item = value ?? new JValue(null);
                if (this._content.Count == 0)
                {
                    this.InsertItem(0, item, false);
                }
                else
                {
                    this.SetItem(0, item);
                }
            }
        }
    }
}

