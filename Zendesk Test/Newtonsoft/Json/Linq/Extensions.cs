﻿namespace Newtonsoft.Json.Linq
{
    using Newtonsoft.Json.Utilities;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Runtime.CompilerServices;
    using System.Threading;

    public static class Extensions
    {
        public static IJEnumerable<JToken> Ancestors<T>(this IEnumerable<T> source) where T: JToken
        {
            ValidationUtils.ArgumentNotNull(source, "source");
            return (from j in source select j.Ancestors()).AsJEnumerable();
        }

        public static IJEnumerable<JToken> AsJEnumerable(this IEnumerable<JToken> source)
        {
            return source.AsJEnumerable<JToken>();
        }

        public static IJEnumerable<T> AsJEnumerable<T>(this IEnumerable<T> source) where T: JToken
        {
            if (source == null)
            {
                return null;
            }
            if (source is IJEnumerable<T>)
            {
                return (IJEnumerable<T>) source;
            }
            return new JEnumerable<T>(source);
        }

        public static IJEnumerable<JToken> Children<T>(this IEnumerable<T> source) where T: JToken
        {
            return source.Children<T, JToken>().AsJEnumerable();
        }

        public static IEnumerable<U> Children<T, U>(this IEnumerable<T> source) where T: JToken
        {
            ValidationUtils.ArgumentNotNull(source, "source");
            return ((IEnumerable<JToken>) (from c in source select c.Children())).Convert<JToken, U>();
        }

        internal static IEnumerable<U> Convert<T, U>(this IEnumerable<T> source) where T: JToken
        {
            ValidationUtils.ArgumentNotNull(source, "source");
            foreach (T iteratorVariable0 in source)
            {
                yield return iteratorVariable0.Convert<JToken, U>();
            }
        }

        internal static U Convert<T, U>(this T token) where T: JToken
        {
            if (token == null)
            {
                return default(U);
            }
            if (((token is U) && (typeof(U) != typeof(IComparable))) && (typeof(U) != typeof(IFormattable)))
            {
                return (U) token;
            }
            JValue value2 = token as JValue;
            if (value2 == null)
            {
                throw new InvalidCastException("Cannot cast {0} to {1}.".FormatWith(CultureInfo.InvariantCulture, token.GetType(), typeof(T)));
            }
            if (value2.Value is U)
            {
                return (U) value2.Value;
            }
            Type t = typeof(U);
            if (ReflectionUtils.IsNullableType(t))
            {
                if (value2.Value == null)
                {
                    return default(U);
                }
                t = Nullable.GetUnderlyingType(t);
            }
            return (U) System.Convert.ChangeType(value2.Value, t, CultureInfo.InvariantCulture);
        }

        public static IJEnumerable<JToken> Descendants<T>(this IEnumerable<T> source) where T: JContainer
        {
            ValidationUtils.ArgumentNotNull(source, "source");
            return (from j in source select j.Descendants()).AsJEnumerable();
        }

        public static IJEnumerable<JProperty> Properties(this IEnumerable<JObject> source)
        {
            ValidationUtils.ArgumentNotNull(source, "source");
            return (from d in source select d.Properties()).AsJEnumerable<JProperty>();
        }

        public static U Value<U>(this IEnumerable<JToken> value)
        {
            return value.Value<JToken, U>();
        }

        public static U Value<T, U>(this IEnumerable<T> value) where T: JToken
        {
            ValidationUtils.ArgumentNotNull(value, "source");
            JToken token = value as JToken;
            if (token == null)
            {
                throw new ArgumentException("Source value must be a JToken.");
            }
            return token.Convert<JToken, U>();
        }

        public static IJEnumerable<JToken> Values(this IEnumerable<JToken> source)
        {
            return source.Values(null);
        }

        public static IEnumerable<U> Values<U>(this IEnumerable<JToken> source)
        {
            return source.Values<JToken, U>(null);
        }

        public static IJEnumerable<JToken> Values(this IEnumerable<JToken> source, object key)
        {
            return source.Values<JToken, JToken>(key).AsJEnumerable();
        }

        public static IEnumerable<U> Values<U>(this IEnumerable<JToken> source, object key)
        {
            return source.Values<JToken, U>(key);
        }

        internal static IEnumerable<U> Values<T, U>(this IEnumerable<T> source, object key) where T: JToken
        {
            ValidationUtils.ArgumentNotNull(source, "source");
            foreach (JToken iteratorVariable0 in source)
            {
                if (key == null)
                {
                    if (iteratorVariable0 is JValue)
                    {
                        yield return ((JValue) iteratorVariable0).Convert<JValue, U>();
                    }
                    else
                    {
                        foreach (JToken iteratorVariable1 in iteratorVariable0.Children())
                        {
                            yield return iteratorVariable1.Convert<JToken, U>();
                        }
                    }
                }
                else
                {
                    JToken token = iteratorVariable0[key];
                    if (token != null)
                    {
                        yield return token.Convert<JToken, U>();
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <Convert>d__f<T, U> : IEnumerable, IDisposable, IEnumerator, IEnumerable<U>, IEnumerator<U> where T: JToken
        {
            private int <>1__state;
            private U <>2__current;
            public IEnumerable<T> <>3__source;
            public IEnumerator<T> <>7__wrap11;
            private int <>l__initialThreadId;
            public T <token>5__10;
            public IEnumerable<T> source;

            [DebuggerHidden]
            public <Convert>d__f(int <>1__state)
            {
                this.<>1__state = <>1__state;
                this.<>l__initialThreadId = Thread.CurrentThread.ManagedThreadId;
            }

            private void <>m__Finally12()
            {
                this.<>1__state = -1;
                if (this.<>7__wrap11 != null)
                {
                    this.<>7__wrap11.Dispose();
                }
            }

            private bool MoveNext()
            {
                bool flag;
                try
                {
                    switch (this.<>1__state)
                    {
                        case 0:
                            this.<>1__state = -1;
                            ValidationUtils.ArgumentNotNull(this.source, "source");
                            this.<>7__wrap11 = this.source.GetEnumerator();
                            this.<>1__state = 1;
                            goto Label_0085;

                        case 2:
                            this.<>1__state = 1;
                            goto Label_0085;

                        default:
                            goto Label_0098;
                    }
                Label_004C:
                    this.<token>5__10 = this.<>7__wrap11.Current;
                    this.<>2__current = this.<token>5__10.Convert<JToken, U>();
                    this.<>1__state = 2;
                    return true;
                Label_0085:
                    if (this.<>7__wrap11.MoveNext())
                    {
                        goto Label_004C;
                    }
                    this.<>m__Finally12();
                Label_0098:
                    flag = false;
                }
                fault
                {
                    this.System.IDisposable.Dispose();
                }
                return flag;
            }

            [DebuggerHidden]
            IEnumerator<U> IEnumerable<U>.GetEnumerator()
            {
                Extensions.<Convert>d__f<T, U> _f;
                if ((Thread.CurrentThread.ManagedThreadId == this.<>l__initialThreadId) && (this.<>1__state == -2))
                {
                    this.<>1__state = 0;
                    _f = (Extensions.<Convert>d__f<T, U>) this;
                }
                else
                {
                    _f = new Extensions.<Convert>d__f<T, U>(0);
                }
                _f.source = this.<>3__source;
                return _f;
            }

            [DebuggerHidden]
            IEnumerator IEnumerable.GetEnumerator()
            {
                return this.System.Collections.Generic.IEnumerable<U>.GetEnumerator();
            }

            [DebuggerHidden]
            void IEnumerator.Reset()
            {
                throw new NotSupportedException();
            }

            void IDisposable.Dispose()
            {
                switch (this.<>1__state)
                {
                    case 1:
                    case 2:
                        try
                        {
                        }
                        finally
                        {
                            this.<>m__Finally12();
                        }
                        return;
                }
            }

            U IEnumerator<U>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.<>2__current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.<>2__current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <Values>d__4<T, U> : IEnumerable, IDisposable, IEnumerator, IEnumerable<U>, IEnumerator<U> where T: JToken
        {
            private int <>1__state;
            private U <>2__current;
            public object <>3__key;
            public IEnumerable<T> <>3__source;
            public IEnumerator<T> <>7__wrap8;
            public IEnumerator<JToken> <>7__wrapa;
            private int <>l__initialThreadId;
            public JToken <t>5__6;
            public JToken <token>5__5;
            public JToken <value>5__7;
            public object key;
            public IEnumerable<T> source;

            [DebuggerHidden]
            public <Values>d__4(int <>1__state)
            {
                this.<>1__state = <>1__state;
                this.<>l__initialThreadId = Thread.CurrentThread.ManagedThreadId;
            }

            private void <>m__Finally9()
            {
                this.<>1__state = -1;
                if (this.<>7__wrap8 != null)
                {
                    this.<>7__wrap8.Dispose();
                }
            }

            private void <>m__Finallyb()
            {
                this.<>1__state = 1;
                if (this.<>7__wrapa != null)
                {
                    this.<>7__wrapa.Dispose();
                }
            }

            private bool MoveNext()
            {
                bool flag;
                try
                {
                    switch (this.<>1__state)
                    {
                        case 0:
                            this.<>1__state = -1;
                            ValidationUtils.ArgumentNotNull(this.source, "source");
                            this.<>7__wrap8 = this.source.GetEnumerator();
                            this.<>1__state = 1;
                            goto Label_0169;

                        case 2:
                            this.<>1__state = 1;
                            goto Label_0169;

                        case 4:
                            goto Label_010B;

                        case 5:
                            this.<>1__state = 1;
                            goto Label_0169;

                        default:
                            goto Label_017F;
                    }
                Label_005E:
                    this.<token>5__5 = this.<>7__wrap8.Current;
                    if (this.key == null)
                    {
                        if (this.<token>5__5 is JValue)
                        {
                            this.<>2__current = ((JValue) this.<token>5__5).Convert<JValue, U>();
                            this.<>1__state = 2;
                            return true;
                        }
                        this.<>7__wrapa = this.<token>5__5.Children().GetEnumerator();
                        this.<>1__state = 3;
                        while (this.<>7__wrapa.MoveNext())
                        {
                            this.<t>5__6 = this.<>7__wrapa.Current;
                            this.<>2__current = this.<t>5__6.Convert<JToken, U>();
                            this.<>1__state = 4;
                            return true;
                        Label_010B:
                            this.<>1__state = 3;
                        }
                        this.<>m__Finallyb();
                    }
                    else
                    {
                        this.<value>5__7 = this.<token>5__5[this.key];
                        if (this.<value>5__7 != null)
                        {
                            this.<>2__current = this.<value>5__7.Convert<JToken, U>();
                            this.<>1__state = 5;
                            return true;
                        }
                    }
                Label_0169:
                    if (this.<>7__wrap8.MoveNext())
                    {
                        goto Label_005E;
                    }
                    this.<>m__Finally9();
                Label_017F:
                    flag = false;
                }
                fault
                {
                    this.System.IDisposable.Dispose();
                }
                return flag;
            }

            [DebuggerHidden]
            IEnumerator<U> IEnumerable<U>.GetEnumerator()
            {
                Extensions.<Values>d__4<T, U> d__;
                if ((Thread.CurrentThread.ManagedThreadId == this.<>l__initialThreadId) && (this.<>1__state == -2))
                {
                    this.<>1__state = 0;
                    d__ = (Extensions.<Values>d__4<T, U>) this;
                }
                else
                {
                    d__ = new Extensions.<Values>d__4<T, U>(0);
                }
                d__.source = this.<>3__source;
                d__.key = this.<>3__key;
                return d__;
            }

            [DebuggerHidden]
            IEnumerator IEnumerable.GetEnumerator()
            {
                return this.System.Collections.Generic.IEnumerable<U>.GetEnumerator();
            }

            [DebuggerHidden]
            void IEnumerator.Reset()
            {
                throw new NotSupportedException();
            }

            void IDisposable.Dispose()
            {
                switch (this.<>1__state)
                {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                        try
                        {
                            switch (this.<>1__state)
                            {
                                case 3:
                                case 4:
                                    try
                                    {
                                    }
                                    finally
                                    {
                                        this.<>m__Finallyb();
                                    }
                                    return;
                            }
                        }
                        finally
                        {
                            this.<>m__Finally9();
                        }
                        break;

                    default:
                        return;
                }
            }

            U IEnumerator<U>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.<>2__current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.<>2__current;
                }
            }
        }
    }
}

