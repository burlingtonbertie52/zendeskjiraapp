﻿namespace Newtonsoft.Json.Linq
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Utilities;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Runtime.CompilerServices;

    internal class JPath
    {
        private int _currentIndex;
        private readonly string _expression;

        public JPath(string expression)
        {
            ValidationUtils.ArgumentNotNull(expression, "expression");
            this._expression = expression;
            this.Parts = new List<object>();
            this.ParseMain();
        }

        internal JToken Evaluate(JToken root, bool errorWhenNoMatch)
        {
            JToken token = root;
            foreach (object obj2 in this.Parts)
            {
                string str = obj2 as string;
                if (str != null)
                {
                    JObject obj3 = token as JObject;
                    if (obj3 == null)
                    {
                        if (errorWhenNoMatch)
                        {
                            throw new JsonException("Property '{0}' not valid on {1}.".FormatWith(CultureInfo.InvariantCulture, str, token.GetType().Name));
                        }
                        return null;
                    }
                    token = obj3[str];
                    if ((token == null) && errorWhenNoMatch)
                    {
                        throw new JsonException("Property '{0}' does not exist on JObject.".FormatWith(CultureInfo.InvariantCulture, str));
                    }
                }
                else
                {
                    int num = (int) obj2;
                    JArray array = token as JArray;
                    JConstructor constructor = token as JConstructor;
                    if (array != null)
                    {
                        if (array.Count <= num)
                        {
                            if (errorWhenNoMatch)
                            {
                                throw new IndexOutOfRangeException("Index {0} outside the bounds of JArray.".FormatWith(CultureInfo.InvariantCulture, num));
                            }
                            return null;
                        }
                        token = array[num];
                    }
                    else if (constructor != null)
                    {
                        if (constructor.Count <= num)
                        {
                            if (errorWhenNoMatch)
                            {
                                throw new IndexOutOfRangeException("Index {0} outside the bounds of JConstructor.".FormatWith(CultureInfo.InvariantCulture, num));
                            }
                            return null;
                        }
                        token = constructor[num];
                    }
                    else
                    {
                        if (errorWhenNoMatch)
                        {
                            throw new JsonException("Index {0} not valid on {1}.".FormatWith(CultureInfo.InvariantCulture, num, token.GetType().Name));
                        }
                        return null;
                    }
                }
            }
            return token;
        }

        private void ParseIndexer(char indexerOpenChar)
        {
            this._currentIndex++;
            char ch = (indexerOpenChar == '[') ? ']' : ')';
            int startIndex = this._currentIndex;
            int length = 0;
            bool flag = false;
            while (this._currentIndex < this._expression.Length)
            {
                char c = this._expression[this._currentIndex];
                if (char.IsDigit(c))
                {
                    length++;
                }
                else
                {
                    if (c != ch)
                    {
                        throw new JsonException("Unexpected character while parsing path indexer: " + c);
                    }
                    flag = true;
                    break;
                }
                this._currentIndex++;
            }
            if (!flag)
            {
                throw new JsonException("Path ended with open indexer. Expected " + ch);
            }
            if (length == 0)
            {
                throw new JsonException("Empty path indexer.");
            }
            string str = this._expression.Substring(startIndex, length);
            this.Parts.Add(Convert.ToInt32(str, CultureInfo.InvariantCulture));
        }

        private void ParseMain()
        {
            int startIndex = this._currentIndex;
            bool flag = false;
            while (this._currentIndex < this._expression.Length)
            {
                char indexerOpenChar = this._expression[this._currentIndex];
                switch (indexerOpenChar)
                {
                    case '(':
                    case '[':
                        if (this._currentIndex > startIndex)
                        {
                            string item = this._expression.Substring(startIndex, this._currentIndex - startIndex);
                            this.Parts.Add(item);
                        }
                        this.ParseIndexer(indexerOpenChar);
                        startIndex = this._currentIndex + 1;
                        flag = true;
                        break;

                    case ')':
                    case ']':
                        throw new JsonException("Unexpected character while parsing path: " + indexerOpenChar);

                    case '.':
                        if (this._currentIndex > startIndex)
                        {
                            string str2 = this._expression.Substring(startIndex, this._currentIndex - startIndex);
                            this.Parts.Add(str2);
                        }
                        startIndex = this._currentIndex + 1;
                        flag = false;
                        break;

                    default:
                        if (flag)
                        {
                            throw new JsonException("Unexpected character following indexer: " + indexerOpenChar);
                        }
                        break;
                }
                this._currentIndex++;
            }
            if (this._currentIndex > startIndex)
            {
                string str3 = this._expression.Substring(startIndex, this._currentIndex - startIndex);
                this.Parts.Add(str3);
            }
        }

        public List<object> Parts { get; private set; }
    }
}

