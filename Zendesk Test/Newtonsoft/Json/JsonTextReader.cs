﻿namespace Newtonsoft.Json
{
    using Newtonsoft.Json.Utilities;
    using System;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;

    public class JsonTextReader : JsonReader, IJsonLineInfo
    {
        private StringBuffer _buffer;
        private int _charPos;
        private char[] _chars;
        private int _charsUsed;
        private bool _isEndOfFile;
        private int _lineNumber;
        private int _lineStartPos;
        private readonly TextReader _reader;
        private StringReference _stringReference;
        private const char UnicodeReplacementChar = '�';

        public JsonTextReader(TextReader reader)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            this._reader = reader;
            this._lineNumber = 1;
            this._chars = new char[0x1001];
        }

        private static void BlockCopyChars(char[] src, int srcOffset, char[] dst, int dstOffset, int count)
        {
            Buffer.BlockCopy(src, srcOffset * 2, dst, dstOffset * 2, count * 2);
        }

        private void ClearRecentString()
        {
            if (this._buffer != null)
            {
                this._buffer.Position = 0;
            }
            this._stringReference = new StringReference();
        }

        public override void Close()
        {
            base.Close();
            if (base.CloseInput && (this._reader != null))
            {
                this._reader.Close();
            }
            if (this._buffer != null)
            {
                this._buffer.Clear();
            }
        }

        private bool EatWhitespace(bool oneOrMore)
        {
            bool flag = false;
            bool flag2 = false;
            while (!flag)
            {
                char c = this._chars[this._charPos];
                switch (c)
                {
                    case '\0':
                    {
                        if (this._charsUsed == this._charPos)
                        {
                            if (this.ReadData(false) == 0)
                            {
                                flag = true;
                            }
                        }
                        else
                        {
                            this._charPos++;
                        }
                        continue;
                    }
                    case '\n':
                    {
                        this.ProcessLineFeed();
                        continue;
                    }
                    case '\r':
                    {
                        this.ProcessCarriageReturn(false);
                        continue;
                    }
                }
                if ((c == ' ') || char.IsWhiteSpace(c))
                {
                    flag2 = true;
                    this._charPos++;
                }
                else
                {
                    flag = true;
                }
            }
            if (oneOrMore)
            {
                return flag2;
            }
            return true;
        }

        private bool EnsureChars(int relativePosition, bool append)
        {
            if ((this._charPos + relativePosition) >= this._charsUsed)
            {
                return this.ReadChars(relativePosition, append);
            }
            return true;
        }

        private StringBuffer GetBuffer()
        {
            if (this._buffer == null)
            {
                this._buffer = new StringBuffer(0x1000);
            }
            else
            {
                this._buffer.Position = 0;
            }
            return this._buffer;
        }

        public bool HasLineInfo()
        {
            return true;
        }

        private bool IsSeperator(char c)
        {
            switch (c)
            {
                case '\t':
                case '\n':
                case '\r':
                case ' ':
                    return true;

                case ')':
                    if ((base.CurrentState != JsonReader.State.Constructor) && (base.CurrentState != JsonReader.State.ConstructorStart))
                    {
                        break;
                    }
                    return true;

                case ']':
                case '}':
                case ',':
                    return true;

                case '/':
                    if (!this.EnsureChars(1, false))
                    {
                        return false;
                    }
                    return (this._chars[this._charPos + 1] == '*');

                default:
                    if (char.IsWhiteSpace(c))
                    {
                        return true;
                    }
                    break;
            }
            return false;
        }

        private bool MatchValue(string value)
        {
            if (!this.EnsureChars(value.Length - 1, true))
            {
                return false;
            }
            for (int i = 0; i < value.Length; i++)
            {
                if (this._chars[this._charPos + i] != value[i])
                {
                    return false;
                }
            }
            this._charPos += value.Length;
            return true;
        }

        private bool MatchValueWithTrailingSeperator(string value)
        {
            if (!this.MatchValue(value))
            {
                return false;
            }
            if (this.EnsureChars(0, false) && !this.IsSeperator(this._chars[this._charPos]))
            {
                return (this._chars[this._charPos] == '\0');
            }
            return true;
        }

        private void OnNewLine(int pos)
        {
            this._lineNumber++;
            this._lineStartPos = pos - 1;
        }

        private void ParseComment()
        {
            this._charPos++;
            if (!this.EnsureChars(1, false) || (this._chars[this._charPos] != '*'))
            {
                throw JsonReaderException.Create(this, "Error parsing comment. Expected: *, got {0}.".FormatWith(CultureInfo.InvariantCulture, this._chars[this._charPos]));
            }
            this._charPos++;
            int startIndex = this._charPos;
            bool flag = false;
            while (!flag)
            {
                switch (this._chars[this._charPos])
                {
                    case '\r':
                    {
                        this.ProcessCarriageReturn(true);
                        continue;
                    }
                    case '*':
                    {
                        this._charPos++;
                        if (this.EnsureChars(0, true) && (this._chars[this._charPos] == '/'))
                        {
                            this._stringReference = new StringReference(this._chars, startIndex, (this._charPos - startIndex) - 1);
                            this._charPos++;
                            flag = true;
                        }
                        continue;
                    }
                    case '\0':
                    {
                        if (this._charsUsed == this._charPos)
                        {
                            if (this.ReadData(true) == 0)
                            {
                                throw JsonReaderException.Create(this, "Unexpected end while parsing comment.");
                            }
                        }
                        else
                        {
                            this._charPos++;
                        }
                        continue;
                    }
                    case '\n':
                    {
                        this.ProcessLineFeed();
                        continue;
                    }
                }
                this._charPos++;
            }
            base.SetToken(JsonToken.Comment, this._stringReference.ToString());
            this.ClearRecentString();
        }

        private void ParseConstructor()
        {
            int num2;
            char ch;
            if (!this.MatchValueWithTrailingSeperator("new"))
            {
                return;
            }
            this.EatWhitespace(false);
            int startIndex = this._charPos;
        Label_001F:
            ch = this._chars[this._charPos];
            if (ch == '\0')
            {
                if (this._charsUsed == this._charPos)
                {
                    if (this.ReadData(true) == 0)
                    {
                        throw JsonReaderException.Create(this, "Unexpected end while parsing constructor.");
                    }
                    goto Label_001F;
                }
                num2 = this._charPos;
                this._charPos++;
            }
            else
            {
                if (char.IsLetterOrDigit(ch))
                {
                    this._charPos++;
                    goto Label_001F;
                }
                switch (ch)
                {
                    case '\r':
                        num2 = this._charPos;
                        this.ProcessCarriageReturn(true);
                        goto Label_00F7;

                    case '\n':
                        num2 = this._charPos;
                        this.ProcessLineFeed();
                        goto Label_00F7;
                }
                if (char.IsWhiteSpace(ch))
                {
                    num2 = this._charPos;
                    this._charPos++;
                }
                else
                {
                    if (ch != '(')
                    {
                        throw JsonReaderException.Create(this, "Unexpected character while parsing constructor: {0}.".FormatWith(CultureInfo.InvariantCulture, ch));
                    }
                    num2 = this._charPos;
                }
            }
        Label_00F7:
            this._stringReference = new StringReference(this._chars, startIndex, num2 - startIndex);
            string str = this._stringReference.ToString();
            this.EatWhitespace(false);
            if (this._chars[this._charPos] != '(')
            {
                throw JsonReaderException.Create(this, "Unexpected character while parsing constructor: {0}.".FormatWith(CultureInfo.InvariantCulture, this._chars[this._charPos]));
            }
            this._charPos++;
            this.ClearRecentString();
            base.SetToken(JsonToken.StartConstructor, str);
        }

        private bool ParseDateIso(string text)
        {
            if ((base._readType == ReadType.ReadAsDateTimeOffset) || ((base._readType == ReadType.Read) && (base._dateParseHandling == DateParseHandling.DateTimeOffset)))
            {
                DateTimeOffset offset;
                if (DateTimeOffset.TryParseExact(text, "yyyy-MM-ddTHH:mm:ss.FFFFFFFK", CultureInfo.InvariantCulture, DateTimeStyles.RoundtripKind, out offset))
                {
                    base.SetToken(JsonToken.Date, offset);
                    return true;
                }
            }
            else
            {
                DateTime time;
                if (DateTime.TryParseExact(text, "yyyy-MM-ddTHH:mm:ss.FFFFFFFK", CultureInfo.InvariantCulture, DateTimeStyles.RoundtripKind, out time))
                {
                    time = JsonConvert.EnsureDateTime(time, base.DateTimeZoneHandling);
                    base.SetToken(JsonToken.Date, time);
                    return true;
                }
            }
            return false;
        }

        private void ParseDateMicrosoft(string text)
        {
            string s = text.Substring(6, text.Length - 8);
            DateTimeKind utc = DateTimeKind.Utc;
            int index = s.IndexOf('+', 1);
            if (index == -1)
            {
                index = s.IndexOf('-', 1);
            }
            TimeSpan zero = TimeSpan.Zero;
            if (index != -1)
            {
                utc = DateTimeKind.Local;
                zero = ReadOffset(s.Substring(index));
                s = s.Substring(0, index);
            }
            DateTime time = JsonConvert.ConvertJavaScriptTicksToDateTime(long.Parse(s, NumberStyles.Integer, CultureInfo.InvariantCulture));
            if ((base._readType == ReadType.ReadAsDateTimeOffset) || ((base._readType == ReadType.Read) && (base._dateParseHandling == DateParseHandling.DateTimeOffset)))
            {
                base.SetToken(JsonToken.Date, new DateTimeOffset(time.Add(zero).Ticks, zero));
            }
            else
            {
                DateTime time2;
                switch (utc)
                {
                    case DateTimeKind.Unspecified:
                        time2 = DateTime.SpecifyKind(time.ToLocalTime(), DateTimeKind.Unspecified);
                        break;

                    case DateTimeKind.Local:
                        time2 = time.ToLocalTime();
                        break;

                    default:
                        time2 = time;
                        break;
                }
                time2 = JsonConvert.EnsureDateTime(time2, base.DateTimeZoneHandling);
                base.SetToken(JsonToken.Date, time2);
            }
        }

        private void ParseFalse()
        {
            if (!this.MatchValueWithTrailingSeperator(JsonConvert.False))
            {
                throw JsonReaderException.Create(this, "Error parsing boolean value.");
            }
            base.SetToken(JsonToken.Boolean, false);
        }

        private void ParseNull()
        {
            if (!this.MatchValueWithTrailingSeperator(JsonConvert.Null))
            {
                throw JsonReaderException.Create(this, "Error parsing null value.");
            }
            base.SetToken(JsonToken.Null);
        }

        private void ParseNumber()
        {
            object obj2;
            JsonToken integer;
            this.ShiftBufferIfNeeded();
            char c = this._chars[this._charPos];
            int startIndex = this._charPos;
            this.ReadNumberIntoBuffer();
            this._stringReference = new StringReference(this._chars, startIndex, this._charPos - startIndex);
            bool flag = char.IsDigit(c) && (this._stringReference.Length == 1);
            bool flag2 = (((c == '0') && (this._stringReference.Length > 1)) && ((this._stringReference.Chars[this._stringReference.StartIndex + 1] != '.') && (this._stringReference.Chars[this._stringReference.StartIndex + 1] != 'e'))) && (this._stringReference.Chars[this._stringReference.StartIndex + 1] != 'E');
            if (base._readType == ReadType.ReadAsInt32)
            {
                if (flag)
                {
                    obj2 = c - '0';
                }
                else if (flag2)
                {
                    string str = this._stringReference.ToString();
                    int num2 = str.StartsWith("0x", StringComparison.OrdinalIgnoreCase) ? Convert.ToInt32(str, 0x10) : Convert.ToInt32(str, 8);
                    obj2 = num2;
                }
                else
                {
                    obj2 = Convert.ToInt32(this._stringReference.ToString(), CultureInfo.InvariantCulture);
                }
                integer = JsonToken.Integer;
            }
            else if (base._readType == ReadType.ReadAsDecimal)
            {
                if (flag)
                {
                    obj2 = c - 48M;
                }
                else if (flag2)
                {
                    string str3 = this._stringReference.ToString();
                    long num3 = str3.StartsWith("0x", StringComparison.OrdinalIgnoreCase) ? Convert.ToInt64(str3, 0x10) : Convert.ToInt64(str3, 8);
                    obj2 = Convert.ToDecimal(num3);
                }
                else
                {
                    obj2 = decimal.Parse(this._stringReference.ToString(), NumberStyles.Float | NumberStyles.AllowThousands | NumberStyles.AllowTrailingSign, CultureInfo.InvariantCulture);
                }
                integer = JsonToken.Float;
            }
            else if (flag)
            {
                obj2 = ((long) c) - 0x30L;
                integer = JsonToken.Integer;
            }
            else if (flag2)
            {
                string str5 = this._stringReference.ToString();
                obj2 = str5.StartsWith("0x", StringComparison.OrdinalIgnoreCase) ? Convert.ToInt64(str5, 0x10) : Convert.ToInt64(str5, 8);
                integer = JsonToken.Integer;
            }
            else
            {
                string str6 = this._stringReference.ToString();
                if (((str6.IndexOf('.') != -1) || (str6.IndexOf('E') != -1)) || (str6.IndexOf('e') != -1))
                {
                    obj2 = Convert.ToDouble(str6, CultureInfo.InvariantCulture);
                    integer = JsonToken.Float;
                }
                else
                {
                    try
                    {
                        obj2 = Convert.ToInt64(str6, CultureInfo.InvariantCulture);
                    }
                    catch (OverflowException exception)
                    {
                        throw JsonReaderException.Create(this, "JSON integer {0} is too large or small for an Int64.".FormatWith(CultureInfo.InvariantCulture, str6), exception);
                    }
                    integer = JsonToken.Integer;
                }
            }
            this.ClearRecentString();
            base.SetToken(integer, obj2);
        }

        private void ParseNumberNaN()
        {
            if (!this.MatchValueWithTrailingSeperator(JsonConvert.NaN))
            {
                throw JsonReaderException.Create(this, "Error parsing NaN value.");
            }
            base.SetToken(JsonToken.Float, (double) 1.0 / (double) 0.0);
        }

        private void ParseNumberNegativeInfinity()
        {
            if (!this.MatchValueWithTrailingSeperator(JsonConvert.NegativeInfinity))
            {
                throw JsonReaderException.Create(this, "Error parsing negative infinity value.");
            }
            base.SetToken(JsonToken.Float, (double) -1.0 / (double) 0.0);
        }

        private void ParseNumberPositiveInfinity()
        {
            if (!this.MatchValueWithTrailingSeperator(JsonConvert.PositiveInfinity))
            {
                throw JsonReaderException.Create(this, "Error parsing positive infinity value.");
            }
            base.SetToken(JsonToken.Float, (double) 1.0 / (double) 0.0);
        }

        private bool ParseObject()
        {
            char ch;
        Label_0000:
            ch = this._chars[this._charPos];
            switch (ch)
            {
                case ' ':
                case '\t':
                    this._charPos++;
                    goto Label_0000;

                case '/':
                    this.ParseComment();
                    return true;

                case '}':
                    base.SetToken(JsonToken.EndObject);
                    this._charPos++;
                    return true;

                case '\n':
                    this.ProcessLineFeed();
                    goto Label_0000;

                case '\r':
                    this.ProcessCarriageReturn(false);
                    goto Label_0000;

                case '\0':
                    if (this._charsUsed == this._charPos)
                    {
                        if (this.ReadData(false) == 0)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        this._charPos++;
                    }
                    goto Label_0000;
            }
            if (char.IsWhiteSpace(ch))
            {
                this._charPos++;
                goto Label_0000;
            }
            return this.ParseProperty();
        }

        private bool ParsePostValue()
        {
            char ch;
        Label_0000:
            ch = this._chars[this._charPos];
            switch (ch)
            {
                case ' ':
                case '\t':
                    this._charPos++;
                    goto Label_0000;

                case ')':
                    this._charPos++;
                    base.SetToken(JsonToken.EndConstructor);
                    return true;

                case '\n':
                    this.ProcessLineFeed();
                    goto Label_0000;

                case '\r':
                    this.ProcessCarriageReturn(false);
                    goto Label_0000;

                case '\0':
                    if (this._charsUsed == this._charPos)
                    {
                        if (this.ReadData(false) == 0)
                        {
                            base._currentState = JsonReader.State.Finished;
                            return false;
                        }
                    }
                    else
                    {
                        this._charPos++;
                    }
                    goto Label_0000;

                case ',':
                    this._charPos++;
                    base.SetStateBasedOnCurrent();
                    return false;

                case '/':
                    this.ParseComment();
                    return true;

                case ']':
                    this._charPos++;
                    base.SetToken(JsonToken.EndArray);
                    return true;

                case '}':
                    this._charPos++;
                    base.SetToken(JsonToken.EndObject);
                    return true;
            }
            if (!char.IsWhiteSpace(ch))
            {
                throw JsonReaderException.Create(this, "After parsing a value an unexpected character was encountered: {0}.".FormatWith(CultureInfo.InvariantCulture, ch));
            }
            this._charPos++;
            goto Label_0000;
        }

        private bool ParseProperty()
        {
            char ch2;
            char ch = this._chars[this._charPos];
            switch (ch)
            {
                case '"':
                case '\'':
                    this._charPos++;
                    ch2 = ch;
                    this.ShiftBufferIfNeeded();
                    this.ReadStringIntoBuffer(ch2);
                    break;

                default:
                    if (!this.ValidIdentifierChar(ch))
                    {
                        throw JsonReaderException.Create(this, "Invalid property identifier character: {0}.".FormatWith(CultureInfo.InvariantCulture, this._chars[this._charPos]));
                    }
                    ch2 = '\0';
                    this.ShiftBufferIfNeeded();
                    this.ParseUnquotedProperty();
                    break;
            }
            string str = this._stringReference.ToString();
            this.EatWhitespace(false);
            if (this._chars[this._charPos] != ':')
            {
                throw JsonReaderException.Create(this, "Invalid character after parsing property name. Expected ':' but got: {0}.".FormatWith(CultureInfo.InvariantCulture, this._chars[this._charPos]));
            }
            this._charPos++;
            base.SetToken(JsonToken.PropertyName, str);
            this.QuoteChar = ch2;
            this.ClearRecentString();
            return true;
        }

        private void ParseString(char quote)
        {
            this._charPos++;
            this.ShiftBufferIfNeeded();
            this.ReadStringIntoBuffer(quote);
            if (base._readType == ReadType.ReadAsBytes)
            {
                byte[] buffer;
                if (this._stringReference.Length == 0)
                {
                    buffer = new byte[0];
                }
                else
                {
                    buffer = Convert.FromBase64CharArray(this._stringReference.Chars, this._stringReference.StartIndex, this._stringReference.Length);
                }
                base.SetToken(JsonToken.Bytes, buffer);
            }
            else if (base._readType == ReadType.ReadAsString)
            {
                string str = this._stringReference.ToString();
                base.SetToken(JsonToken.String, str);
                this.QuoteChar = quote;
            }
            else
            {
                string text = this._stringReference.ToString();
                if ((base._dateParseHandling != DateParseHandling.None) && (text.Length > 0))
                {
                    if (text[0] == '/')
                    {
                        if (text.StartsWith("/Date(", StringComparison.Ordinal) && text.EndsWith(")/", StringComparison.Ordinal))
                        {
                            this.ParseDateMicrosoft(text);
                            return;
                        }
                    }
                    else if ((char.IsDigit(text[0]) && (text.Length >= 0x13)) && ((text.Length <= 40) && this.ParseDateIso(text)))
                    {
                        return;
                    }
                }
                base.SetToken(JsonToken.String, text);
                this.QuoteChar = quote;
            }
        }

        private void ParseTrue()
        {
            if (!this.MatchValueWithTrailingSeperator(JsonConvert.True))
            {
                throw JsonReaderException.Create(this, "Error parsing boolean value.");
            }
            base.SetToken(JsonToken.Boolean, true);
        }

        private void ParseUndefined()
        {
            if (!this.MatchValueWithTrailingSeperator(JsonConvert.Undefined))
            {
                throw JsonReaderException.Create(this, "Error parsing undefined value.");
            }
            base.SetToken(JsonToken.Undefined);
        }

        private char ParseUnicode()
        {
            if (!this.EnsureChars(4, true))
            {
                throw JsonReaderException.Create(this, "Unexpected end while parsing unicode character.");
            }
            string s = new string(this._chars, this._charPos, 4);
            char ch = Convert.ToChar(int.Parse(s, NumberStyles.HexNumber, NumberFormatInfo.InvariantInfo));
            this._charPos += 4;
            return ch;
        }

        private void ParseUnquotedProperty()
        {
            char ch2;
            int startIndex = this._charPos;
        Label_0007:
            ch2 = this._chars[this._charPos];
            if (ch2 == '\0')
            {
                if (this._charsUsed != this._charPos)
                {
                    this._stringReference = new StringReference(this._chars, startIndex, this._charPos - startIndex);
                    return;
                }
                if (this.ReadData(true) == 0)
                {
                    throw JsonReaderException.Create(this, "Unexpected end while parsing unquoted property name.");
                }
                goto Label_0007;
            }
            char ch = this._chars[this._charPos];
            if (this.ValidIdentifierChar(ch))
            {
                this._charPos++;
                goto Label_0007;
            }
            if (!char.IsWhiteSpace(ch) && (ch != ':'))
            {
                throw JsonReaderException.Create(this, "Invalid JavaScript property identifier character: {0}.".FormatWith(CultureInfo.InvariantCulture, ch));
            }
            this._stringReference = new StringReference(this._chars, startIndex, this._charPos - startIndex);
        }

        private bool ParseValue()
        {
            char ch;
        Label_0000:
            ch = this._chars[this._charPos];
            switch (ch)
            {
                case '\t':
                case ' ':
                    this._charPos++;
                    goto Label_0000;

                case '\n':
                    this.ProcessLineFeed();
                    goto Label_0000;

                case '\r':
                    this.ProcessCarriageReturn(false);
                    goto Label_0000;

                case '\0':
                    if (this._charsUsed == this._charPos)
                    {
                        if (this.ReadData(false) == 0)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        this._charPos++;
                    }
                    goto Label_0000;

                case '"':
                case '\'':
                    this.ParseString(ch);
                    return true;

                case ')':
                    this._charPos++;
                    base.SetToken(JsonToken.EndConstructor);
                    return true;

                case ',':
                    base.SetToken(JsonToken.Undefined);
                    return true;

                case '-':
                    if (!this.EnsureChars(1, true) || (this._chars[this._charPos + 1] != 'I'))
                    {
                        this.ParseNumber();
                    }
                    else
                    {
                        this.ParseNumberNegativeInfinity();
                    }
                    return true;

                case '/':
                    this.ParseComment();
                    return true;

                case 'I':
                    this.ParseNumberPositiveInfinity();
                    return true;

                case '[':
                    this._charPos++;
                    base.SetToken(JsonToken.StartArray);
                    return true;

                case ']':
                    this._charPos++;
                    base.SetToken(JsonToken.EndArray);
                    return true;

                case 'f':
                    this.ParseFalse();
                    return true;

                case 'N':
                    this.ParseNumberNaN();
                    return true;

                case 't':
                    this.ParseTrue();
                    return true;

                case 'u':
                    this.ParseUndefined();
                    return true;

                case '{':
                    this._charPos++;
                    base.SetToken(JsonToken.StartObject);
                    return true;

                case 'n':
                    if (!this.EnsureChars(1, true))
                    {
                        throw JsonReaderException.Create(this, "Unexpected end.");
                    }
                    switch (this._chars[this._charPos + 1])
                    {
                        case 'u':
                            this.ParseNull();
                            goto Label_0191;

                        case 'e':
                            this.ParseConstructor();
                            goto Label_0191;
                    }
                    throw JsonReaderException.Create(this, "Unexpected character encountered while parsing value: {0}.".FormatWith(CultureInfo.InvariantCulture, this._chars[this._charPos]));

                default:
                    if (char.IsWhiteSpace(ch))
                    {
                        this._charPos++;
                        goto Label_0000;
                    }
                    if ((!char.IsNumber(ch) && (ch != '-')) && (ch != '.'))
                    {
                        throw JsonReaderException.Create(this, "Unexpected character encountered while parsing value: {0}.".FormatWith(CultureInfo.InvariantCulture, ch));
                    }
                    this.ParseNumber();
                    return true;
            }
        Label_0191:
            return true;
        }

        private void ProcessCarriageReturn(bool append)
        {
            this._charPos++;
            if (this.EnsureChars(1, append) && (this._chars[this._charPos] == '\n'))
            {
                this._charPos++;
            }
            this.OnNewLine(this._charPos);
        }

        private void ProcessLineFeed()
        {
            this._charPos++;
            this.OnNewLine(this._charPos);
        }

        [DebuggerStepThrough]
        public override bool Read()
        {
            base._readType = ReadType.Read;
            if (!this.ReadInternal())
            {
                base.SetToken(JsonToken.None);
                return false;
            }
            return true;
        }

        public override byte[] ReadAsBytes()
        {
            return base.ReadAsBytesInternal();
        }

        public override DateTime? ReadAsDateTime()
        {
            return base.ReadAsDateTimeInternal();
        }

        public override DateTimeOffset? ReadAsDateTimeOffset()
        {
            return base.ReadAsDateTimeOffsetInternal();
        }

        public override decimal? ReadAsDecimal()
        {
            return base.ReadAsDecimalInternal();
        }

        public override int? ReadAsInt32()
        {
            return base.ReadAsInt32Internal();
        }

        public override string ReadAsString()
        {
            return base.ReadAsStringInternal();
        }

        private bool ReadChars(int relativePosition, bool append)
        {
            if (this._isEndOfFile)
            {
                return false;
            }
            int num = ((this._charPos + relativePosition) - this._charsUsed) + 1;
            int num2 = 0;
            do
            {
                int num3 = this.ReadData(append, num - num2);
                if (num3 == 0)
                {
                    break;
                }
                num2 += num3;
            }
            while (num2 < num);
            if (num2 < num)
            {
                return false;
            }
            return true;
        }

        private int ReadData(bool append)
        {
            return this.ReadData(append, 0);
        }

        private int ReadData(bool append, int charsRequired)
        {
            if (this._isEndOfFile)
            {
                return 0;
            }
            if ((this._charsUsed + charsRequired) >= (this._chars.Length - 1))
            {
                if (append)
                {
                    char[] dst = new char[Math.Max((int) (this._chars.Length * 2), (int) ((this._charsUsed + charsRequired) + 1))];
                    BlockCopyChars(this._chars, 0, dst, 0, this._chars.Length);
                    this._chars = dst;
                }
                else
                {
                    int num2 = this._charsUsed - this._charPos;
                    if (((num2 + charsRequired) + 1) >= this._chars.Length)
                    {
                        char[] chArray2 = new char[(num2 + charsRequired) + 1];
                        if (num2 > 0)
                        {
                            BlockCopyChars(this._chars, this._charPos, chArray2, 0, num2);
                        }
                        this._chars = chArray2;
                    }
                    else if (num2 > 0)
                    {
                        BlockCopyChars(this._chars, this._charPos, this._chars, 0, num2);
                    }
                    this._lineStartPos -= this._charPos;
                    this._charPos = 0;
                    this._charsUsed = num2;
                }
            }
            int count = (this._chars.Length - this._charsUsed) - 1;
            int num4 = this._reader.Read(this._chars, this._charsUsed, count);
            this._charsUsed += num4;
            if (num4 == 0)
            {
                this._isEndOfFile = true;
            }
            this._chars[this._charsUsed] = '\0';
            return num4;
        }

        internal override bool ReadInternal()
        {
        Label_0000:
            switch (base._currentState)
            {
                case JsonReader.State.Start:
                case JsonReader.State.Property:
                case JsonReader.State.ArrayStart:
                case JsonReader.State.Array:
                case JsonReader.State.ConstructorStart:
                case JsonReader.State.Constructor:
                    return this.ParseValue();

                case JsonReader.State.Complete:
                case JsonReader.State.Closed:
                case JsonReader.State.Error:
                    goto Label_0000;

                case JsonReader.State.ObjectStart:
                case JsonReader.State.Object:
                    return this.ParseObject();

                case JsonReader.State.PostValue:
                    if (!this.ParsePostValue())
                    {
                        goto Label_0000;
                    }
                    return true;

                case JsonReader.State.Finished:
                    if (this.EnsureChars(0, false))
                    {
                        this.EatWhitespace(false);
                        if (!this._isEndOfFile)
                        {
                            if (this._chars[this._charPos] != '/')
                            {
                                throw JsonReaderException.Create(this, "Additional text encountered after finished reading JSON content: {0}.".FormatWith(CultureInfo.InvariantCulture, this._chars[this._charPos]));
                            }
                            this.ParseComment();
                            return true;
                        }
                    }
                    return false;
            }
            throw JsonReaderException.Create(this, "Unexpected state: {0}.".FormatWith(CultureInfo.InvariantCulture, base.CurrentState));
        }

        private void ReadNumberIntoBuffer()
        {
            int num = this._charPos;
        Label_0007:
            switch (this._chars[num++])
            {
                case 'a':
                case 'b':
                case 'c':
                case 'd':
                case 'e':
                case 'f':
                case 'x':
                case 'X':
                case '+':
                case '-':
                case '.':
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case 'A':
                case 'B':
                case 'C':
                case 'D':
                case 'E':
                case 'F':
                    goto Label_0007;

                case '\0':
                    if (this._charsUsed != (num - 1))
                    {
                        goto Label_0007;
                    }
                    num--;
                    this._charPos = num;
                    if (this.ReadData(true) != 0)
                    {
                        goto Label_0007;
                    }
                    return;
            }
            this._charPos = num - 1;
        }

        private static TimeSpan ReadOffset(string offsetText)
        {
            bool flag = offsetText[0] == '-';
            int num = int.Parse(offsetText.Substring(1, 2), NumberStyles.Integer, CultureInfo.InvariantCulture);
            int num2 = 0;
            if (offsetText.Length >= 5)
            {
                num2 = int.Parse(offsetText.Substring(3, 2), NumberStyles.Integer, CultureInfo.InvariantCulture);
            }
            TimeSpan span = TimeSpan.FromHours((double) num) + TimeSpan.FromMinutes((double) num2);
            if (flag)
            {
                span = span.Negate();
            }
            return span;
        }

        private void ReadStringIntoBuffer(char quote)
        {
            int num4;
            char ch2;
            int index = this._charPos;
            int startIndex = this._charPos;
            int num3 = this._charPos;
            StringBuffer buffer = null;
        Label_0017:
            switch (this._chars[index++])
            {
                case '"':
                case '\'':
                    if (this._chars[index - 1] != quote)
                    {
                        goto Label_0017;
                    }
                    index--;
                    if (startIndex == num3)
                    {
                        this._stringReference = new StringReference(this._chars, startIndex, index - startIndex);
                    }
                    else
                    {
                        if (buffer == null)
                        {
                            buffer = this.GetBuffer();
                        }
                        if (index > num3)
                        {
                            buffer.Append(this._chars, num3, index - num3);
                        }
                        this._stringReference = new StringReference(buffer.GetInternalBuffer(), 0, buffer.Position);
                    }
                    index++;
                    this._charPos = index;
                    return;

                case '\\':
                {
                    this._charPos = index;
                    if (!this.EnsureChars(0, true))
                    {
                        this._charPos = index;
                        throw JsonReaderException.Create(this, "Unterminated string. Expected delimiter: {0}.".FormatWith(CultureInfo.InvariantCulture, quote));
                    }
                    num4 = index - 1;
                    char ch = this._chars[index];
                    switch (ch)
                    {
                        case '/':
                        case '"':
                        case '\'':
                            ch2 = ch;
                            index++;
                            goto Label_02BF;

                        case '\\':
                            index++;
                            ch2 = '\\';
                            goto Label_02BF;

                        case 'b':
                            index++;
                            ch2 = '\b';
                            goto Label_02BF;

                        case 'f':
                            index++;
                            ch2 = '\f';
                            goto Label_02BF;

                        case 'r':
                            index++;
                            ch2 = '\r';
                            goto Label_02BF;

                        case 't':
                            index++;
                            ch2 = '\t';
                            goto Label_02BF;

                        case 'u':
                            index++;
                            this._charPos = index;
                            ch2 = this.ParseUnicode();
                            if (!StringUtils.IsLowSurrogate(ch2))
                            {
                                if (StringUtils.IsHighSurrogate(ch2))
                                {
                                    bool flag;
                                    do
                                    {
                                        flag = false;
                                        if ((this.EnsureChars(2, true) && (this._chars[this._charPos] == '\\')) && (this._chars[this._charPos + 1] == 'u'))
                                        {
                                            char writeChar = ch2;
                                            this._charPos += 2;
                                            ch2 = this.ParseUnicode();
                                            if (!StringUtils.IsLowSurrogate(ch2))
                                            {
                                                if (StringUtils.IsHighSurrogate(ch2))
                                                {
                                                    writeChar = 0xfffd;
                                                    flag = true;
                                                }
                                                else
                                                {
                                                    writeChar = 0xfffd;
                                                }
                                            }
                                            if (buffer == null)
                                            {
                                                buffer = this.GetBuffer();
                                            }
                                            this.WriteCharToBuffer(buffer, writeChar, num3, num4);
                                            num3 = this._charPos;
                                        }
                                        else
                                        {
                                            ch2 = 0xfffd;
                                        }
                                    }
                                    while (flag);
                                }
                            }
                            else
                            {
                                ch2 = 0xfffd;
                            }
                            goto Label_0284;

                        case 'n':
                            index++;
                            ch2 = '\n';
                            goto Label_02BF;
                    }
                    index++;
                    this._charPos = index;
                    throw JsonReaderException.Create(this, "Bad JSON escape sequence: {0}.".FormatWith(CultureInfo.InvariantCulture, @"\" + ch));
                }
                case '\0':
                    if (this._charsUsed == (index - 1))
                    {
                        index--;
                        if (this.ReadData(true) == 0)
                        {
                            this._charPos = index;
                            throw JsonReaderException.Create(this, "Unterminated string. Expected delimiter: {0}.".FormatWith(CultureInfo.InvariantCulture, quote));
                        }
                    }
                    goto Label_0017;

                case '\n':
                    this._charPos = index - 1;
                    this.ProcessLineFeed();
                    index = this._charPos;
                    goto Label_0017;

                case '\r':
                    this._charPos = index - 1;
                    this.ProcessCarriageReturn(true);
                    index = this._charPos;
                    goto Label_0017;

                default:
                    goto Label_0017;
            }
        Label_0284:
            index = this._charPos;
        Label_02BF:
            if (buffer == null)
            {
                buffer = this.GetBuffer();
            }
            this.WriteCharToBuffer(buffer, ch2, num3, num4);
            num3 = index;
            goto Label_0017;
        }

        internal void SetCharBuffer(char[] chars)
        {
            this._chars = chars;
        }

        private void ShiftBufferIfNeeded()
        {
            int length = this._chars.Length;
            if ((length - this._charPos) <= (length * 0.1))
            {
                int count = this._charsUsed - this._charPos;
                if (count > 0)
                {
                    BlockCopyChars(this._chars, this._charPos, this._chars, 0, count);
                }
                this._lineStartPos -= this._charPos;
                this._charPos = 0;
                this._charsUsed = count;
                this._chars[this._charsUsed] = '\0';
            }
        }

        private bool ValidIdentifierChar(char value)
        {
            if (!char.IsLetterOrDigit(value) && (value != '_'))
            {
                return (value == '$');
            }
            return true;
        }

        private void WriteCharToBuffer(StringBuffer buffer, char writeChar, int lastWritePosition, int writeToPosition)
        {
            if (writeToPosition > lastWritePosition)
            {
                buffer.Append(this._chars, lastWritePosition, writeToPosition - lastWritePosition);
            }
            buffer.Append(writeChar);
        }

        public int LineNumber
        {
            get
            {
                if ((base.CurrentState == JsonReader.State.Start) && (this.LinePosition == 0))
                {
                    return 0;
                }
                return this._lineNumber;
            }
        }

        public int LinePosition
        {
            get
            {
                return (this._charPos - this._lineStartPos);
            }
        }
    }
}

