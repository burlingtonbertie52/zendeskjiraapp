﻿namespace Newtonsoft.Json
{
    using Newtonsoft.Json.Converters;
    using Newtonsoft.Json.Utilities;
    using System;
    using System.Globalization;
    using System.IO;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml;
    using System.Xml.Linq;

    public static class JsonConvert
    {
        public static readonly string False = "false";
        internal static readonly long InitialJavaScriptDateTicks = 0x89f7ff5f7b58000L;
        public static readonly string NaN = "NaN";
        public static readonly string NegativeInfinity = "-Infinity";
        public static readonly string Null = "null";
        public static readonly string PositiveInfinity = "Infinity";
        public static readonly string True = "true";
        public static readonly string Undefined = "undefined";

        internal static long ConvertDateTimeToJavaScriptTicks(DateTime dateTime)
        {
            return ConvertDateTimeToJavaScriptTicks(dateTime, true);
        }

        internal static long ConvertDateTimeToJavaScriptTicks(DateTime dateTime, bool convertToUtc)
        {
            long universialTicks = convertToUtc ? ToUniversalTicks(dateTime) : dateTime.Ticks;
            return UniversialTicksToJavaScriptTicks(universialTicks);
        }

        internal static long ConvertDateTimeToJavaScriptTicks(DateTime dateTime, TimeSpan offset)
        {
            return UniversialTicksToJavaScriptTicks(ToUniversalTicks(dateTime, offset));
        }

        internal static DateTime ConvertJavaScriptTicksToDateTime(long javaScriptTicks)
        {
            return new DateTime((javaScriptTicks * 0x2710L) + InitialJavaScriptDateTicks, DateTimeKind.Utc);
        }

        public static T DeserializeAnonymousType<T>(string value, T anonymousTypeObject)
        {
            return DeserializeObject<T>(value);
        }

        public static object DeserializeObject(string value)
        {
            return DeserializeObject(value, null, (JsonSerializerSettings) null);
        }

        public static T DeserializeObject<T>(string value)
        {
            return DeserializeObject<T>(value, (JsonSerializerSettings) null);
        }

        public static object DeserializeObject(string value, JsonSerializerSettings settings)
        {
            return DeserializeObject(value, null, settings);
        }

        public static T DeserializeObject<T>(string value, params JsonConverter[] converters)
        {
            return (T) DeserializeObject(value, typeof(T), converters);
        }

        public static T DeserializeObject<T>(string value, JsonSerializerSettings settings)
        {
            return (T) DeserializeObject(value, typeof(T), settings);
        }

        public static object DeserializeObject(string value, Type type)
        {
            return DeserializeObject(value, type, (JsonSerializerSettings) null);
        }

        public static object DeserializeObject(string value, Type type, params JsonConverter[] converters)
        {
            JsonSerializerSettings settings = ((converters != null) && (converters.Length > 0)) ? new JsonSerializerSettings() : null;
            return DeserializeObject(value, type, settings);
        }

        public static object DeserializeObject(string value, Type type, JsonSerializerSettings settings)
        {
            ValidationUtils.ArgumentNotNull(value, "value");
            StringReader reader = new StringReader(value);
            JsonSerializer serializer = JsonSerializer.Create(settings);
            if (!serializer.IsCheckAdditionalContentSet())
            {
                serializer.CheckAdditionalContent = true;
            }
            return serializer.Deserialize(new JsonTextReader(reader), type);
        }

        public static Task<object> DeserializeObjectAsync(string value)
        {
            return DeserializeObjectAsync(value, null, null);
        }

        public static Task<T> DeserializeObjectAsync<T>(string value)
        {
            return DeserializeObjectAsync<T>(value, null);
        }

        public static Task<T> DeserializeObjectAsync<T>(string value, JsonSerializerSettings settings)
        {
            return Task.Factory.StartNew<T>(() => DeserializeObject<T>(value, settings));
        }

        public static Task<object> DeserializeObjectAsync(string value, Type type, JsonSerializerSettings settings)
        {
            return Task.Factory.StartNew<object>(() => DeserializeObject(value, type, settings));
        }

        public static XmlDocument DeserializeXmlNode(string value)
        {
            return DeserializeXmlNode(value, null);
        }

        public static XmlDocument DeserializeXmlNode(string value, string deserializeRootElementName)
        {
            return DeserializeXmlNode(value, deserializeRootElementName, false);
        }

        public static XmlDocument DeserializeXmlNode(string value, string deserializeRootElementName, bool writeArrayAttribute)
        {
            XmlNodeConverter converter = new XmlNodeConverter {
                DeserializeRootElementName = deserializeRootElementName,
                WriteArrayAttribute = writeArrayAttribute
            };
            return (XmlDocument) DeserializeObject(value, typeof(XmlDocument), new JsonConverter[] { converter });
        }

        public static XDocument DeserializeXNode(string value)
        {
            return DeserializeXNode(value, null);
        }

        public static XDocument DeserializeXNode(string value, string deserializeRootElementName)
        {
            return DeserializeXNode(value, deserializeRootElementName, false);
        }

        public static XDocument DeserializeXNode(string value, string deserializeRootElementName, bool writeArrayAttribute)
        {
            XmlNodeConverter converter = new XmlNodeConverter {
                DeserializeRootElementName = deserializeRootElementName,
                WriteArrayAttribute = writeArrayAttribute
            };
            return (XDocument) DeserializeObject(value, typeof(XDocument), new JsonConverter[] { converter });
        }

        internal static DateTime EnsureDateTime(DateTime value, DateTimeZoneHandling timeZone)
        {
            switch (timeZone)
            {
                case DateTimeZoneHandling.Local:
                    value = SwitchToLocalTime(value);
                    return value;

                case DateTimeZoneHandling.Utc:
                    value = SwitchToUtcTime(value);
                    return value;

                case DateTimeZoneHandling.Unspecified:
                    value = new DateTime(value.Ticks, DateTimeKind.Unspecified);
                    return value;

                case DateTimeZoneHandling.RoundtripKind:
                    return value;
            }
            throw new ArgumentException("Invalid date time handling value.");
        }

        private static string EnsureDecimalPlace(string text)
        {
            if (text.IndexOf('.') != -1)
            {
                return text;
            }
            return (text + ".0");
        }

        private static string EnsureDecimalPlace(double value, string text)
        {
            if ((!double.IsNaN(value) && !double.IsInfinity(value)) && (((text.IndexOf('.') == -1) && (text.IndexOf('E') == -1)) && (text.IndexOf('e') == -1)))
            {
                return (text + ".0");
            }
            return text;
        }

        internal static bool IsJsonPrimitiveType(Type type)
        {
            if (ReflectionUtils.IsNullableType(type))
            {
                type = Nullable.GetUnderlyingType(type);
            }
            return ((type == typeof(DateTimeOffset)) || ((type == typeof(byte[])) || ((type == typeof(Uri)) || ((type == typeof(TimeSpan)) || ((type == typeof(Guid)) || IsJsonPrimitiveTypeCode(ConvertUtils.GetTypeCode(type)))))));
        }

        private static bool IsJsonPrimitiveTypeCode(TypeCode typeCode)
        {
            switch (typeCode)
            {
                case TypeCode.DBNull:
                case TypeCode.Boolean:
                case TypeCode.Char:
                case TypeCode.SByte:
                case TypeCode.Byte:
                case TypeCode.Int16:
                case TypeCode.UInt16:
                case TypeCode.Int32:
                case TypeCode.UInt32:
                case TypeCode.Int64:
                case TypeCode.UInt64:
                case TypeCode.Single:
                case TypeCode.Double:
                case TypeCode.Decimal:
                case TypeCode.DateTime:
                case TypeCode.String:
                    return true;
            }
            return false;
        }

        public static void PopulateObject(string value, object target)
        {
            PopulateObject(value, target, null);
        }

        public static void PopulateObject(string value, object target, JsonSerializerSettings settings)
        {
            StringReader reader = new StringReader(value);
            JsonSerializer serializer = JsonSerializer.Create(settings);
            using (JsonReader reader2 = new JsonTextReader(reader))
            {
                serializer.Populate(reader2, target);
                if (reader2.Read() && (reader2.TokenType != JsonToken.Comment))
                {
                    throw new JsonSerializationException("Additional text found in JSON string after finishing deserializing object.");
                }
            }
        }

        public static Task PopulateObjectAsync(string value, object target, JsonSerializerSettings settings)
        {
            return Task.Factory.StartNew(delegate {
                PopulateObject(value, target, settings);
            });
        }

        public static string SerializeObject(object value)
        {
            return SerializeObject(value, Newtonsoft.Json.Formatting.None, (JsonSerializerSettings) null);
        }

        public static string SerializeObject(object value, Newtonsoft.Json.Formatting formatting)
        {
            return SerializeObject(value, formatting, (JsonSerializerSettings) null);
        }

        public static string SerializeObject(object value, params JsonConverter[] converters)
        {
            return SerializeObject(value, Newtonsoft.Json.Formatting.None, converters);
        }

        public static string SerializeObject(object value, JsonSerializerSettings settings)
        {
            return SerializeObject(value, Newtonsoft.Json.Formatting.None, settings);
        }

        public static string SerializeObject(object value, Newtonsoft.Json.Formatting formatting, params JsonConverter[] converters)
        {
            JsonSerializerSettings settings = ((converters != null) && (converters.Length > 0)) ? new JsonSerializerSettings() : null;
            return SerializeObject(value, formatting, settings);
        }

        public static string SerializeObject(object value, Newtonsoft.Json.Formatting formatting, JsonSerializerSettings settings)
        {
            JsonSerializer serializer = JsonSerializer.Create(settings);
            StringBuilder sb = new StringBuilder(0x100);
            StringWriter textWriter = new StringWriter(sb, CultureInfo.InvariantCulture);
            using (JsonTextWriter writer2 = new JsonTextWriter(textWriter))
            {
                writer2.Formatting = formatting;
                serializer.Serialize(writer2, value);
            }
            return textWriter.ToString();
        }

        public static Task<string> SerializeObjectAsync(object value)
        {
            return SerializeObjectAsync(value, Newtonsoft.Json.Formatting.None, null);
        }

        public static Task<string> SerializeObjectAsync(object value, Newtonsoft.Json.Formatting formatting)
        {
            return SerializeObjectAsync(value, formatting, null);
        }

        public static Task<string> SerializeObjectAsync(object value, Newtonsoft.Json.Formatting formatting, JsonSerializerSettings settings)
        {
            return Task.Factory.StartNew<string>(() => SerializeObject(value, formatting, settings));
        }

        public static string SerializeXmlNode(System.Xml.XmlNode node)
        {
            return SerializeXmlNode(node, Newtonsoft.Json.Formatting.None);
        }

        public static string SerializeXmlNode(System.Xml.XmlNode node, Newtonsoft.Json.Formatting formatting)
        {
            XmlNodeConverter converter = new XmlNodeConverter();
            return SerializeObject(node, formatting, new JsonConverter[] { converter });
        }

        public static string SerializeXmlNode(System.Xml.XmlNode node, Newtonsoft.Json.Formatting formatting, bool omitRootObject)
        {
            XmlNodeConverter converter = new XmlNodeConverter {
                OmitRootObject = omitRootObject
            };
            return SerializeObject(node, formatting, new JsonConverter[] { converter });
        }

        public static string SerializeXNode(XObject node)
        {
            return SerializeXNode(node, Newtonsoft.Json.Formatting.None);
        }

        public static string SerializeXNode(XObject node, Newtonsoft.Json.Formatting formatting)
        {
            return SerializeXNode(node, formatting, false);
        }

        public static string SerializeXNode(XObject node, Newtonsoft.Json.Formatting formatting, bool omitRootObject)
        {
            XmlNodeConverter converter = new XmlNodeConverter {
                OmitRootObject = omitRootObject
            };
            return SerializeObject(node, formatting, new JsonConverter[] { converter });
        }

        private static DateTime SwitchToLocalTime(DateTime value)
        {
            switch (value.Kind)
            {
                case DateTimeKind.Unspecified:
                    return new DateTime(value.Ticks, DateTimeKind.Local);

                case DateTimeKind.Utc:
                    return value.ToLocalTime();

                case DateTimeKind.Local:
                    return value;
            }
            return value;
        }

        private static DateTime SwitchToUtcTime(DateTime value)
        {
            switch (value.Kind)
            {
                case DateTimeKind.Unspecified:
                    return new DateTime(value.Ticks, DateTimeKind.Utc);

                case DateTimeKind.Utc:
                    return value;

                case DateTimeKind.Local:
                    return value.ToUniversalTime();
            }
            return value;
        }

        public static string ToString(bool value)
        {
            if (!value)
            {
                return False;
            }
            return True;
        }

        public static string ToString(byte value)
        {
            return value.ToString(null, CultureInfo.InvariantCulture);
        }

        public static string ToString(char value)
        {
            return ToString(char.ToString(value));
        }

        public static string ToString(DateTime value)
        {
            return ToString(value, DateFormatHandling.IsoDateFormat, DateTimeZoneHandling.RoundtripKind);
        }

        public static string ToString(DateTimeOffset value)
        {
            return ToString(value, DateFormatHandling.IsoDateFormat);
        }

        public static string ToString(decimal value)
        {
            return EnsureDecimalPlace(value.ToString(null, CultureInfo.InvariantCulture));
        }

        public static string ToString(double value)
        {
            return EnsureDecimalPlace(value, value.ToString("R", CultureInfo.InvariantCulture));
        }

        public static string ToString(Enum value)
        {
            return value.ToString("D");
        }

        public static string ToString(Guid value)
        {
            return ToString(value, '"');
        }

        public static string ToString(short value)
        {
            return value.ToString(null, CultureInfo.InvariantCulture);
        }

        public static string ToString(int value)
        {
            return value.ToString(null, CultureInfo.InvariantCulture);
        }

        public static string ToString(long value)
        {
            return value.ToString(null, CultureInfo.InvariantCulture);
        }

        public static string ToString(object value)
        {
            if (value == null)
            {
                return Null;
            }
            IConvertible convertible = ConvertUtils.ToConvertible(value);
            if (convertible != null)
            {
                switch (convertible.GetTypeCode())
                {
                    case TypeCode.DBNull:
                        return Null;

                    case TypeCode.Boolean:
                        return ToString(convertible.ToBoolean(CultureInfo.InvariantCulture));

                    case TypeCode.Char:
                        return ToString(convertible.ToChar(CultureInfo.InvariantCulture));

                    case TypeCode.SByte:
                        return ToString(convertible.ToSByte(CultureInfo.InvariantCulture));

                    case TypeCode.Byte:
                        return ToString(convertible.ToByte(CultureInfo.InvariantCulture));

                    case TypeCode.Int16:
                        return ToString(convertible.ToInt16(CultureInfo.InvariantCulture));

                    case TypeCode.UInt16:
                        return ToString(convertible.ToUInt16(CultureInfo.InvariantCulture));

                    case TypeCode.Int32:
                        return ToString(convertible.ToInt32(CultureInfo.InvariantCulture));

                    case TypeCode.UInt32:
                        return ToString(convertible.ToUInt32(CultureInfo.InvariantCulture));

                    case TypeCode.Int64:
                        return ToString(convertible.ToInt64(CultureInfo.InvariantCulture));

                    case TypeCode.UInt64:
                        return ToString(convertible.ToUInt64(CultureInfo.InvariantCulture));

                    case TypeCode.Single:
                        return ToString(convertible.ToSingle(CultureInfo.InvariantCulture));

                    case TypeCode.Double:
                        return ToString(convertible.ToDouble(CultureInfo.InvariantCulture));

                    case TypeCode.Decimal:
                        return ToString(convertible.ToDecimal(CultureInfo.InvariantCulture));

                    case TypeCode.DateTime:
                        return ToString(convertible.ToDateTime(CultureInfo.InvariantCulture));

                    case TypeCode.String:
                        return ToString(convertible.ToString(CultureInfo.InvariantCulture));
                }
            }
            else
            {
                if (value is DateTimeOffset)
                {
                    return ToString((DateTimeOffset) value);
                }
                if (value is Guid)
                {
                    return ToString((Guid) value);
                }
                if (value is Uri)
                {
                    return ToString((Uri) value);
                }
                if (value is TimeSpan)
                {
                    return ToString((TimeSpan) value);
                }
            }
            throw new ArgumentException("Unsupported type: {0}. Use the JsonSerializer class to get the object's JSON representation.".FormatWith(CultureInfo.InvariantCulture, value.GetType()));
        }

        [CLSCompliant(false)]
        public static string ToString(sbyte value)
        {
            return value.ToString(null, CultureInfo.InvariantCulture);
        }

        public static string ToString(float value)
        {
            return EnsureDecimalPlace((double) value, value.ToString("R", CultureInfo.InvariantCulture));
        }

        public static string ToString(string value)
        {
            return ToString(value, '"');
        }

        public static string ToString(TimeSpan value)
        {
            return ToString(value, '"');
        }

        [CLSCompliant(false)]
        public static string ToString(ushort value)
        {
            return value.ToString(null, CultureInfo.InvariantCulture);
        }

        [CLSCompliant(false)]
        public static string ToString(uint value)
        {
            return value.ToString(null, CultureInfo.InvariantCulture);
        }

        [CLSCompliant(false)]
        public static string ToString(ulong value)
        {
            return value.ToString(null, CultureInfo.InvariantCulture);
        }

        public static string ToString(Uri value)
        {
            if (value == null)
            {
                return Null;
            }
            return ToString(value, '"');
        }

        public static string ToString(DateTimeOffset value, DateFormatHandling format)
        {
            return ToString(value, format, '"');
        }

        internal static string ToString(Guid value, char quoteChar)
        {
            string str = null;
            str = value.ToString("D", CultureInfo.InvariantCulture);
            return (quoteChar + str + quoteChar);
        }

        public static string ToString(string value, char delimiter)
        {
            if ((delimiter != '"') && (delimiter != '\''))
            {
                throw new ArgumentException("Delimiter must be a single or double quote.", "delimiter");
            }
            return JavaScriptUtils.ToEscapedJavaScriptString(value, delimiter, true);
        }

        internal static string ToString(TimeSpan value, char quoteChar)
        {
            return ToString(value.ToString(), quoteChar);
        }

        internal static string ToString(Uri value, char quoteChar)
        {
            return ToString(value.ToString(), quoteChar);
        }

        public static string ToString(DateTime value, DateFormatHandling format, DateTimeZoneHandling timeZoneHandling)
        {
            DateTime d = EnsureDateTime(value, timeZoneHandling);
            using (StringWriter writer = StringUtils.CreateStringWriter(0x40))
            {
                TimeSpan utcOffset = d.GetUtcOffset();
                WriteDateTimeString(writer, d, utcOffset, d.Kind, format, '"');
                return writer.ToString();
            }
        }

        internal static string ToString(DateTimeOffset value, DateFormatHandling format, char quoteChar)
        {
            using (StringWriter writer = StringUtils.CreateStringWriter(0x40))
            {
                WriteDateTimeString(writer, (format == DateFormatHandling.IsoDateFormat) ? value.DateTime : value.UtcDateTime, value.Offset, DateTimeKind.Local, format, quoteChar);
                return writer.ToString();
            }
        }

        private static long ToUniversalTicks(DateTime dateTime)
        {
            if (dateTime.Kind == DateTimeKind.Utc)
            {
                return dateTime.Ticks;
            }
            return ToUniversalTicks(dateTime, dateTime.GetUtcOffset());
        }

        private static long ToUniversalTicks(DateTime dateTime, TimeSpan offset)
        {
            if (((dateTime.Kind == DateTimeKind.Utc) || (dateTime == DateTime.MaxValue)) || (dateTime == DateTime.MinValue))
            {
                return dateTime.Ticks;
            }
            long num = dateTime.Ticks - offset.Ticks;
            if (num > 0x2bca2875f4373fffL)
            {
                return 0x2bca2875f4373fffL;
            }
            if (num < 0L)
            {
                return 0L;
            }
            return num;
        }

        private static long UniversialTicksToJavaScriptTicks(long universialTicks)
        {
            return ((universialTicks - InitialJavaScriptDateTicks) / 0x2710L);
        }

        internal static void WriteDateTimeOffset(TextWriter writer, TimeSpan offset, DateFormatHandling format)
        {
            writer.Write((offset.Ticks >= 0L) ? "+" : "-");
            int num = Math.Abs(offset.Hours);
            if (num < 10)
            {
                writer.Write(0);
            }
            writer.Write(num);
            if (format == DateFormatHandling.IsoDateFormat)
            {
                writer.Write(':');
            }
            int num2 = Math.Abs(offset.Minutes);
            if (num2 < 10)
            {
                writer.Write(0);
            }
            writer.Write(num2);
        }

        internal static void WriteDateTimeString(TextWriter writer, DateTime value, DateFormatHandling format, char quoteChar)
        {
            WriteDateTimeString(writer, value, value.GetUtcOffset(), value.Kind, format, quoteChar);
        }

        internal static void WriteDateTimeString(TextWriter writer, DateTime value, TimeSpan offset, DateTimeKind kind, DateFormatHandling format, char quoteChar)
        {
            if (format != DateFormatHandling.MicrosoftDateFormat)
            {
                writer.Write(quoteChar);
                writer.Write(value.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss.FFFFFFF", CultureInfo.InvariantCulture));
                switch (kind)
                {
                    case DateTimeKind.Utc:
                        writer.Write("Z");
                        break;

                    case DateTimeKind.Local:
                        WriteDateTimeOffset(writer, offset, format);
                        break;
                }
            }
            else
            {
                long num = ConvertDateTimeToJavaScriptTicks(value, offset);
                writer.Write(quoteChar);
                writer.Write(@"\/Date(");
                writer.Write(num);
                switch (kind)
                {
                    case DateTimeKind.Unspecified:
                        if ((value != DateTime.MaxValue) && (value != DateTime.MinValue))
                        {
                            WriteDateTimeOffset(writer, offset, format);
                        }
                        break;

                    case DateTimeKind.Local:
                        WriteDateTimeOffset(writer, offset, format);
                        break;
                }
                writer.Write(@")\/");
                writer.Write(quoteChar);
                return;
            }
            writer.Write(quoteChar);
        }
    }
}

