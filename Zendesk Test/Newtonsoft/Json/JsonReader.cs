﻿namespace Newtonsoft.Json
{
    using Newtonsoft.Json.Utilities;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Runtime.CompilerServices;

    public abstract class JsonReader : IDisposable
    {
        private CultureInfo _culture;
        private JsonPosition _currentPosition;
        internal State _currentState = State.Start;
        internal Newtonsoft.Json.DateParseHandling _dateParseHandling = Newtonsoft.Json.DateParseHandling.DateTime;
        private Newtonsoft.Json.DateTimeZoneHandling _dateTimeZoneHandling = Newtonsoft.Json.DateTimeZoneHandling.RoundtripKind;
        private bool _hasExceededMaxDepth;
        private int? _maxDepth;
        private char _quoteChar;
        internal ReadType _readType;
        private readonly List<JsonPosition> _stack = new List<JsonPosition>(4);
        private JsonToken _tokenType;
        private object _value;

        protected JsonReader()
        {
            this.CloseInput = true;
        }

        public virtual void Close()
        {
            this._currentState = State.Closed;
            this._tokenType = JsonToken.None;
            this._value = null;
        }

        protected virtual void Dispose(bool disposing)
        {
            if ((this._currentState != State.Closed) && disposing)
            {
                this.Close();
            }
        }

        internal JsonPosition GetPosition(int depth)
        {
            if (depth < this._stack.Count)
            {
                return this._stack[depth];
            }
            return this._currentPosition;
        }

        private JsonContainerType GetTypeForCloseToken(JsonToken token)
        {
            switch (token)
            {
                case JsonToken.EndObject:
                    return JsonContainerType.Object;

                case JsonToken.EndArray:
                    return JsonContainerType.Array;

                case JsonToken.EndConstructor:
                    return JsonContainerType.Constructor;
            }
            throw JsonReaderException.Create(this, "Not a valid close JsonToken: {0}".FormatWith(CultureInfo.InvariantCulture, token));
        }

        internal static bool IsPrimitiveToken(JsonToken token)
        {
            switch (token)
            {
                case JsonToken.Integer:
                case JsonToken.Float:
                case JsonToken.String:
                case JsonToken.Boolean:
                case JsonToken.Null:
                case JsonToken.Undefined:
                case JsonToken.Date:
                case JsonToken.Bytes:
                    return true;
            }
            return false;
        }

        internal static bool IsStartToken(JsonToken token)
        {
            switch (token)
            {
                case JsonToken.StartObject:
                case JsonToken.StartArray:
                case JsonToken.StartConstructor:
                    return true;
            }
            return false;
        }

        private bool IsWrappedInTypeObject()
        {
            this._readType = ReadType.Read;
            if (this.TokenType != JsonToken.StartObject)
            {
                return false;
            }
            if (!this.ReadInternal())
            {
                throw JsonReaderException.Create(this, "Unexpected end when reading bytes.");
            }
            if (this.Value.ToString() == "$type")
            {
                this.ReadInternal();
                if ((this.Value != null) && this.Value.ToString().StartsWith("System.Byte[]"))
                {
                    this.ReadInternal();
                    if (this.Value.ToString() == "$value")
                    {
                        return true;
                    }
                }
            }
            throw JsonReaderException.Create(this, "Error reading bytes. Unexpected token: {0}.".FormatWith(CultureInfo.InvariantCulture, JsonToken.StartObject));
        }

        private JsonContainerType Peek()
        {
            return this._currentPosition.Type;
        }

        private JsonContainerType Pop()
        {
            JsonPosition position;
            if (this._stack.Count > 0)
            {
                position = this._currentPosition;
                this._currentPosition = this._stack[this._stack.Count - 1];
                this._stack.RemoveAt(this._stack.Count - 1);
            }
            else
            {
                position = this._currentPosition;
                this._currentPosition = new JsonPosition();
            }
            if (this._maxDepth.HasValue)
            {
                if (this.Depth <= this._maxDepth)
                {
                    this._hasExceededMaxDepth = false;
                }
            }
            return position.Type;
        }

        private void Push(JsonContainerType value)
        {
            this.UpdateScopeWithFinishedValue();
            if (this._currentPosition.Type == JsonContainerType.None)
            {
                this._currentPosition = new JsonPosition(value);
            }
            else
            {
                this._stack.Add(this._currentPosition);
                this._currentPosition = new JsonPosition(value);
                if (this._maxDepth.HasValue)
                {
                    int num = this.Depth + 1;
                    if ((num > this._maxDepth) && !this._hasExceededMaxDepth)
                    {
                        this._hasExceededMaxDepth = true;
                        throw JsonReaderException.Create(this, "The reader's MaxDepth of {0} has been exceeded.".FormatWith(CultureInfo.InvariantCulture, this._maxDepth));
                    }
                }
            }
        }

        public abstract bool Read();
        public abstract byte[] ReadAsBytes();
        internal byte[] ReadAsBytesInternal()
        {
            this._readType = ReadType.ReadAsBytes;
            do
            {
                if (!this.ReadInternal())
                {
                    this.SetToken(JsonToken.None);
                    return null;
                }
            }
            while (this.TokenType == JsonToken.Comment);
            if (this.IsWrappedInTypeObject())
            {
                byte[] buffer = this.ReadAsBytes();
                this.ReadInternal();
                this.SetToken(JsonToken.Bytes, buffer);
                return buffer;
            }
            if (this.TokenType == JsonToken.String)
            {
                string s = (string) this.Value;
                byte[] buffer2 = (s.Length == 0) ? new byte[0] : Convert.FromBase64String(s);
                this.SetToken(JsonToken.Bytes, buffer2);
            }
            if (this.TokenType == JsonToken.Null)
            {
                return null;
            }
            if (this.TokenType == JsonToken.Bytes)
            {
                return (byte[]) this.Value;
            }
            if (this.TokenType != JsonToken.StartArray)
            {
                if (this.TokenType != JsonToken.EndArray)
                {
                    throw JsonReaderException.Create(this, "Error reading bytes. Unexpected token: {0}.".FormatWith(CultureInfo.InvariantCulture, this.TokenType));
                }
                return null;
            }
            List<byte> list = new List<byte>();
            while (this.ReadInternal())
            {
                switch (this.TokenType)
                {
                    case JsonToken.Comment:
                    {
                        continue;
                    }
                    case JsonToken.Integer:
                    {
                        list.Add(Convert.ToByte(this.Value, CultureInfo.InvariantCulture));
                        continue;
                    }
                    case JsonToken.EndArray:
                    {
                        byte[] buffer3 = list.ToArray();
                        this.SetToken(JsonToken.Bytes, buffer3);
                        return buffer3;
                    }
                }
                throw JsonReaderException.Create(this, "Unexpected token when reading bytes: {0}.".FormatWith(CultureInfo.InvariantCulture, this.TokenType));
            }
            throw JsonReaderException.Create(this, "Unexpected end when reading bytes.");
        }

        public abstract DateTime? ReadAsDateTime();
        internal DateTime? ReadAsDateTimeInternal()
        {
            this._readType = ReadType.ReadAsDateTime;
            do
            {
                if (!this.ReadInternal())
                {
                    this.SetToken(JsonToken.None);
                    return null;
                }
            }
            while (this.TokenType == JsonToken.Comment);
            if (this.TokenType == JsonToken.Date)
            {
                return new DateTime?((DateTime) this.Value);
            }
            if (this.TokenType != JsonToken.Null)
            {
                if (this.TokenType == JsonToken.String)
                {
                    DateTime time;
                    string str = (string) this.Value;
                    if (string.IsNullOrEmpty(str))
                    {
                        this.SetToken(JsonToken.Null);
                        return null;
                    }
                    if (!DateTime.TryParse(str, this.Culture, DateTimeStyles.RoundtripKind, out time))
                    {
                        throw JsonReaderException.Create(this, "Could not convert string to DateTime: {0}.".FormatWith(CultureInfo.InvariantCulture, this.Value));
                    }
                    time = JsonConvert.EnsureDateTime(time, this.DateTimeZoneHandling);
                    this.SetToken(JsonToken.Date, time);
                    return new DateTime?(time);
                }
                if (this.TokenType != JsonToken.EndArray)
                {
                    throw JsonReaderException.Create(this, "Error reading date. Unexpected token: {0}.".FormatWith(CultureInfo.InvariantCulture, this.TokenType));
                }
            }
            return null;
        }

        public abstract DateTimeOffset? ReadAsDateTimeOffset();
        internal DateTimeOffset? ReadAsDateTimeOffsetInternal()
        {
            this._readType = ReadType.ReadAsDateTimeOffset;
            do
            {
                if (!this.ReadInternal())
                {
                    this.SetToken(JsonToken.None);
                    return null;
                }
            }
            while (this.TokenType == JsonToken.Comment);
            if (this.TokenType == JsonToken.Date)
            {
                if (this.Value is DateTime)
                {
                    this.SetToken(JsonToken.Date, new DateTimeOffset((DateTime) this.Value));
                }
                return new DateTimeOffset?((DateTimeOffset) this.Value);
            }
            if (this.TokenType != JsonToken.Null)
            {
                if (this.TokenType == JsonToken.String)
                {
                    DateTimeOffset offset;
                    string str = (string) this.Value;
                    if (string.IsNullOrEmpty(str))
                    {
                        this.SetToken(JsonToken.Null);
                        return null;
                    }
                    if (!DateTimeOffset.TryParse(str, this.Culture, DateTimeStyles.RoundtripKind, out offset))
                    {
                        throw JsonReaderException.Create(this, "Could not convert string to DateTimeOffset: {0}.".FormatWith(CultureInfo.InvariantCulture, this.Value));
                    }
                    this.SetToken(JsonToken.Date, offset);
                    return new DateTimeOffset?(offset);
                }
                if (this.TokenType != JsonToken.EndArray)
                {
                    throw JsonReaderException.Create(this, "Error reading date. Unexpected token: {0}.".FormatWith(CultureInfo.InvariantCulture, this.TokenType));
                }
            }
            return null;
        }

        public abstract decimal? ReadAsDecimal();
        internal decimal? ReadAsDecimalInternal()
        {
            this._readType = ReadType.ReadAsDecimal;
            do
            {
                if (!this.ReadInternal())
                {
                    this.SetToken(JsonToken.None);
                    return null;
                }
            }
            while (this.TokenType == JsonToken.Comment);
            if ((this.TokenType == JsonToken.Integer) || (this.TokenType == JsonToken.Float))
            {
                if (!(this.Value is decimal))
                {
                    this.SetToken(JsonToken.Float, Convert.ToDecimal(this.Value, CultureInfo.InvariantCulture));
                }
                return new decimal?((decimal) this.Value);
            }
            if (this.TokenType != JsonToken.Null)
            {
                if (this.TokenType == JsonToken.String)
                {
                    decimal num;
                    string str = (string) this.Value;
                    if (string.IsNullOrEmpty(str))
                    {
                        this.SetToken(JsonToken.Null);
                        return null;
                    }
                    if (!decimal.TryParse(str, NumberStyles.Number, this.Culture, out num))
                    {
                        throw JsonReaderException.Create(this, "Could not convert string to decimal: {0}.".FormatWith(CultureInfo.InvariantCulture, this.Value));
                    }
                    this.SetToken(JsonToken.Float, num);
                    return new decimal?(num);
                }
                if (this.TokenType != JsonToken.EndArray)
                {
                    throw JsonReaderException.Create(this, "Error reading decimal. Unexpected token: {0}.".FormatWith(CultureInfo.InvariantCulture, this.TokenType));
                }
            }
            return null;
        }

        public abstract int? ReadAsInt32();
        internal int? ReadAsInt32Internal()
        {
            this._readType = ReadType.ReadAsInt32;
            do
            {
                if (!this.ReadInternal())
                {
                    this.SetToken(JsonToken.None);
                    return null;
                }
            }
            while (this.TokenType == JsonToken.Comment);
            if ((this.TokenType == JsonToken.Integer) || (this.TokenType == JsonToken.Float))
            {
                if (!(this.Value is int))
                {
                    this.SetToken(JsonToken.Integer, Convert.ToInt32(this.Value, CultureInfo.InvariantCulture));
                }
                return new int?((int) this.Value);
            }
            if (this.TokenType != JsonToken.Null)
            {
                if (this.TokenType == JsonToken.String)
                {
                    int num;
                    string str = (string) this.Value;
                    if (string.IsNullOrEmpty(str))
                    {
                        this.SetToken(JsonToken.Null);
                        return null;
                    }
                    if (!int.TryParse(str, NumberStyles.Integer, this.Culture, out num))
                    {
                        throw JsonReaderException.Create(this, "Could not convert string to integer: {0}.".FormatWith(CultureInfo.InvariantCulture, this.Value));
                    }
                    this.SetToken(JsonToken.Integer, num);
                    return new int?(num);
                }
                if (this.TokenType != JsonToken.EndArray)
                {
                    throw JsonReaderException.Create(this, "Error reading integer. Unexpected token: {0}.".FormatWith(CultureInfo.InvariantCulture, this.TokenType));
                }
            }
            return null;
        }

        public abstract string ReadAsString();
        internal string ReadAsStringInternal()
        {
            this._readType = ReadType.ReadAsString;
            do
            {
                if (!this.ReadInternal())
                {
                    this.SetToken(JsonToken.None);
                    return null;
                }
            }
            while (this.TokenType == JsonToken.Comment);
            if (this.TokenType == JsonToken.String)
            {
                return (string) this.Value;
            }
            if (this.TokenType != JsonToken.Null)
            {
                if (IsPrimitiveToken(this.TokenType) && (this.Value != null))
                {
                    string str;
                    if (ConvertUtils.IsConvertible(this.Value))
                    {
                        str = ConvertUtils.ToConvertible(this.Value).ToString(this.Culture);
                    }
                    else if (this.Value is IFormattable)
                    {
                        str = ((IFormattable) this.Value).ToString(null, this.Culture);
                    }
                    else
                    {
                        str = this.Value.ToString();
                    }
                    this.SetToken(JsonToken.String, str);
                    return str;
                }
                if (this.TokenType != JsonToken.EndArray)
                {
                    throw JsonReaderException.Create(this, "Error reading string. Unexpected token: {0}.".FormatWith(CultureInfo.InvariantCulture, this.TokenType));
                }
            }
            return null;
        }

        internal virtual bool ReadInternal()
        {
            throw new NotImplementedException();
        }

        protected void SetStateBasedOnCurrent()
        {
            JsonContainerType type = this.Peek();
            switch (type)
            {
                case JsonContainerType.None:
                    this._currentState = State.Finished;
                    return;

                case JsonContainerType.Object:
                    this._currentState = State.Object;
                    return;

                case JsonContainerType.Array:
                    this._currentState = State.Array;
                    return;

                case JsonContainerType.Constructor:
                    this._currentState = State.Constructor;
                    return;
            }
            throw JsonReaderException.Create(this, "While setting the reader state back to current object an unexpected JsonType was encountered: {0}".FormatWith(CultureInfo.InvariantCulture, type));
        }

        protected void SetToken(JsonToken newToken)
        {
            this.SetToken(newToken, null);
        }

        protected void SetToken(JsonToken newToken, object value)
        {
            this._tokenType = newToken;
            this._value = value;
            switch (newToken)
            {
                case JsonToken.StartObject:
                    this._currentState = State.ObjectStart;
                    this.Push(JsonContainerType.Object);
                    return;

                case JsonToken.StartArray:
                    this._currentState = State.ArrayStart;
                    this.Push(JsonContainerType.Array);
                    return;

                case JsonToken.StartConstructor:
                    this._currentState = State.ConstructorStart;
                    this.Push(JsonContainerType.Constructor);
                    return;

                case JsonToken.PropertyName:
                    this._currentState = State.Property;
                    this._currentPosition.PropertyName = (string) value;
                    return;

                case JsonToken.Comment:
                    break;

                case JsonToken.Raw:
                case JsonToken.Integer:
                case JsonToken.Float:
                case JsonToken.String:
                case JsonToken.Boolean:
                case JsonToken.Null:
                case JsonToken.Undefined:
                case JsonToken.Date:
                case JsonToken.Bytes:
                    this._currentState = (this.Peek() != JsonContainerType.None) ? State.PostValue : State.Finished;
                    this.UpdateScopeWithFinishedValue();
                    break;

                case JsonToken.EndObject:
                    this.ValidateEnd(JsonToken.EndObject);
                    return;

                case JsonToken.EndArray:
                    this.ValidateEnd(JsonToken.EndArray);
                    return;

                case JsonToken.EndConstructor:
                    this.ValidateEnd(JsonToken.EndConstructor);
                    return;

                default:
                    return;
            }
        }

        public void Skip()
        {
            if (this.TokenType == JsonToken.PropertyName)
            {
                this.Read();
            }
            if (IsStartToken(this.TokenType))
            {
                int depth = this.Depth;
                while (this.Read() && (depth < this.Depth))
                {
                }
            }
        }

        void IDisposable.Dispose()
        {
            this.Dispose(true);
        }

        private void UpdateScopeWithFinishedValue()
        {
            if (this._currentPosition.HasIndex)
            {
                this._currentPosition.Position++;
            }
        }

        private void ValidateEnd(JsonToken endToken)
        {
            JsonContainerType type = this.Pop();
            if (this.GetTypeForCloseToken(endToken) != type)
            {
                throw JsonReaderException.Create(this, "JsonToken {0} is not valid for closing JsonType {1}.".FormatWith(CultureInfo.InvariantCulture, endToken, type));
            }
            this._currentState = (this.Peek() != JsonContainerType.None) ? State.PostValue : State.Finished;
        }

        public bool CloseInput { get; set; }

        public CultureInfo Culture
        {
            get
            {
                return (this._culture ?? CultureInfo.InvariantCulture);
            }
            set
            {
                this._culture = value;
            }
        }

        protected State CurrentState
        {
            get
            {
                return this._currentState;
            }
        }

        public Newtonsoft.Json.DateParseHandling DateParseHandling
        {
            get
            {
                return this._dateParseHandling;
            }
            set
            {
                this._dateParseHandling = value;
            }
        }

        public Newtonsoft.Json.DateTimeZoneHandling DateTimeZoneHandling
        {
            get
            {
                return this._dateTimeZoneHandling;
            }
            set
            {
                this._dateTimeZoneHandling = value;
            }
        }

        public virtual int Depth
        {
            get
            {
                int count = this._stack.Count;
                if (!IsStartToken(this.TokenType) && (this._currentPosition.Type != JsonContainerType.None))
                {
                    return (count + 1);
                }
                return count;
            }
        }

        public int? MaxDepth
        {
            get
            {
                return this._maxDepth;
            }
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentException("Value must be positive.", "value");
                }
                this._maxDepth = value;
            }
        }

        public virtual string Path
        {
            get
            {
                if (this._currentPosition.Type == JsonContainerType.None)
                {
                    return string.Empty;
                }
                IEnumerable<JsonPosition> positions = (((this._currentState == State.ArrayStart) || (this._currentState == State.ConstructorStart)) || (this._currentState == State.ObjectStart)) ? this._stack : this._stack.Concat<JsonPosition>(new JsonPosition[1]);
                return JsonPosition.BuildPath(positions);
            }
        }

        public virtual char QuoteChar
        {
            get
            {
                return this._quoteChar;
            }
            protected internal set
            {
                this._quoteChar = value;
            }
        }

        public virtual JsonToken TokenType
        {
            get
            {
                return this._tokenType;
            }
        }

        public virtual object Value
        {
            get
            {
                return this._value;
            }
        }

        public virtual Type ValueType
        {
            get
            {
                if (this._value == null)
                {
                    return null;
                }
                return this._value.GetType();
            }
        }

        internal protected enum State
        {
            Start,
            Complete,
            Property,
            ObjectStart,
            Object,
            ArrayStart,
            Array,
            Closed,
            PostValue,
            ConstructorStart,
            Constructor,
            Error,
            Finished
        }
    }
}

