﻿namespace Newtonsoft.Json.Serialization
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Utilities;
    using System;
    using System.Globalization;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.Serialization;
    using System.Security;

    public class JsonObjectContract : JsonContainerContract
    {
        private bool? _hasRequiredOrDefaultValueProperties;

        public JsonObjectContract(Type underlyingType) : base(underlyingType)
        {
            base.ContractType = JsonContractType.Object;
            this.Properties = new JsonPropertyCollection(base.UnderlyingType);
            this.ConstructorParameters = new JsonPropertyCollection(base.UnderlyingType);
        }

        [SecuritySafeCritical]
        internal object GetUninitializedObject()
        {
            if (!JsonTypeReflector.FullyTrusted)
            {
                throw new JsonException("Insufficient permissions. Creating an uninitialized '{0}' type requires full trust.".FormatWith(CultureInfo.InvariantCulture, base.NonNullableUnderlyingType));
            }
            return FormatterServices.GetUninitializedObject(base.NonNullableUnderlyingType);
        }

        public JsonPropertyCollection ConstructorParameters { get; private set; }

        internal bool HasRequiredOrDefaultValueProperties
        {
            get
            {
                if (!this._hasRequiredOrDefaultValueProperties.HasValue)
                {
                    this._hasRequiredOrDefaultValueProperties = false;
                    if (((Required) this.ItemRequired.GetValueOrDefault(Required.Default)) != Required.Default)
                    {
                        this._hasRequiredOrDefaultValueProperties = true;
                    }
                    else
                    {
                        foreach (JsonProperty property in this.Properties)
                        {
                            if ((property.Required != Required.Default) || (((((DefaultValueHandling) property.DefaultValueHandling) & DefaultValueHandling.Populate) == DefaultValueHandling.Populate) && property.Writable))
                            {
                                this._hasRequiredOrDefaultValueProperties = true;
                                break;
                            }
                        }
                    }
                }
                return this._hasRequiredOrDefaultValueProperties.Value;
            }
        }

        public Required? ItemRequired { get; set; }

        public Newtonsoft.Json.MemberSerialization MemberSerialization { get; set; }

        public ConstructorInfo OverrideConstructor { get; set; }

        public ConstructorInfo ParametrizedConstructor { get; set; }

        public JsonPropertyCollection Properties { get; private set; }
    }
}

