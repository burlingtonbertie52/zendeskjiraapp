﻿namespace Newtonsoft.Json.Serialization
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Newtonsoft.Json.Utilities;
    using System;
    using System.Collections;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Dynamic;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Runtime.Serialization;

    public class DefaultContractResolver : IContractResolver
    {
        private static readonly IContractResolver _instance = new DefaultContractResolver(true);
        private Dictionary<ResolverContractKey, JsonContract> _instanceContractCache;
        private readonly bool _sharedCache;
        private static Dictionary<ResolverContractKey, JsonContract> _sharedContractCache;
        private static readonly object _typeContractCacheLock = new object();
        private static readonly IList<JsonConverter> BuiltInConverters = new List<JsonConverter> { new EntityKeyMemberConverter(), new ExpandoObjectConverter(), new XmlNodeConverter(), new BinaryConverter(), new DataSetConverter(), new DataTableConverter(), new KeyValuePairConverter(), new BsonObjectIdConverter() };

        public DefaultContractResolver() : this(false)
        {
        }

        public DefaultContractResolver(bool shareCache)
        {
            this.DefaultMembersSearchFlags = BindingFlags.Public | BindingFlags.Instance;
            this.IgnoreSerializableAttribute = true;
            this._sharedCache = shareCache;
        }

        internal static bool CanConvertToString(Type type)
        {
            TypeConverter converter = ConvertUtils.GetConverter(type);
            if (((((converter == null) || (converter is ComponentConverter)) || ((converter is ReferenceConverter) || (converter.GetType() == typeof(TypeConverter)))) || !converter.CanConvertTo(typeof(string))) && (!(type == typeof(Type)) && !type.IsSubclassOf(typeof(Type))))
            {
                return false;
            }
            return true;
        }

        protected virtual JsonArrayContract CreateArrayContract(Type objectType)
        {
            JsonArrayContract contract = new JsonArrayContract(objectType);
            this.InitializeContract(contract);
            return contract;
        }

        protected virtual IList<JsonProperty> CreateConstructorParameters(ConstructorInfo constructor, JsonPropertyCollection memberProperties)
        {
            ParameterInfo[] parameters = constructor.GetParameters();
            JsonPropertyCollection propertys = new JsonPropertyCollection(constructor.DeclaringType);
            foreach (ParameterInfo info in parameters)
            {
                JsonProperty matchingMemberProperty = (info.Name != null) ? memberProperties.GetClosestMatchProperty(info.Name) : null;
                if ((matchingMemberProperty != null) && (matchingMemberProperty.PropertyType != info.ParameterType))
                {
                    matchingMemberProperty = null;
                }
                JsonProperty property = this.CreatePropertyFromConstructorParameter(matchingMemberProperty, info);
                if (property != null)
                {
                    propertys.AddProperty(property);
                }
            }
            return propertys;
        }

        protected virtual JsonContract CreateContract(Type objectType)
        {
            Type type = ReflectionUtils.EnsureNotNullableType(objectType);
            if (JsonConvert.IsJsonPrimitiveType(type))
            {
                return this.CreatePrimitiveContract(objectType);
            }
            if (JsonTypeReflector.GetJsonObjectAttribute(type) == null)
            {
                if (JsonTypeReflector.GetJsonArrayAttribute(type) != null)
                {
                    return this.CreateArrayContract(objectType);
                }
                if (JsonTypeReflector.GetJsonDictionaryAttribute(type) != null)
                {
                    return this.CreateDictionaryContract(objectType);
                }
                if ((type == typeof(JToken)) || type.IsSubclassOf(typeof(JToken)))
                {
                    return this.CreateLinqContract(objectType);
                }
                if (CollectionUtils.IsDictionaryType(type))
                {
                    return this.CreateDictionaryContract(objectType);
                }
                if (typeof(IEnumerable).IsAssignableFrom(type))
                {
                    return this.CreateArrayContract(objectType);
                }
                if (CanConvertToString(type))
                {
                    return this.CreateStringContract(objectType);
                }
                if (!this.IgnoreSerializableInterface && typeof(ISerializable).IsAssignableFrom(type))
                {
                    return this.CreateISerializableContract(objectType);
                }
                if (typeof(IDynamicMetaObjectProvider).IsAssignableFrom(type))
                {
                    return this.CreateDynamicContract(objectType);
                }
            }
            return this.CreateObjectContract(objectType);
        }

        protected virtual JsonDictionaryContract CreateDictionaryContract(Type objectType)
        {
            JsonDictionaryContract contract = new JsonDictionaryContract(objectType);
            this.InitializeContract(contract);
            contract.PropertyNameResolver = new Func<string, string>(this.ResolvePropertyName);
            return contract;
        }

        protected virtual JsonDynamicContract CreateDynamicContract(Type objectType)
        {
            JsonDynamicContract contract = new JsonDynamicContract(objectType);
            this.InitializeContract(contract);
            contract.PropertyNameResolver = new Func<string, string>(this.ResolvePropertyName);
            contract.Properties.AddRange<JsonProperty>(this.CreateProperties(objectType, MemberSerialization.OptOut));
            return contract;
        }

        protected virtual JsonISerializableContract CreateISerializableContract(Type objectType)
        {
            JsonISerializableContract contract = new JsonISerializableContract(objectType);
            this.InitializeContract(contract);
            ConstructorInfo method = contract.NonNullableUnderlyingType.GetConstructor(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance, null, new Type[] { typeof(SerializationInfo), typeof(StreamingContext) }, null);
            if (method != null)
            {
                MethodCall<object, object> methodCall = JsonTypeReflector.ReflectionDelegateFactory.CreateMethodCall<object>(method);
                contract.ISerializableCreator = args => methodCall(null, args);
            }
            return contract;
        }

        protected virtual JsonLinqContract CreateLinqContract(Type objectType)
        {
            JsonLinqContract contract = new JsonLinqContract(objectType);
            this.InitializeContract(contract);
            return contract;
        }

        protected virtual IValueProvider CreateMemberValueProvider(MemberInfo member)
        {
            if (this.DynamicCodeGeneration)
            {
                return new DynamicValueProvider(member);
            }
            return new ReflectionValueProvider(member);
        }

        protected virtual JsonObjectContract CreateObjectContract(Type objectType)
        {
            JsonObjectContract contract = new JsonObjectContract(objectType);
            this.InitializeContract(contract);
            bool ignoreSerializableAttribute = this.IgnoreSerializableAttribute;
            contract.MemberSerialization = JsonTypeReflector.GetObjectMemberSerialization(contract.NonNullableUnderlyingType, ignoreSerializableAttribute);
            contract.Properties.AddRange<JsonProperty>(this.CreateProperties(contract.NonNullableUnderlyingType, contract.MemberSerialization));
            JsonObjectAttribute jsonObjectAttribute = JsonTypeReflector.GetJsonObjectAttribute(contract.NonNullableUnderlyingType);
            if (jsonObjectAttribute != null)
            {
                contract.ItemRequired = jsonObjectAttribute._itemRequired;
            }
            ConstructorInfo attributeConstructor = this.GetAttributeConstructor(contract.NonNullableUnderlyingType);
            if (attributeConstructor != null)
            {
                contract.OverrideConstructor = attributeConstructor;
                contract.ConstructorParameters.AddRange<JsonProperty>(this.CreateConstructorParameters(attributeConstructor, contract.Properties));
                return contract;
            }
            if (contract.MemberSerialization == MemberSerialization.Fields)
            {
                if (JsonTypeReflector.FullyTrusted)
                {
                    contract.DefaultCreator = new Func<object>(contract.GetUninitializedObject);
                }
                return contract;
            }
            if ((contract.DefaultCreator == null) || contract.DefaultCreatorNonPublic)
            {
                ConstructorInfo parametrizedConstructor = this.GetParametrizedConstructor(contract.NonNullableUnderlyingType);
                if (parametrizedConstructor != null)
                {
                    contract.ParametrizedConstructor = parametrizedConstructor;
                    contract.ConstructorParameters.AddRange<JsonProperty>(this.CreateConstructorParameters(parametrizedConstructor, contract.Properties));
                }
            }
            return contract;
        }

        protected virtual JsonPrimitiveContract CreatePrimitiveContract(Type objectType)
        {
            JsonPrimitiveContract contract = new JsonPrimitiveContract(objectType);
            this.InitializeContract(contract);
            return contract;
        }

        protected virtual IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            List<MemberInfo> serializableMembers = this.GetSerializableMembers(type);
            if (serializableMembers == null)
            {
                throw new JsonSerializationException("Null collection of seralizable members returned.");
            }
            JsonPropertyCollection source = new JsonPropertyCollection(type);
            foreach (MemberInfo info in serializableMembers)
            {
                JsonProperty property = this.CreateProperty(info, memberSerialization);
                if (property != null)
                {
                    source.AddProperty(property);
                }
            }
            return source.OrderBy<JsonProperty, int>(delegate (JsonProperty p) {
                int? order = p.Order;
                if (!order.HasValue)
                {
                    return -1;
                }
                return order.GetValueOrDefault();
            }).ToList<JsonProperty>();
        }

        protected virtual JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            bool flag;
            JsonProperty property = new JsonProperty {
                PropertyType = ReflectionUtils.GetMemberUnderlyingType(member),
                DeclaringType = member.DeclaringType,
                ValueProvider = this.CreateMemberValueProvider(member)
            };
            this.SetPropertySettingsFromAttributes(property, member.GetCustomAttributeProvider(), member.Name, member.DeclaringType, memberSerialization, out flag);
            if (memberSerialization != MemberSerialization.Fields)
            {
                property.Readable = ReflectionUtils.CanReadMemberValue(member, flag);
                property.Writable = ReflectionUtils.CanSetMemberValue(member, flag, property.HasMemberAttribute);
            }
            else
            {
                property.Readable = true;
                property.Writable = true;
            }
            property.ShouldSerialize = this.CreateShouldSerializeTest(member);
            this.SetIsSpecifiedActions(property, member, flag);
            return property;
        }

        protected virtual JsonProperty CreatePropertyFromConstructorParameter(JsonProperty matchingMemberProperty, ParameterInfo parameterInfo)
        {
            bool flag;
            JsonProperty property = new JsonProperty {
                PropertyType = parameterInfo.ParameterType
            };
            this.SetPropertySettingsFromAttributes(property, parameterInfo.GetCustomAttributeProvider(), parameterInfo.Name, parameterInfo.Member.DeclaringType, MemberSerialization.OptOut, out flag);
            property.Readable = false;
            property.Writable = true;
            if (matchingMemberProperty != null)
            {
                property.PropertyName = (property.PropertyName != parameterInfo.Name) ? property.PropertyName : matchingMemberProperty.PropertyName;
                property.Converter = property.Converter ?? matchingMemberProperty.Converter;
                property.MemberConverter = property.MemberConverter ?? matchingMemberProperty.MemberConverter;
                if (!property._hasExplicitDefaultValue && matchingMemberProperty._hasExplicitDefaultValue)
                {
                    property.DefaultValue = matchingMemberProperty.DefaultValue;
                }
                Required? nullable = property._required;
                property._required = nullable.HasValue ? new Required?(nullable.GetValueOrDefault()) : matchingMemberProperty._required;
                bool? isReference = property.IsReference;
                property.IsReference = isReference.HasValue ? new bool?(isReference.GetValueOrDefault()) : matchingMemberProperty.IsReference;
                NullValueHandling? nullValueHandling = property.NullValueHandling;
                property.NullValueHandling = nullValueHandling.HasValue ? new NullValueHandling?(nullValueHandling.GetValueOrDefault()) : matchingMemberProperty.NullValueHandling;
                DefaultValueHandling? defaultValueHandling = property.DefaultValueHandling;
                property.DefaultValueHandling = defaultValueHandling.HasValue ? new DefaultValueHandling?(defaultValueHandling.GetValueOrDefault()) : matchingMemberProperty.DefaultValueHandling;
                ReferenceLoopHandling? referenceLoopHandling = property.ReferenceLoopHandling;
                property.ReferenceLoopHandling = referenceLoopHandling.HasValue ? new ReferenceLoopHandling?(referenceLoopHandling.GetValueOrDefault()) : matchingMemberProperty.ReferenceLoopHandling;
                ObjectCreationHandling? objectCreationHandling = property.ObjectCreationHandling;
                property.ObjectCreationHandling = objectCreationHandling.HasValue ? new ObjectCreationHandling?(objectCreationHandling.GetValueOrDefault()) : matchingMemberProperty.ObjectCreationHandling;
                TypeNameHandling? typeNameHandling = property.TypeNameHandling;
                property.TypeNameHandling = typeNameHandling.HasValue ? new TypeNameHandling?(typeNameHandling.GetValueOrDefault()) : matchingMemberProperty.TypeNameHandling;
            }
            return property;
        }

        private Predicate<object> CreateShouldSerializeTest(MemberInfo member)
        {
            MethodInfo method = member.DeclaringType.GetMethod("ShouldSerialize" + member.Name, ReflectionUtils.EmptyTypes);
            if ((method == null) || (method.ReturnType != typeof(bool)))
            {
                return null;
            }
            MethodCall<object, object> shouldSerializeCall = JsonTypeReflector.ReflectionDelegateFactory.CreateMethodCall<object>(method);
            return o => ((bool) shouldSerializeCall(o, new object[0]));
        }

        protected virtual JsonStringContract CreateStringContract(Type objectType)
        {
            JsonStringContract contract = new JsonStringContract(objectType);
            this.InitializeContract(contract);
            return contract;
        }

        private ConstructorInfo GetAttributeConstructor(Type objectType)
        {
            IList<ConstructorInfo> list = (from c in objectType.GetConstructors(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance)
                where c.IsDefined(typeof(JsonConstructorAttribute), true)
                select c).ToList<ConstructorInfo>();
            if (list.Count > 1)
            {
                throw new JsonException("Multiple constructors with the JsonConstructorAttribute.");
            }
            if (list.Count == 1)
            {
                return list[0];
            }
            if (objectType == typeof(Version))
            {
                return objectType.GetConstructor(new Type[] { typeof(int), typeof(int), typeof(int), typeof(int) });
            }
            return null;
        }

        private Dictionary<ResolverContractKey, JsonContract> GetCache()
        {
            if (this._sharedCache)
            {
                return _sharedContractCache;
            }
            return this._instanceContractCache;
        }

        private void GetCallbackMethodsForType(Type type, out MethodInfo onSerializing, out MethodInfo onSerialized, out MethodInfo onDeserializing, out MethodInfo onDeserialized, out MethodInfo onError)
        {
            onSerializing = null;
            onSerialized = null;
            onDeserializing = null;
            onDeserialized = null;
            onError = null;
            foreach (MethodInfo info in type.GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly))
            {
                if (!info.ContainsGenericParameters)
                {
                    Type prevAttributeType = null;
                    ParameterInfo[] parameters = info.GetParameters();
                    if (IsValidCallback(info, parameters, typeof(OnSerializingAttribute), onSerializing, ref prevAttributeType))
                    {
                        onSerializing = info;
                    }
                    if (IsValidCallback(info, parameters, typeof(OnSerializedAttribute), onSerialized, ref prevAttributeType))
                    {
                        onSerialized = info;
                    }
                    if (IsValidCallback(info, parameters, typeof(OnDeserializingAttribute), onDeserializing, ref prevAttributeType))
                    {
                        onDeserializing = info;
                    }
                    if (IsValidCallback(info, parameters, typeof(OnDeserializedAttribute), onDeserialized, ref prevAttributeType))
                    {
                        onDeserialized = info;
                    }
                    if (IsValidCallback(info, parameters, typeof(OnErrorAttribute), onError, ref prevAttributeType))
                    {
                        onError = info;
                    }
                }
            }
        }

        internal static string GetClrTypeFullName(Type type)
        {
            if (type.IsGenericTypeDefinition() || !type.ContainsGenericParameters())
            {
                return type.FullName;
            }
            return string.Format(CultureInfo.InvariantCulture, "{0}.{1}", new object[] { type.Namespace, type.Name });
        }

        private Func<object> GetDefaultCreator(Type createdType)
        {
            return JsonTypeReflector.ReflectionDelegateFactory.CreateDefaultConstructor<object>(createdType);
        }

        private ConstructorInfo GetParametrizedConstructor(Type objectType)
        {
            IList<ConstructorInfo> list = objectType.GetConstructors(BindingFlags.Public | BindingFlags.Instance).ToList<ConstructorInfo>();
            if (list.Count == 1)
            {
                return list[0];
            }
            return null;
        }

        public string GetResolvedPropertyName(string propertyName)
        {
            return this.ResolvePropertyName(propertyName);
        }

        protected virtual List<MemberInfo> GetSerializableMembers(Type objectType)
        {
            bool ignoreSerializableAttribute = this.IgnoreSerializableAttribute;
            MemberSerialization objectMemberSerialization = JsonTypeReflector.GetObjectMemberSerialization(objectType, ignoreSerializableAttribute);
            List<MemberInfo> list = (from m in ReflectionUtils.GetFieldsAndProperties(objectType, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance)
                where !ReflectionUtils.IsIndexedProperty(m)
                select m).ToList<MemberInfo>();
            List<MemberInfo> source = new List<MemberInfo>();
            if (objectMemberSerialization != MemberSerialization.Fields)
            {
                Type type;
                DataContractAttribute dataContractAttribute = JsonTypeReflector.GetDataContractAttribute(objectType);
                List<MemberInfo> list3 = (from m in ReflectionUtils.GetFieldsAndProperties(objectType, this.DefaultMembersSearchFlags)
                    where !ReflectionUtils.IsIndexedProperty(m)
                    select m).ToList<MemberInfo>();
                foreach (MemberInfo info in list)
                {
                    if (this.SerializeCompilerGeneratedMembers || !info.IsDefined(typeof(CompilerGeneratedAttribute), true))
                    {
                        if (list3.Contains(info))
                        {
                            source.Add(info);
                        }
                        else if (JsonTypeReflector.GetAttribute<JsonPropertyAttribute>(info.GetCustomAttributeProvider()) != null)
                        {
                            source.Add(info);
                        }
                        else if ((dataContractAttribute != null) && (JsonTypeReflector.GetAttribute<DataMemberAttribute>(info.GetCustomAttributeProvider()) != null))
                        {
                            source.Add(info);
                        }
                        else if ((objectMemberSerialization == MemberSerialization.Fields) && (info.MemberType() == MemberTypes.Field))
                        {
                            source.Add(info);
                        }
                    }
                }
                if (objectType.AssignableToTypeName("System.Data.Objects.DataClasses.EntityObject", out type))
                {
                    source = source.Where<MemberInfo>(new Func<MemberInfo, bool>(this.ShouldSerializeEntityMember)).ToList<MemberInfo>();
                }
                return source;
            }
            foreach (MemberInfo info2 in list)
            {
                if (info2.MemberType() == MemberTypes.Field)
                {
                    source.Add(info2);
                }
            }
            return source;
        }

        private void InitializeContract(JsonContract contract)
        {
            JsonContainerAttribute jsonContainerAttribute = JsonTypeReflector.GetJsonContainerAttribute(contract.NonNullableUnderlyingType);
            if (jsonContainerAttribute != null)
            {
                contract.IsReference = jsonContainerAttribute._isReference;
            }
            else
            {
                DataContractAttribute dataContractAttribute = JsonTypeReflector.GetDataContractAttribute(contract.NonNullableUnderlyingType);
                if ((dataContractAttribute != null) && dataContractAttribute.IsReference)
                {
                    contract.IsReference = true;
                }
            }
            contract.Converter = this.ResolveContractConverter(contract.NonNullableUnderlyingType);
            contract.InternalConverter = JsonSerializer.GetMatchingConverter(BuiltInConverters, contract.NonNullableUnderlyingType);
            if (ReflectionUtils.HasDefaultConstructor(contract.CreatedType, true) || contract.CreatedType.IsValueType())
            {
                contract.DefaultCreator = this.GetDefaultCreator(contract.CreatedType);
                contract.DefaultCreatorNonPublic = !contract.CreatedType.IsValueType() && (ReflectionUtils.GetDefaultConstructor(contract.CreatedType) == null);
            }
            this.ResolveCallbackMethods(contract, contract.NonNullableUnderlyingType);
        }

        private static bool IsValidCallback(MethodInfo method, ParameterInfo[] parameters, Type attributeType, MethodInfo currentCallback, ref Type prevAttributeType)
        {
            if (!method.IsDefined(attributeType, false))
            {
                return false;
            }
            if (currentCallback != null)
            {
                throw new JsonException("Invalid attribute. Both '{0}' and '{1}' in type '{2}' have '{3}'.".FormatWith(CultureInfo.InvariantCulture, new object[] { method, currentCallback, GetClrTypeFullName(method.DeclaringType), attributeType }));
            }
            if (prevAttributeType != null)
            {
                throw new JsonException("Invalid Callback. Method '{3}' in type '{2}' has both '{0}' and '{1}'.".FormatWith(CultureInfo.InvariantCulture, new object[] { prevAttributeType, attributeType, GetClrTypeFullName(method.DeclaringType), method }));
            }
            if (method.IsVirtual)
            {
                throw new JsonException("Virtual Method '{0}' of type '{1}' cannot be marked with '{2}' attribute.".FormatWith(CultureInfo.InvariantCulture, method, GetClrTypeFullName(method.DeclaringType), attributeType));
            }
            if (method.ReturnType != typeof(void))
            {
                throw new JsonException("Serialization Callback '{1}' in type '{0}' must return void.".FormatWith(CultureInfo.InvariantCulture, GetClrTypeFullName(method.DeclaringType), method));
            }
            if (attributeType == typeof(OnErrorAttribute))
            {
                if (((parameters == null) || (parameters.Length != 2)) || ((parameters[0].ParameterType != typeof(StreamingContext)) || (parameters[1].ParameterType != typeof(ErrorContext))))
                {
                    throw new JsonException("Serialization Error Callback '{1}' in type '{0}' must have two parameters of type '{2}' and '{3}'.".FormatWith(CultureInfo.InvariantCulture, new object[] { GetClrTypeFullName(method.DeclaringType), method, typeof(StreamingContext), typeof(ErrorContext) }));
                }
            }
            else if (((parameters == null) || (parameters.Length != 1)) || (parameters[0].ParameterType != typeof(StreamingContext)))
            {
                throw new JsonException("Serialization Callback '{1}' in type '{0}' must have a single parameter of type '{2}'.".FormatWith(CultureInfo.InvariantCulture, GetClrTypeFullName(method.DeclaringType), method, typeof(StreamingContext)));
            }
            prevAttributeType = attributeType;
            return true;
        }

        private void ResolveCallbackMethods(JsonContract contract, Type t)
        {
            MethodInfo info;
            MethodInfo info2;
            MethodInfo info3;
            MethodInfo info4;
            MethodInfo info5;
            if (t.BaseType() != null)
            {
                this.ResolveCallbackMethods(contract, t.BaseType());
            }
            this.GetCallbackMethodsForType(t, out info, out info2, out info3, out info4, out info5);
            if (info != null)
            {
                contract.OnSerializing = info;
            }
            if (info2 != null)
            {
                contract.OnSerialized = info2;
            }
            if (info3 != null)
            {
                contract.OnDeserializing = info3;
            }
            if ((info4 != null) && (!t.IsGenericType() || (t.GetGenericTypeDefinition() != typeof(ConcurrentDictionary<,>))))
            {
                contract.OnDeserialized = info4;
            }
            if (info5 != null)
            {
                contract.OnError = info5;
            }
        }

        public virtual JsonContract ResolveContract(Type type)
        {
            JsonContract contract;
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }
            ResolverContractKey key = new ResolverContractKey(base.GetType(), type);
            Dictionary<ResolverContractKey, JsonContract> cache = this.GetCache();
            if ((cache == null) || !cache.TryGetValue(key, out contract))
            {
                contract = this.CreateContract(type);
                lock (_typeContractCacheLock)
                {
                    cache = this.GetCache();
                    Dictionary<ResolverContractKey, JsonContract> dictionary2 = (cache != null) ? new Dictionary<ResolverContractKey, JsonContract>(cache) : new Dictionary<ResolverContractKey, JsonContract>();
                    dictionary2[key] = contract;
                    this.UpdateCache(dictionary2);
                }
            }
            return contract;
        }

        protected virtual JsonConverter ResolveContractConverter(Type objectType)
        {
            return JsonTypeReflector.GetJsonConverter(objectType.GetCustomAttributeProvider(), objectType);
        }

        protected internal virtual string ResolvePropertyName(string propertyName)
        {
            return propertyName;
        }

        private void SetIsSpecifiedActions(JsonProperty property, MemberInfo member, bool allowNonPublicAccess)
        {
            Func<object, object> specifiedPropertyGet;
            MemberInfo field = member.DeclaringType.GetProperty(member.Name + "Specified");
            if (field == null)
            {
                field = member.DeclaringType.GetField(member.Name + "Specified");
            }
            if ((field != null) && (ReflectionUtils.GetMemberUnderlyingType(field) == typeof(bool)))
            {
                specifiedPropertyGet = JsonTypeReflector.ReflectionDelegateFactory.CreateGet<object>(field);
                property.GetIsSpecified = o => (bool) specifiedPropertyGet(o);
                if (ReflectionUtils.CanSetMemberValue(field, allowNonPublicAccess, false))
                {
                    property.SetIsSpecified = JsonTypeReflector.ReflectionDelegateFactory.CreateSet<object>(field);
                }
            }
        }

        private void SetPropertySettingsFromAttributes(JsonProperty property, ICustomAttributeProvider attributeProvider, string name, Type declaringType, MemberSerialization memberSerialization, out bool allowNonPublicAccess)
        {
            DataMemberAttribute dataMemberAttribute;
            string propertyName;
            DataContractAttribute dataContractAttribute = JsonTypeReflector.GetDataContractAttribute(declaringType);
            MemberInfo memberInfo = null;
            memberInfo = attributeProvider as MemberInfo;
            if ((dataContractAttribute != null) && (memberInfo != null))
            {
                dataMemberAttribute = JsonTypeReflector.GetDataMemberAttribute(memberInfo);
            }
            else
            {
                dataMemberAttribute = null;
            }
            JsonPropertyAttribute attribute = JsonTypeReflector.GetAttribute<JsonPropertyAttribute>(attributeProvider);
            if (attribute != null)
            {
                property.HasMemberAttribute = true;
            }
            if ((attribute != null) && (attribute.PropertyName != null))
            {
                propertyName = attribute.PropertyName;
            }
            else if ((dataMemberAttribute != null) && (dataMemberAttribute.Name != null))
            {
                propertyName = dataMemberAttribute.Name;
            }
            else
            {
                propertyName = name;
            }
            property.PropertyName = this.ResolvePropertyName(propertyName);
            property.UnderlyingName = name;
            bool flag = false;
            if (attribute != null)
            {
                property._required = attribute._required;
                property.Order = attribute._order;
                property.DefaultValueHandling = attribute._defaultValueHandling;
                flag = true;
            }
            else if (dataMemberAttribute != null)
            {
                property._required = new Required?(dataMemberAttribute.IsRequired ? Required.AllowNull : Required.Default);
                property.Order = (dataMemberAttribute.Order != -1) ? new int?(dataMemberAttribute.Order) : null;
                property.DefaultValueHandling = !dataMemberAttribute.EmitDefaultValue ? ((DefaultValueHandling?) 1) : null;
                flag = true;
            }
            bool flag2 = (JsonTypeReflector.GetAttribute<JsonIgnoreAttribute>(attributeProvider) != null) || (JsonTypeReflector.GetAttribute<NonSerializedAttribute>(attributeProvider) != null);
            if (memberSerialization != MemberSerialization.OptIn)
            {
                bool flag3 = false;
                flag3 = JsonTypeReflector.GetAttribute<IgnoreDataMemberAttribute>(attributeProvider) != null;
                property.Ignored = flag2 || flag3;
            }
            else
            {
                property.Ignored = flag2 || !flag;
            }
            property.Converter = JsonTypeReflector.GetJsonConverter(attributeProvider, property.PropertyType);
            property.MemberConverter = JsonTypeReflector.GetJsonConverter(attributeProvider, property.PropertyType);
            DefaultValueAttribute attribute4 = JsonTypeReflector.GetAttribute<DefaultValueAttribute>(attributeProvider);
            if (attribute4 != null)
            {
                property.DefaultValue = attribute4.Value;
            }
            property.NullValueHandling = (attribute != null) ? attribute._nullValueHandling : null;
            property.ReferenceLoopHandling = (attribute != null) ? attribute._referenceLoopHandling : null;
            property.ObjectCreationHandling = (attribute != null) ? attribute._objectCreationHandling : null;
            property.TypeNameHandling = (attribute != null) ? attribute._typeNameHandling : null;
            property.IsReference = (attribute != null) ? attribute._isReference : null;
            property.ItemIsReference = (attribute != null) ? attribute._itemIsReference : null;
            property.ItemConverter = ((attribute != null) && (attribute.ItemConverterType != null)) ? JsonConverterAttribute.CreateJsonConverterInstance(attribute.ItemConverterType) : null;
            property.ItemReferenceLoopHandling = (attribute != null) ? attribute._itemReferenceLoopHandling : null;
            property.ItemTypeNameHandling = (attribute != null) ? attribute._itemTypeNameHandling : null;
            allowNonPublicAccess = false;
            if ((this.DefaultMembersSearchFlags & BindingFlags.NonPublic) == BindingFlags.NonPublic)
            {
                allowNonPublicAccess = true;
            }
            if (attribute != null)
            {
                allowNonPublicAccess = true;
            }
            if (memberSerialization == MemberSerialization.Fields)
            {
                allowNonPublicAccess = true;
            }
            if (dataMemberAttribute != null)
            {
                allowNonPublicAccess = true;
                property.HasMemberAttribute = true;
            }
        }

        private bool ShouldSerializeEntityMember(MemberInfo memberInfo)
        {
            PropertyInfo info = memberInfo as PropertyInfo;
            if (((info != null) && info.PropertyType.IsGenericType()) && (info.PropertyType.GetGenericTypeDefinition().FullName == "System.Data.Objects.DataClasses.EntityReference`1"))
            {
                return false;
            }
            return true;
        }

        private void UpdateCache(Dictionary<ResolverContractKey, JsonContract> cache)
        {
            if (this._sharedCache)
            {
                _sharedContractCache = cache;
            }
            else
            {
                this._instanceContractCache = cache;
            }
        }

        public BindingFlags DefaultMembersSearchFlags { get; set; }

        public bool DynamicCodeGeneration
        {
            get
            {
                return JsonTypeReflector.DynamicCodeGeneration;
            }
        }

        public bool IgnoreSerializableAttribute { get; set; }

        public bool IgnoreSerializableInterface { get; set; }

        internal static IContractResolver Instance
        {
            get
            {
                return _instance;
            }
        }

        public bool SerializeCompilerGeneratedMembers { get; set; }
    }
}

