﻿namespace Newtonsoft.Json.Serialization
{
    using Newtonsoft.Json.Utilities;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.CompilerServices;

    public class JsonArrayContract : JsonContainerContract
    {
        private readonly Type _genericCollectionDefinitionType;
        private MethodCall<object, object> _genericWrapperCreator;
        private Type _genericWrapperType;
        private readonly bool _isCollectionItemTypeNullableType;

        public JsonArrayContract(Type underlyingType) : base(underlyingType)
        {
            base.ContractType = JsonContractType.Array;
            if (ReflectionUtils.ImplementsGenericDefinition(underlyingType, typeof(ICollection<>), out this._genericCollectionDefinitionType))
            {
                this.CollectionItemType = this._genericCollectionDefinitionType.GetGenericArguments()[0];
            }
            else if (underlyingType.IsGenericType() && (underlyingType.GetGenericTypeDefinition() == typeof(IEnumerable<>)))
            {
                this._genericCollectionDefinitionType = typeof(IEnumerable<>);
                this.CollectionItemType = underlyingType.GetGenericArguments()[0];
            }
            else
            {
                this.CollectionItemType = ReflectionUtils.GetCollectionItemType(base.UnderlyingType);
            }
            if (this.CollectionItemType != null)
            {
                this._isCollectionItemTypeNullableType = ReflectionUtils.IsNullableType(this.CollectionItemType);
            }
            if (this.IsTypeGenericCollectionInterface(base.UnderlyingType))
            {
                base.CreatedType = ReflectionUtils.MakeGenericType(typeof(List<>), new Type[] { this.CollectionItemType });
            }
            else if (this.IsTypeGenericSetnterface(base.UnderlyingType))
            {
                base.CreatedType = ReflectionUtils.MakeGenericType(typeof(HashSet<>), new Type[] { this.CollectionItemType });
            }
            this.IsMultidimensionalArray = base.UnderlyingType.IsArray && (base.UnderlyingType.GetArrayRank() > 1);
        }

        internal IWrappedCollection CreateWrapper(object list)
        {
            if (((list is IList) && ((this.CollectionItemType == null) || !this._isCollectionItemTypeNullableType)) || base.UnderlyingType.IsArray)
            {
                return new CollectionWrapper<object>((IList) list);
            }
            if (this._genericCollectionDefinitionType != null)
            {
                this.EnsureGenericWrapperCreator();
                return (IWrappedCollection) this._genericWrapperCreator(null, new object[] { list });
            }
            IList list2 = ((IEnumerable) list).Cast<object>().ToList<object>();
            if (this.CollectionItemType != null)
            {
                Array array = Array.CreateInstance(this.CollectionItemType, list2.Count);
                for (int i = 0; i < list2.Count; i++)
                {
                    array.SetValue(list2[i], i);
                }
                list2 = array;
            }
            return new CollectionWrapper<object>(list2);
        }

        private void EnsureGenericWrapperCreator()
        {
            if (this._genericWrapperCreator == null)
            {
                Type type;
                this._genericWrapperType = ReflectionUtils.MakeGenericType(typeof(CollectionWrapper<>), new Type[] { this.CollectionItemType });
                if (ReflectionUtils.InheritsGenericDefinition(this._genericCollectionDefinitionType, typeof(List<>)) || (this._genericCollectionDefinitionType.GetGenericTypeDefinition() == typeof(IEnumerable<>)))
                {
                    type = ReflectionUtils.MakeGenericType(typeof(ICollection<>), new Type[] { this.CollectionItemType });
                }
                else
                {
                    type = this._genericCollectionDefinitionType;
                }
                ConstructorInfo constructor = this._genericWrapperType.GetConstructor(new Type[] { type });
                this._genericWrapperCreator = JsonTypeReflector.ReflectionDelegateFactory.CreateMethodCall<object>(constructor);
            }
        }

        private bool IsTypeGenericCollectionInterface(Type type)
        {
            if (!type.IsGenericType())
            {
                return false;
            }
            Type genericTypeDefinition = type.GetGenericTypeDefinition();
            if (!(genericTypeDefinition == typeof(IList<>)) && !(genericTypeDefinition == typeof(ICollection<>)))
            {
                return (genericTypeDefinition == typeof(IEnumerable<>));
            }
            return true;
        }

        private bool IsTypeGenericSetnterface(Type type)
        {
            if (!type.IsGenericType())
            {
                return false;
            }
            return (type.GetGenericTypeDefinition() == typeof(ISet<>));
        }

        public Type CollectionItemType { get; private set; }

        public bool IsMultidimensionalArray { get; private set; }
    }
}

