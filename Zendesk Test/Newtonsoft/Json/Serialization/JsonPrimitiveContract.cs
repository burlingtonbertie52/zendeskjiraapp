﻿namespace Newtonsoft.Json.Serialization
{
    using System;

    public class JsonPrimitiveContract : JsonContract
    {
        public JsonPrimitiveContract(Type underlyingType) : base(underlyingType)
        {
            base.ContractType = JsonContractType.Primitive;
        }
    }
}

