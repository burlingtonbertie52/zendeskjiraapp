﻿namespace Newtonsoft.Json.Serialization
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Newtonsoft.Json.Utilities;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.Dynamic;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.InteropServices;
    using System.Runtime.Serialization;

    internal class JsonSerializerInternalReader : JsonSerializerInternalBase
    {
        private JsonFormatterConverter _formatterConverter;
        private JsonSerializerProxy _internalSerializer;

        public JsonSerializerInternalReader(JsonSerializer serializer) : base(serializer)
        {
        }

        private void AddReference(JsonReader reader, string id, object value)
        {
            try
            {
                if ((base.TraceWriter != null) && (base.TraceWriter.LevelFilter >= TraceLevel.Verbose))
                {
                    base.TraceWriter.Trace(TraceLevel.Verbose, JsonPosition.FormatMessage(reader as IJsonLineInfo, reader.Path, "Read object reference Id '{0}' for {1}.".FormatWith(CultureInfo.InvariantCulture, id, value.GetType())), null);
                }
                base.Serializer.ReferenceResolver.AddReference(this, id, value);
            }
            catch (Exception exception)
            {
                throw JsonSerializationException.Create(reader, "Error reading object reference '{0}'.".FormatWith(CultureInfo.InvariantCulture, id), exception);
            }
        }

        private bool CalculatePropertyDetails(JsonProperty property, ref JsonConverter propertyConverter, JsonContainerContract containerContract, JsonProperty containerProperty, JsonReader reader, object target, out bool useExistingValue, out object currentValue, out JsonContract propertyContract, out bool gottenCurrentValue)
        {
            currentValue = null;
            useExistingValue = false;
            propertyContract = null;
            gottenCurrentValue = false;
            if (property.Ignored)
            {
                reader.Skip();
                return true;
            }
            ObjectCreationHandling valueOrDefault = property.ObjectCreationHandling.GetValueOrDefault(base.Serializer.ObjectCreationHandling);
            if ((((valueOrDefault == ObjectCreationHandling.Auto) || (valueOrDefault == ObjectCreationHandling.Reuse)) && ((reader.TokenType == JsonToken.StartArray) || (reader.TokenType == JsonToken.StartObject))) && property.Readable)
            {
                currentValue = property.ValueProvider.GetValue(target);
                gottenCurrentValue = true;
                useExistingValue = (((currentValue != null) && !property.PropertyType.IsArray) && !ReflectionUtils.InheritsGenericDefinition(property.PropertyType, typeof(ReadOnlyCollection<>))) && !property.PropertyType.IsValueType();
            }
            if (!property.Writable && !useExistingValue)
            {
                reader.Skip();
                return true;
            }
            if ((((NullValueHandling) property.NullValueHandling.GetValueOrDefault(base.Serializer.NullValueHandling)) == NullValueHandling.Ignore) && (reader.TokenType == JsonToken.Null))
            {
                reader.Skip();
                return true;
            }
            if ((this.HasFlag(property.DefaultValueHandling.GetValueOrDefault(base.Serializer.DefaultValueHandling), DefaultValueHandling.Ignore) && JsonReader.IsPrimitiveToken(reader.TokenType)) && MiscellaneousUtils.ValueEquals(reader.Value, property.GetResolvedDefaultValue()))
            {
                reader.Skip();
                return true;
            }
            if (property.PropertyContract == null)
            {
                property.PropertyContract = this.GetContractSafe(property.PropertyType);
            }
            if (currentValue == null)
            {
                propertyContract = property.PropertyContract;
            }
            else
            {
                propertyContract = this.GetContractSafe(currentValue.GetType());
                if (propertyContract != property.PropertyContract)
                {
                    propertyConverter = this.GetConverter(propertyContract, property.MemberConverter, containerContract, containerProperty);
                }
            }
            return false;
        }

        private void CheckedRead(JsonReader reader)
        {
            if (!reader.Read())
            {
                throw JsonSerializationException.Create(reader, "Unexpected end when deserializing object.");
            }
        }

        private object CreateDynamic(JsonReader reader, JsonDynamicContract contract, JsonProperty member, string id)
        {
            JsonToken token;
            if (contract.UnderlyingType.IsInterface() || contract.UnderlyingType.IsAbstract())
            {
                throw JsonSerializationException.Create(reader, "Could not create an instance of type {0}. Type is an interface or abstract class and cannot be instantiated.".FormatWith(CultureInfo.InvariantCulture, contract.UnderlyingType));
            }
            if ((contract.DefaultCreator == null) || (contract.DefaultCreatorNonPublic && (base.Serializer.ConstructorHandling != ConstructorHandling.AllowNonPublicDefaultConstructor)))
            {
                throw JsonSerializationException.Create(reader, "Unable to find a default constructor to use for type {0}.".FormatWith(CultureInfo.InvariantCulture, contract.UnderlyingType));
            }
            IDynamicMetaObjectProvider provider = (IDynamicMetaObjectProvider) contract.DefaultCreator();
            if (id != null)
            {
                this.AddReference(reader, id, provider);
            }
            this.OnDeserializing(reader, contract, provider);
            int depth = reader.Depth;
            bool flag = false;
        Label_00A3:
            token = reader.TokenType;
            if (token != JsonToken.PropertyName)
            {
                if (token != JsonToken.EndObject)
                {
                    throw JsonSerializationException.Create(reader, "Unexpected token when deserializing object: " + reader.TokenType);
                }
            }
            else
            {
                string str = reader.Value.ToString();
                try
                {
                    if (!reader.Read())
                    {
                        throw JsonSerializationException.Create(reader, "Unexpected end when setting {0}'s value.".FormatWith(CultureInfo.InvariantCulture, str));
                    }
                    JsonProperty closestMatchProperty = contract.Properties.GetClosestMatchProperty(str);
                    if (((closestMatchProperty != null) && closestMatchProperty.Writable) && !closestMatchProperty.Ignored)
                    {
                        if (closestMatchProperty.PropertyContract == null)
                        {
                            closestMatchProperty.PropertyContract = this.GetContractSafe(closestMatchProperty.PropertyType);
                        }
                        JsonConverter propertyConverter = this.GetConverter(closestMatchProperty.PropertyContract, closestMatchProperty.MemberConverter, null, null);
                        this.SetPropertyValue(closestMatchProperty, propertyConverter, null, member, reader, provider);
                    }
                    else
                    {
                        object obj2;
                        Type type = JsonReader.IsPrimitiveToken(reader.TokenType) ? reader.ValueType : typeof(IDynamicMetaObjectProvider);
                        JsonContract contractSafe = this.GetContractSafe(type);
                        JsonConverter converter = this.GetConverter(contractSafe, null, null, member);
                        if ((converter != null) && converter.CanRead)
                        {
                            obj2 = this.DeserializeConvertable(converter, reader, type, null);
                        }
                        else
                        {
                            obj2 = this.CreateValueInternal(reader, type, contractSafe, null, null, member, null);
                        }
                        provider.TrySetMember(str, obj2);
                    }
                    goto Label_020E;
                }
                catch (Exception exception)
                {
                    if (!base.IsErrorHandled(provider, contract, str, reader as IJsonLineInfo, reader.Path, exception))
                    {
                        throw;
                    }
                    this.HandleError(reader, true, depth);
                    goto Label_020E;
                }
            }
            flag = true;
        Label_020E:
            if (!flag && reader.Read())
            {
                goto Label_00A3;
            }
            if (!flag)
            {
                this.ThrowUnexpectedEndException(reader, contract, provider, "Unexpected end when deserializing object.");
            }
            this.OnDeserialized(reader, contract, provider);
            return provider;
        }

        private object CreateISerializable(JsonReader reader, JsonISerializableContract contract, string id)
        {
            Type underlyingType = contract.UnderlyingType;
            if (!JsonTypeReflector.FullyTrusted)
            {
                throw JsonSerializationException.Create(reader, "Type '{0}' implements ISerializable but cannot be deserialized using the ISerializable interface because the current application is not fully trusted and ISerializable can expose secure data.\r\nTo fix this error either change the environment to be fully trusted, change the application to not deserialize the type, add JsonObjectAttribute to the type or change the JsonSerializer setting ContractResolver to use a new DefaultContractResolver with IgnoreSerializableInterface set to true.\r\n".FormatWith(CultureInfo.InvariantCulture, underlyingType));
            }
            if ((base.TraceWriter != null) && (base.TraceWriter.LevelFilter >= TraceLevel.Info))
            {
                base.TraceWriter.Trace(TraceLevel.Info, JsonPosition.FormatMessage(reader as IJsonLineInfo, reader.Path, "Deserializing {0} using ISerializable constructor.".FormatWith(CultureInfo.InvariantCulture, contract.UnderlyingType)), null);
            }
            SerializationInfo currentObject = new SerializationInfo(contract.UnderlyingType, this.GetFormatterConverter());
            bool flag = false;
            do
            {
                switch (reader.TokenType)
                {
                    case JsonToken.PropertyName:
                    {
                        string str = reader.Value.ToString();
                        if (!reader.Read())
                        {
                            throw JsonSerializationException.Create(reader, "Unexpected end when setting {0}'s value.".FormatWith(CultureInfo.InvariantCulture, str));
                        }
                        currentObject.AddValue(str, JToken.ReadFrom(reader));
                        break;
                    }
                    case JsonToken.Comment:
                        break;

                    case JsonToken.EndObject:
                        flag = true;
                        break;

                    default:
                        throw JsonSerializationException.Create(reader, "Unexpected token when deserializing object: " + reader.TokenType);
                }
            }
            while (!flag && reader.Read());
            if (!flag)
            {
                this.ThrowUnexpectedEndException(reader, contract, currentObject, "Unexpected end when deserializing object.");
            }
            if (contract.ISerializableCreator == null)
            {
                throw JsonSerializationException.Create(reader, "ISerializable type '{0}' does not have a valid constructor. To correctly implement ISerializable a constructor that takes SerializationInfo and StreamingContext parameters should be present.".FormatWith(CultureInfo.InvariantCulture, underlyingType));
            }
            object obj2 = contract.ISerializableCreator(new object[] { currentObject, base.Serializer.Context });
            if (id != null)
            {
                this.AddReference(reader, id, obj2);
            }
            this.OnDeserializing(reader, contract, obj2);
            this.OnDeserialized(reader, contract, obj2);
            return obj2;
        }

        private JToken CreateJObject(JsonReader reader)
        {
            ValidationUtils.ArgumentNotNull(reader, "reader");
            using (JTokenWriter writer = new JTokenWriter())
            {
                writer.WriteStartObject();
                if (reader.TokenType == JsonToken.PropertyName)
                {
                    writer.WriteToken(reader, reader.Depth - 1);
                }
                else
                {
                    writer.WriteEndObject();
                }
                return writer.Token;
            }
        }

        private JToken CreateJToken(JsonReader reader, JsonContract contract)
        {
            ValidationUtils.ArgumentNotNull(reader, "reader");
            if ((contract != null) && (contract.UnderlyingType == typeof(JRaw)))
            {
                return JRaw.Create(reader);
            }
            using (JTokenWriter writer = new JTokenWriter())
            {
                writer.WriteToken(reader);
                return writer.Token;
            }
        }

        private object CreateList(JsonReader reader, Type objectType, JsonContract contract, JsonProperty member, object existingValue, string id)
        {
            bool flag;
            if (!this.HasDefinedType(objectType))
            {
                return this.CreateJToken(reader, contract);
            }
            JsonArrayContract contract2 = this.EnsureArrayContract(reader, objectType, contract);
            if (existingValue != null)
            {
                return this.PopulateList(contract2.CreateWrapper(existingValue), reader, contract2, member, id);
            }
            IList list = CollectionUtils.CreateList(contract.CreatedType, out flag);
            if ((id != null) && flag)
            {
                throw JsonSerializationException.Create(reader, "Cannot preserve reference to array or readonly list: {0}.".FormatWith(CultureInfo.InvariantCulture, contract.UnderlyingType));
            }
            if ((contract.OnSerializing != null) && flag)
            {
                throw JsonSerializationException.Create(reader, "Cannot call OnSerializing on an array or readonly list: {0}.".FormatWith(CultureInfo.InvariantCulture, contract.UnderlyingType));
            }
            if ((contract.OnError != null) && flag)
            {
                throw JsonSerializationException.Create(reader, "Cannot call OnError on an array or readonly list: {0}.".FormatWith(CultureInfo.InvariantCulture, contract.UnderlyingType));
            }
            if (!contract2.IsMultidimensionalArray)
            {
                this.PopulateList(contract2.CreateWrapper(list), reader, contract2, member, id);
            }
            else
            {
                this.PopulateMultidimensionalArray(list, reader, contract2, member, id);
            }
            if (flag)
            {
                if (contract2.IsMultidimensionalArray)
                {
                    return CollectionUtils.ToMultidimensionalArray(list, ReflectionUtils.GetCollectionItemType(contract.CreatedType), contract.CreatedType.GetArrayRank());
                }
                if (contract.CreatedType.IsArray)
                {
                    return CollectionUtils.ToArray(((List<object>) list).ToArray(), ReflectionUtils.GetCollectionItemType(contract.CreatedType));
                }
                if (ReflectionUtils.InheritsGenericDefinition(contract.CreatedType, typeof(ReadOnlyCollection<>)))
                {
                    list = (IList) ReflectionUtils.CreateInstance(contract.CreatedType, new object[] { list });
                }
                return list;
            }
            if (list is IWrappedCollection)
            {
                return ((IWrappedCollection) list).UnderlyingCollection;
            }
            return list;
        }

        public object CreateNewDictionary(JsonReader reader, JsonDictionaryContract contract)
        {
            if ((contract.DefaultCreator == null) || (contract.DefaultCreatorNonPublic && (base.Serializer.ConstructorHandling != ConstructorHandling.AllowNonPublicDefaultConstructor)))
            {
                throw JsonSerializationException.Create(reader, "Unable to find a default constructor to use for type {0}.".FormatWith(CultureInfo.InvariantCulture, contract.UnderlyingType));
            }
            return contract.DefaultCreator();
        }

        public object CreateNewObject(JsonReader reader, JsonObjectContract objectContract, JsonProperty containerMember, JsonProperty containerProperty, string id, out bool createdFromNonDefaultConstructor)
        {
            object obj2 = null;
            if (objectContract.UnderlyingType.IsInterface() || objectContract.UnderlyingType.IsAbstract())
            {
                throw JsonSerializationException.Create(reader, "Could not create an instance of type {0}. Type is an interface or abstract class and cannot be instantiated.".FormatWith(CultureInfo.InvariantCulture, objectContract.UnderlyingType));
            }
            if (objectContract.OverrideConstructor != null)
            {
                if (objectContract.OverrideConstructor.GetParameters().Length > 0)
                {
                    createdFromNonDefaultConstructor = true;
                    return this.CreateObjectFromNonDefaultConstructor(reader, objectContract, containerMember, objectContract.OverrideConstructor, id);
                }
                obj2 = objectContract.OverrideConstructor.Invoke(null);
            }
            else if ((objectContract.DefaultCreator != null) && ((!objectContract.DefaultCreatorNonPublic || (base.Serializer.ConstructorHandling == ConstructorHandling.AllowNonPublicDefaultConstructor)) || (objectContract.ParametrizedConstructor == null)))
            {
                obj2 = objectContract.DefaultCreator();
            }
            else if (objectContract.ParametrizedConstructor != null)
            {
                createdFromNonDefaultConstructor = true;
                return this.CreateObjectFromNonDefaultConstructor(reader, objectContract, containerMember, objectContract.ParametrizedConstructor, id);
            }
            if (obj2 == null)
            {
                throw JsonSerializationException.Create(reader, "Unable to find a constructor to use for type {0}. A class should either have a default constructor, one constructor with arguments or a constructor marked with the JsonConstructor attribute.".FormatWith(CultureInfo.InvariantCulture, objectContract.UnderlyingType));
            }
            createdFromNonDefaultConstructor = false;
            return obj2;
        }

        private object CreateObject(JsonReader reader, Type objectType, JsonContract contract, JsonProperty member, JsonContainerContract containerContract, JsonProperty containerMember, object existingValue)
        {
            string str;
            object obj2;
            bool flag;
            JsonObjectContract contract2;
            object obj3;
            this.CheckedRead(reader);
            if (this.ReadSpecialProperties(reader, ref objectType, ref contract, member, containerContract, containerMember, existingValue, out obj2, out str))
            {
                return obj2;
            }
            if (!this.HasDefinedType(objectType))
            {
                return this.CreateJObject(reader);
            }
            if (contract == null)
            {
                throw JsonSerializationException.Create(reader, "Could not resolve type '{0}' to a JsonContract.".FormatWith(CultureInfo.InvariantCulture, objectType));
            }
            switch (contract.ContractType)
            {
                case JsonContractType.Object:
                    flag = false;
                    contract2 = (JsonObjectContract) contract;
                    if (existingValue == null)
                    {
                        obj3 = this.CreateNewObject(reader, contract2, member, containerMember, str, out flag);
                        break;
                    }
                    obj3 = existingValue;
                    break;

                case JsonContractType.Primitive:
                {
                    JsonPrimitiveContract contract3 = (JsonPrimitiveContract) contract;
                    if ((reader.TokenType != JsonToken.PropertyName) || !string.Equals(reader.Value.ToString(), "$value", StringComparison.Ordinal))
                    {
                        goto Label_0161;
                    }
                    this.CheckedRead(reader);
                    object obj4 = this.CreateValueInternal(reader, objectType, contract3, member, null, null, existingValue);
                    this.CheckedRead(reader);
                    return obj4;
                }
                case JsonContractType.Dictionary:
                {
                    object obj5;
                    JsonDictionaryContract contract4 = (JsonDictionaryContract) contract;
                    if (existingValue == null)
                    {
                        obj5 = this.CreateNewDictionary(reader, contract4);
                    }
                    else
                    {
                        obj5 = existingValue;
                    }
                    return this.PopulateDictionary(contract4.CreateWrapper(obj5), reader, contract4, member, str);
                }
                case JsonContractType.Dynamic:
                {
                    JsonDynamicContract contract5 = (JsonDynamicContract) contract;
                    return this.CreateDynamic(reader, contract5, member, str);
                }
                case JsonContractType.Serializable:
                {
                    JsonISerializableContract contract6 = (JsonISerializableContract) contract;
                    return this.CreateISerializable(reader, contract6, str);
                }
                default:
                    goto Label_0161;
            }
            if (flag)
            {
                return obj3;
            }
            return this.PopulateObject(obj3, reader, contract2, member, str);
        Label_0161:
            throw JsonSerializationException.Create(reader, "Cannot deserialize the current JSON object (e.g. {{\"name\":\"value\"}}) into type '{0}' because the type requires a {1} to deserialize correctly.\r\nTo fix this error either change the JSON to a {1} or change the deserialized type so that it is a normal .NET type (e.g. not a primitive type like integer, not a collection type like an array or List<T>) that can be deserialized from a JSON object. JsonObjectAttribute can also be added to the type to force it to deserialize from a JSON object.\r\n".FormatWith(CultureInfo.InvariantCulture, objectType, this.GetExpectedDescription(contract)));
        }

        private object CreateObjectFromNonDefaultConstructor(JsonReader reader, JsonObjectContract contract, JsonProperty containerProperty, ConstructorInfo constructorInfo, string id)
        {
            ValidationUtils.ArgumentNotNull(constructorInfo, "constructorInfo");
            Type underlyingType = contract.UnderlyingType;
            if ((base.TraceWriter != null) && (base.TraceWriter.LevelFilter >= TraceLevel.Info))
            {
                base.TraceWriter.Trace(TraceLevel.Info, JsonPosition.FormatMessage(reader as IJsonLineInfo, reader.Path, "Deserializing {0} using a non-default constructor '{1}'.".FormatWith(CultureInfo.InvariantCulture, contract.UnderlyingType, constructorInfo)), null);
            }
            IDictionary<JsonProperty, object> dictionary = this.ResolvePropertyAndConstructorValues(contract, containerProperty, reader, underlyingType);
            IDictionary<ParameterInfo, object> source = constructorInfo.GetParameters().ToDictionary<ParameterInfo, ParameterInfo, object>(p => p, p => null);
            IDictionary<JsonProperty, object> dictionary3 = new Dictionary<JsonProperty, object>();
            foreach (KeyValuePair<JsonProperty, object> pair in dictionary)
            {
                ParameterInfo key = source.ForgivingCaseSensitiveFind<KeyValuePair<ParameterInfo, object>>(kv => kv.Key.Name, pair.Key.UnderlyingName).Key;
                if (key != null)
                {
                    source[key] = pair.Value;
                }
                else
                {
                    dictionary3.Add(pair);
                }
            }
            object obj2 = constructorInfo.Invoke(source.Values.ToArray<object>());
            if (id != null)
            {
                this.AddReference(reader, id, obj2);
            }
            this.OnDeserializing(reader, contract, obj2);
            foreach (KeyValuePair<JsonProperty, object> pair2 in dictionary3)
            {
                JsonProperty property = pair2.Key;
                object obj3 = pair2.Value;
                if (this.ShouldSetPropertyValue(pair2.Key, pair2.Value))
                {
                    property.ValueProvider.SetValue(obj2, obj3);
                }
                else if (!property.Writable && (obj3 != null))
                {
                    JsonContract contract2 = base.Serializer.ContractResolver.ResolveContract(property.PropertyType);
                    if (contract2.ContractType == JsonContractType.Array)
                    {
                        JsonArrayContract contract3 = (JsonArrayContract) contract2;
                        object list = property.ValueProvider.GetValue(obj2);
                        if (list != null)
                        {
                            IWrappedCollection wrappeds = contract3.CreateWrapper(list);
                            foreach (object obj5 in contract3.CreateWrapper(obj3))
                            {
                                wrappeds.Add(obj5);
                            }
                        }
                    }
                    else if (contract2.ContractType == JsonContractType.Dictionary)
                    {
                        JsonDictionaryContract contract4 = (JsonDictionaryContract) contract2;
                        object obj6 = property.ValueProvider.GetValue(obj2);
                        if (obj6 != null)
                        {
                            IWrappedDictionary dictionary4 = contract4.CreateWrapper(obj6);
                            foreach (DictionaryEntry entry in contract4.CreateWrapper(obj3))
                            {
                                dictionary4.Add(entry.Key, entry.Value);
                            }
                        }
                    }
                }
            }
            this.OnDeserialized(reader, contract, obj2);
            return obj2;
        }

        private object CreateValueInternal(JsonReader reader, Type objectType, JsonContract contract, JsonProperty member, JsonContainerContract containerContract, JsonProperty containerMember, object existingValue)
        {
            if ((contract != null) && (contract.ContractType == JsonContractType.Linq))
            {
                return this.CreateJToken(reader, contract);
            }
        Label_0015:
            switch (reader.TokenType)
            {
                case JsonToken.StartObject:
                    return this.CreateObject(reader, objectType, contract, member, containerContract, containerMember, existingValue);

                case JsonToken.StartArray:
                    return this.CreateList(reader, objectType, contract, member, existingValue, null);

                case JsonToken.StartConstructor:
                {
                    string str = reader.Value.ToString();
                    return this.EnsureType(reader, str, CultureInfo.InvariantCulture, contract, objectType);
                }
                case JsonToken.Comment:
                    if (!reader.Read())
                    {
                        throw JsonSerializationException.Create(reader, "Unexpected end when deserializing object.");
                    }
                    goto Label_0015;

                case JsonToken.Raw:
                    return new JRaw((string) reader.Value);

                case JsonToken.Integer:
                case JsonToken.Float:
                case JsonToken.Boolean:
                case JsonToken.Date:
                case JsonToken.Bytes:
                    return this.EnsureType(reader, reader.Value, CultureInfo.InvariantCulture, contract, objectType);

                case JsonToken.String:
                    if (((!string.IsNullOrEmpty((string) reader.Value) || !(objectType != typeof(string))) || (!(objectType != typeof(object)) || (contract == null))) || !contract.IsNullable)
                    {
                        if (objectType == typeof(byte[]))
                        {
                            return Convert.FromBase64String((string) reader.Value);
                        }
                        return this.EnsureType(reader, reader.Value, CultureInfo.InvariantCulture, contract, objectType);
                    }
                    return null;

                case JsonToken.Null:
                case JsonToken.Undefined:
                    if (!(objectType == typeof(DBNull)))
                    {
                        return this.EnsureType(reader, reader.Value, CultureInfo.InvariantCulture, contract, objectType);
                    }
                    return DBNull.Value;
            }
            throw JsonSerializationException.Create(reader, "Unexpected token while deserializing object: " + reader.TokenType);
        }

        public object Deserialize(JsonReader reader, Type objectType, bool checkAdditionalContent)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            JsonContract contractSafe = this.GetContractSafe(objectType);
            try
            {
                object obj2;
                JsonConverter converter = this.GetConverter(contractSafe, null, null, null);
                if ((reader.TokenType == JsonToken.None) && !this.ReadForType(reader, contractSafe, converter != null))
                {
                    if ((contractSafe != null) && !contractSafe.IsNullable)
                    {
                        throw JsonSerializationException.Create(reader, "No JSON content found and type '{0}' is not nullable.".FormatWith(CultureInfo.InvariantCulture, contractSafe.UnderlyingType));
                    }
                    return null;
                }
                if ((converter != null) && converter.CanRead)
                {
                    obj2 = this.DeserializeConvertable(converter, reader, objectType, null);
                }
                else
                {
                    obj2 = this.CreateValueInternal(reader, objectType, contractSafe, null, null, null, null);
                }
                if ((checkAdditionalContent && reader.Read()) && (reader.TokenType != JsonToken.Comment))
                {
                    throw new JsonSerializationException("Additional text found in JSON string after finishing deserializing object.");
                }
                return obj2;
            }
            catch (Exception exception)
            {
                if (!base.IsErrorHandled(null, contractSafe, null, reader as IJsonLineInfo, reader.Path, exception))
                {
                    throw;
                }
                this.HandleError(reader, false, 0);
                return null;
            }
        }

        private object DeserializeConvertable(JsonConverter converter, JsonReader reader, Type objectType, object existingValue)
        {
            if ((base.TraceWriter != null) && (base.TraceWriter.LevelFilter >= TraceLevel.Info))
            {
                base.TraceWriter.Trace(TraceLevel.Info, JsonPosition.FormatMessage(reader as IJsonLineInfo, reader.Path, "Started deserializing {0} with converter {1}.".FormatWith(CultureInfo.InvariantCulture, objectType, converter.GetType())), null);
            }
            object obj2 = converter.ReadJson(reader, objectType, existingValue, this.GetInternalSerializer());
            if ((base.TraceWriter != null) && (base.TraceWriter.LevelFilter >= TraceLevel.Info))
            {
                base.TraceWriter.Trace(TraceLevel.Info, JsonPosition.FormatMessage(reader as IJsonLineInfo, reader.Path, "Finished deserializing {0} with converter {1}.".FormatWith(CultureInfo.InvariantCulture, objectType, converter.GetType())), null);
            }
            return obj2;
        }

        private void EndObject(object newObject, JsonReader reader, JsonObjectContract contract, int initialDepth, Dictionary<JsonProperty, PropertyPresence> propertiesPresence)
        {
            if (propertiesPresence != null)
            {
                foreach (KeyValuePair<JsonProperty, PropertyPresence> pair in propertiesPresence)
                {
                    JsonProperty key = pair.Key;
                    PropertyPresence presence = pair.Value;
                    switch (presence)
                    {
                        case PropertyPresence.None:
                        case PropertyPresence.Null:
                            try
                            {
                                Required? itemRequired;
                                Required? nullable = key._required;
                                if (!nullable.HasValue)
                                {
                                    itemRequired = contract.ItemRequired;
                                }
                                Required required = itemRequired.HasValue ? nullable.GetValueOrDefault() : Required.Default;
                                switch (presence)
                                {
                                    case PropertyPresence.None:
                                        switch (required)
                                        {
                                            case Required.AllowNull:
                                            case Required.Always:
                                                throw JsonSerializationException.Create(reader, "Required property '{0}' not found in JSON.".FormatWith(CultureInfo.InvariantCulture, key.PropertyName));
                                        }
                                        break;

                                    case PropertyPresence.Null:
                                    {
                                        if (required == Required.Always)
                                        {
                                            throw JsonSerializationException.Create(reader, "Required property '{0}' expects a value but got null.".FormatWith(CultureInfo.InvariantCulture, key.PropertyName));
                                        }
                                        continue;
                                    }
                                    default:
                                        break;
                                }
                                if (key.PropertyContract == null)
                                {
                                    key.PropertyContract = this.GetContractSafe(key.PropertyType);
                                }
                                if (this.HasFlag(key.DefaultValueHandling.GetValueOrDefault(base.Serializer.DefaultValueHandling), DefaultValueHandling.Populate) && key.Writable)
                                {
                                    key.ValueProvider.SetValue(newObject, this.EnsureType(reader, key.GetResolvedDefaultValue(), CultureInfo.InvariantCulture, key.PropertyContract, key.PropertyType));
                                }
                            }
                            catch (Exception exception)
                            {
                                if (!base.IsErrorHandled(newObject, contract, key.PropertyName, reader as IJsonLineInfo, reader.Path, exception))
                                {
                                    throw;
                                }
                                this.HandleError(reader, true, initialDepth);
                            }
                            break;
                    }
                }
            }
        }

        private JsonArrayContract EnsureArrayContract(JsonReader reader, Type objectType, JsonContract contract)
        {
            if (contract == null)
            {
                throw JsonSerializationException.Create(reader, "Could not resolve type '{0}' to a JsonContract.".FormatWith(CultureInfo.InvariantCulture, objectType));
            }
            JsonArrayContract contract2 = contract as JsonArrayContract;
            if (contract2 == null)
            {
                throw JsonSerializationException.Create(reader, "Cannot deserialize the current JSON array (e.g. [1,2,3]) into type '{0}' because the type requires a {1} to deserialize correctly.\r\nTo fix this error either change the JSON to a {1} or change the deserialized type to an array or a type that implements a collection interface (e.g. ICollection, IList) like List<T> that can be deserialized from a JSON array. JsonArrayAttribute can also be added to the type to force it to deserialize from a JSON array.\r\n".FormatWith(CultureInfo.InvariantCulture, objectType, this.GetExpectedDescription(contract)));
            }
            return contract2;
        }

        private object EnsureType(JsonReader reader, object value, CultureInfo culture, JsonContract contract, Type targetType)
        {
            if (targetType != null)
            {
                if (!(ReflectionUtils.GetObjectType(value) != targetType))
                {
                    return value;
                }
                try
                {
                    if ((value == null) && contract.IsNullable)
                    {
                        return null;
                    }
                    if (!contract.IsConvertable)
                    {
                        return ConvertUtils.ConvertOrCast(value, culture, contract.NonNullableUnderlyingType);
                    }
                    if (contract.NonNullableUnderlyingType.IsEnum())
                    {
                        if (value is string)
                        {
                            return Enum.Parse(contract.NonNullableUnderlyingType, value.ToString(), true);
                        }
                        if (ConvertUtils.IsInteger(value))
                        {
                            return Enum.ToObject(contract.NonNullableUnderlyingType, value);
                        }
                    }
                    return Convert.ChangeType(value, contract.NonNullableUnderlyingType, culture);
                }
                catch (Exception exception)
                {
                    throw JsonSerializationException.Create(reader, "Error converting value {0} to type '{1}'.".FormatWith(CultureInfo.InvariantCulture, this.FormatValueForPrint(value), targetType), exception);
                }
            }
            return value;
        }

        private string FormatValueForPrint(object value)
        {
            if (value == null)
            {
                return "{null}";
            }
            if (value is string)
            {
                return ("\"" + value + "\"");
            }
            return value.ToString();
        }

        private JsonContract GetContractSafe(Type type)
        {
            if (type == null)
            {
                return null;
            }
            return base.Serializer.ContractResolver.ResolveContract(type);
        }

        private JsonConverter GetConverter(JsonContract contract, JsonConverter memberConverter, JsonContainerContract containerContract, JsonProperty containerProperty)
        {
            JsonConverter internalConverter = null;
            if (memberConverter != null)
            {
                return memberConverter;
            }
            if ((containerProperty != null) && (containerProperty.ItemConverter != null))
            {
                return containerProperty.ItemConverter;
            }
            if ((containerContract != null) && (containerContract.ItemConverter != null))
            {
                return containerContract.ItemConverter;
            }
            if (contract != null)
            {
                if (contract.Converter != null)
                {
                    return contract.Converter;
                }
                JsonConverter matchingConverter = base.Serializer.GetMatchingConverter(contract.UnderlyingType);
                if (matchingConverter != null)
                {
                    return matchingConverter;
                }
                if (contract.InternalConverter != null)
                {
                    internalConverter = contract.InternalConverter;
                }
            }
            return internalConverter;
        }

        internal string GetExpectedDescription(JsonContract contract)
        {
            switch (contract.ContractType)
            {
                case JsonContractType.Object:
                case JsonContractType.Dictionary:
                case JsonContractType.Dynamic:
                case JsonContractType.Serializable:
                    return "JSON object (e.g. {\"name\":\"value\"})";

                case JsonContractType.Array:
                    return "JSON array (e.g. [1,2,3])";

                case JsonContractType.Primitive:
                    return "JSON primitive value (e.g. string, number, boolean, null)";

                case JsonContractType.String:
                    return "JSON string value";
            }
            throw new ArgumentOutOfRangeException();
        }

        private JsonFormatterConverter GetFormatterConverter()
        {
            if (this._formatterConverter == null)
            {
                this._formatterConverter = new JsonFormatterConverter(this.GetInternalSerializer());
            }
            return this._formatterConverter;
        }

        private JsonSerializerProxy GetInternalSerializer()
        {
            if (this._internalSerializer == null)
            {
                this._internalSerializer = new JsonSerializerProxy(this);
            }
            return this._internalSerializer;
        }

        private void HandleError(JsonReader reader, bool readPastError, int initialDepth)
        {
            base.ClearErrorContext();
            if (readPastError)
            {
                reader.Skip();
                while (reader.Depth > (initialDepth + 1))
                {
                    if (!reader.Read())
                    {
                        return;
                    }
                }
            }
        }

        private bool HasDefinedType(Type type)
        {
            return ((((type != null) && (type != typeof(object))) && !typeof(JToken).IsSubclassOf(type)) && (type != typeof(IDynamicMetaObjectProvider)));
        }

        private bool HasFlag(DefaultValueHandling value, DefaultValueHandling flag)
        {
            return ((value & flag) == flag);
        }

        private void OnDeserialized(JsonReader reader, JsonContract contract, object value)
        {
            if ((base.TraceWriter != null) && (base.TraceWriter.LevelFilter >= TraceLevel.Info))
            {
                base.TraceWriter.Trace(TraceLevel.Info, JsonPosition.FormatMessage(reader as IJsonLineInfo, reader.Path, "Finished deserializing {0}".FormatWith(CultureInfo.InvariantCulture, contract.UnderlyingType)), null);
            }
            contract.InvokeOnDeserialized(value, base.Serializer.Context);
        }

        private void OnDeserializing(JsonReader reader, JsonContract contract, object value)
        {
            if ((base.TraceWriter != null) && (base.TraceWriter.LevelFilter >= TraceLevel.Info))
            {
                base.TraceWriter.Trace(TraceLevel.Info, JsonPosition.FormatMessage(reader as IJsonLineInfo, reader.Path, "Started deserializing {0}".FormatWith(CultureInfo.InvariantCulture, contract.UnderlyingType)), null);
            }
            contract.InvokeOnDeserializing(value, base.Serializer.Context);
        }

        public void Populate(JsonReader reader, object target)
        {
            ValidationUtils.ArgumentNotNull(target, "target");
            Type type = target.GetType();
            JsonContract contract = base.Serializer.ContractResolver.ResolveContract(type);
            if (reader.TokenType == JsonToken.None)
            {
                reader.Read();
            }
            if (reader.TokenType == JsonToken.StartArray)
            {
                if (contract.ContractType != JsonContractType.Array)
                {
                    throw JsonSerializationException.Create(reader, "Cannot populate JSON array onto type '{0}'.".FormatWith(CultureInfo.InvariantCulture, type));
                }
                this.PopulateList(CollectionUtils.CreateCollectionWrapper(target), reader, (JsonArrayContract) contract, null, null);
            }
            else
            {
                if (reader.TokenType != JsonToken.StartObject)
                {
                    throw JsonSerializationException.Create(reader, "Unexpected initial token '{0}' when populating object. Expected JSON object or array.".FormatWith(CultureInfo.InvariantCulture, reader.TokenType));
                }
                this.CheckedRead(reader);
                string id = null;
                if ((reader.TokenType == JsonToken.PropertyName) && string.Equals(reader.Value.ToString(), "$id", StringComparison.Ordinal))
                {
                    this.CheckedRead(reader);
                    id = (reader.Value != null) ? reader.Value.ToString() : null;
                    this.CheckedRead(reader);
                }
                if (contract.ContractType == JsonContractType.Dictionary)
                {
                    this.PopulateDictionary(CollectionUtils.CreateDictionaryWrapper(target), reader, (JsonDictionaryContract) contract, null, id);
                }
                else
                {
                    if (contract.ContractType != JsonContractType.Object)
                    {
                        throw JsonSerializationException.Create(reader, "Cannot populate JSON object onto type '{0}'.".FormatWith(CultureInfo.InvariantCulture, type));
                    }
                    this.PopulateObject(target, reader, (JsonObjectContract) contract, null, id);
                }
            }
        }

        private object PopulateDictionary(IWrappedDictionary wrappedDictionary, JsonReader reader, JsonDictionaryContract contract, JsonProperty containerProperty, string id)
        {
            object underlyingDictionary = wrappedDictionary.UnderlyingDictionary;
            if (id != null)
            {
                this.AddReference(reader, id, underlyingDictionary);
            }
            this.OnDeserializing(reader, contract, underlyingDictionary);
            int depth = reader.Depth;
            if (contract.KeyContract == null)
            {
                contract.KeyContract = this.GetContractSafe(contract.DictionaryKeyType);
            }
            if (contract.ItemContract == null)
            {
                contract.ItemContract = this.GetContractSafe(contract.DictionaryValueType);
            }
            JsonConverter converter = contract.ItemConverter ?? this.GetConverter(contract.ItemContract, null, contract, containerProperty);
            bool flag = false;
            do
            {
                switch (reader.TokenType)
                {
                    case JsonToken.PropertyName:
                    {
                        object obj3 = reader.Value;
                        try
                        {
                            object obj4;
                            try
                            {
                                obj3 = this.EnsureType(reader, obj3, CultureInfo.InvariantCulture, contract.KeyContract, contract.DictionaryKeyType);
                            }
                            catch (Exception exception)
                            {
                                throw JsonSerializationException.Create(reader, "Could not convert string '{0}' to dictionary key type '{1}'. Create a TypeConverter to convert from the string to the key type object.".FormatWith(CultureInfo.InvariantCulture, reader.Value, contract.DictionaryKeyType), exception);
                            }
                            if (!this.ReadForType(reader, contract.ItemContract, converter != null))
                            {
                                throw JsonSerializationException.Create(reader, "Unexpected end when deserializing object.");
                            }
                            if ((converter != null) && converter.CanRead)
                            {
                                obj4 = this.DeserializeConvertable(converter, reader, contract.DictionaryValueType, null);
                            }
                            else
                            {
                                obj4 = this.CreateValueInternal(reader, contract.DictionaryValueType, contract.ItemContract, null, contract, containerProperty, null);
                            }
                            wrappedDictionary[obj3] = obj4;
                        }
                        catch (Exception exception2)
                        {
                            if (!base.IsErrorHandled(underlyingDictionary, contract, obj3, reader as IJsonLineInfo, reader.Path, exception2))
                            {
                                throw;
                            }
                            this.HandleError(reader, true, depth);
                        }
                        break;
                    }
                    case JsonToken.Comment:
                        break;

                    case JsonToken.EndObject:
                        flag = true;
                        break;

                    default:
                        throw JsonSerializationException.Create(reader, "Unexpected token when deserializing object: " + reader.TokenType);
                }
            }
            while (!flag && reader.Read());
            if (!flag)
            {
                this.ThrowUnexpectedEndException(reader, contract, underlyingDictionary, "Unexpected end when deserializing object.");
            }
            this.OnDeserialized(reader, contract, underlyingDictionary);
            return underlyingDictionary;
        }

        private object PopulateList(IWrappedCollection wrappedList, JsonReader reader, JsonArrayContract contract, JsonProperty containerProperty, string id)
        {
            object underlyingCollection = wrappedList.UnderlyingCollection;
            if (id != null)
            {
                this.AddReference(reader, id, underlyingCollection);
            }
            if (wrappedList.IsFixedSize)
            {
                reader.Skip();
                return underlyingCollection;
            }
            this.OnDeserializing(reader, contract, underlyingCollection);
            int depth = reader.Depth;
            JsonContract contractSafe = this.GetContractSafe(contract.CollectionItemType);
            JsonConverter converter = this.GetConverter(contractSafe, null, contract, containerProperty);
            int? nullable = null;
            bool flag = false;
        Label_0059:
            try
            {
                object obj3;
                if (!this.ReadForType(reader, contractSafe, converter != null))
                {
                    goto Label_0158;
                }
                switch (reader.TokenType)
                {
                    case JsonToken.Comment:
                        goto Label_0151;

                    case JsonToken.EndArray:
                        flag = true;
                        goto Label_0151;
                }
                if ((converter != null) && converter.CanRead)
                {
                    obj3 = this.DeserializeConvertable(converter, reader, contract.CollectionItemType, null);
                }
                else
                {
                    obj3 = this.CreateValueInternal(reader, contract.CollectionItemType, contractSafe, null, contract, containerProperty, null);
                }
                wrappedList.Add(obj3);
            }
            catch (Exception exception)
            {
                JsonPosition position = reader.GetPosition(depth);
                if (!base.IsErrorHandled(underlyingCollection, contract, position.Position, reader as IJsonLineInfo, reader.Path, exception))
                {
                    throw;
                }
                this.HandleError(reader, true, depth);
                if (nullable.HasValue)
                {
                    int? nullable2 = nullable;
                    int num2 = position.Position;
                    if ((nullable2.GetValueOrDefault() == num2) && nullable2.HasValue)
                    {
                        throw JsonSerializationException.Create(reader, "Infinite loop detected from error handling.", exception);
                    }
                }
                nullable = new int?(position.Position);
            }
        Label_0151:
            if (!flag)
            {
                goto Label_0059;
            }
        Label_0158:
            if (!flag)
            {
                this.ThrowUnexpectedEndException(reader, contract, underlyingCollection, "Unexpected end when deserializing array.");
            }
            this.OnDeserialized(reader, contract, underlyingCollection);
            return underlyingCollection;
        }

        private object PopulateMultidimensionalArray(IList list, JsonReader reader, JsonArrayContract contract, JsonProperty containerProperty, string id)
        {
            int num2;
            int arrayRank = contract.UnderlyingType.GetArrayRank();
            if (id != null)
            {
                this.AddReference(reader, id, list);
            }
            this.OnDeserializing(reader, contract, list);
            JsonContract contractSafe = this.GetContractSafe(contract.CollectionItemType);
            JsonConverter converter = this.GetConverter(contractSafe, null, contract, containerProperty);
            int? nullable = null;
            Stack<IList> stack = new Stack<IList>();
            stack.Push(list);
            IList list2 = list;
            bool flag = false;
        Label_0059:
            num2 = reader.Depth;
            if (stack.Count == arrayRank)
            {
                try
                {
                    object obj2;
                    if (!this.ReadForType(reader, contractSafe, converter != null))
                    {
                        goto Label_0208;
                    }
                    switch (reader.TokenType)
                    {
                        case JsonToken.Comment:
                            goto Label_0201;

                        case JsonToken.EndArray:
                            stack.Pop();
                            list2 = stack.Peek();
                            nullable = null;
                            goto Label_0201;
                    }
                    if ((converter != null) && converter.CanRead)
                    {
                        obj2 = this.DeserializeConvertable(converter, reader, contract.CollectionItemType, null);
                    }
                    else
                    {
                        obj2 = this.CreateValueInternal(reader, contract.CollectionItemType, contractSafe, null, contract, containerProperty, null);
                    }
                    list2.Add(obj2);
                }
                catch (Exception exception)
                {
                    JsonPosition position = reader.GetPosition(num2);
                    if (!base.IsErrorHandled(list, contract, position.Position, reader as IJsonLineInfo, reader.Path, exception))
                    {
                        throw;
                    }
                    this.HandleError(reader, true, num2);
                    if (nullable.HasValue)
                    {
                        int? nullable2 = nullable;
                        int num3 = position.Position;
                        if ((nullable2.GetValueOrDefault() == num3) && nullable2.HasValue)
                        {
                            throw JsonSerializationException.Create(reader, "Infinite loop detected from error handling.", exception);
                        }
                    }
                    nullable = new int?(position.Position);
                }
            }
            else
            {
                if (!reader.Read())
                {
                    goto Label_0208;
                }
                JsonToken tokenType = reader.TokenType;
                if (tokenType != JsonToken.StartArray)
                {
                    if (tokenType != JsonToken.Comment)
                    {
                        if (tokenType != JsonToken.EndArray)
                        {
                            throw JsonSerializationException.Create(reader, "Unexpected token when deserializing multidimensional array: " + reader.TokenType);
                        }
                        stack.Pop();
                        if (stack.Count > 0)
                        {
                            list2 = stack.Peek();
                        }
                        else
                        {
                            flag = true;
                        }
                    }
                }
                else
                {
                    IList list3 = new List<object>();
                    list2.Add(list3);
                    stack.Push(list3);
                    list2 = list3;
                }
            }
        Label_0201:
            if (!flag)
            {
                goto Label_0059;
            }
        Label_0208:
            if (!flag)
            {
                this.ThrowUnexpectedEndException(reader, contract, list, "Unexpected end when deserializing array.");
            }
            this.OnDeserialized(reader, contract, list);
            return list;
        }

        private object PopulateObject(object newObject, JsonReader reader, JsonObjectContract contract, JsonProperty member, string id)
        {
            this.OnDeserializing(reader, contract, newObject);
            Dictionary<JsonProperty, PropertyPresence> requiredProperties = (contract.HasRequiredOrDefaultValueProperties || this.HasFlag(base.Serializer.DefaultValueHandling, DefaultValueHandling.Populate)) ? contract.Properties.ToDictionary<JsonProperty, JsonProperty, PropertyPresence>(m => m, m => PropertyPresence.None) : null;
            if (id != null)
            {
                this.AddReference(reader, id, newObject);
            }
            int depth = reader.Depth;
            bool flag = false;
            do
            {
                switch (reader.TokenType)
                {
                    case JsonToken.PropertyName:
                    {
                        string propertyName = reader.Value.ToString();
                        try
                        {
                            JsonProperty closestMatchProperty = contract.Properties.GetClosestMatchProperty(propertyName);
                            if (closestMatchProperty == null)
                            {
                                if ((base.TraceWriter != null) && (base.TraceWriter.LevelFilter >= TraceLevel.Verbose))
                                {
                                    base.TraceWriter.Trace(TraceLevel.Verbose, JsonPosition.FormatMessage(reader as IJsonLineInfo, reader.Path, "Could not find member '{0}' on {1}".FormatWith(CultureInfo.InvariantCulture, propertyName, contract.UnderlyingType)), null);
                                }
                                if (base.Serializer.MissingMemberHandling == MissingMemberHandling.Error)
                                {
                                    throw JsonSerializationException.Create(reader, "Could not find member '{0}' on object of type '{1}'".FormatWith(CultureInfo.InvariantCulture, propertyName, contract.UnderlyingType.Name));
                                }
                                reader.Skip();
                            }
                            else
                            {
                                if (closestMatchProperty.PropertyContract == null)
                                {
                                    closestMatchProperty.PropertyContract = this.GetContractSafe(closestMatchProperty.PropertyType);
                                }
                                JsonConverter propertyConverter = this.GetConverter(closestMatchProperty.PropertyContract, closestMatchProperty.MemberConverter, contract, member);
                                if (!this.ReadForType(reader, closestMatchProperty.PropertyContract, propertyConverter != null))
                                {
                                    throw JsonSerializationException.Create(reader, "Unexpected end when setting {0}'s value.".FormatWith(CultureInfo.InvariantCulture, propertyName));
                                }
                                this.SetPropertyPresence(reader, closestMatchProperty, requiredProperties);
                                this.SetPropertyValue(closestMatchProperty, propertyConverter, contract, member, reader, newObject);
                            }
                        }
                        catch (Exception exception)
                        {
                            if (!base.IsErrorHandled(newObject, contract, propertyName, reader as IJsonLineInfo, reader.Path, exception))
                            {
                                throw;
                            }
                            this.HandleError(reader, true, depth);
                        }
                        break;
                    }
                    case JsonToken.Comment:
                        break;

                    case JsonToken.EndObject:
                        flag = true;
                        break;

                    default:
                        throw JsonSerializationException.Create(reader, "Unexpected token when deserializing object: " + reader.TokenType);
                }
            }
            while (!flag && reader.Read());
            if (!flag)
            {
                this.ThrowUnexpectedEndException(reader, contract, newObject, "Unexpected end when deserializing object.");
            }
            this.EndObject(newObject, reader, contract, depth, requiredProperties);
            this.OnDeserialized(reader, contract, newObject);
            return newObject;
        }

        private bool ReadForType(JsonReader reader, JsonContract contract, bool hasConverter)
        {
            if (hasConverter)
            {
                return reader.Read();
            }
            switch (((contract != null) ? contract.InternalReadType : ReadType.Read))
            {
                case ReadType.Read:
                    do
                    {
                        if (!reader.Read())
                        {
                            return false;
                        }
                    }
                    while (reader.TokenType == JsonToken.Comment);
                    return true;

                case ReadType.ReadAsInt32:
                    reader.ReadAsInt32();
                    break;

                case ReadType.ReadAsBytes:
                    reader.ReadAsBytes();
                    break;

                case ReadType.ReadAsString:
                    reader.ReadAsString();
                    break;

                case ReadType.ReadAsDecimal:
                    reader.ReadAsDecimal();
                    break;

                case ReadType.ReadAsDateTime:
                    reader.ReadAsDateTime();
                    break;

                case ReadType.ReadAsDateTimeOffset:
                    reader.ReadAsDateTimeOffset();
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
            return (reader.TokenType != JsonToken.None);
        }

        private bool ReadSpecialProperties(JsonReader reader, ref Type objectType, ref JsonContract contract, JsonProperty member, JsonContainerContract containerContract, JsonProperty containerMember, object existingValue, out object newValue, out string id)
        {
            id = null;
            newValue = null;
            if (reader.TokenType == JsonToken.PropertyName)
            {
                string a = reader.Value.ToString();
                if ((a.Length > 0) && (a[0] == '$'))
                {
                    bool flag;
                    do
                    {
                        a = reader.Value.ToString();
                        if (string.Equals(a, "$ref", StringComparison.Ordinal))
                        {
                            this.CheckedRead(reader);
                            if ((reader.TokenType != JsonToken.String) && (reader.TokenType != JsonToken.Null))
                            {
                                throw JsonSerializationException.Create(reader, "JSON reference {0} property must have a string or null value.".FormatWith(CultureInfo.InvariantCulture, "$ref"));
                            }
                            string reference = (reader.Value != null) ? reader.Value.ToString() : null;
                            this.CheckedRead(reader);
                            if (reference != null)
                            {
                                if (reader.TokenType == JsonToken.PropertyName)
                                {
                                    throw JsonSerializationException.Create(reader, "Additional content found in JSON reference object. A JSON reference object should only have a {0} property.".FormatWith(CultureInfo.InvariantCulture, "$ref"));
                                }
                                newValue = base.Serializer.ReferenceResolver.ResolveReference(this, reference);
                                if ((base.TraceWriter != null) && (base.TraceWriter.LevelFilter >= TraceLevel.Info))
                                {
                                    base.TraceWriter.Trace(TraceLevel.Info, JsonPosition.FormatMessage(reader as IJsonLineInfo, reader.Path, "Resolved object reference '{0}' to {1}.".FormatWith(CultureInfo.InvariantCulture, reference, newValue.GetType())), null);
                                }
                                return true;
                            }
                            flag = true;
                        }
                        else if (string.Equals(a, "$type", StringComparison.Ordinal))
                        {
                            TypeNameHandling? nullable6;
                            this.CheckedRead(reader);
                            string fullyQualifiedTypeName = reader.Value.ToString();
                            TypeNameHandling? nullable2 = (member != null) ? member.TypeNameHandling : null;
                            if (!nullable2.HasValue)
                            {
                                TypeNameHandling? nullable4 = (containerContract != null) ? containerContract.ItemTypeNameHandling : null;
                                if (!nullable4.HasValue)
                                {
                                    nullable6 = (containerMember != null) ? containerMember.ItemTypeNameHandling : null;
                                }
                            }
                            if (((TypeNameHandling) (nullable6.HasValue ? nullable2.GetValueOrDefault() : base.Serializer.TypeNameHandling)) != TypeNameHandling.None)
                            {
                                string str4;
                                string str5;
                                Type type;
                                ReflectionUtils.SplitFullyQualifiedTypeName(fullyQualifiedTypeName, out str4, out str5);
                                try
                                {
                                    type = base.Serializer.Binder.BindToType(str5, str4);
                                }
                                catch (Exception exception)
                                {
                                    throw JsonSerializationException.Create(reader, "Error resolving type specified in JSON '{0}'.".FormatWith(CultureInfo.InvariantCulture, fullyQualifiedTypeName), exception);
                                }
                                if (type == null)
                                {
                                    throw JsonSerializationException.Create(reader, "Type specified in JSON '{0}' was not resolved.".FormatWith(CultureInfo.InvariantCulture, fullyQualifiedTypeName));
                                }
                                if ((base.TraceWriter != null) && (base.TraceWriter.LevelFilter >= TraceLevel.Verbose))
                                {
                                    base.TraceWriter.Trace(TraceLevel.Verbose, JsonPosition.FormatMessage(reader as IJsonLineInfo, reader.Path, "Resolved type '{0}' to {1}.".FormatWith(CultureInfo.InvariantCulture, fullyQualifiedTypeName, type)), null);
                                }
                                if (((objectType != null) && (objectType != typeof(IDynamicMetaObjectProvider))) && !objectType.IsAssignableFrom(type))
                                {
                                    throw JsonSerializationException.Create(reader, "Type specified in JSON '{0}' is not compatible with '{1}'.".FormatWith(CultureInfo.InvariantCulture, type.AssemblyQualifiedName, objectType.AssemblyQualifiedName));
                                }
                                objectType = type;
                                contract = this.GetContractSafe(type);
                            }
                            this.CheckedRead(reader);
                            flag = true;
                        }
                        else if (string.Equals(a, "$id", StringComparison.Ordinal))
                        {
                            this.CheckedRead(reader);
                            id = (reader.Value != null) ? reader.Value.ToString() : null;
                            this.CheckedRead(reader);
                            flag = true;
                        }
                        else
                        {
                            if (string.Equals(a, "$values", StringComparison.Ordinal))
                            {
                                this.CheckedRead(reader);
                                object obj2 = this.CreateList(reader, objectType, contract, member, existingValue, id);
                                this.CheckedRead(reader);
                                newValue = obj2;
                                return true;
                            }
                            flag = false;
                        }
                    }
                    while (flag && (reader.TokenType == JsonToken.PropertyName));
                }
            }
            return false;
        }

        private IDictionary<JsonProperty, object> ResolvePropertyAndConstructorValues(JsonObjectContract contract, JsonProperty containerProperty, JsonReader reader, Type objectType)
        {
            IDictionary<JsonProperty, object> dictionary = new Dictionary<JsonProperty, object>();
            bool flag = false;
            do
            {
                switch (reader.TokenType)
                {
                    case JsonToken.PropertyName:
                    {
                        object obj2;
                        string propertyName = reader.Value.ToString();
                        JsonProperty member = contract.ConstructorParameters.GetClosestMatchProperty(propertyName) ?? contract.Properties.GetClosestMatchProperty(propertyName);
                        if (member == null)
                        {
                            if (!reader.Read())
                            {
                                throw JsonSerializationException.Create(reader, "Unexpected end when setting {0}'s value.".FormatWith(CultureInfo.InvariantCulture, propertyName));
                            }
                            if ((base.TraceWriter != null) && (base.TraceWriter.LevelFilter >= TraceLevel.Verbose))
                            {
                                base.TraceWriter.Trace(TraceLevel.Verbose, JsonPosition.FormatMessage(reader as IJsonLineInfo, reader.Path, "Could not find member '{0}' on {1}.".FormatWith(CultureInfo.InvariantCulture, propertyName, contract.UnderlyingType)), null);
                            }
                            if (base.Serializer.MissingMemberHandling == MissingMemberHandling.Error)
                            {
                                throw JsonSerializationException.Create(reader, "Could not find member '{0}' on object of type '{1}'".FormatWith(CultureInfo.InvariantCulture, propertyName, objectType.Name));
                            }
                            reader.Skip();
                            break;
                        }
                        if (member.PropertyContract == null)
                        {
                            member.PropertyContract = this.GetContractSafe(member.PropertyType);
                        }
                        JsonConverter converter = this.GetConverter(member.PropertyContract, member.MemberConverter, contract, containerProperty);
                        if (!this.ReadForType(reader, member.PropertyContract, converter != null))
                        {
                            throw JsonSerializationException.Create(reader, "Unexpected end when setting {0}'s value.".FormatWith(CultureInfo.InvariantCulture, propertyName));
                        }
                        if (member.Ignored)
                        {
                            reader.Skip();
                            break;
                        }
                        if (member.PropertyContract == null)
                        {
                            member.PropertyContract = this.GetContractSafe(member.PropertyType);
                        }
                        if ((converter != null) && converter.CanRead)
                        {
                            obj2 = this.DeserializeConvertable(converter, reader, member.PropertyType, null);
                        }
                        else
                        {
                            obj2 = this.CreateValueInternal(reader, member.PropertyType, member.PropertyContract, member, contract, containerProperty, null);
                        }
                        dictionary[member] = obj2;
                        break;
                    }
                    case JsonToken.Comment:
                        break;

                    case JsonToken.EndObject:
                        flag = true;
                        break;

                    default:
                        throw JsonSerializationException.Create(reader, "Unexpected token when deserializing object: " + reader.TokenType);
                }
            }
            while (!flag && reader.Read());
            return dictionary;
        }

        private void SetPropertyPresence(JsonReader reader, JsonProperty property, Dictionary<JsonProperty, PropertyPresence> requiredProperties)
        {
            if ((property != null) && (requiredProperties != null))
            {
                requiredProperties[property] = ((reader.TokenType == JsonToken.Null) || (reader.TokenType == JsonToken.Undefined)) ? PropertyPresence.Null : PropertyPresence.Value;
            }
        }

        private void SetPropertyValue(JsonProperty property, JsonConverter propertyConverter, JsonContainerContract containerContract, JsonProperty containerProperty, JsonReader reader, object target)
        {
            object obj2;
            bool flag;
            JsonContract contract;
            bool flag2;
            if (!this.CalculatePropertyDetails(property, ref propertyConverter, containerContract, containerProperty, reader, target, out flag, out obj2, out contract, out flag2))
            {
                object obj3;
                if ((propertyConverter != null) && propertyConverter.CanRead)
                {
                    if ((!flag2 && (target != null)) && property.Readable)
                    {
                        obj2 = property.ValueProvider.GetValue(target);
                    }
                    obj3 = this.DeserializeConvertable(propertyConverter, reader, property.PropertyType, obj2);
                }
                else
                {
                    obj3 = this.CreateValueInternal(reader, property.PropertyType, contract, property, containerContract, containerProperty, flag ? obj2 : null);
                }
                if ((!flag || (obj3 != obj2)) && this.ShouldSetPropertyValue(property, obj3))
                {
                    property.ValueProvider.SetValue(target, obj3);
                    if (property.SetIsSpecified != null)
                    {
                        if ((base.TraceWriter != null) && (base.TraceWriter.LevelFilter >= TraceLevel.Verbose))
                        {
                            base.TraceWriter.Trace(TraceLevel.Verbose, JsonPosition.FormatMessage(reader as IJsonLineInfo, reader.Path, "IsSpecified for property '{0}' on {1} set to true.".FormatWith(CultureInfo.InvariantCulture, property.PropertyName, property.DeclaringType)), null);
                        }
                        property.SetIsSpecified(target, true);
                    }
                }
            }
        }

        private bool ShouldSetPropertyValue(JsonProperty property, object value)
        {
            if ((((NullValueHandling) property.NullValueHandling.GetValueOrDefault(base.Serializer.NullValueHandling)) == NullValueHandling.Ignore) && (value == null))
            {
                return false;
            }
            if (this.HasFlag(property.DefaultValueHandling.GetValueOrDefault(base.Serializer.DefaultValueHandling), DefaultValueHandling.Ignore) && MiscellaneousUtils.ValueEquals(value, property.GetResolvedDefaultValue()))
            {
                return false;
            }
            if (!property.Writable)
            {
                return false;
            }
            return true;
        }

        private void ThrowUnexpectedEndException(JsonReader reader, JsonContract contract, object currentObject, string message)
        {
            try
            {
                throw JsonSerializationException.Create(reader, message);
            }
            catch (Exception exception)
            {
                if (!base.IsErrorHandled(currentObject, contract, null, reader as IJsonLineInfo, reader.Path, exception))
                {
                    throw;
                }
                this.HandleError(reader, false, 0);
            }
        }

        internal enum PropertyPresence
        {
            None,
            Null,
            Value
        }
    }
}

