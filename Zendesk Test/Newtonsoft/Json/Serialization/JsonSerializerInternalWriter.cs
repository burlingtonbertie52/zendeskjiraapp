﻿namespace Newtonsoft.Json.Serialization
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Newtonsoft.Json.Utilities;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Dynamic;
    using System.Globalization;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Runtime.Serialization;
    using System.Security;

    internal class JsonSerializerInternalWriter : JsonSerializerInternalBase
    {
        private JsonSerializerProxy _internalSerializer;
        private readonly List<object> _serializeStack;

        public JsonSerializerInternalWriter(JsonSerializer serializer) : base(serializer)
        {
            this._serializeStack = new List<object>();
        }

        private bool CalculatePropertyValues(JsonWriter writer, object value, JsonContainerContract contract, JsonProperty member, JsonProperty property, out JsonContract memberContract, out object memberValue)
        {
            if ((!property.Ignored && property.Readable) && (this.ShouldSerialize(writer, property, value) && this.IsSpecified(writer, property, value)))
            {
                if (property.PropertyContract == null)
                {
                    property.PropertyContract = base.Serializer.ContractResolver.ResolveContract(property.PropertyType);
                }
                memberValue = property.ValueProvider.GetValue(value);
                memberContract = property.PropertyContract.UnderlyingType.IsSealed() ? property.PropertyContract : this.GetContractSafe(memberValue);
                if (this.ShouldWriteProperty(memberValue, property))
                {
                    if (this.ShouldWriteReference(memberValue, property, memberContract, contract, member))
                    {
                        writer.WritePropertyName(property.PropertyName);
                        this.WriteReference(writer, memberValue);
                        return false;
                    }
                    if (!this.CheckForCircularReference(writer, memberValue, property, memberContract, contract, member))
                    {
                        return false;
                    }
                    if (memberValue == null)
                    {
                        Required? nullable3;
                        JsonObjectContract contract2 = contract as JsonObjectContract;
                        Required? nullable = property._required;
                        if (!nullable.HasValue)
                        {
                            nullable3 = (contract2 != null) ? contract2.ItemRequired : null;
                        }
                        Required required = nullable3.HasValue ? nullable.GetValueOrDefault() : Required.Default;
                        if (required == Required.Always)
                        {
                            throw JsonSerializationException.Create(null, writer.ContainerPath, "Cannot write a null value for property '{0}'. Property requires a value.".FormatWith(CultureInfo.InvariantCulture, property.PropertyName), null);
                        }
                    }
                    return true;
                }
            }
            memberContract = null;
            memberValue = null;
            return false;
        }

        private bool CheckForCircularReference(JsonWriter writer, object value, JsonProperty property, JsonContract contract, JsonContainerContract containerContract, JsonProperty containerProperty)
        {
            if (((value != null) && (contract.ContractType != JsonContractType.Primitive)) && (contract.ContractType != JsonContractType.String))
            {
                ReferenceLoopHandling? referenceLoopHandling = null;
                if (property != null)
                {
                    referenceLoopHandling = property.ReferenceLoopHandling;
                }
                if (!referenceLoopHandling.HasValue && (containerProperty != null))
                {
                    referenceLoopHandling = containerProperty.ItemReferenceLoopHandling;
                }
                if (!referenceLoopHandling.HasValue && (containerContract != null))
                {
                    referenceLoopHandling = containerContract.ItemReferenceLoopHandling;
                }
                if (this._serializeStack.IndexOf(value) != -1)
                {
                    string message = "Self referencing loop detected";
                    if (property != null)
                    {
                        message = message + " for property '{0}'".FormatWith(CultureInfo.InvariantCulture, property.PropertyName);
                    }
                    message = message + " with type '{0}'.".FormatWith(CultureInfo.InvariantCulture, value.GetType());
                    switch (referenceLoopHandling.GetValueOrDefault(base.Serializer.ReferenceLoopHandling))
                    {
                        case ReferenceLoopHandling.Error:
                            throw JsonSerializationException.Create(null, writer.ContainerPath, message, null);

                        case ReferenceLoopHandling.Ignore:
                            if ((base.TraceWriter != null) && (base.TraceWriter.LevelFilter >= TraceLevel.Verbose))
                            {
                                base.TraceWriter.Trace(TraceLevel.Verbose, JsonPosition.FormatMessage(null, writer.Path, message + ". Skipping serializing self referenced value."), null);
                            }
                            return false;

                        case ReferenceLoopHandling.Serialize:
                            if ((base.TraceWriter != null) && (base.TraceWriter.LevelFilter >= TraceLevel.Verbose))
                            {
                                base.TraceWriter.Trace(TraceLevel.Verbose, JsonPosition.FormatMessage(null, writer.Path, message + ". Serializing self referenced value."), null);
                            }
                            return true;
                    }
                }
            }
            return true;
        }

        private JsonContract GetContractSafe(object value)
        {
            if (value == null)
            {
                return null;
            }
            return base.Serializer.ContractResolver.ResolveContract(value.GetType());
        }

        private JsonSerializerProxy GetInternalSerializer()
        {
            if (this._internalSerializer == null)
            {
                this._internalSerializer = new JsonSerializerProxy(this);
            }
            return this._internalSerializer;
        }

        private string GetPropertyName(DictionaryEntry entry)
        {
            string str;
            if (ConvertUtils.IsConvertible(entry.Key))
            {
                return Convert.ToString(entry.Key, CultureInfo.InvariantCulture);
            }
            if (TryConvertToString(entry.Key, entry.Key.GetType(), out str))
            {
                return str;
            }
            return entry.Key.ToString();
        }

        private string GetReference(JsonWriter writer, object value)
        {
            string reference;
            try
            {
                reference = base.Serializer.ReferenceResolver.GetReference(this, value);
            }
            catch (Exception exception)
            {
                throw JsonSerializationException.Create(null, writer.ContainerPath, "Error writing object reference for '{0}'.".FormatWith(CultureInfo.InvariantCulture, value.GetType()), exception);
            }
            return reference;
        }

        private void HandleError(JsonWriter writer, int initialDepth)
        {
            base.ClearErrorContext();
            if (writer.WriteState == WriteState.Property)
            {
                writer.WriteNull();
            }
            while (writer.Top > initialDepth)
            {
                writer.WriteEnd();
            }
        }

        private bool HasFlag(DefaultValueHandling value, DefaultValueHandling flag)
        {
            return ((value & flag) == flag);
        }

        private bool HasFlag(PreserveReferencesHandling value, PreserveReferencesHandling flag)
        {
            return ((value & flag) == flag);
        }

        private bool HasFlag(TypeNameHandling value, TypeNameHandling flag)
        {
            return ((value & flag) == flag);
        }

        private bool IsSpecified(JsonWriter writer, JsonProperty property, object target)
        {
            if (property.GetIsSpecified == null)
            {
                return true;
            }
            bool flag = property.GetIsSpecified(target);
            if ((base.TraceWriter != null) && (base.TraceWriter.LevelFilter >= TraceLevel.Verbose))
            {
                base.TraceWriter.Trace(TraceLevel.Verbose, JsonPosition.FormatMessage(null, writer.Path, "IsSpecified result for property '{0}' on {1}: {2}".FormatWith(CultureInfo.InvariantCulture, property.PropertyName, property.DeclaringType, flag)), null);
            }
            return flag;
        }

        private void OnSerialized(JsonWriter writer, JsonContract contract, object value)
        {
            if ((base.TraceWriter != null) && (base.TraceWriter.LevelFilter >= TraceLevel.Info))
            {
                base.TraceWriter.Trace(TraceLevel.Info, JsonPosition.FormatMessage(null, writer.Path, "Finished serializing {0}".FormatWith(CultureInfo.InvariantCulture, contract.UnderlyingType)), null);
            }
            contract.InvokeOnSerialized(value, base.Serializer.Context);
        }

        private void OnSerializing(JsonWriter writer, JsonContract contract, object value)
        {
            if ((base.TraceWriter != null) && (base.TraceWriter.LevelFilter >= TraceLevel.Info))
            {
                base.TraceWriter.Trace(TraceLevel.Info, JsonPosition.FormatMessage(null, writer.Path, "Started serializing {0}".FormatWith(CultureInfo.InvariantCulture, contract.UnderlyingType)), null);
            }
            contract.InvokeOnSerializing(value, base.Serializer.Context);
        }

        private bool? ResolveIsReference(JsonContract contract, JsonProperty property, JsonContainerContract collectionContract, JsonProperty containerProperty)
        {
            bool? isReference = null;
            if (property != null)
            {
                isReference = property.IsReference;
            }
            if (!isReference.HasValue && (containerProperty != null))
            {
                isReference = containerProperty.ItemIsReference;
            }
            if (!isReference.HasValue && (collectionContract != null))
            {
                isReference = collectionContract.ItemIsReference;
            }
            if (!isReference.HasValue)
            {
                isReference = contract.IsReference;
            }
            return isReference;
        }

        public void Serialize(JsonWriter jsonWriter, object value)
        {
            if (jsonWriter == null)
            {
                throw new ArgumentNullException("jsonWriter");
            }
            this.SerializeValue(jsonWriter, value, this.GetContractSafe(value), null, null, null);
        }

        private void SerializeConvertable(JsonWriter writer, JsonConverter converter, object value, JsonContract contract, JsonContainerContract collectionContract, JsonProperty containerProperty)
        {
            if (this.ShouldWriteReference(value, null, contract, collectionContract, containerProperty))
            {
                this.WriteReference(writer, value);
            }
            else if (this.CheckForCircularReference(writer, value, null, contract, collectionContract, containerProperty))
            {
                this._serializeStack.Add(value);
                if ((base.TraceWriter != null) && (base.TraceWriter.LevelFilter >= TraceLevel.Info))
                {
                    base.TraceWriter.Trace(TraceLevel.Info, JsonPosition.FormatMessage(null, writer.Path, "Started serializing {0} with converter {1}.".FormatWith(CultureInfo.InvariantCulture, value.GetType(), converter.GetType())), null);
                }
                converter.WriteJson(writer, value, this.GetInternalSerializer());
                if ((base.TraceWriter != null) && (base.TraceWriter.LevelFilter >= TraceLevel.Info))
                {
                    base.TraceWriter.Trace(TraceLevel.Info, JsonPosition.FormatMessage(null, writer.Path, "Finished serializing {0} with converter {1}.".FormatWith(CultureInfo.InvariantCulture, value.GetType(), converter.GetType())), null);
                }
                this._serializeStack.RemoveAt(this._serializeStack.Count - 1);
            }
        }

        private void SerializeDictionary(JsonWriter writer, IWrappedDictionary values, JsonDictionaryContract contract, JsonProperty member, JsonContainerContract collectionContract, JsonProperty containerProperty)
        {
            this.OnSerializing(writer, contract, values.UnderlyingDictionary);
            this._serializeStack.Add(values.UnderlyingDictionary);
            this.WriteObjectStart(writer, values.UnderlyingDictionary, contract, member, collectionContract, containerProperty);
            if (contract.ItemContract == null)
            {
                contract.ItemContract = base.Serializer.ContractResolver.ResolveContract(contract.DictionaryValueType ?? typeof(object));
            }
            int top = writer.Top;
            IWrappedDictionary dictionary = values;
            foreach (DictionaryEntry entry in dictionary)
            {
                string propertyName = this.GetPropertyName(entry);
                propertyName = (contract.PropertyNameResolver != null) ? contract.PropertyNameResolver(propertyName) : propertyName;
                try
                {
                    object obj2 = entry.Value;
                    JsonContract valueContract = contract.FinalItemContract ?? this.GetContractSafe(obj2);
                    if (this.ShouldWriteReference(obj2, null, valueContract, contract, member))
                    {
                        writer.WritePropertyName(propertyName);
                        this.WriteReference(writer, obj2);
                    }
                    else if (this.CheckForCircularReference(writer, obj2, null, valueContract, contract, member))
                    {
                        writer.WritePropertyName(propertyName);
                        this.SerializeValue(writer, obj2, valueContract, null, contract, member);
                    }
                }
                catch (Exception exception)
                {
                    if (!base.IsErrorHandled(values.UnderlyingDictionary, contract, propertyName, null, writer.ContainerPath, exception))
                    {
                        throw;
                    }
                    this.HandleError(writer, top);
                }
            }
            writer.WriteEndObject();
            this._serializeStack.RemoveAt(this._serializeStack.Count - 1);
            this.OnSerialized(writer, contract, values.UnderlyingDictionary);
        }

        private void SerializeDynamic(JsonWriter writer, IDynamicMetaObjectProvider value, JsonDynamicContract contract, JsonProperty member, JsonContainerContract collectionContract, JsonProperty containerProperty)
        {
            this.OnSerializing(writer, contract, value);
            this._serializeStack.Add(value);
            this.WriteObjectStart(writer, value, contract, member, collectionContract, containerProperty);
            int top = writer.Top;
            foreach (JsonProperty property in contract.Properties)
            {
                if (property.HasMemberAttribute)
                {
                    try
                    {
                        object obj2;
                        JsonContract contract2;
                        if (this.CalculatePropertyValues(writer, value, contract, member, property, out contract2, out obj2))
                        {
                            writer.WritePropertyName(property.PropertyName);
                            this.SerializeValue(writer, obj2, contract2, property, contract, member);
                        }
                    }
                    catch (Exception exception)
                    {
                        if (!base.IsErrorHandled(value, contract, property.PropertyName, null, writer.ContainerPath, exception))
                        {
                            throw;
                        }
                        this.HandleError(writer, top);
                    }
                }
            }
            foreach (string str in value.GetDynamicMemberNames())
            {
                object obj3;
                if (value.TryGetMember(str, out obj3))
                {
                    try
                    {
                        JsonContract contractSafe = this.GetContractSafe(obj3);
                        if (this.CheckForCircularReference(writer, obj3, null, contractSafe, contract, member))
                        {
                            string name = (contract.PropertyNameResolver != null) ? contract.PropertyNameResolver(str) : str;
                            writer.WritePropertyName(name);
                            this.SerializeValue(writer, obj3, contractSafe, null, contract, member);
                        }
                    }
                    catch (Exception exception2)
                    {
                        if (!base.IsErrorHandled(value, contract, str, null, writer.ContainerPath, exception2))
                        {
                            throw;
                        }
                        this.HandleError(writer, top);
                    }
                }
            }
            writer.WriteEndObject();
            this._serializeStack.RemoveAt(this._serializeStack.Count - 1);
            this.OnSerialized(writer, contract, value);
        }

        [SecuritySafeCritical]
        private void SerializeISerializable(JsonWriter writer, ISerializable value, JsonISerializableContract contract, JsonProperty member, JsonContainerContract collectionContract, JsonProperty containerProperty)
        {
            if (!JsonTypeReflector.FullyTrusted)
            {
                throw JsonSerializationException.Create(null, writer.ContainerPath, "Type '{0}' implements ISerializable but cannot be serialized using the ISerializable interface because the current application is not fully trusted and ISerializable can expose secure data.\r\nTo fix this error either change the environment to be fully trusted, change the application to not deserialize the type, add JsonObjectAttribute to the type or change the JsonSerializer setting ContractResolver to use a new DefaultContractResolver with IgnoreSerializableInterface set to true.".FormatWith(CultureInfo.InvariantCulture, value.GetType()), null);
            }
            this.OnSerializing(writer, contract, value);
            this._serializeStack.Add(value);
            this.WriteObjectStart(writer, value, contract, member, collectionContract, containerProperty);
            SerializationInfo info = new SerializationInfo(contract.UnderlyingType, new FormatterConverter());
            value.GetObjectData(info, base.Serializer.Context);
            SerializationInfoEnumerator enumerator = info.GetEnumerator();
            while (enumerator.MoveNext())
            {
                SerializationEntry current = enumerator.Current;
                JsonContract contractSafe = this.GetContractSafe(current.Value);
                if (this.CheckForCircularReference(writer, current.Value, null, contractSafe, contract, member))
                {
                    writer.WritePropertyName(current.Name);
                    this.SerializeValue(writer, current.Value, contractSafe, null, contract, member);
                }
            }
            writer.WriteEndObject();
            this._serializeStack.RemoveAt(this._serializeStack.Count - 1);
            this.OnSerialized(writer, contract, value);
        }

        private void SerializeList(JsonWriter writer, IWrappedCollection values, JsonArrayContract contract, JsonProperty member, JsonContainerContract collectionContract, JsonProperty containerProperty)
        {
            this.OnSerializing(writer, contract, values.UnderlyingCollection);
            this._serializeStack.Add(values.UnderlyingCollection);
            bool flag = this.WriteStartArray(writer, values.UnderlyingCollection, contract, member, collectionContract, containerProperty);
            writer.WriteStartArray();
            int top = writer.Top;
            int keyValue = 0;
            foreach (object obj2 in values)
            {
                try
                {
                    JsonContract valueContract = contract.FinalItemContract ?? this.GetContractSafe(obj2);
                    if (this.ShouldWriteReference(obj2, null, valueContract, contract, member))
                    {
                        this.WriteReference(writer, obj2);
                    }
                    else if (this.CheckForCircularReference(writer, obj2, null, valueContract, contract, member))
                    {
                        this.SerializeValue(writer, obj2, valueContract, null, contract, member);
                    }
                }
                catch (Exception exception)
                {
                    if (!base.IsErrorHandled(values.UnderlyingCollection, contract, keyValue, null, writer.ContainerPath, exception))
                    {
                        throw;
                    }
                    this.HandleError(writer, top);
                }
                finally
                {
                    keyValue++;
                }
            }
            writer.WriteEndArray();
            if (flag)
            {
                writer.WriteEndObject();
            }
            this._serializeStack.RemoveAt(this._serializeStack.Count - 1);
            this.OnSerialized(writer, contract, values.UnderlyingCollection);
        }

        private void SerializeMultidimensionalArray(JsonWriter writer, Array values, JsonArrayContract contract, JsonProperty member, JsonContainerContract collectionContract, JsonProperty containerProperty)
        {
            this.OnSerializing(writer, contract, values);
            this._serializeStack.Add(values);
            bool flag = this.WriteStartArray(writer, values, contract, member, collectionContract, containerProperty);
            this.SerializeMultidimensionalArray(writer, values, contract, member, writer.Top, new int[0]);
            if (flag)
            {
                writer.WriteEndObject();
            }
            this._serializeStack.RemoveAt(this._serializeStack.Count - 1);
            this.OnSerialized(writer, contract, values);
        }

        private void SerializeMultidimensionalArray(JsonWriter writer, Array values, JsonArrayContract contract, JsonProperty member, int initialDepth, int[] indices)
        {
            int length = indices.Length;
            int[] numArray = new int[length + 1];
            for (int i = 0; i < length; i++)
            {
                numArray[i] = indices[i];
            }
            writer.WriteStartArray();
            for (int j = 0; j < values.GetLength(length); j++)
            {
                numArray[length] = j;
                if (numArray.Length == values.Rank)
                {
                    object obj2 = values.GetValue(numArray);
                    try
                    {
                        JsonContract valueContract = contract.FinalItemContract ?? this.GetContractSafe(obj2);
                        if (this.ShouldWriteReference(obj2, null, valueContract, contract, member))
                        {
                            this.WriteReference(writer, obj2);
                        }
                        else if (this.CheckForCircularReference(writer, obj2, null, valueContract, contract, member))
                        {
                            this.SerializeValue(writer, obj2, valueContract, null, contract, member);
                        }
                        continue;
                    }
                    catch (Exception exception)
                    {
                        if (!base.IsErrorHandled(values, contract, j, null, writer.ContainerPath, exception))
                        {
                            throw;
                        }
                        this.HandleError(writer, initialDepth + 1);
                        continue;
                    }
                }
                this.SerializeMultidimensionalArray(writer, values, contract, member, initialDepth + 1, numArray);
            }
            writer.WriteEndArray();
        }

        private void SerializeObject(JsonWriter writer, object value, JsonObjectContract contract, JsonProperty member, JsonContainerContract collectionContract, JsonProperty containerProperty)
        {
            this.OnSerializing(writer, contract, value);
            this._serializeStack.Add(value);
            this.WriteObjectStart(writer, value, contract, member, collectionContract, containerProperty);
            int top = writer.Top;
            foreach (JsonProperty property in contract.Properties)
            {
                try
                {
                    object obj2;
                    JsonContract contract2;
                    if (this.CalculatePropertyValues(writer, value, contract, member, property, out contract2, out obj2))
                    {
                        writer.WritePropertyName(property.PropertyName);
                        this.SerializeValue(writer, obj2, contract2, property, contract, member);
                    }
                }
                catch (Exception exception)
                {
                    if (!base.IsErrorHandled(value, contract, property.PropertyName, null, writer.ContainerPath, exception))
                    {
                        throw;
                    }
                    this.HandleError(writer, top);
                }
            }
            writer.WriteEndObject();
            this._serializeStack.RemoveAt(this._serializeStack.Count - 1);
            this.OnSerialized(writer, contract, value);
        }

        private void SerializePrimitive(JsonWriter writer, object value, JsonPrimitiveContract contract, JsonProperty member, JsonContainerContract containerContract, JsonProperty containerProperty)
        {
            if ((contract.UnderlyingType == typeof(byte[])) && this.ShouldWriteType(TypeNameHandling.Objects, contract, member, containerContract, containerProperty))
            {
                writer.WriteStartObject();
                this.WriteTypeProperty(writer, contract.CreatedType);
                writer.WritePropertyName("$value");
                writer.WriteValue(value);
                writer.WriteEndObject();
            }
            else
            {
                writer.WriteValue(value);
            }
        }

        private void SerializeString(JsonWriter writer, object value, JsonStringContract contract)
        {
            string str;
            this.OnSerializing(writer, contract, value);
            TryConvertToString(value, contract.UnderlyingType, out str);
            writer.WriteValue(str);
            this.OnSerialized(writer, contract, value);
        }

        private void SerializeValue(JsonWriter writer, object value, JsonContract valueContract, JsonProperty member, JsonContainerContract containerContract, JsonProperty containerProperty)
        {
            if (value == null)
            {
                writer.WriteNull();
            }
            else
            {
                JsonConverter converter;
                if ((((((converter = (member != null) ? member.Converter : null) != null) || ((converter = (containerProperty != null) ? containerProperty.ItemConverter : null) != null)) || ((converter = (containerContract != null) ? containerContract.ItemConverter : null) != null)) || ((((converter = valueContract.Converter) != null) || ((converter = base.Serializer.GetMatchingConverter(valueContract.UnderlyingType)) != null)) || ((converter = valueContract.InternalConverter) != null))) && converter.CanWrite)
                {
                    this.SerializeConvertable(writer, converter, value, valueContract, containerContract, containerProperty);
                }
                else
                {
                    switch (valueContract.ContractType)
                    {
                        case JsonContractType.Object:
                            this.SerializeObject(writer, value, (JsonObjectContract) valueContract, member, containerContract, containerProperty);
                            return;

                        case JsonContractType.Array:
                        {
                            JsonArrayContract contract = (JsonArrayContract) valueContract;
                            if (contract.IsMultidimensionalArray)
                            {
                                this.SerializeMultidimensionalArray(writer, (Array) value, contract, member, containerContract, containerProperty);
                                return;
                            }
                            this.SerializeList(writer, contract.CreateWrapper(value), contract, member, containerContract, containerProperty);
                            return;
                        }
                        case JsonContractType.Primitive:
                            this.SerializePrimitive(writer, value, (JsonPrimitiveContract) valueContract, member, containerContract, containerProperty);
                            return;

                        case JsonContractType.String:
                            this.SerializeString(writer, value, (JsonStringContract) valueContract);
                            return;

                        case JsonContractType.Dictionary:
                        {
                            JsonDictionaryContract contract2 = (JsonDictionaryContract) valueContract;
                            this.SerializeDictionary(writer, contract2.CreateWrapper(value), contract2, member, containerContract, containerProperty);
                            return;
                        }
                        case JsonContractType.Dynamic:
                            this.SerializeDynamic(writer, (IDynamicMetaObjectProvider) value, (JsonDynamicContract) valueContract, member, containerContract, containerProperty);
                            return;

                        case JsonContractType.Serializable:
                            this.SerializeISerializable(writer, (ISerializable) value, (JsonISerializableContract) valueContract, member, containerContract, containerProperty);
                            return;

                        case JsonContractType.Linq:
                            ((JToken) value).WriteTo(writer, (base.Serializer.Converters != null) ? base.Serializer.Converters.ToArray<JsonConverter>() : null);
                            return;
                    }
                }
            }
        }

        private bool ShouldSerialize(JsonWriter writer, JsonProperty property, object target)
        {
            if (property.ShouldSerialize == null)
            {
                return true;
            }
            bool flag = property.ShouldSerialize(target);
            if ((base.TraceWriter != null) && (base.TraceWriter.LevelFilter >= TraceLevel.Verbose))
            {
                base.TraceWriter.Trace(TraceLevel.Verbose, JsonPosition.FormatMessage(null, writer.Path, "ShouldSerialize result for property '{0}' on {1}: {2}".FormatWith(CultureInfo.InvariantCulture, property.PropertyName, property.DeclaringType, flag)), null);
            }
            return flag;
        }

        private bool ShouldWriteProperty(object memberValue, JsonProperty property)
        {
            if ((((NullValueHandling) property.NullValueHandling.GetValueOrDefault(base.Serializer.NullValueHandling)) == NullValueHandling.Ignore) && (memberValue == null))
            {
                return false;
            }
            if (this.HasFlag(property.DefaultValueHandling.GetValueOrDefault(base.Serializer.DefaultValueHandling), DefaultValueHandling.Ignore) && MiscellaneousUtils.ValueEquals(memberValue, property.GetResolvedDefaultValue()))
            {
                return false;
            }
            return true;
        }

        private bool ShouldWriteReference(object value, JsonProperty property, JsonContract valueContract, JsonContainerContract collectionContract, JsonProperty containerProperty)
        {
            if (value == null)
            {
                return false;
            }
            if ((valueContract.ContractType == JsonContractType.Primitive) || (valueContract.ContractType == JsonContractType.String))
            {
                return false;
            }
            bool? nullable = this.ResolveIsReference(valueContract, property, collectionContract, containerProperty);
            if (!nullable.HasValue)
            {
                if (valueContract.ContractType == JsonContractType.Array)
                {
                    nullable = new bool?(this.HasFlag(base.Serializer.PreserveReferencesHandling, PreserveReferencesHandling.Arrays));
                }
                else
                {
                    nullable = new bool?(this.HasFlag(base.Serializer.PreserveReferencesHandling, PreserveReferencesHandling.Objects));
                }
            }
            if (!nullable.Value)
            {
                return false;
            }
            return base.Serializer.ReferenceResolver.IsReferenced(this, value);
        }

        private bool ShouldWriteType(TypeNameHandling typeNameHandlingFlag, JsonContract contract, JsonProperty member, JsonContainerContract containerContract, JsonProperty containerProperty)
        {
            TypeNameHandling? nullable6;
            TypeNameHandling? nullable2 = (member != null) ? member.TypeNameHandling : null;
            if (!nullable2.HasValue)
            {
                TypeNameHandling? nullable4 = (containerProperty != null) ? containerProperty.ItemTypeNameHandling : null;
                if (!nullable4.HasValue)
                {
                    nullable6 = (containerContract != null) ? containerContract.ItemTypeNameHandling : null;
                }
            }
            TypeNameHandling handling = nullable6.HasValue ? nullable2.GetValueOrDefault() : base.Serializer.TypeNameHandling;
            if (this.HasFlag(handling, typeNameHandlingFlag))
            {
                return true;
            }
            if (this.HasFlag(handling, TypeNameHandling.Auto))
            {
                if (member != null)
                {
                    if (contract.UnderlyingType != member.PropertyContract.CreatedType)
                    {
                        return true;
                    }
                }
                else if (((containerContract != null) && (containerContract.ItemContract != null)) && (contract.UnderlyingType != containerContract.ItemContract.CreatedType))
                {
                    return true;
                }
            }
            return false;
        }

        internal static bool TryConvertToString(object value, Type type, out string s)
        {
            TypeConverter converter = ConvertUtils.GetConverter(type);
            if (((converter != null) && !(converter is ComponentConverter)) && ((converter.GetType() != typeof(TypeConverter)) && converter.CanConvertTo(typeof(string))))
            {
                s = converter.ConvertToInvariantString(value);
                return true;
            }
            if (value is Type)
            {
                s = ((Type) value).AssemblyQualifiedName;
                return true;
            }
            s = null;
            return false;
        }

        private void WriteObjectStart(JsonWriter writer, object value, JsonContract contract, JsonProperty member, JsonContainerContract collectionContract, JsonProperty containerProperty)
        {
            writer.WriteStartObject();
            bool? nullable = this.ResolveIsReference(contract, member, collectionContract, containerProperty);
            if (nullable.HasValue ? nullable.GetValueOrDefault() : this.HasFlag(base.Serializer.PreserveReferencesHandling, PreserveReferencesHandling.Objects))
            {
                this.WriteReferenceIdProperty(writer, contract.UnderlyingType, value);
            }
            if (this.ShouldWriteType(TypeNameHandling.Objects, contract, member, collectionContract, containerProperty))
            {
                this.WriteTypeProperty(writer, contract.UnderlyingType);
            }
        }

        private void WriteReference(JsonWriter writer, object value)
        {
            string reference = this.GetReference(writer, value);
            if ((base.TraceWriter != null) && (base.TraceWriter.LevelFilter >= TraceLevel.Info))
            {
                base.TraceWriter.Trace(TraceLevel.Info, JsonPosition.FormatMessage(null, writer.Path, "Writing object reference to Id '{0}' for {1}.".FormatWith(CultureInfo.InvariantCulture, reference, value.GetType())), null);
            }
            writer.WriteStartObject();
            writer.WritePropertyName("$ref");
            writer.WriteValue(reference);
            writer.WriteEndObject();
        }

        private void WriteReferenceIdProperty(JsonWriter writer, Type type, object value)
        {
            string reference = this.GetReference(writer, value);
            if ((base.TraceWriter != null) && (base.TraceWriter.LevelFilter >= TraceLevel.Verbose))
            {
                base.TraceWriter.Trace(TraceLevel.Verbose, JsonPosition.FormatMessage(null, writer.Path, "Writing object reference Id '{0}' for {1}.".FormatWith(CultureInfo.InvariantCulture, reference, type)), null);
            }
            writer.WritePropertyName("$id");
            writer.WriteValue(reference);
        }

        private bool WriteStartArray(JsonWriter writer, object values, JsonArrayContract contract, JsonProperty member, JsonContainerContract containerContract, JsonProperty containerProperty)
        {
            bool? nullable = this.ResolveIsReference(contract, member, containerContract, containerProperty);
            bool flag = nullable.HasValue ? nullable.GetValueOrDefault() : this.HasFlag(base.Serializer.PreserveReferencesHandling, PreserveReferencesHandling.Arrays);
            bool flag2 = this.ShouldWriteType(TypeNameHandling.Arrays, contract, member, containerContract, containerProperty);
            bool flag3 = flag || flag2;
            if (flag3)
            {
                writer.WriteStartObject();
                if (flag)
                {
                    this.WriteReferenceIdProperty(writer, contract.UnderlyingType, values);
                }
                if (flag2)
                {
                    this.WriteTypeProperty(writer, values.GetType());
                }
                writer.WritePropertyName("$values");
            }
            if (contract.ItemContract == null)
            {
                contract.ItemContract = base.Serializer.ContractResolver.ResolveContract(contract.CollectionItemType ?? typeof(object));
            }
            return flag3;
        }

        private void WriteTypeProperty(JsonWriter writer, Type type)
        {
            string str = ReflectionUtils.GetTypeName(type, base.Serializer.TypeNameAssemblyFormat, base.Serializer.Binder);
            if ((base.TraceWriter != null) && (base.TraceWriter.LevelFilter >= TraceLevel.Verbose))
            {
                base.TraceWriter.Trace(TraceLevel.Verbose, JsonPosition.FormatMessage(null, writer.Path, "Writing type name '{0}' for {1}.".FormatWith(CultureInfo.InvariantCulture, str, type)), null);
            }
            writer.WritePropertyName("$type");
            writer.WriteValue(str);
        }
    }
}

