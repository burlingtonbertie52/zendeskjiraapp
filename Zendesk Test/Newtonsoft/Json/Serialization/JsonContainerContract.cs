﻿namespace Newtonsoft.Json.Serialization
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Utilities;
    using System;
    using System.Runtime.CompilerServices;

    public class JsonContainerContract : JsonContract
    {
        private JsonContract _finalItemContract;
        private JsonContract _itemContract;

        internal JsonContainerContract(Type underlyingType) : base(underlyingType)
        {
            JsonContainerAttribute jsonContainerAttribute = JsonTypeReflector.GetJsonContainerAttribute(underlyingType);
            if (jsonContainerAttribute != null)
            {
                if (jsonContainerAttribute.ItemConverterType != null)
                {
                    this.ItemConverter = JsonConverterAttribute.CreateJsonConverterInstance(jsonContainerAttribute.ItemConverterType);
                }
                this.ItemIsReference = jsonContainerAttribute._itemIsReference;
                this.ItemReferenceLoopHandling = jsonContainerAttribute._itemReferenceLoopHandling;
                this.ItemTypeNameHandling = jsonContainerAttribute._itemTypeNameHandling;
            }
        }

        internal JsonContract FinalItemContract
        {
            get
            {
                return this._finalItemContract;
            }
        }

        internal JsonContract ItemContract
        {
            get
            {
                return this._itemContract;
            }
            set
            {
                this._itemContract = value;
                if (this._itemContract != null)
                {
                    this._finalItemContract = this._itemContract.UnderlyingType.IsSealed() ? this._itemContract : null;
                }
                else
                {
                    this._finalItemContract = null;
                }
            }
        }

        public JsonConverter ItemConverter { get; set; }

        public bool? ItemIsReference { get; set; }

        public ReferenceLoopHandling? ItemReferenceLoopHandling { get; set; }

        public TypeNameHandling? ItemTypeNameHandling { get; set; }
    }
}

