﻿namespace Newtonsoft.Json.Serialization
{
    using System;
    using System.Runtime.CompilerServices;

    public class JsonDynamicContract : JsonContainerContract
    {
        public JsonDynamicContract(Type underlyingType) : base(underlyingType)
        {
            base.ContractType = JsonContractType.Dynamic;
            this.Properties = new JsonPropertyCollection(base.UnderlyingType);
        }

        public JsonPropertyCollection Properties { get; private set; }

        public Func<string, string> PropertyNameResolver { get; set; }
    }
}

