﻿namespace Newtonsoft.Json.Serialization
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Utilities;
    using System;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.Serialization;

    public abstract class JsonContract
    {
        internal JsonContractType ContractType;
        internal ReadType InternalReadType;
        internal bool IsConvertable;
        internal bool IsNullable;
        internal Type NonNullableUnderlyingType;

        internal JsonContract(Type underlyingType)
        {
            ValidationUtils.ArgumentNotNull(underlyingType, "underlyingType");
            this.UnderlyingType = underlyingType;
            this.IsNullable = ReflectionUtils.IsNullable(underlyingType);
            this.NonNullableUnderlyingType = (this.IsNullable && ReflectionUtils.IsNullableType(underlyingType)) ? Nullable.GetUnderlyingType(underlyingType) : underlyingType;
            this.CreatedType = this.NonNullableUnderlyingType;
            this.IsConvertable = ConvertUtils.IsConvertible(this.NonNullableUnderlyingType);
            if (this.NonNullableUnderlyingType == typeof(byte[]))
            {
                this.InternalReadType = ReadType.ReadAsBytes;
            }
            else if (this.NonNullableUnderlyingType == typeof(int))
            {
                this.InternalReadType = ReadType.ReadAsInt32;
            }
            else if (this.NonNullableUnderlyingType == typeof(decimal))
            {
                this.InternalReadType = ReadType.ReadAsDecimal;
            }
            else if (this.NonNullableUnderlyingType == typeof(string))
            {
                this.InternalReadType = ReadType.ReadAsString;
            }
            else if (this.NonNullableUnderlyingType == typeof(DateTime))
            {
                this.InternalReadType = ReadType.ReadAsDateTime;
            }
            else if (this.NonNullableUnderlyingType == typeof(DateTimeOffset))
            {
                this.InternalReadType = ReadType.ReadAsDateTimeOffset;
            }
            else
            {
                this.InternalReadType = ReadType.Read;
            }
        }

        internal void InvokeOnDeserialized(object o, StreamingContext context)
        {
            if (this.OnDeserialized != null)
            {
                this.OnDeserialized.Invoke(o, new object[] { context });
            }
        }

        internal void InvokeOnDeserializing(object o, StreamingContext context)
        {
            if (this.OnDeserializing != null)
            {
                this.OnDeserializing.Invoke(o, new object[] { context });
            }
        }

        internal void InvokeOnError(object o, StreamingContext context, ErrorContext errorContext)
        {
            if (this.OnError != null)
            {
                this.OnError.Invoke(o, new object[] { context, errorContext });
            }
        }

        internal void InvokeOnSerialized(object o, StreamingContext context)
        {
            if (this.OnSerialized != null)
            {
                this.OnSerialized.Invoke(o, new object[] { context });
            }
        }

        internal void InvokeOnSerializing(object o, StreamingContext context)
        {
            if (this.OnSerializing != null)
            {
                this.OnSerializing.Invoke(o, new object[] { context });
            }
        }

        public JsonConverter Converter { get; set; }

        public Type CreatedType { get; set; }

        public Func<object> DefaultCreator { get; set; }

        public bool DefaultCreatorNonPublic { get; set; }

        internal JsonConverter InternalConverter { get; set; }

        public bool? IsReference { get; set; }

        public MethodInfo OnDeserialized { get; set; }

        public MethodInfo OnDeserializing { get; set; }

        public MethodInfo OnError { get; set; }

        public MethodInfo OnSerialized { get; set; }

        public MethodInfo OnSerializing { get; set; }

        public Type UnderlyingType { get; private set; }
    }
}

