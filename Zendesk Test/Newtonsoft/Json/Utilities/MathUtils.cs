﻿namespace Newtonsoft.Json.Utilities
{
    using System;

    internal class MathUtils
    {
        public static bool ApproxEquals(double d1, double d2)
        {
            if (d1 == d2)
            {
                return true;
            }
            double num = ((Math.Abs(d1) + Math.Abs(d2)) + 10.0) * 2.2204460492503131E-16;
            double num2 = d1 - d2;
            return ((-num < num2) && (num > num2));
        }

        public static int IntLength(int i)
        {
            if (i < 0)
            {
                throw new ArgumentOutOfRangeException();
            }
            if (i == 0)
            {
                return 1;
            }
            return (((int) Math.Floor(Math.Log10((double) i))) + 1);
        }

        public static char IntToHex(int n)
        {
            if (n <= 9)
            {
                return (char) (n + 0x30);
            }
            return (char) ((n - 10) + 0x61);
        }

        public static double? Max(double? val1, double? val2)
        {
            if (!val1.HasValue)
            {
                return val2;
            }
            if (!val2.HasValue)
            {
                return val1;
            }
            return new double?(Math.Max(val1.Value, val2.Value));
        }

        public static int? Max(int? val1, int? val2)
        {
            if (!val1.HasValue)
            {
                return val2;
            }
            if (!val2.HasValue)
            {
                return val1;
            }
            return new int?(Math.Max(val1.Value, val2.Value));
        }

        public static int? Min(int? val1, int? val2)
        {
            if (!val1.HasValue)
            {
                return val2;
            }
            if (!val2.HasValue)
            {
                return val1;
            }
            return new int?(Math.Min(val1.Value, val2.Value));
        }
    }
}

