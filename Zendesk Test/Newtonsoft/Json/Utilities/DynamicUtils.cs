﻿namespace Newtonsoft.Json.Utilities
{
    using Newtonsoft.Json.Serialization;
    using System;
    using System.Collections.Generic;
    using System.Dynamic;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    internal static class DynamicUtils
    {
        public static IEnumerable<string> GetDynamicMemberNames(this IDynamicMetaObjectProvider dynamicProvider)
        {
            return dynamicProvider.GetMetaObject(Expression.Constant(dynamicProvider)).GetDynamicMemberNames();
        }

        public static bool TryGetMember(this IDynamicMetaObjectProvider dynamicProvider, string name, out object value)
        {
            ValidationUtils.ArgumentNotNull(dynamicProvider, "dynamicProvider");
            GetMemberBinder member = (GetMemberBinder) BinderWrapper.GetMember(name, typeof(DynamicUtils));
            CallSite<Func<CallSite, object, object>> site = CallSite<Func<CallSite, object, object>>.Create(new NoThrowGetBinderMember(member));
            object objA = site.Target(site, dynamicProvider);
            if (!object.ReferenceEquals(objA, NoThrowExpressionVisitor.ErrorResult))
            {
                value = objA;
                return true;
            }
            value = null;
            return false;
        }

        public static bool TrySetMember(this IDynamicMetaObjectProvider dynamicProvider, string name, object value)
        {
            ValidationUtils.ArgumentNotNull(dynamicProvider, "dynamicProvider");
            SetMemberBinder innerBinder = (SetMemberBinder) BinderWrapper.SetMember(name, typeof(DynamicUtils));
            CallSite<Func<CallSite, object, object, object>> site = CallSite<Func<CallSite, object, object, object>>.Create(new NoThrowSetBinderMember(innerBinder));
            return !object.ReferenceEquals(site.Target(site, dynamicProvider, value), NoThrowExpressionVisitor.ErrorResult);
        }

        internal static class BinderWrapper
        {
            private static object _getCSharpArgumentInfoArray;
            private static MethodCall<object, object> _getMemberCall;
            private static bool _init;
            private static object _setCSharpArgumentInfoArray;
            private static MethodCall<object, object> _setMemberCall;
            private const string BinderTypeName = "Microsoft.CSharp.RuntimeBinder.Binder, Microsoft.CSharp, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";
            private const string CSharpArgumentInfoFlagsTypeName = "Microsoft.CSharp.RuntimeBinder.CSharpArgumentInfoFlags, Microsoft.CSharp, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";
            private const string CSharpArgumentInfoTypeName = "Microsoft.CSharp.RuntimeBinder.CSharpArgumentInfo, Microsoft.CSharp, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";
            public const string CSharpAssemblyName = "Microsoft.CSharp, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";
            private const string CSharpBinderFlagsTypeName = "Microsoft.CSharp.RuntimeBinder.CSharpBinderFlags, Microsoft.CSharp, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";

            private static void CreateMemberCalls()
            {
                Type type = Type.GetType("Microsoft.CSharp.RuntimeBinder.CSharpArgumentInfo, Microsoft.CSharp, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a");
                Type type2 = Type.GetType("Microsoft.CSharp.RuntimeBinder.CSharpBinderFlags, Microsoft.CSharp, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a");
                Type type3 = Type.GetType("Microsoft.CSharp.RuntimeBinder.Binder, Microsoft.CSharp, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a");
                Type type4 = typeof(IEnumerable<>).MakeGenericType(new Type[] { type });
                MethodInfo method = type3.GetMethod("GetMember", BindingFlags.Public | BindingFlags.Static, null, new Type[] { type2, typeof(string), typeof(Type), type4 }, null);
                _getMemberCall = JsonTypeReflector.ReflectionDelegateFactory.CreateMethodCall<object>(method);
                MethodInfo info2 = type3.GetMethod("SetMember", BindingFlags.Public | BindingFlags.Static, null, new Type[] { type2, typeof(string), typeof(Type), type4 }, null);
                _setMemberCall = JsonTypeReflector.ReflectionDelegateFactory.CreateMethodCall<object>(info2);
            }

            private static object CreateSharpArgumentInfoArray(params int[] values)
            {
                Type elementType = Type.GetType("Microsoft.CSharp.RuntimeBinder.CSharpArgumentInfo, Microsoft.CSharp, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a");
                Type type = Type.GetType("Microsoft.CSharp.RuntimeBinder.CSharpArgumentInfoFlags, Microsoft.CSharp, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a");
                Array array = Array.CreateInstance(elementType, values.Length);
                for (int i = 0; i < values.Length; i++)
                {
                    object[] parameters = new object[2];
                    parameters[0] = 0;
                    object obj2 = elementType.GetMethod("Create", BindingFlags.Public | BindingFlags.Static, null, new Type[] { type, typeof(string) }, null).Invoke(null, parameters);
                    array.SetValue(obj2, i);
                }
                return array;
            }

            public static CallSiteBinder GetMember(string name, Type context)
            {
                Init();
                return (CallSiteBinder) _getMemberCall(null, new object[] { 0, name, context, _getCSharpArgumentInfoArray });
            }

            private static void Init()
            {
                if (!_init)
                {
                    if (Type.GetType("Microsoft.CSharp.RuntimeBinder.Binder, Microsoft.CSharp, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", false) == null)
                    {
                        throw new InvalidOperationException("Could not resolve type '{0}'. You may need to add a reference to Microsoft.CSharp.dll to work with dynamic types.".FormatWith(CultureInfo.InvariantCulture, "Microsoft.CSharp.RuntimeBinder.Binder, Microsoft.CSharp, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"));
                    }
                    _getCSharpArgumentInfoArray = CreateSharpArgumentInfoArray(new int[1]);
                    int[] values = new int[2];
                    values[1] = 3;
                    _setCSharpArgumentInfoArray = CreateSharpArgumentInfoArray(values);
                    CreateMemberCalls();
                    _init = true;
                }
            }

            public static CallSiteBinder SetMember(string name, Type context)
            {
                Init();
                return (CallSiteBinder) _setMemberCall(null, new object[] { 0, name, context, _setCSharpArgumentInfoArray });
            }
        }

        internal class NoThrowExpressionVisitor : ExpressionVisitor
        {
            internal static readonly object ErrorResult = new object();

            protected override Expression VisitConditional(ConditionalExpression node)
            {
                if (node.IfFalse.NodeType == ExpressionType.Throw)
                {
                    return Expression.Condition(node.Test, node.IfTrue, Expression.Constant(ErrorResult));
                }
                return base.VisitConditional(node);
            }
        }

        internal class NoThrowGetBinderMember : GetMemberBinder
        {
            private readonly GetMemberBinder _innerBinder;

            public NoThrowGetBinderMember(GetMemberBinder innerBinder) : base(innerBinder.Name, innerBinder.IgnoreCase)
            {
                this._innerBinder = innerBinder;
            }

            public override DynamicMetaObject FallbackGetMember(DynamicMetaObject target, DynamicMetaObject errorSuggestion)
            {
                DynamicMetaObject obj2 = this._innerBinder.Bind(target, new DynamicMetaObject[0]);
                DynamicUtils.NoThrowExpressionVisitor visitor = new DynamicUtils.NoThrowExpressionVisitor();
                return new DynamicMetaObject(visitor.Visit(obj2.Expression), obj2.Restrictions);
            }
        }

        internal class NoThrowSetBinderMember : SetMemberBinder
        {
            private readonly SetMemberBinder _innerBinder;

            public NoThrowSetBinderMember(SetMemberBinder innerBinder) : base(innerBinder.Name, innerBinder.IgnoreCase)
            {
                this._innerBinder = innerBinder;
            }

            public override DynamicMetaObject FallbackSetMember(DynamicMetaObject target, DynamicMetaObject value, DynamicMetaObject errorSuggestion)
            {
                DynamicMetaObject obj2 = this._innerBinder.Bind(target, new DynamicMetaObject[] { value });
                DynamicUtils.NoThrowExpressionVisitor visitor = new DynamicUtils.NoThrowExpressionVisitor();
                return new DynamicMetaObject(visitor.Visit(obj2.Expression), obj2.Restrictions);
            }
        }
    }
}

