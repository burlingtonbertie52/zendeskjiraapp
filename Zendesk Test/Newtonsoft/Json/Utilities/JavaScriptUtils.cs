﻿namespace Newtonsoft.Json.Utilities
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    internal static class JavaScriptUtils
    {
        internal static readonly bool[] DoubleQuoteCharEscapeFlags = new bool[0x80];
        private const string EscapedUnicodeText = "!";
        internal static readonly bool[] HtmlCharEscapeFlags = new bool[0x80];
        internal static readonly bool[] SingleQuoteCharEscapeFlags = new bool[0x80];

        static JavaScriptUtils()
        {
            IList<char> first = new List<char> { 10, 13, 9, 0x5c, 12, 8 };
            for (int i = 0; i < 0x20; i++)
            {
                first.Add((char) ((ushort) i));
            }
            foreach (char ch in first.Union<char>(new char[] { '\'' }))
            {
                SingleQuoteCharEscapeFlags[ch] = true;
            }
            foreach (char ch2 in first.Union<char>(new char[] { '"' }))
            {
                DoubleQuoteCharEscapeFlags[ch2] = true;
            }
            foreach (char ch3 in first.Union<char>(new char[] { '"', '\'', '<', '>', '&' }))
            {
                HtmlCharEscapeFlags[ch3] = true;
            }
        }

        public static string ToEscapedJavaScriptString(string value)
        {
            return ToEscapedJavaScriptString(value, '"', true);
        }

        public static string ToEscapedJavaScriptString(string value, char delimiter, bool appendDelimiters)
        {
            int? length = StringUtils.GetLength(value);
            using (StringWriter writer = StringUtils.CreateStringWriter(length.HasValue ? length.GetValueOrDefault() : 0x10))
            {
                WriteEscapedJavaScriptString(writer, value, delimiter, appendDelimiters, (delimiter == '"') ? DoubleQuoteCharEscapeFlags : SingleQuoteCharEscapeFlags, StringEscapeHandling.Default);
                return writer.ToString();
            }
        }

        public static void WriteEscapedJavaScriptString(TextWriter writer, string s, char delimiter, bool appendDelimiters, bool[] charEscapeFlags, StringEscapeHandling stringEscapeHandling)
        {
            if (appendDelimiters)
            {
                writer.Write(delimiter);
            }
            if (s != null)
            {
                char[] buffer = null;
                char[] chArray2 = null;
                int index = 0;
                for (int i = 0; i < s.Length; i++)
                {
                    char ch = s[i];
                    if ((ch >= charEscapeFlags.Length) || charEscapeFlags[ch])
                    {
                        string str;
                        switch (ch)
                        {
                            case '\u2028':
                                str = @"\u2028";
                                break;

                            case '\u2029':
                                str = @"\u2029";
                                break;

                            case '\x0085':
                                str = @"\u0085";
                                break;

                            case '\b':
                                str = @"\b";
                                break;

                            case '\t':
                                str = @"\t";
                                break;

                            case '\n':
                                str = @"\n";
                                break;

                            case '\f':
                                str = @"\f";
                                break;

                            case '\r':
                                str = @"\r";
                                break;

                            case '\\':
                                str = @"\\";
                                break;

                            default:
                                if ((ch < charEscapeFlags.Length) || (stringEscapeHandling == StringEscapeHandling.EscapeNonAscii))
                                {
                                    if ((ch == '\'') && (stringEscapeHandling != StringEscapeHandling.EscapeHtml))
                                    {
                                        str = @"\'";
                                    }
                                    else if ((ch == '"') && (stringEscapeHandling != StringEscapeHandling.EscapeHtml))
                                    {
                                        str = "\\\"";
                                    }
                                    else
                                    {
                                        if (chArray2 == null)
                                        {
                                            chArray2 = new char[6];
                                        }
                                        StringUtils.ToCharAsUnicode(ch, chArray2);
                                        str = "!";
                                    }
                                }
                                else
                                {
                                    str = null;
                                }
                                break;
                        }
                        if (str != null)
                        {
                            if (i > index)
                            {
                                if (buffer == null)
                                {
                                    buffer = s.ToCharArray();
                                }
                                writer.Write(buffer, index, i - index);
                            }
                            index = i + 1;
                            if (!string.Equals(str, "!"))
                            {
                                writer.Write(str);
                            }
                            else
                            {
                                writer.Write(chArray2);
                            }
                        }
                    }
                }
                if (index == 0)
                {
                    writer.Write(s);
                }
                else
                {
                    if (buffer == null)
                    {
                        buffer = s.ToCharArray();
                    }
                    writer.Write(buffer, index, s.Length - index);
                }
            }
            if (appendDelimiters)
            {
                writer.Write(delimiter);
            }
        }
    }
}

