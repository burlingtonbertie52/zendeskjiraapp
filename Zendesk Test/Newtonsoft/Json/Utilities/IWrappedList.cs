﻿namespace Newtonsoft.Json.Utilities
{
    using System;
    using System.Collections;

    internal interface IWrappedList : IEnumerable, IList, ICollection
    {
        object UnderlyingList { get; }
    }
}

