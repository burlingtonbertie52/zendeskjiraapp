﻿namespace Newtonsoft.Json.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;

    internal static class EnumUtils
    {
        public static IList<T> GetFlagsValues<T>(T value) where T: struct
        {
            Type type = typeof(T);
            if (!type.IsDefined(typeof(FlagsAttribute), false))
            {
                throw new ArgumentException("Enum type {0} is not a set of flags.".FormatWith(CultureInfo.InvariantCulture, type));
            }
            Type underlyingType = Enum.GetUnderlyingType(value.GetType());
            ulong num = Convert.ToUInt64(value, CultureInfo.InvariantCulture);
            EnumValues<ulong> namesAndValues = GetNamesAndValues<T>();
            IList<T> list = new List<T>();
            foreach (EnumValue<ulong> value2 in namesAndValues)
            {
                if (((num & value2.Value) == value2.Value) && (value2.Value != 0L))
                {
                    list.Add((T) Convert.ChangeType(value2.Value, underlyingType, CultureInfo.CurrentCulture));
                }
            }
            if ((list.Count == 0) && (namesAndValues.SingleOrDefault<EnumValue<ulong>>(v => (v.Value == 0L)) != null))
            {
                list.Add(default(T));
            }
            return list;
        }

        public static IList<string> GetNames(Type enumType)
        {
            if (!enumType.IsEnum())
            {
                throw new ArgumentException("Type '" + enumType.Name + "' is not an enum.");
            }
            List<string> list = new List<string>();
            foreach (FieldInfo info in from field in enumType.GetFields()
                where field.IsLiteral
                select field)
            {
                list.Add(info.Name);
            }
            return list;
        }

        public static EnumValues<ulong> GetNamesAndValues<T>() where T: struct
        {
            return GetNamesAndValues<ulong>(typeof(T));
        }

        public static EnumValues<TUnderlyingType> GetNamesAndValues<TUnderlyingType>(Type enumType) where TUnderlyingType: struct
        {
            if (enumType == null)
            {
                throw new ArgumentNullException("enumType");
            }
            ValidationUtils.ArgumentTypeIsEnum(enumType, "enumType");
            IList<object> list = GetValues(enumType);
            IList<string> names = GetNames(enumType);
            EnumValues<TUnderlyingType> values = new EnumValues<TUnderlyingType>();
            for (int i = 0; i < list.Count; i++)
            {
                try
                {
                    values.Add(new EnumValue<TUnderlyingType>(names[i], (TUnderlyingType) Convert.ChangeType(list[i], typeof(TUnderlyingType), CultureInfo.CurrentCulture)));
                }
                catch (OverflowException exception)
                {
                    throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, "Value from enum with the underlying type of {0} cannot be added to dictionary with a value type of {1}. Value was too large: {2}", new object[] { Enum.GetUnderlyingType(enumType), typeof(TUnderlyingType), Convert.ToUInt64(list[i], CultureInfo.InvariantCulture) }), exception);
                }
            }
            return values;
        }

        public static IList<object> GetValues(Type enumType)
        {
            if (!enumType.IsEnum())
            {
                throw new ArgumentException("Type '" + enumType.Name + "' is not an enum.");
            }
            List<object> list = new List<object>();
            foreach (FieldInfo info in from field in enumType.GetFields()
                where field.IsLiteral
                select field)
            {
                object item = info.GetValue(enumType);
                list.Add(item);
            }
            return list;
        }
    }
}

