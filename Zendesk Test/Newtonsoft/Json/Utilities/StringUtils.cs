﻿namespace Newtonsoft.Json.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Text;

    internal static class StringUtils
    {
        public const char CarriageReturn = '\r';
        public const string CarriageReturnLineFeed = "\r\n";
        public const string Empty = "";
        public const char LineFeed = '\n';
        public const char Tab = '\t';

        public static StringWriter CreateStringWriter(int capacity)
        {
            return new StringWriter(new StringBuilder(capacity), CultureInfo.InvariantCulture);
        }

        public static TSource ForgivingCaseSensitiveFind<TSource>(this IEnumerable<TSource> source, Func<TSource, string> valueSelector, string testValue)
        {
            Func<TSource, bool> predicate = null;
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }
            if (valueSelector == null)
            {
                throw new ArgumentNullException("valueSelector");
            }
            IEnumerable<TSource> enumerable = from s in source
                where string.Equals(valueSelector(s), testValue, StringComparison.OrdinalIgnoreCase)
                select s;
            if (enumerable.Count<TSource>() <= 1)
            {
                return enumerable.SingleOrDefault<TSource>();
            }
            if (predicate == null)
            {
                predicate = s => string.Equals(valueSelector(s), testValue, StringComparison.Ordinal);
            }
            return source.Where<TSource>(predicate).SingleOrDefault<TSource>();
        }

        public static string FormatWith(this string format, IFormatProvider provider, object arg0)
        {
            return format.FormatWith(provider, new object[] { arg0 });
        }

        public static string FormatWith(this string format, IFormatProvider provider, params object[] args)
        {
            ValidationUtils.ArgumentNotNull(format, "format");
            return string.Format(provider, format, args);
        }

        public static string FormatWith(this string format, IFormatProvider provider, object arg0, object arg1)
        {
            return format.FormatWith(provider, new object[] { arg0, arg1 });
        }

        public static string FormatWith(this string format, IFormatProvider provider, object arg0, object arg1, object arg2)
        {
            return format.FormatWith(provider, new object[] { arg0, arg1, arg2 });
        }

        public static int? GetLength(string value)
        {
            if (value == null)
            {
                return null;
            }
            return new int?(value.Length);
        }

        public static bool IsHighSurrogate(char c)
        {
            return char.IsHighSurrogate(c);
        }

        public static bool IsLowSurrogate(char c)
        {
            return char.IsLowSurrogate(c);
        }

        public static bool IsWhiteSpace(string s)
        {
            if (s == null)
            {
                throw new ArgumentNullException("s");
            }
            if (s.Length == 0)
            {
                return false;
            }
            for (int i = 0; i < s.Length; i++)
            {
                if (!char.IsWhiteSpace(s[i]))
                {
                    return false;
                }
            }
            return true;
        }

        public static string NullEmptyString(string s)
        {
            if (!string.IsNullOrEmpty(s))
            {
                return s;
            }
            return null;
        }

        public static string ToCamelCase(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return s;
            }
            if (!char.IsUpper(s[0]))
            {
                return s;
            }
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < s.Length; i++)
            {
                bool flag = (i + 1) < s.Length;
                if (((i == 0) || !flag) || char.IsUpper(s[i + 1]))
                {
                    char ch = char.ToLower(s[i], CultureInfo.InvariantCulture);
                    builder.Append(ch);
                }
                else
                {
                    builder.Append(s.Substring(i));
                    break;
                }
            }
            return builder.ToString();
        }

        public static void ToCharAsUnicode(char c, char[] buffer)
        {
            buffer[0] = '\\';
            buffer[1] = 'u';
            buffer[2] = MathUtils.IntToHex((c >> 12) & '\x000f');
            buffer[3] = MathUtils.IntToHex((c >> 8) & '\x000f');
            buffer[4] = MathUtils.IntToHex((c >> 4) & '\x000f');
            buffer[5] = MathUtils.IntToHex(c & '\x000f');
        }
    }
}

