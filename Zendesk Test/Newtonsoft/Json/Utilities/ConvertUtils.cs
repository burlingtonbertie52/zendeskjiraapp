﻿namespace Newtonsoft.Json.Utilities
{
    using Newtonsoft.Json.Serialization;
    using System;
    using System.ComponentModel;
    using System.Data.SqlTypes;
    using System.Globalization;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    internal static class ConvertUtils
    {
        private static readonly ThreadSafeStore<TypeConvertKey, Func<object, object>> CastConverters = new ThreadSafeStore<TypeConvertKey, Func<object, object>>(new Func<TypeConvertKey, Func<object, object>>(ConvertUtils.CreateCastConverter));

        public static object Convert(object initialValue, CultureInfo culture, Type targetType)
        {
            if (initialValue == null)
            {
                throw new ArgumentNullException("initialValue");
            }
            if (ReflectionUtils.IsNullableType(targetType))
            {
                targetType = Nullable.GetUnderlyingType(targetType);
            }
            Type t = initialValue.GetType();
            if (targetType == t)
            {
                return initialValue;
            }
            if (IsConvertible(initialValue) && IsConvertible(targetType))
            {
                if (targetType.IsEnum())
                {
                    if (initialValue is string)
                    {
                        return Enum.Parse(targetType, initialValue.ToString(), true);
                    }
                    if (IsInteger(initialValue))
                    {
                        return Enum.ToObject(targetType, initialValue);
                    }
                }
                return System.Convert.ChangeType(initialValue, targetType, culture);
            }
            if ((initialValue is string) && typeof(Type).IsAssignableFrom(targetType))
            {
                return Type.GetType((string) initialValue, true);
            }
            if ((targetType.IsInterface() || targetType.IsGenericTypeDefinition()) || targetType.IsAbstract())
            {
                throw new ArgumentException("Target type {0} is not a value type or a non-abstract class.".FormatWith(CultureInfo.InvariantCulture, targetType), "targetType");
            }
            if ((initialValue is DateTime) && (targetType == typeof(DateTimeOffset)))
            {
                return new DateTimeOffset((DateTime) initialValue);
            }
            if (initialValue is string)
            {
                if (targetType == typeof(Guid))
                {
                    return new Guid((string) initialValue);
                }
                if (targetType == typeof(Uri))
                {
                    return new Uri((string) initialValue, UriKind.RelativeOrAbsolute);
                }
                if (targetType == typeof(TimeSpan))
                {
                    return ParseTimeSpan((string) initialValue);
                }
            }
            TypeConverter converter = GetConverter(t);
            if ((converter != null) && converter.CanConvertTo(targetType))
            {
                return converter.ConvertTo(null, culture, initialValue, targetType);
            }
            TypeConverter converter2 = GetConverter(targetType);
            if ((converter2 != null) && converter2.CanConvertFrom(t))
            {
                return converter2.ConvertFrom(null, culture, initialValue);
            }
            if (initialValue == DBNull.Value)
            {
                if (!ReflectionUtils.IsNullable(targetType))
                {
                    throw new Exception("Can not convert null {0} into non-nullable {1}.".FormatWith(CultureInfo.InvariantCulture, t, targetType));
                }
                return EnsureTypeAssignable(null, t, targetType);
            }
            if (!(initialValue is INullable))
            {
                throw new InvalidOperationException("Can not convert from {0} to {1}.".FormatWith(CultureInfo.InvariantCulture, t, targetType));
            }
            return EnsureTypeAssignable(ToValue((INullable) initialValue), t, targetType);
        }

        public static object ConvertOrCast(object initialValue, CultureInfo culture, Type targetType)
        {
            object obj2;
            if (targetType == typeof(object))
            {
                return initialValue;
            }
            if ((initialValue == null) && ReflectionUtils.IsNullable(targetType))
            {
                return null;
            }
            if (TryConvert(initialValue, culture, targetType, out obj2))
            {
                return obj2;
            }
            return EnsureTypeAssignable(initialValue, ReflectionUtils.GetObjectType(initialValue), targetType);
        }

        private static Func<object, object> CreateCastConverter(TypeConvertKey t)
        {
            MethodInfo method = t.TargetType.GetMethod("op_Implicit", new Type[] { t.InitialType });
            if (method == null)
            {
                method = t.TargetType.GetMethod("op_Explicit", new Type[] { t.InitialType });
            }
            if (method == null)
            {
                return null;
            }
            MethodCall<object, object> call = JsonTypeReflector.ReflectionDelegateFactory.CreateMethodCall<object>(method);
            return o => call(null, new object[] { o });
        }

        private static object EnsureTypeAssignable(object value, Type initialType, Type targetType)
        {
            Type c = (value != null) ? value.GetType() : null;
            if (value != null)
            {
                if (targetType.IsAssignableFrom(c))
                {
                    return value;
                }
                Func<object, object> func = CastConverters.Get(new TypeConvertKey(c, targetType));
                if (func != null)
                {
                    return func(value);
                }
            }
            else if (ReflectionUtils.IsNullable(targetType))
            {
                return null;
            }
            throw new ArgumentException("Could not cast or convert from {0} to {1}.".FormatWith(CultureInfo.InvariantCulture, (initialType != null) ? initialType.ToString() : "{null}", targetType));
        }

        internal static TypeConverter GetConverter(Type t)
        {
            return JsonTypeReflector.GetTypeConverter(t);
        }

        public static TypeCode GetTypeCode(this IConvertible convertible)
        {
            return convertible.GetTypeCode();
        }

        public static TypeCode GetTypeCode(object o)
        {
            return System.Convert.GetTypeCode(o);
        }

        public static TypeCode GetTypeCode(Type t)
        {
            return Type.GetTypeCode(t);
        }

        public static bool IsConvertible(object o)
        {
            return (o is IConvertible);
        }

        public static bool IsConvertible(Type t)
        {
            return typeof(IConvertible).IsAssignableFrom(t);
        }

        public static bool IsInteger(object value)
        {
            switch (GetTypeCode(value))
            {
                case TypeCode.SByte:
                case TypeCode.Byte:
                case TypeCode.Int16:
                case TypeCode.UInt16:
                case TypeCode.Int32:
                case TypeCode.UInt32:
                case TypeCode.Int64:
                case TypeCode.UInt64:
                    return true;
            }
            return false;
        }

        public static TimeSpan ParseTimeSpan(string input)
        {
            return TimeSpan.Parse(input, CultureInfo.InvariantCulture);
        }

        public static IConvertible ToConvertible(object o)
        {
            return (o as IConvertible);
        }

        public static object ToValue(INullable nullableValue)
        {
            if (nullableValue == null)
            {
                return null;
            }
            if (nullableValue is SqlInt32)
            {
                return ToValue((SqlInt32) nullableValue);
            }
            if (nullableValue is SqlInt64)
            {
                return ToValue((SqlInt64) nullableValue);
            }
            if (nullableValue is SqlBoolean)
            {
                return ToValue((SqlBoolean) nullableValue);
            }
            if (nullableValue is SqlString)
            {
                return ToValue((SqlString) nullableValue);
            }
            if (!(nullableValue is SqlDateTime))
            {
                throw new ArgumentException("Unsupported INullable type: {0}".FormatWith(CultureInfo.InvariantCulture, nullableValue.GetType()));
            }
            return ToValue((SqlDateTime) nullableValue);
        }

        public static bool TryConvert(object initialValue, CultureInfo culture, Type targetType, out object convertedValue)
        {
            try
            {
                convertedValue = Convert(initialValue, culture, targetType);
                return true;
            }
            catch
            {
                convertedValue = null;
                return false;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct TypeConvertKey : IEquatable<ConvertUtils.TypeConvertKey>
        {
            private readonly Type _initialType;
            private readonly Type _targetType;
            public Type InitialType
            {
                get
                {
                    return this._initialType;
                }
            }
            public Type TargetType
            {
                get
                {
                    return this._targetType;
                }
            }
            public TypeConvertKey(Type initialType, Type targetType)
            {
                this._initialType = initialType;
                this._targetType = targetType;
            }

            public override int GetHashCode()
            {
                return (this._initialType.GetHashCode() ^ this._targetType.GetHashCode());
            }

            public override bool Equals(object obj)
            {
                return ((obj is ConvertUtils.TypeConvertKey) && this.Equals((ConvertUtils.TypeConvertKey) obj));
            }

            public bool Equals(ConvertUtils.TypeConvertKey other)
            {
                return ((this._initialType == other._initialType) && (this._targetType == other._targetType));
            }
        }
    }
}

