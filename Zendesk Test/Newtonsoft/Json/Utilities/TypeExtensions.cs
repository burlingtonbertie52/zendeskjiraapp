﻿namespace Newtonsoft.Json.Utilities
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Threading;

    internal static class TypeExtensions
    {
        public static bool AssignableToTypeName(this Type type, string fullTypeName)
        {
            Type type2;
            return type.AssignableToTypeName(fullTypeName, out type2);
        }

        public static bool AssignableToTypeName(this Type type, string fullTypeName, out Type match)
        {
            for (Type type2 = type; type2 != null; type2 = type2.BaseType())
            {
                if (string.Equals(type2.FullName, fullTypeName, StringComparison.Ordinal))
                {
                    match = type2;
                    return true;
                }
            }
            foreach (Type type3 in type.GetInterfaces())
            {
                if (string.Equals(type3.Name, fullTypeName, StringComparison.Ordinal))
                {
                    match = type;
                    return true;
                }
            }
            match = null;
            return false;
        }

        public static Type BaseType(this Type type)
        {
            return type.BaseType;
        }

        public static bool ContainsGenericParameters(this Type type)
        {
            return type.ContainsGenericParameters;
        }

        public static IEnumerable<Type> GetAllInterfaces(this Type target)
        {
            foreach (Type iteratorVariable0 in target.GetInterfaces())
            {
                yield return iteratorVariable0;
                foreach (Type iteratorVariable1 in iteratorVariable0.GetInterfaces())
                {
                    yield return iteratorVariable1;
                }
            }
        }

        public static IEnumerable<MethodInfo> GetAllMethods(this Type target)
        {
            List<Type> list = target.GetAllInterfaces().ToList<Type>();
            list.Add(target);
            return (from type in list
                from method in type.GetMethods()
                select method);
        }

        public static MethodInfo GetGenericMethod(this Type type, string name, params Type[] parameterTypes)
        {
            foreach (MethodInfo info in from method in type.GetMethods()
                where method.Name == name
                select method)
            {
                if (info.HasParameters(parameterTypes))
                {
                    return info;
                }
            }
            return null;
        }

        public static bool HasParameters(this MethodInfo method, params Type[] parameterTypes)
        {
            Type[] typeArray = (from parameter in method.GetParameters() select parameter.ParameterType).ToArray<Type>();
            if (typeArray.Length != parameterTypes.Length)
            {
                return false;
            }
            for (int i = 0; i < typeArray.Length; i++)
            {
                if (typeArray[i].ToString() != parameterTypes[i].ToString())
                {
                    return false;
                }
            }
            return true;
        }

        public static bool IsAbstract(this Type type)
        {
            return type.IsAbstract;
        }

        public static bool IsClass(this Type type)
        {
            return type.IsClass;
        }

        public static bool IsEnum(this Type type)
        {
            return type.IsEnum;
        }

        public static bool IsGenericType(this Type type)
        {
            return type.IsGenericType;
        }

        public static bool IsGenericTypeDefinition(this Type type)
        {
            return type.IsGenericTypeDefinition;
        }

        public static bool IsInterface(this Type type)
        {
            return type.IsInterface;
        }

        public static bool IsSealed(this Type type)
        {
            return type.IsSealed;
        }

        public static bool IsValueType(this Type type)
        {
            return type.IsValueType;
        }

        public static bool IsVisible(this Type type)
        {
            return type.IsVisible;
        }

        public static MemberTypes MemberType(this MemberInfo memberInfo)
        {
            return memberInfo.MemberType;
        }

    }
}

