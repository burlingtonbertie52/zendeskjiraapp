﻿namespace Newtonsoft.Json.Utilities
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Formatters;
    using System.Text;

    internal static class ReflectionUtils
    {
        public static readonly Type[] EmptyTypes = Type.EmptyTypes;

        public static bool CanReadMemberValue(MemberInfo member, bool nonPublic)
        {
            MemberTypes types = member.MemberType();
            if (types != MemberTypes.Field)
            {
                if (types != MemberTypes.Property)
                {
                    return false;
                }
            }
            else
            {
                FieldInfo info = (FieldInfo) member;
                return (nonPublic || info.IsPublic);
            }
            PropertyInfo info2 = (PropertyInfo) member;
            if (!info2.CanRead)
            {
                return false;
            }
            return (nonPublic || (info2.GetGetMethod(nonPublic) != null));
        }

        public static bool CanSetMemberValue(MemberInfo member, bool nonPublic, bool canSetReadOnly)
        {
            MemberTypes types = member.MemberType();
            if (types != MemberTypes.Field)
            {
                if (types != MemberTypes.Property)
                {
                    return false;
                }
            }
            else
            {
                FieldInfo info = (FieldInfo) member;
                if (!info.IsInitOnly || canSetReadOnly)
                {
                    if (nonPublic)
                    {
                        return true;
                    }
                    if (info.IsPublic)
                    {
                        return true;
                    }
                }
                return false;
            }
            PropertyInfo info2 = (PropertyInfo) member;
            if (!info2.CanWrite)
            {
                return false;
            }
            return (nonPublic || (info2.GetSetMethod(nonPublic) != null));
        }

        public static object CreateGeneric(Type genericTypeDefinition, IList<Type> innerTypes, params object[] args)
        {
            return CreateGeneric(genericTypeDefinition, innerTypes, (t, a) => CreateInstance(t, a.ToArray<object>()), args);
        }

        public static object CreateGeneric(Type genericTypeDefinition, Type innerType, params object[] args)
        {
            return CreateGeneric(genericTypeDefinition, new Type[] { innerType }, args);
        }

        public static object CreateGeneric(Type genericTypeDefinition, IList<Type> innerTypes, Func<Type, IList<object>, object> instanceCreator, params object[] args)
        {
            ValidationUtils.ArgumentNotNull(genericTypeDefinition, "genericTypeDefinition");
            ValidationUtils.ArgumentNotNullOrEmpty<Type>(innerTypes, "innerTypes");
            ValidationUtils.ArgumentNotNull(instanceCreator, "createInstance");
            Type type = MakeGenericType(genericTypeDefinition, innerTypes.ToArray<Type>());
            return instanceCreator(type, args);
        }

        public static object CreateInstance(Type type, params object[] args)
        {
            ValidationUtils.ArgumentNotNull(type, "type");
            return Activator.CreateInstance(type, args);
        }

        public static Type EnsureNotNullableType(Type t)
        {
            if (!IsNullableType(t))
            {
                return t;
            }
            return Nullable.GetUnderlyingType(t);
        }

        private static int? GetAssemblyDelimiterIndex(string fullyQualifiedTypeName)
        {
            int num = 0;
            for (int i = 0; i < fullyQualifiedTypeName.Length; i++)
            {
                switch (fullyQualifiedTypeName[i])
                {
                    case '[':
                        num++;
                        break;

                    case ']':
                        num--;
                        break;

                    case ',':
                        if (num == 0)
                        {
                            return new int?(i);
                        }
                        break;
                }
            }
            return null;
        }

        public static T GetAttribute<T>(ICustomAttributeProvider attributeProvider) where T: Attribute
        {
            return GetAttribute<T>(attributeProvider, true);
        }

        public static T GetAttribute<T>(ICustomAttributeProvider attributeProvider, bool inherit) where T: Attribute
        {
            T[] attributes = GetAttributes<T>(attributeProvider, inherit);
            if (attributes == null)
            {
                return default(T);
            }
            return attributes.SingleOrDefault<T>();
        }

        public static T[] GetAttributes<T>(ICustomAttributeProvider attributeProvider, bool inherit) where T: Attribute
        {
            ValidationUtils.ArgumentNotNull(attributeProvider, "attributeProvider");
            object obj2 = attributeProvider;
            if (obj2 is Type)
            {
                return (T[]) ((Type) obj2).GetCustomAttributes(typeof(T), inherit);
            }
            if (obj2 is Assembly)
            {
                return (T[]) Attribute.GetCustomAttributes((Assembly) obj2, typeof(T));
            }
            if (obj2 is MemberInfo)
            {
                return (T[]) Attribute.GetCustomAttributes((MemberInfo) obj2, typeof(T), inherit);
            }
            if (obj2 is Module)
            {
                return (T[]) Attribute.GetCustomAttributes((Module) obj2, typeof(T), inherit);
            }
            if (obj2 is ParameterInfo)
            {
                return (T[]) Attribute.GetCustomAttributes((ParameterInfo) obj2, typeof(T), inherit);
            }
            return (T[]) attributeProvider.GetCustomAttributes(typeof(T), inherit);
        }

        private static void GetChildPrivateFields(IList<MemberInfo> initialFields, Type targetType, BindingFlags bindingAttr)
        {
            if ((bindingAttr & BindingFlags.NonPublic) != BindingFlags.Default)
            {
                BindingFlags flags = bindingAttr.RemoveFlag(BindingFlags.Public);
                while ((targetType = targetType.BaseType()) != null)
                {
                    IEnumerable<MemberInfo> collection = (from f in targetType.GetFields(flags)
                        where f.IsPrivate
                        select f).Cast<MemberInfo>();
                    initialFields.AddRange<MemberInfo>(collection);
                }
            }
        }

        private static void GetChildPrivateProperties(IList<PropertyInfo> initialProperties, Type targetType, BindingFlags bindingAttr)
        {
            if ((bindingAttr & BindingFlags.NonPublic) != BindingFlags.Default)
            {
                BindingFlags flags = bindingAttr.RemoveFlag(BindingFlags.Public);
                while ((targetType = targetType.BaseType()) != null)
                {
                    foreach (PropertyInfo info in targetType.GetProperties(flags))
                    {
                        PropertyInfo nonPublicProperty = info;
                        int index = initialProperties.IndexOf<PropertyInfo>(p => p.Name == nonPublicProperty.Name);
                        if (index == -1)
                        {
                            initialProperties.Add(nonPublicProperty);
                        }
                        else
                        {
                            initialProperties[index] = nonPublicProperty;
                        }
                    }
                }
            }
        }

        public static Type GetCollectionItemType(Type type)
        {
            Type type2;
            ValidationUtils.ArgumentNotNull(type, "type");
            if (type.IsArray)
            {
                return type.GetElementType();
            }
            if (ImplementsGenericDefinition(type, typeof(IEnumerable<>), out type2))
            {
                if (type2.IsGenericTypeDefinition())
                {
                    throw new Exception("Type {0} is not a collection.".FormatWith(CultureInfo.InvariantCulture, type));
                }
                return type2.GetGenericArguments()[0];
            }
            if (!typeof(IEnumerable).IsAssignableFrom(type))
            {
                throw new Exception("Type {0} is not a collection.".FormatWith(CultureInfo.InvariantCulture, type));
            }
            return null;
        }

        public static ICustomAttributeProvider GetCustomAttributeProvider(this object o)
        {
            return (ICustomAttributeProvider) o;
        }

        public static ConstructorInfo GetDefaultConstructor(Type t)
        {
            return GetDefaultConstructor(t, false);
        }

        public static ConstructorInfo GetDefaultConstructor(Type t, bool nonPublic)
        {
            BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance;
            if (nonPublic)
            {
                bindingAttr |= BindingFlags.NonPublic;
            }
            return t.GetConstructors(bindingAttr).SingleOrDefault<ConstructorInfo>(c => !c.GetParameters().Any<ParameterInfo>());
        }

        public static object GetDefaultValue(Type type)
        {
            if (!type.IsValueType())
            {
                return null;
            }
            switch (ConvertUtils.GetTypeCode(type))
            {
                case TypeCode.Boolean:
                    return false;

                case TypeCode.Char:
                case TypeCode.SByte:
                case TypeCode.Byte:
                case TypeCode.Int16:
                case TypeCode.UInt16:
                case TypeCode.Int32:
                case TypeCode.UInt32:
                    return 0;

                case TypeCode.Int64:
                case TypeCode.UInt64:
                    return 0L;

                case TypeCode.Single:
                    return 0f;

                case TypeCode.Double:
                    return 0.0;

                case TypeCode.Decimal:
                    return 0M;

                case TypeCode.DateTime:
                    return new DateTime();
            }
            if (type == typeof(Guid))
            {
                return new Guid();
            }
            if (type == typeof(DateTimeOffset))
            {
                return new DateTimeOffset();
            }
            if (IsNullable(type))
            {
                return null;
            }
            return Activator.CreateInstance(type);
        }

        public static Type GetDictionaryKeyType(Type dictionaryType)
        {
            Type type;
            Type type2;
            GetDictionaryKeyValueTypes(dictionaryType, out type, out type2);
            return type;
        }

        public static void GetDictionaryKeyValueTypes(Type dictionaryType, out Type keyType, out Type valueType)
        {
            Type type;
            ValidationUtils.ArgumentNotNull(dictionaryType, "type");
            if (ImplementsGenericDefinition(dictionaryType, typeof(IDictionary<,>), out type))
            {
                if (type.IsGenericTypeDefinition())
                {
                    throw new Exception("Type {0} is not a dictionary.".FormatWith(CultureInfo.InvariantCulture, dictionaryType));
                }
                Type[] genericArguments = type.GetGenericArguments();
                keyType = genericArguments[0];
                valueType = genericArguments[1];
            }
            else
            {
                if (!typeof(IDictionary).IsAssignableFrom(dictionaryType))
                {
                    throw new Exception("Type {0} is not a dictionary.".FormatWith(CultureInfo.InvariantCulture, dictionaryType));
                }
                keyType = null;
                valueType = null;
            }
        }

        public static Type GetDictionaryValueType(Type dictionaryType)
        {
            Type type;
            Type type2;
            GetDictionaryKeyValueTypes(dictionaryType, out type, out type2);
            return type2;
        }

        public static IEnumerable<FieldInfo> GetFields(Type targetType, BindingFlags bindingAttr)
        {
            ValidationUtils.ArgumentNotNull(targetType, "targetType");
            List<MemberInfo> initialFields = new List<MemberInfo>(targetType.GetFields(bindingAttr));
            GetChildPrivateFields(initialFields, targetType, bindingAttr);
            return initialFields.Cast<FieldInfo>();
        }

        public static List<MemberInfo> GetFieldsAndProperties(Type type, BindingFlags bindingAttr)
        {
            Func<MemberInfo, bool> predicate = null;
            List<MemberInfo> list = new List<MemberInfo>();
            list.AddRange(GetFields(type, bindingAttr));
            list.AddRange(GetProperties(type, bindingAttr));
            List<MemberInfo> list2 = new List<MemberInfo>(list.Count);
            foreach (IGrouping<string, MemberInfo> grouping in from m in list group m by m.Name)
            {
                int num = grouping.Count<MemberInfo>();
                IList<MemberInfo> source = grouping.ToList<MemberInfo>();
                if (num == 1)
                {
                    list2.Add(source.First<MemberInfo>());
                }
                else
                {
                    if (predicate == null)
                    {
                        predicate = delegate (MemberInfo m) {
                            if (IsOverridenGenericMember(m, bindingAttr))
                            {
                                return m.Name == "Item";
                            }
                            return true;
                        };
                    }
                    IEnumerable<MemberInfo> collection = source.Where<MemberInfo>(predicate);
                    list2.AddRange(collection);
                }
            }
            return list2;
        }

        public static MemberInfo GetMemberInfoFromType(Type targetType, MemberInfo memberInfo)
        {
            if (memberInfo.MemberType() != MemberTypes.Property)
            {
                return targetType.GetMember(memberInfo.Name, memberInfo.MemberType(), BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance).SingleOrDefault<MemberInfo>();
            }
            PropertyInfo info = (PropertyInfo) memberInfo;
            Type[] types = (from p in info.GetIndexParameters() select p.ParameterType).ToArray<Type>();
            return targetType.GetProperty(info.Name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance, null, info.PropertyType, types, null);
        }

        public static Type GetMemberUnderlyingType(MemberInfo member)
        {
            ValidationUtils.ArgumentNotNull(member, "member");
            switch (member.MemberType())
            {
                case MemberTypes.Event:
                    return ((EventInfo) member).EventHandlerType;

                case MemberTypes.Field:
                    return ((FieldInfo) member).FieldType;

                case MemberTypes.Property:
                    return ((PropertyInfo) member).PropertyType;
            }
            throw new ArgumentException("MemberInfo must be of type FieldInfo, PropertyInfo or EventInfo", "member");
        }

        public static object GetMemberValue(MemberInfo member, object target)
        {
            ValidationUtils.ArgumentNotNull(member, "member");
            ValidationUtils.ArgumentNotNull(target, "target");
            switch (member.MemberType())
            {
                case MemberTypes.Field:
                    return ((FieldInfo) member).GetValue(target);

                case MemberTypes.Property:
                    try
                    {
                        return ((PropertyInfo) member).GetValue(target, null);
                    }
                    catch (TargetParameterCountException exception)
                    {
                        throw new ArgumentException("MemberInfo '{0}' has index parameters".FormatWith(CultureInfo.InvariantCulture, member.Name), exception);
                    }
                    break;
            }
            throw new ArgumentException("MemberInfo '{0}' is not of type FieldInfo or PropertyInfo".FormatWith(CultureInfo.InvariantCulture, CultureInfo.InvariantCulture, member.Name), "member");
        }

        public static Type GetObjectType(object v)
        {
            if (v == null)
            {
                return null;
            }
            return v.GetType();
        }

        public static IEnumerable<PropertyInfo> GetProperties(Type targetType, BindingFlags bindingAttr)
        {
            ValidationUtils.ArgumentNotNull(targetType, "targetType");
            List<PropertyInfo> initialProperties = new List<PropertyInfo>(targetType.GetProperties(bindingAttr));
            GetChildPrivateProperties(initialProperties, targetType, bindingAttr);
            for (int i = 0; i < initialProperties.Count; i++)
            {
                PropertyInfo memberInfo = initialProperties[i];
                if (memberInfo.DeclaringType != targetType)
                {
                    PropertyInfo memberInfoFromType = (PropertyInfo) GetMemberInfoFromType(memberInfo.DeclaringType, memberInfo);
                    initialProperties[i] = memberInfoFromType;
                }
            }
            return initialProperties;
        }

        public static string GetTypeName(Type t, FormatterAssemblyStyle assemblyFormat)
        {
            return GetTypeName(t, assemblyFormat, null);
        }

        public static string GetTypeName(Type t, FormatterAssemblyStyle assemblyFormat, SerializationBinder binder)
        {
            string assemblyQualifiedName;
            if (binder != null)
            {
                string str2;
                string str3;
                binder.BindToName(t, out str2, out str3);
                assemblyQualifiedName = str3 + ((str2 == null) ? "" : (", " + str2));
            }
            else
            {
                assemblyQualifiedName = t.AssemblyQualifiedName;
            }
            switch (assemblyFormat)
            {
                case FormatterAssemblyStyle.Simple:
                    return RemoveAssemblyDetails(assemblyQualifiedName);

                case FormatterAssemblyStyle.Full:
                    return assemblyQualifiedName;
            }
            throw new ArgumentOutOfRangeException();
        }

        public static bool HasDefaultConstructor(Type t)
        {
            return HasDefaultConstructor(t, false);
        }

        public static bool HasDefaultConstructor(Type t, bool nonPublic)
        {
            ValidationUtils.ArgumentNotNull(t, "t");
            return (t.IsValueType() || (GetDefaultConstructor(t, nonPublic) != null));
        }

        public static bool ImplementsGenericDefinition(Type type, Type genericInterfaceDefinition)
        {
            Type type2;
            return ImplementsGenericDefinition(type, genericInterfaceDefinition, out type2);
        }

        public static bool ImplementsGenericDefinition(Type type, Type genericInterfaceDefinition, out Type implementingType)
        {
            ValidationUtils.ArgumentNotNull(type, "type");
            ValidationUtils.ArgumentNotNull(genericInterfaceDefinition, "genericInterfaceDefinition");
            if (!genericInterfaceDefinition.IsInterface() || !genericInterfaceDefinition.IsGenericTypeDefinition())
            {
                throw new ArgumentNullException("'{0}' is not a generic interface definition.".FormatWith(CultureInfo.InvariantCulture, genericInterfaceDefinition));
            }
            if (type.IsInterface() && type.IsGenericType())
            {
                Type genericTypeDefinition = type.GetGenericTypeDefinition();
                if (genericInterfaceDefinition == genericTypeDefinition)
                {
                    implementingType = type;
                    return true;
                }
            }
            foreach (Type type3 in type.GetInterfaces())
            {
                if (type3.IsGenericType())
                {
                    Type type4 = type3.GetGenericTypeDefinition();
                    if (genericInterfaceDefinition == type4)
                    {
                        implementingType = type3;
                        return true;
                    }
                }
            }
            implementingType = null;
            return false;
        }

        public static bool InheritsGenericDefinition(Type type, Type genericClassDefinition)
        {
            Type type2;
            return InheritsGenericDefinition(type, genericClassDefinition, out type2);
        }

        public static bool InheritsGenericDefinition(Type type, Type genericClassDefinition, out Type implementingType)
        {
            ValidationUtils.ArgumentNotNull(type, "type");
            ValidationUtils.ArgumentNotNull(genericClassDefinition, "genericClassDefinition");
            if (!genericClassDefinition.IsClass() || !genericClassDefinition.IsGenericTypeDefinition())
            {
                throw new ArgumentNullException("'{0}' is not a generic class definition.".FormatWith(CultureInfo.InvariantCulture, genericClassDefinition));
            }
            return InheritsGenericDefinitionInternal(type, genericClassDefinition, out implementingType);
        }

        private static bool InheritsGenericDefinitionInternal(Type currentType, Type genericClassDefinition, out Type implementingType)
        {
            if (currentType.IsGenericType())
            {
                Type genericTypeDefinition = currentType.GetGenericTypeDefinition();
                if (genericClassDefinition == genericTypeDefinition)
                {
                    implementingType = currentType;
                    return true;
                }
            }
            if (currentType.BaseType() == null)
            {
                implementingType = null;
                return false;
            }
            return InheritsGenericDefinitionInternal(currentType.BaseType(), genericClassDefinition, out implementingType);
        }

        public static bool IsIndexedProperty(MemberInfo member)
        {
            ValidationUtils.ArgumentNotNull(member, "member");
            PropertyInfo property = member as PropertyInfo;
            return ((property != null) && IsIndexedProperty(property));
        }

        public static bool IsIndexedProperty(PropertyInfo property)
        {
            ValidationUtils.ArgumentNotNull(property, "property");
            return (property.GetIndexParameters().Length > 0);
        }

        public static bool IsInstantiatableType(Type t)
        {
            ValidationUtils.ArgumentNotNull(t, "t");
            if ((t.IsAbstract() || t.IsInterface()) || ((t.IsArray || t.IsGenericTypeDefinition()) || (t == typeof(void))))
            {
                return false;
            }
            if (!HasDefaultConstructor(t))
            {
                return false;
            }
            return true;
        }

        public static bool IsMethodOverridden(Type currentType, Type methodDeclaringType, string method)
        {
            return currentType.GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance).Any<MethodInfo>(info => (((info.Name == method) && (info.DeclaringType != methodDeclaringType)) && (info.GetBaseDefinition().DeclaringType == methodDeclaringType)));
        }

        public static bool IsNullable(Type t)
        {
            ValidationUtils.ArgumentNotNull(t, "t");
            if (t.IsValueType())
            {
                return IsNullableType(t);
            }
            return true;
        }

        public static bool IsNullableType(Type t)
        {
            ValidationUtils.ArgumentNotNull(t, "t");
            return (t.IsGenericType() && (t.GetGenericTypeDefinition() == typeof(Nullable<>)));
        }

        private static bool IsOverridenGenericMember(MemberInfo memberInfo, BindingFlags bindingAttr)
        {
            MemberTypes types = memberInfo.MemberType();
            if ((types != MemberTypes.Field) && (types != MemberTypes.Property))
            {
                throw new ArgumentException("Member must be a field or property.");
            }
            Type declaringType = memberInfo.DeclaringType;
            if (!declaringType.IsGenericType())
            {
                return false;
            }
            Type genericTypeDefinition = declaringType.GetGenericTypeDefinition();
            if (genericTypeDefinition == null)
            {
                return false;
            }
            MemberInfo[] member = genericTypeDefinition.GetMember(memberInfo.Name, bindingAttr);
            if (member.Length == 0)
            {
                return false;
            }
            if (!GetMemberUnderlyingType(member[0]).IsGenericParameter)
            {
                return false;
            }
            return true;
        }

        public static bool IsVirtual(this PropertyInfo propertyInfo)
        {
            ValidationUtils.ArgumentNotNull(propertyInfo, "propertyInfo");
            MethodInfo getMethod = propertyInfo.GetGetMethod();
            if ((getMethod != null) && getMethod.IsVirtual)
            {
                return true;
            }
            getMethod = propertyInfo.GetSetMethod();
            return ((getMethod != null) && getMethod.IsVirtual);
        }

        public static Type MakeGenericType(Type genericTypeDefinition, params Type[] innerTypes)
        {
            ValidationUtils.ArgumentNotNull(genericTypeDefinition, "genericTypeDefinition");
            ValidationUtils.ArgumentNotNullOrEmpty<Type>(innerTypes, "innerTypes");
            ValidationUtils.ArgumentConditionTrue(genericTypeDefinition.IsGenericTypeDefinition(), "genericTypeDefinition", "Type {0} is not a generic type definition.".FormatWith(CultureInfo.InvariantCulture, genericTypeDefinition));
            return genericTypeDefinition.MakeGenericType(innerTypes);
        }

        private static string RemoveAssemblyDetails(string fullyQualifiedTypeName)
        {
            StringBuilder builder = new StringBuilder();
            bool flag = false;
            bool flag2 = false;
            for (int i = 0; i < fullyQualifiedTypeName.Length; i++)
            {
                char ch = fullyQualifiedTypeName[i];
                switch (ch)
                {
                    case '[':
                    {
                        flag = false;
                        flag2 = false;
                        builder.Append(ch);
                        continue;
                    }
                    case ']':
                    {
                        flag = false;
                        flag2 = false;
                        builder.Append(ch);
                        continue;
                    }
                    case ',':
                    {
                        if (!flag)
                        {
                            flag = true;
                            builder.Append(ch);
                        }
                        else
                        {
                            flag2 = true;
                        }
                        continue;
                    }
                }
                if (!flag2)
                {
                    builder.Append(ch);
                }
            }
            return builder.ToString();
        }

        public static BindingFlags RemoveFlag(this BindingFlags bindingAttr, BindingFlags flag)
        {
            if ((bindingAttr & flag) != flag)
            {
                return bindingAttr;
            }
            return (bindingAttr ^ flag);
        }

        public static void SetMemberValue(MemberInfo member, object target, object value)
        {
            ValidationUtils.ArgumentNotNull(member, "member");
            ValidationUtils.ArgumentNotNull(target, "target");
            MemberTypes types = member.MemberType();
            if (types != MemberTypes.Field)
            {
                if (types != MemberTypes.Property)
                {
                    throw new ArgumentException("MemberInfo '{0}' must be of type FieldInfo or PropertyInfo".FormatWith(CultureInfo.InvariantCulture, member.Name), "member");
                }
            }
            else
            {
                ((FieldInfo) member).SetValue(target, value);
                return;
            }
            ((PropertyInfo) member).SetValue(target, value, null);
        }

        public static void SplitFullyQualifiedTypeName(string fullyQualifiedTypeName, out string typeName, out string assemblyName)
        {
            int? assemblyDelimiterIndex = GetAssemblyDelimiterIndex(fullyQualifiedTypeName);
            if (assemblyDelimiterIndex.HasValue)
            {
                typeName = fullyQualifiedTypeName.Substring(0, assemblyDelimiterIndex.Value).Trim();
                assemblyName = fullyQualifiedTypeName.Substring(assemblyDelimiterIndex.Value + 1, (fullyQualifiedTypeName.Length - assemblyDelimiterIndex.Value) - 1).Trim();
            }
            else
            {
                typeName = fullyQualifiedTypeName;
                assemblyName = null;
            }
        }
    }
}

