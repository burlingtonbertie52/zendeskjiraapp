﻿namespace Newtonsoft.Json.Utilities
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.InteropServices;
    using System.Threading;

    internal class DictionaryWrapper<TKey, TValue> : IEnumerable, IDictionary<TKey, TValue>, ICollection, ICollection<KeyValuePair<TKey, TValue>>, IEnumerable<KeyValuePair<TKey, TValue>>, IDictionary, IWrappedDictionary
    {
        private readonly IDictionary _dictionary;
        private readonly IDictionary<TKey, TValue> _genericDictionary;
        private object _syncRoot;

        public DictionaryWrapper(IDictionary<TKey, TValue> dictionary)
        {
            ValidationUtils.ArgumentNotNull(dictionary, "dictionary");
            this._genericDictionary = dictionary;
        }

        public DictionaryWrapper(IDictionary dictionary)
        {
            ValidationUtils.ArgumentNotNull(dictionary, "dictionary");
            this._dictionary = dictionary;
        }

        public void Add(KeyValuePair<TKey, TValue> item)
        {
            if (this._dictionary != null)
            {
                ((IList) this._dictionary).Add(item);
            }
            else
            {
                this._genericDictionary.Add(item);
            }
        }

        public void Add(TKey key, TValue value)
        {
            if (this._dictionary != null)
            {
                this._dictionary.Add(key, value);
            }
            else
            {
                this._genericDictionary.Add(key, value);
            }
        }

        public void Clear()
        {
            if (this._dictionary != null)
            {
                this._dictionary.Clear();
            }
            else
            {
                this._genericDictionary.Clear();
            }
        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            if (this._dictionary != null)
            {
                return ((IList) this._dictionary).Contains(item);
            }
            return this._genericDictionary.Contains(item);
        }

        public bool ContainsKey(TKey key)
        {
            if (this._dictionary != null)
            {
                return this._dictionary.Contains(key);
            }
            return this._genericDictionary.ContainsKey(key);
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            if (this._dictionary != null)
            {
                foreach (DictionaryEntry entry in this._dictionary)
                {
                    array[arrayIndex++] = new KeyValuePair<TKey, TValue>((TKey) entry.Key, (TValue) entry.Value);
                }
            }
            else
            {
                this._genericDictionary.CopyTo(array, arrayIndex);
            }
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            if (this._dictionary == null)
            {
                return this._genericDictionary.GetEnumerator();
            }
            return (from de in this._dictionary.Cast<DictionaryEntry>() select new KeyValuePair<TKey, TValue>((TKey) de.Key, (TValue) de.Value)).GetEnumerator();
        }

        public bool Remove(TKey key)
        {
            if (this._dictionary == null)
            {
                return this._genericDictionary.Remove(key);
            }
            if (this._dictionary.Contains(key))
            {
                this._dictionary.Remove(key);
                return true;
            }
            return false;
        }

        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            if (this._dictionary == null)
            {
                return this._genericDictionary.Remove(item);
            }
            if (!this._dictionary.Contains(item.Key))
            {
                return true;
            }
            object objA = this._dictionary[item.Key];
            if (object.Equals(objA, item.Value))
            {
                this._dictionary.Remove(item.Key);
                return true;
            }
            return false;
        }

        public void Remove(object key)
        {
            if (this._dictionary != null)
            {
                this._dictionary.Remove(key);
            }
            else
            {
                this._genericDictionary.Remove((TKey) key);
            }
        }

        void ICollection.CopyTo(Array array, int index)
        {
            if (this._dictionary != null)
            {
                this._dictionary.CopyTo(array, index);
            }
            else
            {
                this._genericDictionary.CopyTo((KeyValuePair<TKey, TValue>[]) array, index);
            }
        }

        void IDictionary.Add(object key, object value)
        {
            if (this._dictionary != null)
            {
                this._dictionary.Add(key, value);
            }
            else
            {
                this._genericDictionary.Add((TKey) key, (TValue) value);
            }
        }

        bool IDictionary.Contains(object key)
        {
            if (this._genericDictionary != null)
            {
                return this._genericDictionary.ContainsKey((TKey) key);
            }
            return this._dictionary.Contains(key);
        }

        IDictionaryEnumerator IDictionary.GetEnumerator()
        {
            if (this._dictionary != null)
            {
                return this._dictionary.GetEnumerator();
            }
            return new DictionaryEnumerator<TKey, TValue, TKey, TValue>(this._genericDictionary.GetEnumerator());
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            if (this._dictionary == null)
            {
                return this._genericDictionary.TryGetValue(key, out value);
            }
            if (!this._dictionary.Contains(key))
            {
                value = default(TValue);
                return false;
            }
            value = (TValue) this._dictionary[key];
            return true;
        }

        public int Count
        {
            get
            {
                if (this._dictionary != null)
                {
                    return this._dictionary.Count;
                }
                return this._genericDictionary.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                if (this._dictionary != null)
                {
                    return this._dictionary.IsReadOnly;
                }
                return this._genericDictionary.IsReadOnly;
            }
        }

        public TValue this[TKey key]
        {
            get
            {
                if (this._dictionary != null)
                {
                    return (TValue) this._dictionary[key];
                }
                return this._genericDictionary[key];
            }
            set
            {
                if (this._dictionary != null)
                {
                    this._dictionary[key] = value;
                }
                else
                {
                    this._genericDictionary[key] = value;
                }
            }
        }

        public ICollection<TKey> Keys
        {
            get
            {
                if (this._dictionary != null)
                {
                    return this._dictionary.Keys.Cast<TKey>().ToList<TKey>();
                }
                return this._genericDictionary.Keys;
            }
        }

        bool ICollection.IsSynchronized
        {
            get
            {
                return ((this._dictionary != null) && this._dictionary.IsSynchronized);
            }
        }

        object ICollection.SyncRoot
        {
            get
            {
                if (this._syncRoot == null)
                {
                    Interlocked.CompareExchange(ref this._syncRoot, new object(), null);
                }
                return this._syncRoot;
            }
        }

        bool IDictionary.IsFixedSize
        {
            get
            {
                if (this._genericDictionary != null)
                {
                    return false;
                }
                return this._dictionary.IsFixedSize;
            }
        }

        object IDictionary.this[object key]
        {
            get
            {
                if (this._dictionary != null)
                {
                    return this._dictionary[key];
                }
                return this._genericDictionary[(TKey) key];
            }
            set
            {
                if (this._dictionary != null)
                {
                    this._dictionary[key] = value;
                }
                else
                {
                    this._genericDictionary[(TKey) key] = (TValue) value;
                }
            }
        }

        ICollection IDictionary.Keys
        {
            get
            {
                if (this._genericDictionary != null)
                {
                    return this._genericDictionary.Keys.ToList<TKey>();
                }
                return this._dictionary.Keys;
            }
        }

        ICollection IDictionary.Values
        {
            get
            {
                if (this._genericDictionary != null)
                {
                    return this._genericDictionary.Values.ToList<TValue>();
                }
                return this._dictionary.Values;
            }
        }

        public object UnderlyingDictionary
        {
            get
            {
                if (this._dictionary != null)
                {
                    return this._dictionary;
                }
                return this._genericDictionary;
            }
        }

        public ICollection<TValue> Values
        {
            get
            {
                if (this._dictionary != null)
                {
                    return (ICollection<TValue>) this._dictionary.Values.Cast<TValue>().ToList<TValue>();
                }
                return this._genericDictionary.Values;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct DictionaryEnumerator<TEnumeratorKey, TEnumeratorValue> : IEnumerator, IDictionaryEnumerator
        {
            private readonly IEnumerator<KeyValuePair<TEnumeratorKey, TEnumeratorValue>> _e;
            public DictionaryEnumerator(IEnumerator<KeyValuePair<TEnumeratorKey, TEnumeratorValue>> e)
            {
                ValidationUtils.ArgumentNotNull(e, "e");
                this._e = e;
            }

            public DictionaryEntry Entry
            {
                get
                {
                    return (DictionaryEntry) this.Current;
                }
            }
            public object Key
            {
                get
                {
                    return this.Entry.Key;
                }
            }
            public object Value
            {
                get
                {
                    return this.Entry.Value;
                }
            }
            public object Current
            {
                get
                {
                    return new DictionaryEntry(this._e.Current.Key, this._e.Current.Value);
                }
            }
            public bool MoveNext()
            {
                return this._e.MoveNext();
            }

            public void Reset()
            {
                this._e.Reset();
            }
        }
    }
}

