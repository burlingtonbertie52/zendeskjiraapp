﻿namespace Newtonsoft.Json.Utilities
{
    using System;
    using System.Globalization;
    using System.Reflection;
    using System.Reflection.Emit;

    internal class DynamicReflectionDelegateFactory : ReflectionDelegateFactory
    {
        public static DynamicReflectionDelegateFactory Instance = new DynamicReflectionDelegateFactory();

        public override Func<T> CreateDefaultConstructor<T>(Type type)
        {
            DynamicMethod method = CreateDynamicMethod("Create" + type.FullName, typeof(T), ReflectionUtils.EmptyTypes, type);
            method.InitLocals = true;
            ILGenerator iLGenerator = method.GetILGenerator();
            this.GenerateCreateDefaultConstructorIL(type, iLGenerator);
            return (Func<T>) method.CreateDelegate(typeof(Func<T>));
        }

        private static DynamicMethod CreateDynamicMethod(string name, Type returnType, Type[] parameterTypes, Type owner)
        {
            return (!owner.IsInterface() ? new DynamicMethod(name, returnType, parameterTypes, owner, true) : new DynamicMethod(name, returnType, parameterTypes, owner.Module, true));
        }

        public override Func<T, object> CreateGet<T>(FieldInfo fieldInfo)
        {
            DynamicMethod method = CreateDynamicMethod("Get" + fieldInfo.Name, typeof(T), new Type[] { typeof(object) }, fieldInfo.DeclaringType);
            ILGenerator iLGenerator = method.GetILGenerator();
            this.GenerateCreateGetFieldIL(fieldInfo, iLGenerator);
            return (Func<T, object>) method.CreateDelegate(typeof(Func<T, object>));
        }

        public override Func<T, object> CreateGet<T>(PropertyInfo propertyInfo)
        {
            DynamicMethod method = CreateDynamicMethod("Get" + propertyInfo.Name, typeof(T), new Type[] { typeof(object) }, propertyInfo.DeclaringType);
            ILGenerator iLGenerator = method.GetILGenerator();
            this.GenerateCreateGetPropertyIL(propertyInfo, iLGenerator);
            return (Func<T, object>) method.CreateDelegate(typeof(Func<T, object>));
        }

        public override MethodCall<T, object> CreateMethodCall<T>(MethodBase method)
        {
            DynamicMethod method2 = CreateDynamicMethod(method.ToString(), typeof(object), new Type[] { typeof(object), typeof(object[]) }, method.DeclaringType);
            ILGenerator iLGenerator = method2.GetILGenerator();
            this.GenerateCreateMethodCallIL(method, iLGenerator);
            return (MethodCall<T, object>) method2.CreateDelegate(typeof(MethodCall<T, object>));
        }

        public override Action<T, object> CreateSet<T>(FieldInfo fieldInfo)
        {
            DynamicMethod method = CreateDynamicMethod("Set" + fieldInfo.Name, null, new Type[] { typeof(T), typeof(object) }, fieldInfo.DeclaringType);
            ILGenerator iLGenerator = method.GetILGenerator();
            GenerateCreateSetFieldIL(fieldInfo, iLGenerator);
            return (Action<T, object>) method.CreateDelegate(typeof(Action<T, object>));
        }

        public override Action<T, object> CreateSet<T>(PropertyInfo propertyInfo)
        {
            DynamicMethod method = CreateDynamicMethod("Set" + propertyInfo.Name, null, new Type[] { typeof(T), typeof(object) }, propertyInfo.DeclaringType);
            ILGenerator iLGenerator = method.GetILGenerator();
            GenerateCreateSetPropertyIL(propertyInfo, iLGenerator);
            return (Action<T, object>) method.CreateDelegate(typeof(Action<T, object>));
        }

        private void GenerateCreateDefaultConstructorIL(Type type, ILGenerator generator)
        {
            if (type.IsValueType())
            {
                generator.DeclareLocal(type);
                generator.Emit(OpCodes.Ldloc_0);
                generator.Emit(OpCodes.Box, type);
            }
            else
            {
                ConstructorInfo con = type.GetConstructor(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance, null, ReflectionUtils.EmptyTypes, null);
                if (con == null)
                {
                    throw new ArgumentException("Could not get constructor for {0}.".FormatWith(CultureInfo.InvariantCulture, type));
                }
                generator.Emit(OpCodes.Newobj, con);
            }
            generator.Return();
        }

        private void GenerateCreateGetFieldIL(FieldInfo fieldInfo, ILGenerator generator)
        {
            if (!fieldInfo.IsStatic)
            {
                generator.PushInstance(fieldInfo.DeclaringType);
            }
            generator.Emit(OpCodes.Ldfld, fieldInfo);
            generator.BoxIfNeeded(fieldInfo.FieldType);
            generator.Return();
        }

        private void GenerateCreateGetPropertyIL(PropertyInfo propertyInfo, ILGenerator generator)
        {
            MethodInfo getMethod = propertyInfo.GetGetMethod(true);
            if (getMethod == null)
            {
                throw new ArgumentException("Property '{0}' does not have a getter.".FormatWith(CultureInfo.InvariantCulture, propertyInfo.Name));
            }
            if (!getMethod.IsStatic)
            {
                generator.PushInstance(propertyInfo.DeclaringType);
            }
            generator.CallMethod(getMethod);
            generator.BoxIfNeeded(propertyInfo.PropertyType);
            generator.Return();
        }

        private void GenerateCreateMethodCallIL(MethodBase method, ILGenerator generator)
        {
            ParameterInfo[] parameters = method.GetParameters();
            Label label = generator.DefineLabel();
            generator.Emit(OpCodes.Ldarg_1);
            generator.Emit(OpCodes.Ldlen);
            generator.Emit(OpCodes.Ldc_I4, parameters.Length);
            generator.Emit(OpCodes.Beq, label);
            generator.Emit(OpCodes.Newobj, typeof(TargetParameterCountException).GetConstructor(ReflectionUtils.EmptyTypes));
            generator.Emit(OpCodes.Throw);
            generator.MarkLabel(label);
            if (!method.IsConstructor && !method.IsStatic)
            {
                generator.PushInstance(method.DeclaringType);
            }
            for (int i = 0; i < parameters.Length; i++)
            {
                generator.Emit(OpCodes.Ldarg_1);
                generator.Emit(OpCodes.Ldc_I4, i);
                generator.Emit(OpCodes.Ldelem_Ref);
                generator.UnboxIfNeeded(parameters[i].ParameterType);
            }
            if (method.IsConstructor)
            {
                generator.Emit(OpCodes.Newobj, (ConstructorInfo) method);
            }
            else if (method.IsFinal || !method.IsVirtual)
            {
                generator.CallMethod((MethodInfo) method);
            }
            Type type = method.IsConstructor ? method.DeclaringType : ((MethodInfo) method).ReturnType;
            if (type != typeof(void))
            {
                generator.BoxIfNeeded(type);
            }
            else
            {
                generator.Emit(OpCodes.Ldnull);
            }
            generator.Return();
        }

        internal static void GenerateCreateSetFieldIL(FieldInfo fieldInfo, ILGenerator generator)
        {
            if (!fieldInfo.IsStatic)
            {
                generator.PushInstance(fieldInfo.DeclaringType);
            }
            generator.Emit(OpCodes.Ldarg_1);
            generator.UnboxIfNeeded(fieldInfo.FieldType);
            generator.Emit(OpCodes.Stfld, fieldInfo);
            generator.Return();
        }

        internal static void GenerateCreateSetPropertyIL(PropertyInfo propertyInfo, ILGenerator generator)
        {
            MethodInfo setMethod = propertyInfo.GetSetMethod(true);
            if (!setMethod.IsStatic)
            {
                generator.PushInstance(propertyInfo.DeclaringType);
            }
            generator.Emit(OpCodes.Ldarg_1);
            generator.UnboxIfNeeded(propertyInfo.PropertyType);
            generator.CallMethod(setMethod);
            generator.Return();
        }
    }
}

