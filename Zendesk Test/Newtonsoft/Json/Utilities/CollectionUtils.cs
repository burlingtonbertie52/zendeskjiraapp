﻿namespace Newtonsoft.Json.Utilities
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    internal static class CollectionUtils
    {
        public static bool AddDistinct<T>(this IList<T> list, T value)
        {
            return list.AddDistinct<T>(value, EqualityComparer<T>.Default);
        }

        public static bool AddDistinct<T>(this IList<T> list, T value, IEqualityComparer<T> comparer)
        {
            if (list.ContainsValue<T>(value, comparer))
            {
                return false;
            }
            list.Add(value);
            return true;
        }

        public static void AddRange(this IList initial, IEnumerable collection)
        {
            ValidationUtils.ArgumentNotNull(initial, "initial");
            new ListWrapper<object>(initial).AddRange<object>(collection.Cast<object>());
        }

        public static void AddRange<T>(this IList<T> initial, IEnumerable<T> collection)
        {
            if (initial == null)
            {
                throw new ArgumentNullException("initial");
            }
            if (collection != null)
            {
                foreach (T local in collection)
                {
                    initial.Add(local);
                }
            }
        }

        public static bool AddRangeDistinct<T>(this IList<T> list, IEnumerable<T> values, IEqualityComparer<T> comparer)
        {
            bool flag = true;
            foreach (T local in values)
            {
                if (!list.AddDistinct<T>(local, comparer))
                {
                    flag = false;
                }
            }
            return flag;
        }

        public static IEnumerable<T> CastValid<T>(this IEnumerable enumerable)
        {
            ValidationUtils.ArgumentNotNull(enumerable, "enumerable");
            return (from o in enumerable.Cast<object>()
                where o is T
                select o).Cast<T>();
        }

        public static bool ContainsValue<TSource>(this IEnumerable<TSource> source, TSource value, IEqualityComparer<TSource> comparer)
        {
            if (comparer == null)
            {
                comparer = EqualityComparer<TSource>.Default;
            }
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }
            foreach (TSource local in source)
            {
                if (comparer.Equals(local, value))
                {
                    return true;
                }
            }
            return false;
        }

        private static void CopyFromJaggedToMultidimensionalArray(IList values, Array multidimensionalArray, int[] indices)
        {
            int length = indices.Length;
            if (length == multidimensionalArray.Rank)
            {
                multidimensionalArray.SetValue(JaggedArrayGetValue(values, indices), indices);
            }
            else
            {
                int num2 = multidimensionalArray.GetLength(length);
                IList list = (IList) JaggedArrayGetValue(values, indices);
                if (list.Count != num2)
                {
                    throw new Exception("Cannot deserialize non-cubical array as multidimensional array.");
                }
                int[] numArray = new int[length + 1];
                for (int i = 0; i < length; i++)
                {
                    numArray[i] = indices[i];
                }
                for (int j = 0; j < multidimensionalArray.GetLength(length); j++)
                {
                    numArray[length] = j;
                    CopyFromJaggedToMultidimensionalArray(values, multidimensionalArray, numArray);
                }
            }
        }

        public static IWrappedCollection CreateCollectionWrapper(object list)
        {
            Func<Type, IList<object>, object> func2 = null;
            Type collectionDefinition;
            ValidationUtils.ArgumentNotNull(list, "list");
            if (ReflectionUtils.ImplementsGenericDefinition(list.GetType(), typeof(ICollection<>), out collectionDefinition))
            {
                Type collectionItemType = ReflectionUtils.GetCollectionItemType(collectionDefinition);
                if (func2 == null)
                {
                    func2 = (t, a) => t.GetConstructor(new Type[] { collectionDefinition }).Invoke(new object[] { list });
                }
                Func<Type, IList<object>, object> instanceCreator = func2;
                return (IWrappedCollection) ReflectionUtils.CreateGeneric(typeof(CollectionWrapper<>), new Type[] { collectionItemType }, instanceCreator, new object[] { list });
            }
            if (!(list is IList))
            {
                throw new ArgumentException("Can not create ListWrapper for type {0}.".FormatWith(CultureInfo.InvariantCulture, list.GetType()), "list");
            }
            return new CollectionWrapper<object>((IList) list);
        }

        public static IWrappedDictionary CreateDictionaryWrapper(object dictionary)
        {
            Func<Type, IList<object>, object> func2 = null;
            Type dictionaryDefinition;
            ValidationUtils.ArgumentNotNull(dictionary, "dictionary");
            if (ReflectionUtils.ImplementsGenericDefinition(dictionary.GetType(), typeof(IDictionary<,>), out dictionaryDefinition))
            {
                Type dictionaryKeyType = ReflectionUtils.GetDictionaryKeyType(dictionaryDefinition);
                Type dictionaryValueType = ReflectionUtils.GetDictionaryValueType(dictionaryDefinition);
                if (func2 == null)
                {
                    func2 = (t, a) => t.GetConstructor(new Type[] { dictionaryDefinition }).Invoke(new object[] { dictionary });
                }
                Func<Type, IList<object>, object> instanceCreator = func2;
                return (IWrappedDictionary) ReflectionUtils.CreateGeneric(typeof(DictionaryWrapper<,>), new Type[] { dictionaryKeyType, dictionaryValueType }, instanceCreator, new object[] { dictionary });
            }
            if (!(dictionary is IDictionary))
            {
                throw new ArgumentException("Can not create DictionaryWrapper for type {0}.".FormatWith(CultureInfo.InvariantCulture, dictionary.GetType()), "dictionary");
            }
            return new DictionaryWrapper<object, object>((IDictionary) dictionary);
        }

        public static IList CreateGenericList(Type listType)
        {
            ValidationUtils.ArgumentNotNull(listType, "listType");
            return (IList) ReflectionUtils.CreateGeneric(typeof(List<>), listType, new object[0]);
        }

        public static IList CreateList(Type listType, out bool isReadOnlyOrFixedSize)
        {
            IList list;
            ValidationUtils.ArgumentNotNull(listType, "listType");
            isReadOnlyOrFixedSize = false;
            if (listType.IsArray)
            {
                list = new List<object>();
                isReadOnlyOrFixedSize = true;
            }
            else
            {
                Type type;
                if (!ReflectionUtils.InheritsGenericDefinition(listType, typeof(ReadOnlyCollection<>), out type))
                {
                    if (typeof(IList).IsAssignableFrom(listType))
                    {
                        if (ReflectionUtils.IsInstantiatableType(listType))
                        {
                            list = (IList) Activator.CreateInstance(listType);
                        }
                        else if (listType == typeof(IList))
                        {
                            list = new List<object>();
                        }
                        else
                        {
                            list = null;
                        }
                    }
                    else if (ReflectionUtils.ImplementsGenericDefinition(listType, typeof(ICollection<>)))
                    {
                        if (ReflectionUtils.IsInstantiatableType(listType))
                        {
                            list = CreateCollectionWrapper(Activator.CreateInstance(listType));
                        }
                        else
                        {
                            list = null;
                        }
                    }
                    else
                    {
                        list = null;
                    }
                }
                else
                {
                    Type type2 = type.GetGenericArguments()[0];
                    Type type3 = ReflectionUtils.MakeGenericType(typeof(IEnumerable<>), new Type[] { type2 });
                    bool flag = false;
                    foreach (ConstructorInfo info in listType.GetConstructors())
                    {
                        IList<ParameterInfo> parameters = info.GetParameters();
                        if ((parameters.Count == 1) && type3.IsAssignableFrom(parameters[0].ParameterType))
                        {
                            flag = true;
                            break;
                        }
                    }
                    if (!flag)
                    {
                        throw new Exception("Read-only type {0} does not have a public constructor that takes a type that implements {1}.".FormatWith(CultureInfo.InvariantCulture, listType, type3));
                    }
                    list = CreateGenericList(type2);
                    isReadOnlyOrFixedSize = true;
                }
            }
            if (list == null)
            {
                throw new InvalidOperationException("Cannot create and populate list type {0}.".FormatWith(CultureInfo.InvariantCulture, listType));
            }
            return list;
        }

        private static IList<int> GetDimensions(IList values)
        {
            IList<int> list = new List<int>();
            IList list2 = values;
            while (true)
            {
                list.Add(list2.Count);
                if (list2.Count == 0)
                {
                    return list;
                }
                object obj2 = list2[0];
                if (!(obj2 is IList))
                {
                    return list;
                }
                list2 = (IList) obj2;
            }
        }

        public static int IndexOf<T>(this IEnumerable<T> collection, Func<T, bool> predicate)
        {
            int num = 0;
            foreach (T local in collection)
            {
                if (predicate(local))
                {
                    return num;
                }
                num++;
            }
            return -1;
        }

        public static int IndexOf<TSource>(this IEnumerable<TSource> list, TSource value, IEqualityComparer<TSource> comparer)
        {
            int num = 0;
            foreach (TSource local in list)
            {
                if (comparer.Equals(local, value))
                {
                    return num;
                }
                num++;
            }
            return -1;
        }

        public static bool IsDictionaryType(Type type)
        {
            ValidationUtils.ArgumentNotNull(type, "type");
            return (typeof(IDictionary).IsAssignableFrom(type) || ReflectionUtils.ImplementsGenericDefinition(type, typeof(IDictionary<,>)));
        }

        public static bool IsNullOrEmpty<T>(ICollection<T> collection)
        {
            if (collection != null)
            {
                return (collection.Count == 0);
            }
            return true;
        }

        private static object JaggedArrayGetValue(IList values, int[] indices)
        {
            IList list = values;
            for (int i = 0; i < indices.Length; i++)
            {
                int num2 = indices[i];
                if (i == (indices.Length - 1))
                {
                    return list[num2];
                }
                list = (IList) list[num2];
            }
            return list;
        }

        public static Array ToArray(Array initial, Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }
            Array destinationArray = Array.CreateInstance(type, initial.Length);
            Array.Copy(initial, 0, destinationArray, 0, initial.Length);
            return destinationArray;
        }

        public static Array ToMultidimensionalArray(IList values, Type type, int rank)
        {
            IList<int> dimensions = GetDimensions(values);
            while (dimensions.Count < rank)
            {
                dimensions.Add(0);
            }
            Array multidimensionalArray = Array.CreateInstance(type, dimensions.ToArray<int>());
            CopyFromJaggedToMultidimensionalArray(values, multidimensionalArray, new int[0]);
            return multidimensionalArray;
        }
    }
}

