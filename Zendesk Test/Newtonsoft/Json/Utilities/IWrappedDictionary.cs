﻿namespace Newtonsoft.Json.Utilities
{
    using System;
    using System.Collections;

    internal interface IWrappedDictionary : IEnumerable, ICollection, IDictionary
    {
        object UnderlyingDictionary { get; }
    }
}

