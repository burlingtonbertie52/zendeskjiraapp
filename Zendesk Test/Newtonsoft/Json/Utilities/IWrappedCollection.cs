﻿namespace Newtonsoft.Json.Utilities
{
    using System;
    using System.Collections;

    internal interface IWrappedCollection : IEnumerable, IList, ICollection
    {
        object UnderlyingCollection { get; }
    }
}

