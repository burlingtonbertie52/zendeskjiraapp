﻿namespace Newtonsoft.Json.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;

    internal class XmlNodeWrapper : IXmlNode
    {
        private readonly System.Xml.XmlNode _node;

        public XmlNodeWrapper(System.Xml.XmlNode node)
        {
            this._node = node;
        }

        public IXmlNode AppendChild(IXmlNode newChild)
        {
            XmlNodeWrapper wrapper = (XmlNodeWrapper) newChild;
            this._node.AppendChild(wrapper._node);
            return newChild;
        }

        private IXmlNode WrapNode(System.Xml.XmlNode node)
        {
            XmlNodeType nodeType = node.NodeType;
            if (nodeType != XmlNodeType.Element)
            {
                if (nodeType == XmlNodeType.XmlDeclaration)
                {
                    return new XmlDeclarationWrapper((XmlDeclaration) node);
                }
                return new XmlNodeWrapper(node);
            }
            return new XmlElementWrapper((XmlElement) node);
        }

        public IList<IXmlNode> Attributes
        {
            get
            {
                if (this._node.Attributes == null)
                {
                    return null;
                }
                return (from a in this._node.Attributes.Cast<System.Xml.XmlAttribute>() select this.WrapNode(a)).ToList<IXmlNode>();
            }
        }

        public IList<IXmlNode> ChildNodes
        {
            get
            {
                return (from n in this._node.ChildNodes.Cast<System.Xml.XmlNode>() select this.WrapNode(n)).ToList<IXmlNode>();
            }
        }

        public string LocalName
        {
            get
            {
                return this._node.LocalName;
            }
        }

        public string Name
        {
            get
            {
                return this._node.Name;
            }
        }

        public string NamespaceUri
        {
            get
            {
                return this._node.NamespaceURI;
            }
        }

        public XmlNodeType NodeType
        {
            get
            {
                return this._node.NodeType;
            }
        }

        public IXmlNode ParentNode
        {
            get
            {
                System.Xml.XmlNode node = (this._node is System.Xml.XmlAttribute) ? ((System.Xml.XmlAttribute) this._node).OwnerElement : this._node.ParentNode;
                if (node == null)
                {
                    return null;
                }
                return this.WrapNode(node);
            }
        }

        public string Prefix
        {
            get
            {
                return this._node.Prefix;
            }
        }

        public string Value
        {
            get
            {
                return this._node.Value;
            }
            set
            {
                this._node.Value = value;
            }
        }

        public object WrappedNode
        {
            get
            {
                return this._node;
            }
        }
    }
}

