﻿namespace Newtonsoft.Json.Converters
{
    using System;

    internal interface IBinary
    {
        byte[] ToArray();
    }
}

