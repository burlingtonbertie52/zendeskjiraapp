﻿namespace Newtonsoft.Json.Converters
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Xml;
    using System.Xml.Linq;

    internal class XDeclarationWrapper : XObjectWrapper, IXmlNode, IXmlDeclaration
    {
        public XDeclarationWrapper(XDeclaration declaration) : base(null)
        {
            this.Declaration = declaration;
        }

        internal XDeclaration Declaration { get; private set; }

        public string Encoding
        {
            get
            {
                return this.Declaration.Encoding;
            }
            set
            {
                this.Declaration.Encoding = value;
            }
        }

        public override XmlNodeType NodeType
        {
            get
            {
                return XmlNodeType.XmlDeclaration;
            }
        }

        public string Standalone
        {
            get
            {
                return this.Declaration.Standalone;
            }
            set
            {
                this.Declaration.Standalone = value;
            }
        }

        public string Version
        {
            get
            {
                return this.Declaration.Version;
            }
        }
    }
}

