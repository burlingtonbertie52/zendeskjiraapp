﻿namespace Newtonsoft.Json.Converters
{
    using System;

    internal interface IXmlDeclaration : IXmlNode
    {
        string Encoding { get; set; }

        string Standalone { get; set; }

        string Version { get; }
    }
}

