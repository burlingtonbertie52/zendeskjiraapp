﻿namespace Newtonsoft.Json.Converters
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Bson;
    using System;
    using System.Globalization;
    using System.Text.RegularExpressions;

    public class RegexConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(Regex));
        }

        private bool HasFlag(RegexOptions options, RegexOptions flag)
        {
            return ((options & flag) == flag);
        }

        private object ReadBson(BsonReader reader)
        {
            string str = (string) reader.Value;
            int num = str.LastIndexOf('/');
            string pattern = str.Substring(1, num - 1);
            string str3 = str.Substring(num + 1);
            RegexOptions none = RegexOptions.None;
            foreach (char ch in str3)
            {
                switch (ch)
                {
                    case 's':
                        none |= RegexOptions.Singleline;
                        break;

                    case 'x':
                        none |= RegexOptions.ExplicitCapture;
                        break;

                    case 'i':
                        none |= RegexOptions.IgnoreCase;
                        break;

                    case 'm':
                        none |= RegexOptions.Multiline;
                        break;
                }
            }
            return new Regex(pattern, none);
        }

        private Regex ReadJson(JsonReader reader)
        {
            reader.Read();
            reader.Read();
            string pattern = (string) reader.Value;
            reader.Read();
            reader.Read();
            int num = Convert.ToInt32(reader.Value, CultureInfo.InvariantCulture);
            reader.Read();
            return new Regex(pattern, (RegexOptions) num);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            BsonReader reader2 = reader as BsonReader;
            if (reader2 != null)
            {
                return this.ReadBson(reader2);
            }
            return this.ReadJson(reader);
        }

        private void WriteBson(BsonWriter writer, Regex regex)
        {
            string options = null;
            if (this.HasFlag(regex.Options, RegexOptions.IgnoreCase))
            {
                options = options + "i";
            }
            if (this.HasFlag(regex.Options, RegexOptions.Multiline))
            {
                options = options + "m";
            }
            if (this.HasFlag(regex.Options, RegexOptions.Singleline))
            {
                options = options + "s";
            }
            options = options + "u";
            if (this.HasFlag(regex.Options, RegexOptions.ExplicitCapture))
            {
                options = options + "x";
            }
            writer.WriteRegex(regex.ToString(), options);
        }

        private void WriteJson(JsonWriter writer, Regex regex)
        {
            writer.WriteStartObject();
            writer.WritePropertyName("Pattern");
            writer.WriteValue(regex.ToString());
            writer.WritePropertyName("Options");
            writer.WriteValue(regex.Options);
            writer.WriteEndObject();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            Regex regex = (Regex) value;
            BsonWriter writer2 = writer as BsonWriter;
            if (writer2 != null)
            {
                this.WriteBson(writer2, regex);
            }
            else
            {
                this.WriteJson(writer, regex);
            }
        }
    }
}

