﻿namespace Newtonsoft.Json.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    internal class XElementWrapper : XContainerWrapper, IXmlNode, IXmlElement
    {
        public XElementWrapper(XElement element) : base(element)
        {
        }

        public string GetPrefixOfNamespace(string namespaceUri)
        {
            return this.Element.GetPrefixOfNamespace(namespaceUri);
        }

        public void SetAttributeNode(IXmlNode attribute)
        {
            XObjectWrapper wrapper = (XObjectWrapper) attribute;
            this.Element.Add(wrapper.WrappedNode);
        }

        public override IList<IXmlNode> Attributes
        {
            get
            {
                return (from a in this.Element.Attributes() select new XAttributeWrapper(a)).Cast<IXmlNode>().ToList<IXmlNode>();
            }
        }

        private XElement Element
        {
            get
            {
                return (XElement) base.WrappedNode;
            }
        }

        public override string LocalName
        {
            get
            {
                return this.Element.Name.LocalName;
            }
        }

        public override string NamespaceUri
        {
            get
            {
                return this.Element.Name.NamespaceName;
            }
        }

        public override string Value
        {
            get
            {
                return this.Element.Value;
            }
            set
            {
                this.Element.Value = value;
            }
        }
    }
}

