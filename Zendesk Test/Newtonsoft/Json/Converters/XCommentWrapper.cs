﻿namespace Newtonsoft.Json.Converters
{
    using System;
    using System.Xml.Linq;

    internal class XCommentWrapper : XObjectWrapper
    {
        public XCommentWrapper(XComment text) : base(text)
        {
        }

        public override IXmlNode ParentNode
        {
            get
            {
                if (this.Text.Parent == null)
                {
                    return null;
                }
                return XContainerWrapper.WrapNode(this.Text.Parent);
            }
        }

        private XComment Text
        {
            get
            {
                return (XComment) base.WrappedNode;
            }
        }

        public override string Value
        {
            get
            {
                return this.Text.Value;
            }
            set
            {
                this.Text.Value = value;
            }
        }
    }
}

