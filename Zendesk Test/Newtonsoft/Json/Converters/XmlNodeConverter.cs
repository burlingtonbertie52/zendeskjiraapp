﻿namespace Newtonsoft.Json.Converters
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Utilities;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Xml;
    using System.Xml.Linq;

    public class XmlNodeConverter : JsonConverter
    {
        private const string CDataName = "#cdata-section";
        private const string CommentName = "#comment";
        private const string DeclarationName = "?xml";
        private const string JsonNamespaceUri = "http://james.newtonking.com/projects/json";
        private const string SignificantWhitespaceName = "#significant-whitespace";
        private const string TextName = "#text";
        private const string WhitespaceName = "#whitespace";

        private void AddJsonArrayAttribute(IXmlElement element, IXmlDocument document)
        {
            element.SetAttributeNode(document.CreateAttribute("json:Array", "http://james.newtonking.com/projects/json", "true"));
            if ((element is XElementWrapper) && (element.GetPrefixOfNamespace("http://james.newtonking.com/projects/json") == null))
            {
                element.SetAttributeNode(document.CreateAttribute("xmlns:json", "http://www.w3.org/2000/xmlns/", "http://james.newtonking.com/projects/json"));
            }
        }

        public override bool CanConvert(Type valueType)
        {
            return (typeof(XObject).IsAssignableFrom(valueType) || typeof(System.Xml.XmlNode).IsAssignableFrom(valueType));
        }

        private string ConvertTokenToXmlValue(JsonReader reader)
        {
            if (reader.TokenType == JsonToken.String)
            {
                return reader.Value.ToString();
            }
            if (reader.TokenType == JsonToken.Integer)
            {
                return XmlConvert.ToString(Convert.ToInt64(reader.Value, CultureInfo.InvariantCulture));
            }
            if (reader.TokenType == JsonToken.Float)
            {
                return XmlConvert.ToString(Convert.ToDouble(reader.Value, CultureInfo.InvariantCulture));
            }
            if (reader.TokenType == JsonToken.Boolean)
            {
                return XmlConvert.ToString(Convert.ToBoolean(reader.Value, CultureInfo.InvariantCulture));
            }
            if (reader.TokenType == JsonToken.Date)
            {
                DateTime time = Convert.ToDateTime(reader.Value, CultureInfo.InvariantCulture);
                return XmlConvert.ToString(time, DateTimeUtils.ToSerializationMode(time.Kind));
            }
            if (reader.TokenType != JsonToken.Null)
            {
                throw JsonSerializationException.Create(reader, "Cannot get an XML string value from token type '{0}'.".FormatWith(CultureInfo.InvariantCulture, reader.TokenType));
            }
            return null;
        }

        private IXmlElement CreateElement(string elementName, IXmlDocument document, string elementPrefix, XmlNamespaceManager manager)
        {
            string str = string.IsNullOrEmpty(elementPrefix) ? manager.DefaultNamespace : manager.LookupNamespace(elementPrefix);
            return (!string.IsNullOrEmpty(str) ? document.CreateElement(elementName, str) : document.CreateElement(elementName));
        }

        private void CreateInstruction(JsonReader reader, IXmlDocument document, IXmlNode currentNode, string propertyName)
        {
            if (!(propertyName == "?xml"))
            {
                IXmlNode newChild = document.CreateProcessingInstruction(propertyName.Substring(1), reader.Value.ToString());
                currentNode.AppendChild(newChild);
            }
            else
            {
                string version = null;
                string encoding = null;
                string standalone = null;
                while (reader.Read() && (reader.TokenType != JsonToken.EndObject))
                {
                    string str4 = reader.Value.ToString();
                    if (str4 == null)
                    {
                        goto Label_0097;
                    }
                    if (!(str4 == "@version"))
                    {
                        if (str4 == "@encoding")
                        {
                            goto Label_006D;
                        }
                        if (str4 == "@standalone")
                        {
                            goto Label_0082;
                        }
                        goto Label_0097;
                    }
                    reader.Read();
                    version = reader.Value.ToString();
                    continue;
                Label_006D:
                    reader.Read();
                    encoding = reader.Value.ToString();
                    continue;
                Label_0082:
                    reader.Read();
                    standalone = reader.Value.ToString();
                    continue;
                Label_0097:
                    throw new JsonSerializationException("Unexpected property name encountered while deserializing XmlDeclaration: " + reader.Value);
                }
                IXmlNode node = document.CreateXmlDeclaration(version, encoding, standalone);
                currentNode.AppendChild(node);
            }
        }

        private void DeserializeNode(JsonReader reader, IXmlDocument document, XmlNamespaceManager manager, IXmlNode currentNode)
        {
            do
            {
                Func<IXmlElement, bool> predicate = null;
                string propertyName;
                switch (reader.TokenType)
                {
                    case JsonToken.StartConstructor:
                    {
                        string str = reader.Value.ToString();
                        while (reader.Read() && (reader.TokenType != JsonToken.EndConstructor))
                        {
                            this.DeserializeValue(reader, document, manager, str, currentNode);
                        }
                        break;
                    }
                    case JsonToken.PropertyName:
                        if ((currentNode.NodeType == XmlNodeType.Document) && (document.DocumentElement != null))
                        {
                            throw new JsonSerializationException("JSON root object has multiple properties. The root object must have a single property in order to create a valid XML document. Consider specifing a DeserializeRootElementName.");
                        }
                        propertyName = reader.Value.ToString();
                        reader.Read();
                        if (reader.TokenType == JsonToken.StartArray)
                        {
                            int num = 0;
                            while (reader.Read() && (reader.TokenType != JsonToken.EndArray))
                            {
                                this.DeserializeValue(reader, document, manager, propertyName, currentNode);
                                num++;
                            }
                            if ((num == 1) && this.WriteArrayAttribute)
                            {
                                if (predicate == null)
                                {
                                    predicate = n => n.LocalName == propertyName;
                                }
                                IXmlElement element = currentNode.ChildNodes.CastValid<IXmlElement>().Single<IXmlElement>(predicate);
                                this.AddJsonArrayAttribute(element, document);
                            }
                        }
                        else
                        {
                            this.DeserializeValue(reader, document, manager, propertyName, currentNode);
                        }
                        break;

                    case JsonToken.Comment:
                        currentNode.AppendChild(document.CreateComment((string) reader.Value));
                        break;

                    case JsonToken.EndObject:
                    case JsonToken.EndArray:
                        return;

                    default:
                        throw new JsonSerializationException("Unexpected JsonToken when deserializing node: " + reader.TokenType);
                }
            }
            while ((reader.TokenType == JsonToken.PropertyName) || reader.Read());
        }

        private void DeserializeValue(JsonReader reader, IXmlDocument document, XmlNamespaceManager manager, string propertyName, IXmlNode currentNode)
        {
            switch (propertyName)
            {
                case "#text":
                    currentNode.AppendChild(document.CreateTextNode(reader.Value.ToString()));
                    return;

                case "#cdata-section":
                    currentNode.AppendChild(document.CreateCDataSection(reader.Value.ToString()));
                    return;

                case "#whitespace":
                    currentNode.AppendChild(document.CreateWhitespace(reader.Value.ToString()));
                    return;

                case "#significant-whitespace":
                    currentNode.AppendChild(document.CreateSignificantWhitespace(reader.Value.ToString()));
                    return;
            }
            if (!string.IsNullOrEmpty(propertyName) && (propertyName[0] == '?'))
            {
                this.CreateInstruction(reader, document, currentNode, propertyName);
            }
            else if (reader.TokenType == JsonToken.StartArray)
            {
                this.ReadArrayElements(reader, document, propertyName, currentNode, manager);
            }
            else
            {
                this.ReadElement(reader, document, currentNode, propertyName, manager);
            }
        }

        private string GetPropertyName(IXmlNode node, XmlNamespaceManager manager)
        {
            switch (node.NodeType)
            {
                case XmlNodeType.Element:
                    return this.ResolveFullName(node, manager);

                case XmlNodeType.Attribute:
                    if (!(node.NamespaceUri == "http://james.newtonking.com/projects/json"))
                    {
                        return ("@" + this.ResolveFullName(node, manager));
                    }
                    return ("$" + node.LocalName);

                case XmlNodeType.Text:
                    return "#text";

                case XmlNodeType.CDATA:
                    return "#cdata-section";

                case XmlNodeType.ProcessingInstruction:
                    return ("?" + this.ResolveFullName(node, manager));

                case XmlNodeType.Comment:
                    return "#comment";

                case XmlNodeType.Whitespace:
                    return "#whitespace";

                case XmlNodeType.SignificantWhitespace:
                    return "#significant-whitespace";

                case XmlNodeType.XmlDeclaration:
                    return "?xml";
            }
            throw new JsonSerializationException("Unexpected XmlNodeType when getting node name: " + node.NodeType);
        }

        private bool IsArray(IXmlNode node)
        {
            IXmlNode node2 = (node.Attributes != null) ? node.Attributes.SingleOrDefault<IXmlNode>(a => ((a.LocalName == "Array") && (a.NamespaceUri == "http://james.newtonking.com/projects/json"))) : null;
            return ((node2 != null) && XmlConvert.ToBoolean(node2.Value));
        }

        private bool IsNamespaceAttribute(string attributeName, out string prefix)
        {
            if (attributeName.StartsWith("xmlns", StringComparison.Ordinal))
            {
                if (attributeName.Length == 5)
                {
                    prefix = string.Empty;
                    return true;
                }
                if (attributeName[5] == ':')
                {
                    prefix = attributeName.Substring(6, attributeName.Length - 6);
                    return true;
                }
            }
            prefix = null;
            return false;
        }

        private void PushParentNamespaces(IXmlNode node, XmlNamespaceManager manager)
        {
            List<IXmlNode> list = null;
            IXmlNode item = node;
            while ((item = item.ParentNode) != null)
            {
                if (item.NodeType == XmlNodeType.Element)
                {
                    if (list == null)
                    {
                        list = new List<IXmlNode>();
                    }
                    list.Add(item);
                }
            }
            if (list != null)
            {
                list.Reverse();
                foreach (IXmlNode node3 in list)
                {
                    manager.PushScope();
                    foreach (IXmlNode node4 in node3.Attributes)
                    {
                        if ((node4.NamespaceUri == "http://www.w3.org/2000/xmlns/") && (node4.LocalName != "xmlns"))
                        {
                            manager.AddNamespace(node4.LocalName, node4.Value);
                        }
                    }
                }
            }
        }

        private void ReadArrayElements(JsonReader reader, IXmlDocument document, string propertyName, IXmlNode currentNode, XmlNamespaceManager manager)
        {
            Func<IXmlElement, bool> predicate = null;
            string prefix = MiscellaneousUtils.GetPrefix(propertyName);
            IXmlElement newChild = this.CreateElement(propertyName, document, prefix, manager);
            currentNode.AppendChild(newChild);
            int num = 0;
            while (reader.Read() && (reader.TokenType != JsonToken.EndArray))
            {
                this.DeserializeValue(reader, document, manager, propertyName, newChild);
                num++;
            }
            if (this.WriteArrayAttribute)
            {
                this.AddJsonArrayAttribute(newChild, document);
            }
            if ((num == 1) && this.WriteArrayAttribute)
            {
                if (predicate == null)
                {
                    predicate = n => n.LocalName == propertyName;
                }
                IXmlElement element = newChild.ChildNodes.CastValid<IXmlElement>().Single<IXmlElement>(predicate);
                this.AddJsonArrayAttribute(element, document);
            }
        }

        private Dictionary<string, string> ReadAttributeElements(JsonReader reader, XmlNamespaceManager manager)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            bool flag = false;
            bool flag2 = false;
            if ((((reader.TokenType != JsonToken.String) && (reader.TokenType != JsonToken.Null)) && ((reader.TokenType != JsonToken.Boolean) && (reader.TokenType != JsonToken.Integer))) && (((reader.TokenType != JsonToken.Float) && (reader.TokenType != JsonToken.Date)) && (reader.TokenType != JsonToken.StartConstructor)))
            {
                while ((!flag && !flag2) && reader.Read())
                {
                    string str2;
                    JsonToken tokenType = reader.TokenType;
                    if (tokenType != JsonToken.PropertyName)
                    {
                        if (tokenType != JsonToken.EndObject)
                        {
                            throw new JsonSerializationException("Unexpected JsonToken: " + reader.TokenType);
                        }
                        goto Label_01A9;
                    }
                    string str = reader.Value.ToString();
                    if (string.IsNullOrEmpty(str))
                    {
                        goto Label_01A5;
                    }
                    char ch2 = str[0];
                    if (ch2 != '$')
                    {
                        string str3;
                        if (ch2 != '@')
                        {
                            goto Label_01A1;
                        }
                        str = str.Substring(1);
                        reader.Read();
                        str2 = this.ConvertTokenToXmlValue(reader);
                        dictionary.Add(str, str2);
                        if (this.IsNamespaceAttribute(str, out str3))
                        {
                            manager.AddNamespace(str3, str2);
                        }
                    }
                    else
                    {
                        str = str.Substring(1);
                        reader.Read();
                        str2 = reader.Value.ToString();
                        string prefix = manager.LookupPrefix("http://james.newtonking.com/projects/json");
                        if (prefix == null)
                        {
                            int? nullable = null;
                            while (manager.LookupNamespace("json" + nullable) != null)
                            {
                                nullable = new int?(nullable.GetValueOrDefault() + 1);
                            }
                            prefix = "json" + nullable;
                            dictionary.Add("xmlns:" + prefix, "http://james.newtonking.com/projects/json");
                            manager.AddNamespace(prefix, "http://james.newtonking.com/projects/json");
                        }
                        dictionary.Add(prefix + ":" + str, str2);
                    }
                    continue;
                Label_01A1:
                    flag = true;
                    continue;
                Label_01A5:
                    flag = true;
                    continue;
                Label_01A9:
                    flag2 = true;
                }
            }
            return dictionary;
        }

        private void ReadElement(JsonReader reader, IXmlDocument document, IXmlNode currentNode, string propertyName, XmlNamespaceManager manager)
        {
            if (string.IsNullOrEmpty(propertyName))
            {
                throw new JsonSerializationException("XmlNodeConverter cannot convert JSON with an empty property name to XML.");
            }
            Dictionary<string, string> dictionary = this.ReadAttributeElements(reader, manager);
            string prefix = MiscellaneousUtils.GetPrefix(propertyName);
            if (propertyName.StartsWith("@"))
            {
                string qualifiedName = propertyName.Substring(1);
                string str3 = reader.Value.ToString();
                string str4 = MiscellaneousUtils.GetPrefix(qualifiedName);
                IXmlNode attribute = !string.IsNullOrEmpty(str4) ? document.CreateAttribute(qualifiedName, manager.LookupNamespace(str4), str3) : document.CreateAttribute(qualifiedName, str3);
                ((IXmlElement) currentNode).SetAttributeNode(attribute);
            }
            else
            {
                IXmlElement newChild = this.CreateElement(propertyName, document, prefix, manager);
                currentNode.AppendChild(newChild);
                foreach (KeyValuePair<string, string> pair in dictionary)
                {
                    string str5 = MiscellaneousUtils.GetPrefix(pair.Key);
                    IXmlNode node2 = !string.IsNullOrEmpty(str5) ? document.CreateAttribute(pair.Key, manager.LookupNamespace(str5), pair.Value) : document.CreateAttribute(pair.Key, pair.Value);
                    newChild.SetAttributeNode(node2);
                }
                if (((reader.TokenType == JsonToken.String) || (reader.TokenType == JsonToken.Integer)) || (((reader.TokenType == JsonToken.Float) || (reader.TokenType == JsonToken.Boolean)) || (reader.TokenType == JsonToken.Date)))
                {
                    newChild.AppendChild(document.CreateTextNode(this.ConvertTokenToXmlValue(reader)));
                }
                else if ((reader.TokenType != JsonToken.Null) && (reader.TokenType != JsonToken.EndObject))
                {
                    manager.PushScope();
                    this.DeserializeNode(reader, document, manager, newChild);
                    manager.PopScope();
                }
            }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            XmlNamespaceManager manager = new XmlNamespaceManager(new NameTable());
            IXmlDocument document = null;
            IXmlNode currentNode = null;
            if (typeof(XObject).IsAssignableFrom(objectType))
            {
                if ((objectType != typeof(XDocument)) && (objectType != typeof(XElement)))
                {
                    throw new JsonSerializationException("XmlNodeConverter only supports deserializing XDocument or XElement.");
                }
                XDocument document2 = new XDocument();
                document = new XDocumentWrapper(document2);
                currentNode = document;
            }
            if (typeof(System.Xml.XmlNode).IsAssignableFrom(objectType))
            {
                if (objectType != typeof(XmlDocument))
                {
                    throw new JsonSerializationException("XmlNodeConverter only supports deserializing XmlDocuments");
                }
                XmlDocument document3 = new XmlDocument();
                document = new XmlDocumentWrapper(document3);
                currentNode = document;
            }
            if ((document == null) || (currentNode == null))
            {
                throw new JsonSerializationException("Unexpected type when converting XML: " + objectType);
            }
            if (reader.TokenType != JsonToken.StartObject)
            {
                throw new JsonSerializationException("XmlNodeConverter can only convert JSON that begins with an object.");
            }
            if (!string.IsNullOrEmpty(this.DeserializeRootElementName))
            {
                this.ReadElement(reader, document, currentNode, this.DeserializeRootElementName, manager);
            }
            else
            {
                reader.Read();
                this.DeserializeNode(reader, document, manager, currentNode);
            }
            if (objectType == typeof(XElement))
            {
                XElement wrappedNode = (XElement) document.DocumentElement.WrappedNode;
                wrappedNode.Remove();
                return wrappedNode;
            }
            return document.WrappedNode;
        }

        private string ResolveFullName(IXmlNode node, XmlNamespaceManager manager)
        {
            string str = ((node.NamespaceUri == null) || ((node.LocalName == "xmlns") && (node.NamespaceUri == "http://www.w3.org/2000/xmlns/"))) ? null : manager.LookupPrefix(node.NamespaceUri);
            if (!string.IsNullOrEmpty(str))
            {
                return (str + ":" + node.LocalName);
            }
            return node.LocalName;
        }

        private void SerializeGroupedNodes(JsonWriter writer, IXmlNode node, XmlNamespaceManager manager, bool writePropertyName)
        {
            Dictionary<string, List<IXmlNode>> dictionary = new Dictionary<string, List<IXmlNode>>();
            for (int i = 0; i < node.ChildNodes.Count; i++)
            {
                List<IXmlNode> list;
                IXmlNode node2 = node.ChildNodes[i];
                string propertyName = this.GetPropertyName(node2, manager);
                if (!dictionary.TryGetValue(propertyName, out list))
                {
                    list = new List<IXmlNode>();
                    dictionary.Add(propertyName, list);
                }
                list.Add(node2);
            }
            foreach (KeyValuePair<string, List<IXmlNode>> pair in dictionary)
            {
                bool flag;
                List<IXmlNode> list2 = pair.Value;
                if (list2.Count == 1)
                {
                    flag = this.IsArray(list2[0]);
                }
                else
                {
                    flag = true;
                }
                if (!flag)
                {
                    this.SerializeNode(writer, list2[0], manager, writePropertyName);
                }
                else
                {
                    string key = pair.Key;
                    if (writePropertyName)
                    {
                        writer.WritePropertyName(key);
                    }
                    writer.WriteStartArray();
                    for (int j = 0; j < list2.Count; j++)
                    {
                        this.SerializeNode(writer, list2[j], manager, false);
                    }
                    writer.WriteEndArray();
                }
            }
        }

        private void SerializeNode(JsonWriter writer, IXmlNode node, XmlNamespaceManager manager, bool writePropertyName)
        {
            Func<IXmlNode, bool> predicate = null;
            switch (node.NodeType)
            {
                case XmlNodeType.Element:
                    if (this.IsArray(node))
                    {
                        if (predicate == null)
                        {
                            predicate = n => n.LocalName == node.LocalName;
                        }
                        if (node.ChildNodes.All<IXmlNode>(predicate) && (node.ChildNodes.Count > 0))
                        {
                            this.SerializeGroupedNodes(writer, node, manager, false);
                            return;
                        }
                    }
                    foreach (IXmlNode node2 in node.Attributes)
                    {
                        if (node2.NamespaceUri == "http://www.w3.org/2000/xmlns/")
                        {
                            string prefix = (node2.LocalName != "xmlns") ? node2.LocalName : string.Empty;
                            manager.AddNamespace(prefix, node2.Value);
                        }
                    }
                    if (writePropertyName)
                    {
                        writer.WritePropertyName(this.GetPropertyName(node, manager));
                    }
                    if ((!this.ValueAttributes(node.Attributes).Any<IXmlNode>() && (node.ChildNodes.Count == 1)) && (node.ChildNodes[0].NodeType == XmlNodeType.Text))
                    {
                        writer.WriteValue(node.ChildNodes[0].Value);
                        return;
                    }
                    if ((node.ChildNodes.Count == 0) && CollectionUtils.IsNullOrEmpty<IXmlNode>(node.Attributes))
                    {
                        writer.WriteNull();
                        return;
                    }
                    writer.WriteStartObject();
                    for (int i = 0; i < node.Attributes.Count; i++)
                    {
                        this.SerializeNode(writer, node.Attributes[i], manager, true);
                    }
                    this.SerializeGroupedNodes(writer, node, manager, true);
                    writer.WriteEndObject();
                    return;

                case XmlNodeType.Attribute:
                case XmlNodeType.Text:
                case XmlNodeType.CDATA:
                case XmlNodeType.ProcessingInstruction:
                case XmlNodeType.Whitespace:
                case XmlNodeType.SignificantWhitespace:
                    if (!(node.NamespaceUri == "http://www.w3.org/2000/xmlns/") || !(node.Value == "http://james.newtonking.com/projects/json"))
                    {
                        if ((node.NamespaceUri != "http://james.newtonking.com/projects/json") || (node.LocalName != "Array"))
                        {
                            if (writePropertyName)
                            {
                                writer.WritePropertyName(this.GetPropertyName(node, manager));
                            }
                            writer.WriteValue(node.Value);
                        }
                        return;
                    }
                    return;

                case XmlNodeType.Comment:
                    if (writePropertyName)
                    {
                        writer.WriteComment(node.Value);
                    }
                    return;

                case XmlNodeType.Document:
                case XmlNodeType.DocumentFragment:
                    this.SerializeGroupedNodes(writer, node, manager, writePropertyName);
                    return;

                case XmlNodeType.XmlDeclaration:
                {
                    IXmlDeclaration declaration = (IXmlDeclaration) node;
                    writer.WritePropertyName(this.GetPropertyName(node, manager));
                    writer.WriteStartObject();
                    if (!string.IsNullOrEmpty(declaration.Version))
                    {
                        writer.WritePropertyName("@version");
                        writer.WriteValue(declaration.Version);
                    }
                    if (!string.IsNullOrEmpty(declaration.Encoding))
                    {
                        writer.WritePropertyName("@encoding");
                        writer.WriteValue(declaration.Encoding);
                    }
                    if (!string.IsNullOrEmpty(declaration.Standalone))
                    {
                        writer.WritePropertyName("@standalone");
                        writer.WriteValue(declaration.Standalone);
                    }
                    writer.WriteEndObject();
                    return;
                }
            }
            throw new JsonSerializationException("Unexpected XmlNodeType when serializing nodes: " + node.NodeType);
        }

        private IEnumerable<IXmlNode> ValueAttributes(IEnumerable<IXmlNode> c)
        {
            return (from a in c
                where a.NamespaceUri != "http://james.newtonking.com/projects/json"
                select a);
        }

        private IXmlNode WrapXml(object value)
        {
            if (value is XObject)
            {
                return XContainerWrapper.WrapNode((XObject) value);
            }
            if (!(value is System.Xml.XmlNode))
            {
                throw new ArgumentException("Value must be an XML object.", "value");
            }
            return new XmlNodeWrapper((System.Xml.XmlNode) value);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            IXmlNode node = this.WrapXml(value);
            XmlNamespaceManager manager = new XmlNamespaceManager(new NameTable());
            this.PushParentNamespaces(node, manager);
            if (!this.OmitRootObject)
            {
                writer.WriteStartObject();
            }
            this.SerializeNode(writer, node, manager, !this.OmitRootObject);
            if (!this.OmitRootObject)
            {
                writer.WriteEndObject();
            }
        }

        public string DeserializeRootElementName { get; set; }

        public bool OmitRootObject { get; set; }

        public bool WriteArrayAttribute { get; set; }
    }
}

