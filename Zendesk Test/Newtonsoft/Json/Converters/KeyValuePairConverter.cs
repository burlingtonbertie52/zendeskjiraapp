﻿namespace Newtonsoft.Json.Converters
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;
    using Newtonsoft.Json.Utilities;
    using System;
    using System.Collections.Generic;
    using System.Reflection;

    public class KeyValuePairConverter : JsonConverter
    {
        private const string KeyName = "Key";
        private const string ValueName = "Value";

        public override bool CanConvert(Type objectType)
        {
            Type type = ReflectionUtils.IsNullableType(objectType) ? Nullable.GetUnderlyingType(objectType) : objectType;
            return ((type.IsValueType() && type.IsGenericType()) && (type.GetGenericTypeDefinition() == typeof(KeyValuePair<,>)));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            bool flag = ReflectionUtils.IsNullableType(objectType);
            if (reader.TokenType == JsonToken.Null)
            {
                if (!flag)
                {
                    throw JsonSerializationException.Create(reader, "Cannot convert null value to KeyValuePair.");
                }
                return null;
            }
            Type type = flag ? Nullable.GetUnderlyingType(objectType) : objectType;
            IList<Type> genericArguments = type.GetGenericArguments();
            Type type2 = genericArguments[0];
            Type type3 = genericArguments[1];
            object obj2 = null;
            object obj3 = null;
            reader.Read();
            while (reader.TokenType == JsonToken.PropertyName)
            {
                string a = reader.Value.ToString();
                if (string.Equals(a, "Key", StringComparison.OrdinalIgnoreCase))
                {
                    reader.Read();
                    obj2 = serializer.Deserialize(reader, type2);
                }
                else if (string.Equals(a, "Value", StringComparison.OrdinalIgnoreCase))
                {
                    reader.Read();
                    obj3 = serializer.Deserialize(reader, type3);
                }
                else
                {
                    reader.Skip();
                }
                reader.Read();
            }
            return ReflectionUtils.CreateInstance(type, new object[] { obj2, obj3 });
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            Type type = value.GetType();
            PropertyInfo property = type.GetProperty("Key");
            PropertyInfo member = type.GetProperty("Value");
            DefaultContractResolver contractResolver = serializer.ContractResolver as DefaultContractResolver;
            writer.WriteStartObject();
            writer.WritePropertyName((contractResolver != null) ? contractResolver.GetResolvedPropertyName("Key") : "Key");
            serializer.Serialize(writer, ReflectionUtils.GetMemberValue(property, value));
            writer.WritePropertyName((contractResolver != null) ? contractResolver.GetResolvedPropertyName("Value") : "Value");
            serializer.Serialize(writer, ReflectionUtils.GetMemberValue(member, value));
            writer.WriteEndObject();
        }
    }
}

