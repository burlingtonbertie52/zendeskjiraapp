﻿namespace Newtonsoft.Json
{
    using Newtonsoft.Json.Linq;
    using Newtonsoft.Json.Schema;
    using Newtonsoft.Json.Utilities;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Text.RegularExpressions;
    using System.Threading;

    public class JsonValidatingReader : JsonReader, IJsonLineInfo
    {
        private SchemaScope _currentScope;
        private JsonSchemaModel _model;
        private readonly JsonReader _reader;
        private JsonSchema _schema;
        private readonly Stack<SchemaScope> _stack;

        public event Newtonsoft.Json.Schema.ValidationEventHandler ValidationEventHandler;

        public JsonValidatingReader(JsonReader reader)
        {
            ValidationUtils.ArgumentNotNull(reader, "reader");
            this._reader = reader;
            this._stack = new Stack<SchemaScope>();
        }

        private JsonSchemaType? GetCurrentNodeSchemaType()
        {
            switch (this._reader.TokenType)
            {
                case JsonToken.StartObject:
                    return 0x10;

                case JsonToken.StartArray:
                    return 0x20;

                case JsonToken.Integer:
                    return 4;

                case JsonToken.Float:
                    return 2;

                case JsonToken.String:
                    return 1;

                case JsonToken.Boolean:
                    return 8;

                case JsonToken.Null:
                    return 0x40;
            }
            return null;
        }

        private bool IsPropertyDefinied(JsonSchemaModel schema, string propertyName)
        {
            if ((schema.Properties != null) && schema.Properties.ContainsKey(propertyName))
            {
                return true;
            }
            if (schema.PatternProperties != null)
            {
                foreach (string str in schema.PatternProperties.Keys)
                {
                    if (Regex.IsMatch(propertyName, str))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private static bool IsZero(double value)
        {
            return (Math.Abs(value) < 2.2204460492503131E-15);
        }

        bool IJsonLineInfo.HasLineInfo()
        {
            IJsonLineInfo info = this._reader as IJsonLineInfo;
            return ((info != null) && info.HasLineInfo());
        }

        private void OnValidationEvent(JsonSchemaException exception)
        {
            Newtonsoft.Json.Schema.ValidationEventHandler validationEventHandler = this.ValidationEventHandler;
            if (validationEventHandler == null)
            {
                throw exception;
            }
            validationEventHandler(this, new ValidationEventArgs(exception));
        }

        private SchemaScope Pop()
        {
            SchemaScope scope = this._stack.Pop();
            this._currentScope = (this._stack.Count != 0) ? this._stack.Peek() : null;
            return scope;
        }

        private void ProcessValue()
        {
            if ((this._currentScope != null) && (this._currentScope.TokenType == JTokenType.Array))
            {
                this._currentScope.ArrayItemCount++;
                foreach (JsonSchemaModel model in this.CurrentSchemas)
                {
                    if (((model != null) && (model.Items != null)) && ((model.Items.Count > 1) && (this._currentScope.ArrayItemCount >= model.Items.Count)))
                    {
                        this.RaiseError("Index {0} has not been defined and the schema does not allow additional items.".FormatWith(CultureInfo.InvariantCulture, this._currentScope.ArrayItemCount), model);
                    }
                }
            }
        }

        private void Push(SchemaScope scope)
        {
            this._stack.Push(scope);
            this._currentScope = scope;
        }

        private void RaiseError(string message, JsonSchemaModel schema)
        {
            IJsonLineInfo info = this;
            string str = info.HasLineInfo() ? (message + " Line {0}, position {1}.".FormatWith(CultureInfo.InvariantCulture, info.LineNumber, info.LinePosition)) : message;
            this.OnValidationEvent(new JsonSchemaException(str, null, this.Path, info.LineNumber, info.LinePosition));
        }

        public override bool Read()
        {
            if (!this._reader.Read())
            {
                return false;
            }
            if (this._reader.TokenType != JsonToken.Comment)
            {
                this.ValidateCurrentToken();
            }
            return true;
        }

        public override byte[] ReadAsBytes()
        {
            byte[] buffer = this._reader.ReadAsBytes();
            this.ValidateCurrentToken();
            return buffer;
        }

        public override DateTime? ReadAsDateTime()
        {
            DateTime? nullable = this._reader.ReadAsDateTime();
            this.ValidateCurrentToken();
            return nullable;
        }

        public override DateTimeOffset? ReadAsDateTimeOffset()
        {
            DateTimeOffset? nullable = this._reader.ReadAsDateTimeOffset();
            this.ValidateCurrentToken();
            return nullable;
        }

        public override decimal? ReadAsDecimal()
        {
            decimal? nullable = this._reader.ReadAsDecimal();
            this.ValidateCurrentToken();
            return nullable;
        }

        public override int? ReadAsInt32()
        {
            int? nullable = this._reader.ReadAsInt32();
            this.ValidateCurrentToken();
            return nullable;
        }

        public override string ReadAsString()
        {
            string str = this._reader.ReadAsString();
            this.ValidateCurrentToken();
            return str;
        }

        private bool TestType(JsonSchemaModel currentSchema, JsonSchemaType currentType)
        {
            if (!JsonSchemaGenerator.HasFlag(new JsonSchemaType?(currentSchema.Type), currentType))
            {
                this.RaiseError("Invalid type. Expected {0} but got {1}.".FormatWith(CultureInfo.InvariantCulture, currentSchema.Type, currentType), currentSchema);
                return false;
            }
            return true;
        }

        private bool ValidateArray(JsonSchemaModel schema)
        {
            return ((schema == null) || this.TestType(schema, JsonSchemaType.Array));
        }

        private void ValidateBoolean(JsonSchemaModel schema)
        {
            if ((schema != null) && this.TestType(schema, JsonSchemaType.Boolean))
            {
                this.ValidateInEnumAndNotDisallowed(schema);
            }
        }

        private void ValidateCurrentToken()
        {
            if (this._model == null)
            {
                this._model = new JsonSchemaModelBuilder().Build(this._schema);
            }
            switch (this._reader.TokenType)
            {
                case JsonToken.None:
                case JsonToken.Raw:
                case JsonToken.Undefined:
                case JsonToken.Date:
                case JsonToken.Bytes:
                    return;

                case JsonToken.StartObject:
                {
                    this.ProcessValue();
                    IList<JsonSchemaModel> schemas = this.CurrentMemberSchemas.Where<JsonSchemaModel>(new Func<JsonSchemaModel, bool>(this.ValidateObject)).ToList<JsonSchemaModel>();
                    this.Push(new SchemaScope(JTokenType.Object, schemas));
                    return;
                }
                case JsonToken.StartArray:
                {
                    this.ProcessValue();
                    IList<JsonSchemaModel> list2 = this.CurrentMemberSchemas.Where<JsonSchemaModel>(new Func<JsonSchemaModel, bool>(this.ValidateArray)).ToList<JsonSchemaModel>();
                    this.Push(new SchemaScope(JTokenType.Array, list2));
                    return;
                }
                case JsonToken.StartConstructor:
                    this.Push(new SchemaScope(JTokenType.Constructor, null));
                    return;

                case JsonToken.PropertyName:
                    foreach (JsonSchemaModel model in this.CurrentSchemas)
                    {
                        this.ValidatePropertyName(model);
                    }
                    return;

                case JsonToken.Integer:
                    this.ProcessValue();
                    foreach (JsonSchemaModel model2 in this.CurrentMemberSchemas)
                    {
                        this.ValidateInteger(model2);
                    }
                    return;

                case JsonToken.Float:
                    this.ProcessValue();
                    foreach (JsonSchemaModel model3 in this.CurrentMemberSchemas)
                    {
                        this.ValidateFloat(model3);
                    }
                    return;

                case JsonToken.String:
                    this.ProcessValue();
                    foreach (JsonSchemaModel model4 in this.CurrentMemberSchemas)
                    {
                        this.ValidateString(model4);
                    }
                    return;

                case JsonToken.Boolean:
                    this.ProcessValue();
                    foreach (JsonSchemaModel model5 in this.CurrentMemberSchemas)
                    {
                        this.ValidateBoolean(model5);
                    }
                    return;

                case JsonToken.Null:
                    this.ProcessValue();
                    foreach (JsonSchemaModel model6 in this.CurrentMemberSchemas)
                    {
                        this.ValidateNull(model6);
                    }
                    return;

                case JsonToken.EndObject:
                    foreach (JsonSchemaModel model7 in this.CurrentSchemas)
                    {
                        this.ValidateEndObject(model7);
                    }
                    this.Pop();
                    return;

                case JsonToken.EndArray:
                    foreach (JsonSchemaModel model8 in this.CurrentSchemas)
                    {
                        this.ValidateEndArray(model8);
                    }
                    this.Pop();
                    return;

                case JsonToken.EndConstructor:
                    this.Pop();
                    return;
            }
            throw new ArgumentOutOfRangeException();
        }

        private void ValidateEndArray(JsonSchemaModel schema)
        {
            if (schema != null)
            {
                int arrayItemCount = this._currentScope.ArrayItemCount;
                if (schema.MaximumItems.HasValue)
                {
                    if (arrayItemCount > schema.MaximumItems)
                    {
                        this.RaiseError("Array item count {0} exceeds maximum count of {1}.".FormatWith(CultureInfo.InvariantCulture, arrayItemCount, schema.MaximumItems), schema);
                    }
                }
                if (schema.MinimumItems.HasValue)
                {
                    if (arrayItemCount < schema.MinimumItems)
                    {
                        this.RaiseError("Array item count {0} is less than minimum count of {1}.".FormatWith(CultureInfo.InvariantCulture, arrayItemCount, schema.MinimumItems), schema);
                    }
                }
            }
        }

        private void ValidateEndObject(JsonSchemaModel schema)
        {
            if (schema != null)
            {
                Dictionary<string, bool> requiredProperties = this._currentScope.RequiredProperties;
                if (requiredProperties != null)
                {
                    List<string> list = (from kv in requiredProperties
                        where !kv.Value
                        select kv.Key).ToList<string>();
                    if (list.Count > 0)
                    {
                        this.RaiseError("Required properties are missing from object: {0}.".FormatWith(CultureInfo.InvariantCulture, string.Join(", ", list.ToArray())), schema);
                    }
                }
            }
        }

        private void ValidateFloat(JsonSchemaModel schema)
        {
            if ((schema != null) && this.TestType(schema, JsonSchemaType.Float))
            {
                this.ValidateInEnumAndNotDisallowed(schema);
                double num = Convert.ToDouble(this._reader.Value, CultureInfo.InvariantCulture);
                if (schema.Maximum.HasValue)
                {
                    if (num > schema.Maximum)
                    {
                        this.RaiseError("Float {0} exceeds maximum value of {1}.".FormatWith(CultureInfo.InvariantCulture, JsonConvert.ToString(num), schema.Maximum), schema);
                    }
                    if (schema.ExclusiveMaximum)
                    {
                        if (num == schema.Maximum)
                        {
                            this.RaiseError("Float {0} equals maximum value of {1} and exclusive maximum is true.".FormatWith(CultureInfo.InvariantCulture, JsonConvert.ToString(num), schema.Maximum), schema);
                        }
                    }
                }
                if (schema.Minimum.HasValue)
                {
                    if (num < schema.Minimum)
                    {
                        this.RaiseError("Float {0} is less than minimum value of {1}.".FormatWith(CultureInfo.InvariantCulture, JsonConvert.ToString(num), schema.Minimum), schema);
                    }
                    if (schema.ExclusiveMinimum)
                    {
                        if (num == schema.Minimum)
                        {
                            this.RaiseError("Float {0} equals minimum value of {1} and exclusive minimum is true.".FormatWith(CultureInfo.InvariantCulture, JsonConvert.ToString(num), schema.Minimum), schema);
                        }
                    }
                }
                if (schema.DivisibleBy.HasValue && !IsZero(num % schema.DivisibleBy.Value))
                {
                    this.RaiseError("Float {0} is not evenly divisible by {1}.".FormatWith(CultureInfo.InvariantCulture, JsonConvert.ToString(num), schema.DivisibleBy), schema);
                }
            }
        }

        private void ValidateInEnumAndNotDisallowed(JsonSchemaModel schema)
        {
            if (schema != null)
            {
                JToken token = new JValue(this._reader.Value);
                if (schema.Enum != null)
                {
                    StringWriter textWriter = new StringWriter(CultureInfo.InvariantCulture);
                    token.WriteTo(new JsonTextWriter(textWriter), new JsonConverter[0]);
                    if (!schema.Enum.ContainsValue<JToken>(token, new JTokenEqualityComparer()))
                    {
                        this.RaiseError("Value {0} is not defined in enum.".FormatWith(CultureInfo.InvariantCulture, textWriter.ToString()), schema);
                    }
                }
                JsonSchemaType? currentNodeSchemaType = this.GetCurrentNodeSchemaType();
                if (currentNodeSchemaType.HasValue && JsonSchemaGenerator.HasFlag(new JsonSchemaType?(schema.Disallow), currentNodeSchemaType.Value))
                {
                    this.RaiseError("Type {0} is disallowed.".FormatWith(CultureInfo.InvariantCulture, currentNodeSchemaType), schema);
                }
            }
        }

        private void ValidateInteger(JsonSchemaModel schema)
        {
            if ((schema != null) && this.TestType(schema, JsonSchemaType.Integer))
            {
                this.ValidateInEnumAndNotDisallowed(schema);
                long num = Convert.ToInt64(this._reader.Value, CultureInfo.InvariantCulture);
                if (schema.Maximum.HasValue)
                {
                    if (num > schema.Maximum)
                    {
                        this.RaiseError("Integer {0} exceeds maximum value of {1}.".FormatWith(CultureInfo.InvariantCulture, num, schema.Maximum), schema);
                    }
                    if (schema.ExclusiveMaximum)
                    {
                        if (num == schema.Maximum)
                        {
                            this.RaiseError("Integer {0} equals maximum value of {1} and exclusive maximum is true.".FormatWith(CultureInfo.InvariantCulture, num, schema.Maximum), schema);
                        }
                    }
                }
                if (schema.Minimum.HasValue)
                {
                    if (num < schema.Minimum)
                    {
                        this.RaiseError("Integer {0} is less than minimum value of {1}.".FormatWith(CultureInfo.InvariantCulture, num, schema.Minimum), schema);
                    }
                    if (schema.ExclusiveMinimum)
                    {
                        if (num == schema.Minimum)
                        {
                            this.RaiseError("Integer {0} equals minimum value of {1} and exclusive minimum is true.".FormatWith(CultureInfo.InvariantCulture, num, schema.Minimum), schema);
                        }
                    }
                }
                if (schema.DivisibleBy.HasValue && !IsZero(((double) num) % schema.DivisibleBy.Value))
                {
                    this.RaiseError("Integer {0} is not evenly divisible by {1}.".FormatWith(CultureInfo.InvariantCulture, JsonConvert.ToString(num), schema.DivisibleBy), schema);
                }
            }
        }

        private void ValidateNull(JsonSchemaModel schema)
        {
            if ((schema != null) && this.TestType(schema, JsonSchemaType.Null))
            {
                this.ValidateInEnumAndNotDisallowed(schema);
            }
        }

        private bool ValidateObject(JsonSchemaModel schema)
        {
            return ((schema == null) || this.TestType(schema, JsonSchemaType.Object));
        }

        private void ValidatePropertyName(JsonSchemaModel schema)
        {
            if (schema != null)
            {
                string key = Convert.ToString(this._reader.Value, CultureInfo.InvariantCulture);
                if (this._currentScope.RequiredProperties.ContainsKey(key))
                {
                    this._currentScope.RequiredProperties[key] = true;
                }
                if (!schema.AllowAdditionalProperties && !this.IsPropertyDefinied(schema, key))
                {
                    this.RaiseError("Property '{0}' has not been defined and the schema does not allow additional properties.".FormatWith(CultureInfo.InvariantCulture, key), schema);
                }
                this._currentScope.CurrentPropertyName = key;
            }
        }

        private void ValidateString(JsonSchemaModel schema)
        {
            if ((schema != null) && this.TestType(schema, JsonSchemaType.String))
            {
                this.ValidateInEnumAndNotDisallowed(schema);
                string str = this._reader.Value.ToString();
                if (schema.MaximumLength.HasValue)
                {
                    if (str.Length > schema.MaximumLength)
                    {
                        this.RaiseError("String '{0}' exceeds maximum length of {1}.".FormatWith(CultureInfo.InvariantCulture, str, schema.MaximumLength), schema);
                    }
                }
                if (schema.MinimumLength.HasValue)
                {
                    if (str.Length < schema.MinimumLength)
                    {
                        this.RaiseError("String '{0}' is less than minimum length of {1}.".FormatWith(CultureInfo.InvariantCulture, str, schema.MinimumLength), schema);
                    }
                }
                if (schema.Patterns != null)
                {
                    foreach (string str2 in schema.Patterns)
                    {
                        if (!Regex.IsMatch(str, str2))
                        {
                            this.RaiseError("String '{0}' does not match regex pattern '{1}'.".FormatWith(CultureInfo.InvariantCulture, str, str2), schema);
                        }
                    }
                }
            }
        }

        private IEnumerable<JsonSchemaModel> CurrentMemberSchemas
        {
            get
            {
                if (this._currentScope == null)
                {
                    return new List<JsonSchemaModel>(new JsonSchemaModel[] { this._model });
                }
                if ((this._currentScope.Schemas == null) || (this._currentScope.Schemas.Count == 0))
                {
                    return Enumerable.Empty<JsonSchemaModel>();
                }
                switch (this._currentScope.TokenType)
                {
                    case JTokenType.None:
                        return this._currentScope.Schemas;

                    case JTokenType.Object:
                    {
                        if (this._currentScope.CurrentPropertyName == null)
                        {
                            throw new JsonReaderException("CurrentPropertyName has not been set on scope.");
                        }
                        IList<JsonSchemaModel> list = new List<JsonSchemaModel>();
                        foreach (JsonSchemaModel model in this.CurrentSchemas)
                        {
                            JsonSchemaModel model2;
                            if ((model.Properties != null) && model.Properties.TryGetValue(this._currentScope.CurrentPropertyName, out model2))
                            {
                                list.Add(model2);
                            }
                            if (model.PatternProperties != null)
                            {
                                foreach (KeyValuePair<string, JsonSchemaModel> pair in model.PatternProperties)
                                {
                                    if (Regex.IsMatch(this._currentScope.CurrentPropertyName, pair.Key))
                                    {
                                        list.Add(pair.Value);
                                    }
                                }
                            }
                            if (((list.Count == 0) && model.AllowAdditionalProperties) && (model.AdditionalProperties != null))
                            {
                                list.Add(model.AdditionalProperties);
                            }
                        }
                        return list;
                    }
                    case JTokenType.Array:
                    {
                        IList<JsonSchemaModel> list2 = new List<JsonSchemaModel>();
                        foreach (JsonSchemaModel model3 in this.CurrentSchemas)
                        {
                            if (!CollectionUtils.IsNullOrEmpty<JsonSchemaModel>(model3.Items))
                            {
                                if (model3.Items.Count == 1)
                                {
                                    list2.Add(model3.Items[0]);
                                }
                                else if (model3.Items.Count > (this._currentScope.ArrayItemCount - 1))
                                {
                                    list2.Add(model3.Items[this._currentScope.ArrayItemCount - 1]);
                                }
                            }
                            if (model3.AllowAdditionalProperties && (model3.AdditionalProperties != null))
                            {
                                list2.Add(model3.AdditionalProperties);
                            }
                        }
                        return list2;
                    }
                    case JTokenType.Constructor:
                        return Enumerable.Empty<JsonSchemaModel>();
                }
                throw new ArgumentOutOfRangeException("TokenType", "Unexpected token type: {0}".FormatWith(CultureInfo.InvariantCulture, this._currentScope.TokenType));
            }
        }

        private IEnumerable<JsonSchemaModel> CurrentSchemas
        {
            get
            {
                return this._currentScope.Schemas;
            }
        }

        public override int Depth
        {
            get
            {
                return this._reader.Depth;
            }
        }

        int IJsonLineInfo.LineNumber
        {
            get
            {
                IJsonLineInfo info = this._reader as IJsonLineInfo;
                if (info == null)
                {
                    return 0;
                }
                return info.LineNumber;
            }
        }

        int IJsonLineInfo.LinePosition
        {
            get
            {
                IJsonLineInfo info = this._reader as IJsonLineInfo;
                if (info == null)
                {
                    return 0;
                }
                return info.LinePosition;
            }
        }

        public override string Path
        {
            get
            {
                return this._reader.Path;
            }
        }

        public override char QuoteChar
        {
            get
            {
                return this._reader.QuoteChar;
            }
            protected internal set
            {
            }
        }

        public JsonReader Reader
        {
            get
            {
                return this._reader;
            }
        }

        public JsonSchema Schema
        {
            get
            {
                return this._schema;
            }
            set
            {
                if (this.TokenType != JsonToken.None)
                {
                    throw new InvalidOperationException("Cannot change schema while validating JSON.");
                }
                this._schema = value;
                this._model = null;
            }
        }

        public override JsonToken TokenType
        {
            get
            {
                return this._reader.TokenType;
            }
        }

        public override object Value
        {
            get
            {
                return this._reader.Value;
            }
        }

        public override Type ValueType
        {
            get
            {
                return this._reader.ValueType;
            }
        }

        private class SchemaScope
        {
            private readonly Dictionary<string, bool> _requiredProperties;
            private readonly IList<JsonSchemaModel> _schemas;
            private readonly JTokenType _tokenType;

            public SchemaScope(JTokenType tokenType, IList<JsonSchemaModel> schemas)
            {
                this._tokenType = tokenType;
                this._schemas = schemas;
                this._requiredProperties = schemas.SelectMany<JsonSchemaModel, string>(new Func<JsonSchemaModel, IEnumerable<string>>(this.GetRequiredProperties)).Distinct<string>().ToDictionary<string, string, bool>(p => p, p => false);
            }

            private IEnumerable<string> GetRequiredProperties(JsonSchemaModel schema)
            {
                if ((schema == null) || (schema.Properties == null))
                {
                    return Enumerable.Empty<string>();
                }
                return (from p in schema.Properties
                    where p.Value.Required
                    select p.Key);
            }

            public int ArrayItemCount { get; set; }

            public string CurrentPropertyName { get; set; }

            public Dictionary<string, bool> RequiredProperties
            {
                get
                {
                    return this._requiredProperties;
                }
            }

            public IList<JsonSchemaModel> Schemas
            {
                get
                {
                    return this._schemas;
                }
            }

            public JTokenType TokenType
            {
                get
                {
                    return this._tokenType;
                }
            }
        }
    }
}

