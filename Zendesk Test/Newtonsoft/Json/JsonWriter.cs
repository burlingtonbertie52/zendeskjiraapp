﻿namespace Newtonsoft.Json
{
    using Newtonsoft.Json.Utilities;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Runtime.CompilerServices;

    public abstract class JsonWriter : IDisposable
    {
        private JsonPosition _currentPosition;
        private State _currentState = State.Start;
        private Newtonsoft.Json.DateFormatHandling _dateFormatHandling;
        private Newtonsoft.Json.DateTimeZoneHandling _dateTimeZoneHandling = Newtonsoft.Json.DateTimeZoneHandling.RoundtripKind;
        private Newtonsoft.Json.Formatting _formatting = Newtonsoft.Json.Formatting.None;
        private readonly List<JsonPosition> _stack = new List<JsonPosition>(4);
        private Newtonsoft.Json.StringEscapeHandling _stringEscapeHandling;
        private static readonly State[][] StateArray;
        internal static readonly State[][] StateArrayTempate;

        static JsonWriter()
        {
            State[][] stateArray = new State[8][];
            stateArray[0] = new State[] { State.Error, State.Error, State.Error, State.Error, State.Error, State.Error, State.Error, State.Error, State.Error, State.Error };
            stateArray[1] = new State[] { State.ObjectStart, State.ObjectStart, State.Error, State.Error, State.ObjectStart, State.ObjectStart, State.ObjectStart, State.ObjectStart, State.Error, State.Error };
            stateArray[2] = new State[] { State.ArrayStart, State.ArrayStart, State.Error, State.Error, State.ArrayStart, State.ArrayStart, State.ArrayStart, State.ArrayStart, State.Error, State.Error };
            stateArray[3] = new State[] { State.ConstructorStart, State.ConstructorStart, State.Error, State.Error, State.ConstructorStart, State.ConstructorStart, State.ConstructorStart, State.ConstructorStart, State.Error, State.Error };
            stateArray[4] = new State[] { State.Property, State.Error, State.Property, State.Property, State.Error, State.Error, State.Error, State.Error, State.Error, State.Error };
            State[] stateArray7 = new State[10];
            stateArray7[1] = State.Property;
            stateArray7[2] = State.ObjectStart;
            stateArray7[3] = State.Object;
            stateArray7[4] = State.ArrayStart;
            stateArray7[5] = State.Array;
            stateArray7[6] = State.Constructor;
            stateArray7[7] = State.Constructor;
            stateArray7[8] = State.Error;
            stateArray7[9] = State.Error;
            stateArray[5] = stateArray7;
            State[] stateArray8 = new State[10];
            stateArray8[1] = State.Property;
            stateArray8[2] = State.ObjectStart;
            stateArray8[3] = State.Object;
            stateArray8[4] = State.ArrayStart;
            stateArray8[5] = State.Array;
            stateArray8[6] = State.Constructor;
            stateArray8[7] = State.Constructor;
            stateArray8[8] = State.Error;
            stateArray8[9] = State.Error;
            stateArray[6] = stateArray8;
            State[] stateArray9 = new State[10];
            stateArray9[1] = State.Object;
            stateArray9[2] = State.Error;
            stateArray9[3] = State.Error;
            stateArray9[4] = State.Array;
            stateArray9[5] = State.Array;
            stateArray9[6] = State.Constructor;
            stateArray9[7] = State.Constructor;
            stateArray9[8] = State.Error;
            stateArray9[9] = State.Error;
            stateArray[7] = stateArray9;
            StateArrayTempate = stateArray;
            StateArray = BuildStateArray();
        }

        protected JsonWriter()
        {
            this.CloseOutput = true;
        }

        internal void AutoComplete(JsonToken tokenBeingWritten)
        {
            State state = StateArray[(int) tokenBeingWritten][(int) this._currentState];
            if (state == State.Error)
            {
                throw JsonWriterException.Create(this, "Token {0} in state {1} would result in an invalid JSON object.".FormatWith(CultureInfo.InvariantCulture, tokenBeingWritten.ToString(), this._currentState.ToString()), null);
            }
            if ((((this._currentState == State.Object) || (this._currentState == State.Array)) || (this._currentState == State.Constructor)) && (tokenBeingWritten != JsonToken.Comment))
            {
                this.WriteValueDelimiter();
            }
            if (this._formatting == Newtonsoft.Json.Formatting.Indented)
            {
                if (this._currentState == State.Property)
                {
                    this.WriteIndentSpace();
                }
                if (((this._currentState == State.Array) || (this._currentState == State.ArrayStart)) || (((this._currentState == State.Constructor) || (this._currentState == State.ConstructorStart)) || ((tokenBeingWritten == JsonToken.PropertyName) && (this._currentState != State.Start))))
                {
                    this.WriteIndent();
                }
            }
            this._currentState = state;
        }

        private void AutoCompleteAll()
        {
            while (this.Top > 0)
            {
                this.WriteEnd();
            }
        }

        private void AutoCompleteClose(JsonContainerType type)
        {
            int num = 0;
            if (this._currentPosition.Type == type)
            {
                num = 1;
            }
            else
            {
                int num2 = this.Top - 2;
                for (int j = num2; j >= 0; j--)
                {
                    int num4 = num2 - j;
                    if (this._stack[num4].Type == type)
                    {
                        num = j + 2;
                        break;
                    }
                }
            }
            if (num == 0)
            {
                throw JsonWriterException.Create(this, "No token to close.", null);
            }
            for (int i = 0; i < num; i++)
            {
                JsonToken closeTokenForType = this.GetCloseTokenForType(this.Pop());
                if (this._currentState == State.Property)
                {
                    this.WriteNull();
                }
                if (((this._formatting == Newtonsoft.Json.Formatting.Indented) && (this._currentState != State.ObjectStart)) && (this._currentState != State.ArrayStart))
                {
                    this.WriteIndent();
                }
                this.WriteEnd(closeTokenForType);
                JsonContainerType type2 = this.Peek();
                switch (type2)
                {
                    case JsonContainerType.None:
                        this._currentState = State.Start;
                        break;

                    case JsonContainerType.Object:
                        this._currentState = State.Object;
                        break;

                    case JsonContainerType.Array:
                        this._currentState = State.Array;
                        break;

                    case JsonContainerType.Constructor:
                        this._currentState = State.Array;
                        break;

                    default:
                        throw JsonWriterException.Create(this, "Unknown JsonType: " + type2, null);
                }
            }
        }

        internal static State[][] BuildStateArray()
        {
            List<State[]> list = StateArrayTempate.ToList<State[]>();
            State[] item = StateArrayTempate[0];
            State[] stateArray2 = StateArrayTempate[7];
            foreach (JsonToken token in EnumUtils.GetValues(typeof(JsonToken)))
            {
                if (list.Count > token)
                {
                    continue;
                }
                switch (token)
                {
                    case JsonToken.Integer:
                    case JsonToken.Float:
                    case JsonToken.String:
                    case JsonToken.Boolean:
                    case JsonToken.Null:
                    case JsonToken.Undefined:
                    case JsonToken.Date:
                    case JsonToken.Bytes:
                    {
                        list.Add(stateArray2);
                        continue;
                    }
                }
                list.Add(item);
            }
            return list.ToArray();
        }

        public virtual void Close()
        {
            this.AutoCompleteAll();
        }

        private void Dispose(bool disposing)
        {
            if (this._currentState != State.Closed)
            {
                this.Close();
            }
        }

        public abstract void Flush();
        private JsonToken GetCloseTokenForType(JsonContainerType type)
        {
            switch (type)
            {
                case JsonContainerType.Object:
                    return JsonToken.EndObject;

                case JsonContainerType.Array:
                    return JsonToken.EndArray;

                case JsonContainerType.Constructor:
                    return JsonToken.EndConstructor;
            }
            throw JsonWriterException.Create(this, "No close token for type: " + type, null);
        }

        internal void InternalWriteComment()
        {
            this.AutoComplete(JsonToken.Comment);
        }

        internal void InternalWriteEnd(JsonContainerType container)
        {
            this.AutoCompleteClose(container);
        }

        internal void InternalWriteNull()
        {
            this.UpdateScopeWithFinishedValue();
            this.AutoComplete(JsonToken.Null);
        }

        internal void InternalWritePropertyName(string name)
        {
            this._currentPosition.PropertyName = name;
            this.AutoComplete(JsonToken.PropertyName);
        }

        internal void InternalWriteRaw()
        {
        }

        internal void InternalWriteStart(JsonToken token, JsonContainerType container)
        {
            this.UpdateScopeWithFinishedValue();
            this.AutoComplete(token);
            this.Push(container);
        }

        internal void InternalWriteUndefined()
        {
            this.UpdateScopeWithFinishedValue();
            this.AutoComplete(JsonToken.Undefined);
        }

        internal void InternalWriteValue(JsonToken token)
        {
            this.UpdateScopeWithFinishedValue();
            this.AutoComplete(token);
        }

        internal void InternalWriteWhitespace(string ws)
        {
            if ((ws != null) && !StringUtils.IsWhiteSpace(ws))
            {
                throw JsonWriterException.Create(this, "Only white space characters should be used.", null);
            }
        }

        private bool IsEndToken(JsonToken token)
        {
            switch (token)
            {
                case JsonToken.EndObject:
                case JsonToken.EndArray:
                case JsonToken.EndConstructor:
                    return true;
            }
            return false;
        }

        private bool IsStartToken(JsonToken token)
        {
            switch (token)
            {
                case JsonToken.StartObject:
                case JsonToken.StartArray:
                case JsonToken.StartConstructor:
                    return true;
            }
            return false;
        }

        private JsonContainerType Peek()
        {
            return this._currentPosition.Type;
        }

        private JsonContainerType Pop()
        {
            JsonPosition position = this._currentPosition;
            if (this._stack.Count > 0)
            {
                this._currentPosition = this._stack[this._stack.Count - 1];
                this._stack.RemoveAt(this._stack.Count - 1);
            }
            else
            {
                this._currentPosition = new JsonPosition();
            }
            return position.Type;
        }

        private void Push(JsonContainerType value)
        {
            if (this._currentPosition.Type != JsonContainerType.None)
            {
                this._stack.Add(this._currentPosition);
            }
            this._currentPosition = new JsonPosition(value);
        }

        void IDisposable.Dispose()
        {
            this.Dispose(true);
        }

        internal void UpdateScopeWithFinishedValue()
        {
            if (this._currentPosition.HasIndex)
            {
                this._currentPosition.Position++;
            }
        }

        public virtual void WriteComment(string text)
        {
            this.InternalWriteComment();
        }

        private void WriteConstructorDate(JsonReader reader)
        {
            if (!reader.Read())
            {
                throw JsonWriterException.Create(this, "Unexpected end when reading date constructor.", null);
            }
            if (reader.TokenType != JsonToken.Integer)
            {
                throw JsonWriterException.Create(this, "Unexpected token when reading date constructor. Expected Integer, got " + reader.TokenType, null);
            }
            long javaScriptTicks = (long) reader.Value;
            DateTime time = JsonConvert.ConvertJavaScriptTicksToDateTime(javaScriptTicks);
            if (!reader.Read())
            {
                throw JsonWriterException.Create(this, "Unexpected end when reading date constructor.", null);
            }
            if (reader.TokenType != JsonToken.EndConstructor)
            {
                throw JsonWriterException.Create(this, "Unexpected token when reading date constructor. Expected EndConstructor, got " + reader.TokenType, null);
            }
            this.WriteValue(time);
        }

        public virtual void WriteEnd()
        {
            this.WriteEnd(this.Peek());
        }

        private void WriteEnd(JsonContainerType type)
        {
            switch (type)
            {
                case JsonContainerType.Object:
                    this.WriteEndObject();
                    return;

                case JsonContainerType.Array:
                    this.WriteEndArray();
                    return;

                case JsonContainerType.Constructor:
                    this.WriteEndConstructor();
                    return;
            }
            throw JsonWriterException.Create(this, "Unexpected type when writing end: " + type, null);
        }

        protected virtual void WriteEnd(JsonToken token)
        {
        }

        public virtual void WriteEndArray()
        {
            this.InternalWriteEnd(JsonContainerType.Array);
        }

        public virtual void WriteEndConstructor()
        {
            this.InternalWriteEnd(JsonContainerType.Constructor);
        }

        public virtual void WriteEndObject()
        {
            this.InternalWriteEnd(JsonContainerType.Object);
        }

        protected virtual void WriteIndent()
        {
        }

        protected virtual void WriteIndentSpace()
        {
        }

        public virtual void WriteNull()
        {
            this.InternalWriteNull();
        }

        public virtual void WritePropertyName(string name)
        {
            this.InternalWritePropertyName(name);
        }

        public virtual void WriteRaw(string json)
        {
            this.InternalWriteRaw();
        }

        public virtual void WriteRawValue(string json)
        {
            this.UpdateScopeWithFinishedValue();
            this.AutoComplete(JsonToken.Undefined);
            this.WriteRaw(json);
        }

        public virtual void WriteStartArray()
        {
            this.InternalWriteStart(JsonToken.StartArray, JsonContainerType.Array);
        }

        public virtual void WriteStartConstructor(string name)
        {
            this.InternalWriteStart(JsonToken.StartConstructor, JsonContainerType.Constructor);
        }

        public virtual void WriteStartObject()
        {
            this.InternalWriteStart(JsonToken.StartObject, JsonContainerType.Object);
        }

        public void WriteToken(JsonReader reader)
        {
            int depth;
            ValidationUtils.ArgumentNotNull(reader, "reader");
            if (reader.TokenType == JsonToken.None)
            {
                depth = -1;
            }
            else if (!this.IsStartToken(reader.TokenType))
            {
                depth = reader.Depth + 1;
            }
            else
            {
                depth = reader.Depth;
            }
            this.WriteToken(reader, depth);
        }

        internal void WriteToken(JsonReader reader, int initialDepth)
        {
            do
            {
                switch (reader.TokenType)
                {
                    case JsonToken.None:
                        break;

                    case JsonToken.StartObject:
                        this.WriteStartObject();
                        break;

                    case JsonToken.StartArray:
                        this.WriteStartArray();
                        break;

                    case JsonToken.StartConstructor:
                        if (!string.Equals(reader.Value.ToString(), "Date", StringComparison.Ordinal))
                        {
                            this.WriteStartConstructor(reader.Value.ToString());
                            break;
                        }
                        this.WriteConstructorDate(reader);
                        break;

                    case JsonToken.PropertyName:
                        this.WritePropertyName(reader.Value.ToString());
                        break;

                    case JsonToken.Comment:
                        this.WriteComment((reader.Value != null) ? reader.Value.ToString() : null);
                        break;

                    case JsonToken.Raw:
                        this.WriteRawValue((reader.Value != null) ? reader.Value.ToString() : null);
                        break;

                    case JsonToken.Integer:
                        this.WriteValue(Convert.ToInt64(reader.Value, CultureInfo.InvariantCulture));
                        break;

                    case JsonToken.Float:
                    {
                        object obj2 = reader.Value;
                        if (!(obj2 is decimal))
                        {
                            if (obj2 is double)
                            {
                                this.WriteValue((double) obj2);
                            }
                            else if (obj2 is float)
                            {
                                this.WriteValue((float) obj2);
                            }
                            else
                            {
                                this.WriteValue(Convert.ToDouble(obj2, CultureInfo.InvariantCulture));
                            }
                            break;
                        }
                        this.WriteValue((decimal) obj2);
                        break;
                    }
                    case JsonToken.String:
                        this.WriteValue(reader.Value.ToString());
                        break;

                    case JsonToken.Boolean:
                        this.WriteValue(Convert.ToBoolean(reader.Value, CultureInfo.InvariantCulture));
                        break;

                    case JsonToken.Null:
                        this.WriteNull();
                        break;

                    case JsonToken.Undefined:
                        this.WriteUndefined();
                        break;

                    case JsonToken.EndObject:
                        this.WriteEndObject();
                        break;

                    case JsonToken.EndArray:
                        this.WriteEndArray();
                        break;

                    case JsonToken.EndConstructor:
                        this.WriteEndConstructor();
                        break;

                    case JsonToken.Date:
                        if (!(reader.Value is DateTimeOffset))
                        {
                            this.WriteValue(Convert.ToDateTime(reader.Value, CultureInfo.InvariantCulture));
                            break;
                        }
                        this.WriteValue((DateTimeOffset) reader.Value);
                        break;

                    case JsonToken.Bytes:
                        this.WriteValue((byte[]) reader.Value);
                        break;

                    default:
                        throw MiscellaneousUtils.CreateArgumentOutOfRangeException("TokenType", reader.TokenType, "Unexpected token type.");
                }
            }
            while (((initialDepth - 1) < (reader.Depth - (this.IsEndToken(reader.TokenType) ? 1 : 0))) && reader.Read());
        }

        public virtual void WriteUndefined()
        {
            this.InternalWriteUndefined();
        }

        public virtual void WriteValue(bool value)
        {
            this.InternalWriteValue(JsonToken.Boolean);
        }

        public virtual void WriteValue(byte value)
        {
            this.InternalWriteValue(JsonToken.Integer);
        }

        public virtual void WriteValue(char value)
        {
            this.InternalWriteValue(JsonToken.String);
        }

        public virtual void WriteValue(DateTime value)
        {
            this.InternalWriteValue(JsonToken.Date);
        }

        public virtual void WriteValue(DateTimeOffset value)
        {
            this.InternalWriteValue(JsonToken.Date);
        }

        public virtual void WriteValue(decimal value)
        {
            this.InternalWriteValue(JsonToken.Float);
        }

        public virtual void WriteValue(double value)
        {
            this.InternalWriteValue(JsonToken.Float);
        }

        public virtual void WriteValue(Guid value)
        {
            this.InternalWriteValue(JsonToken.String);
        }

        public virtual void WriteValue(short value)
        {
            this.InternalWriteValue(JsonToken.Integer);
        }

        public virtual void WriteValue(int value)
        {
            this.InternalWriteValue(JsonToken.Integer);
        }

        public virtual void WriteValue(long value)
        {
            this.InternalWriteValue(JsonToken.Integer);
        }

        public virtual void WriteValue(DateTime? value)
        {
            if (!value.HasValue)
            {
                this.WriteNull();
            }
            else
            {
                this.WriteValue(value.Value);
            }
        }

        public virtual void WriteValue(DateTimeOffset? value)
        {
            if (!value.HasValue)
            {
                this.WriteNull();
            }
            else
            {
                this.WriteValue(value.Value);
            }
        }

        public virtual void WriteValue(decimal? value)
        {
            if (!value.HasValue)
            {
                this.WriteNull();
            }
            else
            {
                this.WriteValue(value.Value);
            }
        }

        public virtual void WriteValue(Guid? value)
        {
            if (!value.HasValue)
            {
                this.WriteNull();
            }
            else
            {
                this.WriteValue(value.Value);
            }
        }

        [CLSCompliant(false)]
        public virtual void WriteValue(sbyte? value)
        {
            sbyte? nullable = value;
            int? nullable3 = nullable.HasValue ? new int?(nullable.GetValueOrDefault()) : null;
            if (!nullable3.HasValue)
            {
                this.WriteNull();
            }
            else
            {
                this.WriteValue(value.Value);
            }
        }

        public virtual void WriteValue(TimeSpan? value)
        {
            if (!value.HasValue)
            {
                this.WriteNull();
            }
            else
            {
                this.WriteValue(value.Value);
            }
        }

        public virtual void WriteValue(byte? value)
        {
            byte? nullable = value;
            int? nullable3 = nullable.HasValue ? new int?(nullable.GetValueOrDefault()) : null;
            if (!nullable3.HasValue)
            {
                this.WriteNull();
            }
            else
            {
                this.WriteValue(value.Value);
            }
        }

        public virtual void WriteValue(object value)
        {
            if (value == null)
            {
                this.WriteNull();
            }
            else
            {
                if (ConvertUtils.IsConvertible(value))
                {
                    IConvertible convertible = ConvertUtils.ToConvertible(value);
                    switch (convertible.GetTypeCode())
                    {
                        case TypeCode.DBNull:
                            this.WriteNull();
                            return;

                        case TypeCode.Boolean:
                            this.WriteValue(convertible.ToBoolean(CultureInfo.InvariantCulture));
                            return;

                        case TypeCode.Char:
                            this.WriteValue(convertible.ToChar(CultureInfo.InvariantCulture));
                            return;

                        case TypeCode.SByte:
                            this.WriteValue(convertible.ToSByte(CultureInfo.InvariantCulture));
                            return;

                        case TypeCode.Byte:
                            this.WriteValue(convertible.ToByte(CultureInfo.InvariantCulture));
                            return;

                        case TypeCode.Int16:
                            this.WriteValue(convertible.ToInt16(CultureInfo.InvariantCulture));
                            return;

                        case TypeCode.UInt16:
                            this.WriteValue(convertible.ToUInt16(CultureInfo.InvariantCulture));
                            return;

                        case TypeCode.Int32:
                            this.WriteValue(convertible.ToInt32(CultureInfo.InvariantCulture));
                            return;

                        case TypeCode.UInt32:
                            this.WriteValue(convertible.ToUInt32(CultureInfo.InvariantCulture));
                            return;

                        case TypeCode.Int64:
                            this.WriteValue(convertible.ToInt64(CultureInfo.InvariantCulture));
                            return;

                        case TypeCode.UInt64:
                            this.WriteValue(convertible.ToUInt64(CultureInfo.InvariantCulture));
                            return;

                        case TypeCode.Single:
                            this.WriteValue(convertible.ToSingle(CultureInfo.InvariantCulture));
                            return;

                        case TypeCode.Double:
                            this.WriteValue(convertible.ToDouble(CultureInfo.InvariantCulture));
                            return;

                        case TypeCode.Decimal:
                            this.WriteValue(convertible.ToDecimal(CultureInfo.InvariantCulture));
                            return;

                        case TypeCode.DateTime:
                            this.WriteValue(convertible.ToDateTime(CultureInfo.InvariantCulture));
                            return;

                        case TypeCode.String:
                            this.WriteValue(convertible.ToString(CultureInfo.InvariantCulture));
                            return;
                    }
                }
                else
                {
                    if (value is DateTimeOffset)
                    {
                        this.WriteValue((DateTimeOffset) value);
                        return;
                    }
                    if (value is byte[])
                    {
                        this.WriteValue((byte[]) value);
                        return;
                    }
                    if (value is Guid)
                    {
                        this.WriteValue((Guid) value);
                        return;
                    }
                    if (value is Uri)
                    {
                        this.WriteValue((Uri) value);
                        return;
                    }
                    if (value is TimeSpan)
                    {
                        this.WriteValue((TimeSpan) value);
                        return;
                    }
                }
                throw JsonWriterException.Create(this, "Unsupported type: {0}. Use the JsonSerializer class to get the object's JSON representation.".FormatWith(CultureInfo.InvariantCulture, value.GetType()), null);
            }
        }

        [CLSCompliant(false)]
        public virtual void WriteValue(sbyte value)
        {
            this.InternalWriteValue(JsonToken.Integer);
        }

        public virtual void WriteValue(float value)
        {
            this.InternalWriteValue(JsonToken.Float);
        }

        public virtual void WriteValue(string value)
        {
            this.InternalWriteValue(JsonToken.String);
        }

        public virtual void WriteValue(TimeSpan value)
        {
            this.InternalWriteValue(JsonToken.String);
        }

        public virtual void WriteValue(char? value)
        {
            char? nullable = value;
            int? nullable3 = nullable.HasValue ? new int?(nullable.GetValueOrDefault()) : null;
            if (!nullable3.HasValue)
            {
                this.WriteNull();
            }
            else
            {
                this.WriteValue(value.Value);
            }
        }

        public virtual void WriteValue(bool? value)
        {
            if (!value.HasValue)
            {
                this.WriteNull();
            }
            else
            {
                this.WriteValue(value.Value);
            }
        }

        [CLSCompliant(false)]
        public virtual void WriteValue(uint value)
        {
            this.InternalWriteValue(JsonToken.Integer);
        }

        public virtual void WriteValue(short? value)
        {
            short? nullable = value;
            int? nullable3 = nullable.HasValue ? new int?(nullable.GetValueOrDefault()) : null;
            if (!nullable3.HasValue)
            {
                this.WriteNull();
            }
            else
            {
                this.WriteValue(value.Value);
            }
        }

        [CLSCompliant(false)]
        public virtual void WriteValue(ushort value)
        {
            this.InternalWriteValue(JsonToken.Integer);
        }

        public virtual void WriteValue(double? value)
        {
            if (!value.HasValue)
            {
                this.WriteNull();
            }
            else
            {
                this.WriteValue(value.Value);
            }
        }

        public virtual void WriteValue(int? value)
        {
            if (!value.HasValue)
            {
                this.WriteNull();
            }
            else
            {
                this.WriteValue(value.Value);
            }
        }

        public virtual void WriteValue(long? value)
        {
            if (!value.HasValue)
            {
                this.WriteNull();
            }
            else
            {
                this.WriteValue(value.Value);
            }
        }

        public virtual void WriteValue(float? value)
        {
            if (!value.HasValue)
            {
                this.WriteNull();
            }
            else
            {
                this.WriteValue(value.Value);
            }
        }

        public virtual void WriteValue(Uri value)
        {
            if (value == null)
            {
                this.WriteNull();
            }
            else
            {
                this.InternalWriteValue(JsonToken.String);
            }
        }

        [CLSCompliant(false)]
        public virtual void WriteValue(ushort? value)
        {
            ushort? nullable = value;
            int? nullable3 = nullable.HasValue ? new int?(nullable.GetValueOrDefault()) : null;
            if (!nullable3.HasValue)
            {
                this.WriteNull();
            }
            else
            {
                this.WriteValue(value.Value);
            }
        }

        [CLSCompliant(false)]
        public virtual void WriteValue(ulong value)
        {
            this.InternalWriteValue(JsonToken.Integer);
        }

        [CLSCompliant(false)]
        public virtual void WriteValue(uint? value)
        {
            if (!value.HasValue)
            {
                this.WriteNull();
            }
            else
            {
                this.WriteValue(value.Value);
            }
        }

        [CLSCompliant(false)]
        public virtual void WriteValue(ulong? value)
        {
            if (!value.HasValue)
            {
                this.WriteNull();
            }
            else
            {
                this.WriteValue(value.Value);
            }
        }

        public virtual void WriteValue(byte[] value)
        {
            if (value == null)
            {
                this.WriteNull();
            }
            else
            {
                this.InternalWriteValue(JsonToken.Bytes);
            }
        }

        protected virtual void WriteValueDelimiter()
        {
        }

        public virtual void WriteWhitespace(string ws)
        {
            this.InternalWriteWhitespace(ws);
        }

        public bool CloseOutput { get; set; }

        internal string ContainerPath
        {
            get
            {
                if (this._currentPosition.Type == JsonContainerType.None)
                {
                    return string.Empty;
                }
                return JsonPosition.BuildPath(this._stack);
            }
        }

        public Newtonsoft.Json.DateFormatHandling DateFormatHandling
        {
            get
            {
                return this._dateFormatHandling;
            }
            set
            {
                this._dateFormatHandling = value;
            }
        }

        public Newtonsoft.Json.DateTimeZoneHandling DateTimeZoneHandling
        {
            get
            {
                return this._dateTimeZoneHandling;
            }
            set
            {
                this._dateTimeZoneHandling = value;
            }
        }

        public Newtonsoft.Json.Formatting Formatting
        {
            get
            {
                return this._formatting;
            }
            set
            {
                this._formatting = value;
            }
        }

        public string Path
        {
            get
            {
                if (this._currentPosition.Type == JsonContainerType.None)
                {
                    return string.Empty;
                }
                IEnumerable<JsonPosition> positions = (((this._currentState == State.ArrayStart) || (this._currentState == State.ConstructorStart)) || (this._currentState == State.ObjectStart)) ? this._stack : this._stack.Concat<JsonPosition>(new JsonPosition[1]);
                return JsonPosition.BuildPath(positions);
            }
        }

        public Newtonsoft.Json.StringEscapeHandling StringEscapeHandling
        {
            get
            {
                return this._stringEscapeHandling;
            }
            set
            {
                this._stringEscapeHandling = value;
            }
        }

        protected internal int Top
        {
            get
            {
                int count = this._stack.Count;
                if (this.Peek() != JsonContainerType.None)
                {
                    count++;
                }
                return count;
            }
        }

        public Newtonsoft.Json.WriteState WriteState
        {
            get
            {
                switch (this._currentState)
                {
                    case State.Start:
                        return Newtonsoft.Json.WriteState.Start;

                    case State.Property:
                        return Newtonsoft.Json.WriteState.Property;

                    case State.ObjectStart:
                    case State.Object:
                        return Newtonsoft.Json.WriteState.Object;

                    case State.ArrayStart:
                    case State.Array:
                        return Newtonsoft.Json.WriteState.Array;

                    case State.ConstructorStart:
                    case State.Constructor:
                        return Newtonsoft.Json.WriteState.Constructor;

                    case State.Closed:
                        return Newtonsoft.Json.WriteState.Closed;

                    case State.Error:
                        return Newtonsoft.Json.WriteState.Error;
                }
                throw JsonWriterException.Create(this, "Invalid state: " + this._currentState, null);
            }
        }

        internal enum State
        {
            Start,
            Property,
            ObjectStart,
            Object,
            ArrayStart,
            Array,
            ConstructorStart,
            Constructor,
            Closed,
            Error
        }
    }
}

