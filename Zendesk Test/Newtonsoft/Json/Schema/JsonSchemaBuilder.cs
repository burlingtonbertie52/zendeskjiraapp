﻿namespace Newtonsoft.Json.Schema
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Newtonsoft.Json.Utilities;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    internal class JsonSchemaBuilder
    {
        private JsonSchema _currentSchema;
        private JsonReader _reader;
        private readonly JsonSchemaResolver _resolver;
        private readonly IList<JsonSchema> _stack = new List<JsonSchema>();

        public JsonSchemaBuilder(JsonSchemaResolver resolver)
        {
            this._resolver = resolver;
        }

        private JsonSchema BuildSchema()
        {
            if (this._reader.TokenType != JsonToken.StartObject)
            {
                throw JsonReaderException.Create(this._reader, "Expected StartObject while parsing schema object, got {0}.".FormatWith(CultureInfo.InvariantCulture, this._reader.TokenType));
            }
            this._reader.Read();
            if (this._reader.TokenType == JsonToken.EndObject)
            {
                this.Push(new JsonSchema());
                return this.Pop();
            }
            string propertyName = Convert.ToString(this._reader.Value, CultureInfo.InvariantCulture);
            this._reader.Read();
            if (propertyName == "$ref")
            {
                string str2 = (string) this._reader.Value;
                while (this._reader.Read() && (this._reader.TokenType != JsonToken.EndObject))
                {
                    if (this._reader.TokenType == JsonToken.StartObject)
                    {
                        throw JsonReaderException.Create(this._reader, "Found StartObject within the schema reference with the Id '{0}'".FormatWith(CultureInfo.InvariantCulture, str2));
                    }
                }
                JsonSchema schema = this._resolver.GetSchema(str2);
                if (schema == null)
                {
                    throw new JsonException("Could not resolve schema reference for Id '{0}'.".FormatWith(CultureInfo.InvariantCulture, str2));
                }
                return schema;
            }
            this.Push(new JsonSchema());
            this.ProcessSchemaProperty(propertyName);
            while (this._reader.Read() && (this._reader.TokenType != JsonToken.EndObject))
            {
                propertyName = Convert.ToString(this._reader.Value, CultureInfo.InvariantCulture);
                this._reader.Read();
                this.ProcessSchemaProperty(propertyName);
            }
            return this.Pop();
        }

        internal static string MapType(JsonSchemaType type)
        {
            return JsonSchemaConstants.JsonSchemaTypeMapping.Single<KeyValuePair<string, JsonSchemaType>>(kv => (((JsonSchemaType) kv.Value) == type)).Key;
        }

        internal static JsonSchemaType MapType(string type)
        {
            JsonSchemaType type2;
            if (!JsonSchemaConstants.JsonSchemaTypeMapping.TryGetValue(type, out type2))
            {
                throw new JsonException("Invalid JSON schema type: {0}".FormatWith(CultureInfo.InvariantCulture, type));
            }
            return type2;
        }

        internal JsonSchema Parse(JsonReader reader)
        {
            this._reader = reader;
            if (reader.TokenType == JsonToken.None)
            {
                this._reader.Read();
            }
            return this.BuildSchema();
        }

        private JsonSchema Pop()
        {
            JsonSchema schema = this._currentSchema;
            this._stack.RemoveAt(this._stack.Count - 1);
            this._currentSchema = this._stack.LastOrDefault<JsonSchema>();
            return schema;
        }

        private void ProcessAdditionalProperties()
        {
            if (this._reader.TokenType == JsonToken.Boolean)
            {
                this.CurrentSchema.AllowAdditionalProperties = (bool) this._reader.Value;
            }
            else
            {
                this.CurrentSchema.AdditionalProperties = this.BuildSchema();
            }
        }

        private void ProcessDefault()
        {
            this.CurrentSchema.Default = JToken.ReadFrom(this._reader);
        }

        private void ProcessEnum()
        {
            if (this._reader.TokenType != JsonToken.StartArray)
            {
                throw JsonReaderException.Create(this._reader, "Expected StartArray token while parsing enum values, got {0}.".FormatWith(CultureInfo.InvariantCulture, this._reader.TokenType));
            }
            this.CurrentSchema.Enum = new List<JToken>();
            while (this._reader.Read() && (this._reader.TokenType != JsonToken.EndArray))
            {
                JToken item = JToken.ReadFrom(this._reader);
                this.CurrentSchema.Enum.Add(item);
            }
        }

        private void ProcessExtends()
        {
            this.CurrentSchema.Extends = this.BuildSchema();
        }

        private void ProcessIdentity()
        {
            this.CurrentSchema.Identity = new List<string>();
            JsonToken tokenType = this._reader.TokenType;
            if (tokenType != JsonToken.StartArray)
            {
                if (tokenType != JsonToken.String)
                {
                    throw JsonReaderException.Create(this._reader, "Expected array or JSON property name string token, got {0}.".FormatWith(CultureInfo.InvariantCulture, this._reader.TokenType));
                }
                this.CurrentSchema.Identity.Add(this._reader.Value.ToString());
                return;
            }
            while (this._reader.Read())
            {
                if (this._reader.TokenType == JsonToken.EndArray)
                {
                    return;
                }
                if (this._reader.TokenType != JsonToken.String)
                {
                    throw JsonReaderException.Create(this._reader, "Exception JSON property name string token, got {0}.".FormatWith(CultureInfo.InvariantCulture, this._reader.TokenType));
                }
                this.CurrentSchema.Identity.Add(this._reader.Value.ToString());
            }
            return;
        }

        private void ProcessItems()
        {
            this.CurrentSchema.Items = new List<JsonSchema>();
            switch (this._reader.TokenType)
            {
                case JsonToken.StartObject:
                    this.CurrentSchema.Items.Add(this.BuildSchema());
                    return;

                case JsonToken.StartArray:
                    break;

                default:
                    throw JsonReaderException.Create(this._reader, "Expected array or JSON schema object token, got {0}.".FormatWith(CultureInfo.InvariantCulture, this._reader.TokenType));
            }
            while (this._reader.Read())
            {
                if (this._reader.TokenType == JsonToken.EndArray)
                {
                    return;
                }
                this.CurrentSchema.Items.Add(this.BuildSchema());
            }
            return;
        }

        private void ProcessOptions()
        {
            this.CurrentSchema.Options = new Dictionary<JToken, string>(new JTokenEqualityComparer());
            if (this._reader.TokenType != JsonToken.StartArray)
            {
                throw JsonReaderException.Create(this._reader, "Expected array token, got {0}.".FormatWith(CultureInfo.InvariantCulture, this._reader.TokenType));
            }
            while (this._reader.Read())
            {
                if (this._reader.TokenType == JsonToken.EndArray)
                {
                    return;
                }
                if (this._reader.TokenType != JsonToken.StartObject)
                {
                    throw JsonReaderException.Create(this._reader, "Expect object token, got {0}.".FormatWith(CultureInfo.InvariantCulture, this._reader.TokenType));
                }
                string str = null;
                JToken key = null;
                while (this._reader.Read() && (this._reader.TokenType != JsonToken.EndObject))
                {
                    string str2 = Convert.ToString(this._reader.Value, CultureInfo.InvariantCulture);
                    this._reader.Read();
                    string str3 = str2;
                    if (str3 == null)
                    {
                        goto Label_00D6;
                    }
                    if (!(str3 == "value"))
                    {
                        if (str3 == "label")
                        {
                            goto Label_00C3;
                        }
                        goto Label_00D6;
                    }
                    key = JToken.ReadFrom(this._reader);
                    continue;
                Label_00C3:
                    str = (string) this._reader.Value;
                    continue;
                Label_00D6:
                    throw JsonReaderException.Create(this._reader, "Unexpected property in JSON schema option: {0}.".FormatWith(CultureInfo.InvariantCulture, str2));
                }
                if (key == null)
                {
                    throw new JsonException("No value specified for JSON schema option.");
                }
                if (this.CurrentSchema.Options.ContainsKey(key))
                {
                    throw new JsonException("Duplicate value in JSON schema option collection: {0}".FormatWith(CultureInfo.InvariantCulture, key));
                }
                this.CurrentSchema.Options.Add(key, str);
            }
            return;
        }

        private void ProcessPatternProperties()
        {
            Dictionary<string, JsonSchema> dictionary = new Dictionary<string, JsonSchema>();
            if (this._reader.TokenType != JsonToken.StartObject)
            {
                throw JsonReaderException.Create(this._reader, "Expected StartObject token.");
            }
            while (this._reader.Read() && (this._reader.TokenType != JsonToken.EndObject))
            {
                string key = Convert.ToString(this._reader.Value, CultureInfo.InvariantCulture);
                this._reader.Read();
                if (dictionary.ContainsKey(key))
                {
                    throw new JsonException("Property {0} has already been defined in schema.".FormatWith(CultureInfo.InvariantCulture, key));
                }
                dictionary.Add(key, this.BuildSchema());
            }
            this.CurrentSchema.PatternProperties = dictionary;
        }

        private void ProcessProperties()
        {
            IDictionary<string, JsonSchema> dictionary = new Dictionary<string, JsonSchema>();
            if (this._reader.TokenType != JsonToken.StartObject)
            {
                throw JsonReaderException.Create(this._reader, "Expected StartObject token while parsing schema properties, got {0}.".FormatWith(CultureInfo.InvariantCulture, this._reader.TokenType));
            }
            while (this._reader.Read() && (this._reader.TokenType != JsonToken.EndObject))
            {
                string key = Convert.ToString(this._reader.Value, CultureInfo.InvariantCulture);
                this._reader.Read();
                if (dictionary.ContainsKey(key))
                {
                    throw new JsonException("Property {0} has already been defined in schema.".FormatWith(CultureInfo.InvariantCulture, key));
                }
                dictionary.Add(key, this.BuildSchema());
            }
            this.CurrentSchema.Properties = dictionary;
        }

        private void ProcessSchemaProperty(string propertyName)
        {
            switch (propertyName)
            {
                case "type":
                    this.CurrentSchema.Type = this.ProcessType();
                    return;

                case "id":
                    this.CurrentSchema.Id = (string) this._reader.Value;
                    return;

                case "title":
                    this.CurrentSchema.Title = (string) this._reader.Value;
                    return;

                case "description":
                    this.CurrentSchema.Description = (string) this._reader.Value;
                    return;

                case "properties":
                    this.ProcessProperties();
                    return;

                case "items":
                    this.ProcessItems();
                    return;

                case "additionalProperties":
                    this.ProcessAdditionalProperties();
                    return;

                case "patternProperties":
                    this.ProcessPatternProperties();
                    return;

                case "required":
                    this.CurrentSchema.Required = new bool?((bool) this._reader.Value);
                    return;

                case "requires":
                    this.CurrentSchema.Requires = (string) this._reader.Value;
                    return;

                case "identity":
                    this.ProcessIdentity();
                    return;

                case "minimum":
                    this.CurrentSchema.Minimum = new double?(Convert.ToDouble(this._reader.Value, CultureInfo.InvariantCulture));
                    return;

                case "maximum":
                    this.CurrentSchema.Maximum = new double?(Convert.ToDouble(this._reader.Value, CultureInfo.InvariantCulture));
                    return;

                case "exclusiveMinimum":
                    this.CurrentSchema.ExclusiveMinimum = new bool?((bool) this._reader.Value);
                    return;

                case "exclusiveMaximum":
                    this.CurrentSchema.ExclusiveMaximum = new bool?((bool) this._reader.Value);
                    return;

                case "maxLength":
                    this.CurrentSchema.MaximumLength = new int?(Convert.ToInt32(this._reader.Value, CultureInfo.InvariantCulture));
                    return;

                case "minLength":
                    this.CurrentSchema.MinimumLength = new int?(Convert.ToInt32(this._reader.Value, CultureInfo.InvariantCulture));
                    return;

                case "maxItems":
                    this.CurrentSchema.MaximumItems = new int?(Convert.ToInt32(this._reader.Value, CultureInfo.InvariantCulture));
                    return;

                case "minItems":
                    this.CurrentSchema.MinimumItems = new int?(Convert.ToInt32(this._reader.Value, CultureInfo.InvariantCulture));
                    return;

                case "divisibleBy":
                    this.CurrentSchema.DivisibleBy = new double?(Convert.ToDouble(this._reader.Value, CultureInfo.InvariantCulture));
                    return;

                case "disallow":
                    this.CurrentSchema.Disallow = this.ProcessType();
                    return;

                case "default":
                    this.ProcessDefault();
                    return;

                case "hidden":
                    this.CurrentSchema.Hidden = new bool?((bool) this._reader.Value);
                    return;

                case "readonly":
                    this.CurrentSchema.ReadOnly = new bool?((bool) this._reader.Value);
                    return;

                case "format":
                    this.CurrentSchema.Format = (string) this._reader.Value;
                    return;

                case "pattern":
                    this.CurrentSchema.Pattern = (string) this._reader.Value;
                    return;

                case "options":
                    this.ProcessOptions();
                    return;

                case "enum":
                    this.ProcessEnum();
                    return;

                case "extends":
                    this.ProcessExtends();
                    return;
            }
            this._reader.Skip();
        }

        private JsonSchemaType? ProcessType()
        {
            JsonToken tokenType = this._reader.TokenType;
            if (tokenType != JsonToken.StartArray)
            {
                if (tokenType != JsonToken.String)
                {
                    throw JsonReaderException.Create(this._reader, "Expected array or JSON schema type string token, got {0}.".FormatWith(CultureInfo.InvariantCulture, this._reader.TokenType));
                }
                return new JsonSchemaType?(MapType(this._reader.Value.ToString()));
            }
            JsonSchemaType? nullable = new JsonSchemaType?(JsonSchemaType.None);
            while (this._reader.Read() && (this._reader.TokenType != JsonToken.EndArray))
            {
                if (this._reader.TokenType != JsonToken.String)
                {
                    throw JsonReaderException.Create(this._reader, "Exception JSON schema type string token, got {0}.".FormatWith(CultureInfo.InvariantCulture, this._reader.TokenType));
                }
                JsonSchemaType? nullable2 = nullable;
                JsonSchemaType type = MapType(this._reader.Value.ToString());
                nullable = nullable2.HasValue ? new JsonSchemaType?(((JsonSchemaType) nullable2.GetValueOrDefault()) | type) : null;
            }
            return nullable;
        }

        private void Push(JsonSchema value)
        {
            this._currentSchema = value;
            this._stack.Add(value);
            this._resolver.LoadedSchemas.Add(value);
        }

        private JsonSchema CurrentSchema
        {
            get
            {
                return this._currentSchema;
            }
        }
    }
}

