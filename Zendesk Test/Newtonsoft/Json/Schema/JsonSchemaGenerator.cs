﻿namespace Newtonsoft.Json.Schema
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Newtonsoft.Json.Serialization;
    using Newtonsoft.Json.Utilities;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Globalization;
    using System.Linq;
    using System.Runtime.CompilerServices;

    public class JsonSchemaGenerator
    {
        private IContractResolver _contractResolver;
        private JsonSchema _currentSchema;
        private JsonSchemaResolver _resolver;
        private readonly IList<TypeSchema> _stack = new List<TypeSchema>();

        private JsonSchemaType AddNullType(JsonSchemaType type, Required valueRequired)
        {
            if (valueRequired != Required.Always)
            {
                return (type | JsonSchemaType.Null);
            }
            return type;
        }

        public JsonSchema Generate(Type type)
        {
            return this.Generate(type, new JsonSchemaResolver(), false);
        }

        public JsonSchema Generate(Type type, JsonSchemaResolver resolver)
        {
            return this.Generate(type, resolver, false);
        }

        public JsonSchema Generate(Type type, bool rootSchemaNullable)
        {
            return this.Generate(type, new JsonSchemaResolver(), rootSchemaNullable);
        }

        public JsonSchema Generate(Type type, JsonSchemaResolver resolver, bool rootSchemaNullable)
        {
            ValidationUtils.ArgumentNotNull(type, "type");
            ValidationUtils.ArgumentNotNull(resolver, "resolver");
            this._resolver = resolver;
            return this.GenerateInternal(type, !rootSchemaNullable ? Required.Always : Required.Default, false);
        }

        private JsonSchema GenerateInternal(Type type, Required valueRequired, bool required)
        {
            JsonConverter converter;
            ValidationUtils.ArgumentNotNull(type, "type");
            string typeId = this.GetTypeId(type, false);
            string str2 = this.GetTypeId(type, true);
            if (!string.IsNullOrEmpty(typeId))
            {
                JsonSchema schema = this._resolver.GetSchema(typeId);
                if (schema != null)
                {
                    if ((valueRequired != Required.Always) && !HasFlag(schema.Type, JsonSchemaType.Null))
                    {
                        schema.Type = ((JsonSchemaType) schema.Type) | JsonSchemaType.Null;
                    }
                    if (required && (schema.Required != true))
                    {
                        schema.Required = true;
                    }
                    return schema;
                }
            }
            if (this._stack.Any<TypeSchema>(tc => tc.Type == type))
            {
                throw new JsonException("Unresolved circular reference for type '{0}'. Explicitly define an Id for the type using a JsonObject/JsonArray attribute or automatically generate a type Id using the UndefinedSchemaIdHandling property.".FormatWith(CultureInfo.InvariantCulture, type));
            }
            JsonContract contract = this.ContractResolver.ResolveContract(type);
            if (((converter = contract.Converter) != null) || ((converter = contract.InternalConverter) != null))
            {
                JsonSchema schema2 = converter.GetSchema();
                if (schema2 != null)
                {
                    return schema2;
                }
            }
            this.Push(new TypeSchema(type, new JsonSchema()));
            if (str2 != null)
            {
                this.CurrentSchema.Id = str2;
            }
            if (required)
            {
                this.CurrentSchema.Required = true;
            }
            this.CurrentSchema.Title = this.GetTitle(type);
            this.CurrentSchema.Description = this.GetDescription(type);
            if (converter != null)
            {
                this.CurrentSchema.Type = 0x7f;
            }
            else
            {
                switch (contract.ContractType)
                {
                    case JsonContractType.Object:
                        this.CurrentSchema.Type = new JsonSchemaType?(this.AddNullType(JsonSchemaType.Object, valueRequired));
                        this.CurrentSchema.Id = this.GetTypeId(type, false);
                        this.GenerateObjectSchema(type, (JsonObjectContract) contract);
                        goto Label_04EF;

                    case JsonContractType.Array:
                    {
                        this.CurrentSchema.Type = new JsonSchemaType?(this.AddNullType(JsonSchemaType.Array, valueRequired));
                        this.CurrentSchema.Id = this.GetTypeId(type, false);
                        JsonArrayAttribute jsonContainerAttribute = JsonTypeReflector.GetJsonContainerAttribute(type) as JsonArrayAttribute;
                        bool flag = (jsonContainerAttribute == null) || jsonContainerAttribute.AllowNullItems;
                        Type collectionItemType = ReflectionUtils.GetCollectionItemType(type);
                        if (collectionItemType != null)
                        {
                            this.CurrentSchema.Items = new List<JsonSchema>();
                            this.CurrentSchema.Items.Add(this.GenerateInternal(collectionItemType, !flag ? Required.Always : Required.Default, false));
                        }
                        goto Label_04EF;
                    }
                    case JsonContractType.Primitive:
                        this.CurrentSchema.Type = new JsonSchemaType?(this.GetJsonSchemaType(type, valueRequired));
                        if (((((JsonSchemaType) this.CurrentSchema.Type) == JsonSchemaType.Integer) && type.IsEnum()) && !type.IsDefined(typeof(FlagsAttribute), true))
                        {
                            this.CurrentSchema.Enum = new List<JToken>();
                            this.CurrentSchema.Options = new Dictionary<JToken, string>();
                            foreach (EnumValue<long> value2 in EnumUtils.GetNamesAndValues<long>(type))
                            {
                                JToken item = JToken.FromObject(value2.Value);
                                this.CurrentSchema.Enum.Add(item);
                                this.CurrentSchema.Options.Add(item, value2.Name);
                            }
                        }
                        goto Label_04EF;

                    case JsonContractType.String:
                    {
                        JsonSchemaType type3 = !ReflectionUtils.IsNullable(contract.UnderlyingType) ? JsonSchemaType.String : this.AddNullType(JsonSchemaType.String, valueRequired);
                        this.CurrentSchema.Type = new JsonSchemaType?(type3);
                        goto Label_04EF;
                    }
                    case JsonContractType.Dictionary:
                        Type type4;
                        Type type5;
                        this.CurrentSchema.Type = new JsonSchemaType?(this.AddNullType(JsonSchemaType.Object, valueRequired));
                        ReflectionUtils.GetDictionaryKeyValueTypes(type, out type4, out type5);
                        if ((type4 != null) && ConvertUtils.IsConvertible(type4))
                        {
                            this.CurrentSchema.AdditionalProperties = this.GenerateInternal(type5, Required.Default, false);
                        }
                        goto Label_04EF;

                    case JsonContractType.Dynamic:
                    case JsonContractType.Linq:
                        this.CurrentSchema.Type = 0x7f;
                        goto Label_04EF;

                    case JsonContractType.Serializable:
                        this.CurrentSchema.Type = new JsonSchemaType?(this.AddNullType(JsonSchemaType.Object, valueRequired));
                        this.CurrentSchema.Id = this.GetTypeId(type, false);
                        this.GenerateISerializableContract(type, (JsonISerializableContract) contract);
                        goto Label_04EF;
                }
                throw new JsonException("Unexpected contract type: {0}".FormatWith(CultureInfo.InvariantCulture, contract));
            }
        Label_04EF:
            return this.Pop().Schema;
        }

        private void GenerateISerializableContract(Type type, JsonISerializableContract contract)
        {
            this.CurrentSchema.AllowAdditionalProperties = true;
        }

        private void GenerateObjectSchema(Type type, JsonObjectContract contract)
        {
            this.CurrentSchema.Properties = new Dictionary<string, JsonSchema>();
            foreach (JsonProperty property in contract.Properties)
            {
                if (!property.Ignored)
                {
                    bool flag = (((((NullValueHandling) property.NullValueHandling) == NullValueHandling.Ignore) || this.HasFlag(property.DefaultValueHandling.GetValueOrDefault(), DefaultValueHandling.Ignore)) || (property.ShouldSerialize != null)) || (property.GetIsSpecified != null);
                    JsonSchema schema = this.GenerateInternal(property.PropertyType, property.Required, !flag);
                    if (property.DefaultValue != null)
                    {
                        schema.Default = JToken.FromObject(property.DefaultValue);
                    }
                    this.CurrentSchema.Properties.Add(property.PropertyName, schema);
                }
            }
            if (type.IsSealed())
            {
                this.CurrentSchema.AllowAdditionalProperties = false;
            }
        }

        private string GetDescription(Type type)
        {
            JsonContainerAttribute jsonContainerAttribute = JsonTypeReflector.GetJsonContainerAttribute(type);
            if ((jsonContainerAttribute != null) && !string.IsNullOrEmpty(jsonContainerAttribute.Description))
            {
                return jsonContainerAttribute.Description;
            }
            DescriptionAttribute attribute = ReflectionUtils.GetAttribute<DescriptionAttribute>(type);
            if (attribute != null)
            {
                return attribute.Description;
            }
            return null;
        }

        private JsonSchemaType GetJsonSchemaType(Type type, Required valueRequired)
        {
            JsonSchemaType none = JsonSchemaType.None;
            if ((valueRequired != Required.Always) && ReflectionUtils.IsNullable(type))
            {
                none = JsonSchemaType.Null;
                if (ReflectionUtils.IsNullableType(type))
                {
                    type = Nullable.GetUnderlyingType(type);
                }
            }
            TypeCode typeCode = ConvertUtils.GetTypeCode(type);
            switch (typeCode)
            {
                case TypeCode.Empty:
                case TypeCode.Object:
                    return (none | JsonSchemaType.String);

                case TypeCode.DBNull:
                    return (none | JsonSchemaType.Null);

                case TypeCode.Boolean:
                    return (none | JsonSchemaType.Boolean);

                case TypeCode.Char:
                    return (none | JsonSchemaType.String);

                case TypeCode.SByte:
                case TypeCode.Byte:
                case TypeCode.Int16:
                case TypeCode.UInt16:
                case TypeCode.Int32:
                case TypeCode.UInt32:
                case TypeCode.Int64:
                case TypeCode.UInt64:
                    return (none | JsonSchemaType.Integer);

                case TypeCode.Single:
                case TypeCode.Double:
                case TypeCode.Decimal:
                    return (none | JsonSchemaType.Float);

                case TypeCode.DateTime:
                    return (none | JsonSchemaType.String);

                case TypeCode.String:
                    return (none | JsonSchemaType.String);
            }
            throw new JsonException("Unexpected type code '{0}' for type '{1}'.".FormatWith(CultureInfo.InvariantCulture, typeCode, type));
        }

        private string GetTitle(Type type)
        {
            JsonContainerAttribute jsonContainerAttribute = JsonTypeReflector.GetJsonContainerAttribute(type);
            if ((jsonContainerAttribute != null) && !string.IsNullOrEmpty(jsonContainerAttribute.Title))
            {
                return jsonContainerAttribute.Title;
            }
            return null;
        }

        private string GetTypeId(Type type, bool explicitOnly)
        {
            JsonContainerAttribute jsonContainerAttribute = JsonTypeReflector.GetJsonContainerAttribute(type);
            if ((jsonContainerAttribute != null) && !string.IsNullOrEmpty(jsonContainerAttribute.Id))
            {
                return jsonContainerAttribute.Id;
            }
            if (!explicitOnly)
            {
                switch (this.UndefinedSchemaIdHandling)
                {
                    case Newtonsoft.Json.Schema.UndefinedSchemaIdHandling.UseTypeName:
                        return type.FullName;

                    case Newtonsoft.Json.Schema.UndefinedSchemaIdHandling.UseAssemblyQualifiedName:
                        return type.AssemblyQualifiedName;
                }
            }
            return null;
        }

        private bool HasFlag(DefaultValueHandling value, DefaultValueHandling flag)
        {
            return ((value & flag) == flag);
        }

        internal static bool HasFlag(JsonSchemaType? value, JsonSchemaType flag)
        {
            if (!value.HasValue)
            {
                return true;
            }
            JsonSchemaType? nullable = value;
            JsonSchemaType type = flag;
            JsonSchemaType? nullable3 = nullable.HasValue ? new JsonSchemaType?(((JsonSchemaType) nullable.GetValueOrDefault()) & type) : null;
            JsonSchemaType type2 = flag;
            return (((((JsonSchemaType) nullable3.GetValueOrDefault()) == type2) && nullable3.HasValue) || ((((JsonSchemaType) value) == JsonSchemaType.Float) && (flag == JsonSchemaType.Integer)));
        }

        private TypeSchema Pop()
        {
            TypeSchema schema = this._stack[this._stack.Count - 1];
            this._stack.RemoveAt(this._stack.Count - 1);
            TypeSchema schema2 = this._stack.LastOrDefault<TypeSchema>();
            if (schema2 != null)
            {
                this._currentSchema = schema2.Schema;
                return schema;
            }
            this._currentSchema = null;
            return schema;
        }

        private void Push(TypeSchema typeSchema)
        {
            this._currentSchema = typeSchema.Schema;
            this._stack.Add(typeSchema);
            this._resolver.LoadedSchemas.Add(typeSchema.Schema);
        }

        public IContractResolver ContractResolver
        {
            get
            {
                if (this._contractResolver == null)
                {
                    return DefaultContractResolver.Instance;
                }
                return this._contractResolver;
            }
            set
            {
                this._contractResolver = value;
            }
        }

        private JsonSchema CurrentSchema
        {
            get
            {
                return this._currentSchema;
            }
        }

        public Newtonsoft.Json.Schema.UndefinedSchemaIdHandling UndefinedSchemaIdHandling { get; set; }

        private class TypeSchema
        {
            public TypeSchema(System.Type type, JsonSchema schema)
            {
                ValidationUtils.ArgumentNotNull(type, "type");
                ValidationUtils.ArgumentNotNull(schema, "schema");
                this.Type = type;
                this.Schema = schema;
            }

            public JsonSchema Schema { get; private set; }

            public System.Type Type { get; private set; }
        }
    }
}

