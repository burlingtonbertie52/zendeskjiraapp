﻿namespace Newtonsoft.Json
{
    using Newtonsoft.Json.Utilities;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Runtime.InteropServices;
    using System.Text;

    [StructLayout(LayoutKind.Sequential)]
    internal struct JsonPosition
    {
        internal JsonContainerType Type;
        internal int Position;
        internal string PropertyName;
        internal bool HasIndex;
        public JsonPosition(JsonContainerType type)
        {
            this.Type = type;
            this.HasIndex = TypeHasIndex(type);
            this.Position = -1;
            this.PropertyName = null;
        }

        internal void WriteTo(StringBuilder sb)
        {
            switch (this.Type)
            {
                case JsonContainerType.Object:
                    if (sb.Length > 0)
                    {
                        sb.Append(".");
                    }
                    sb.Append(this.PropertyName);
                    return;

                case JsonContainerType.Array:
                case JsonContainerType.Constructor:
                    sb.Append("[");
                    sb.Append(this.Position);
                    sb.Append("]");
                    return;
            }
        }

        internal static bool TypeHasIndex(JsonContainerType type)
        {
            if (type != JsonContainerType.Array)
            {
                return (type == JsonContainerType.Constructor);
            }
            return true;
        }

        internal static string BuildPath(IEnumerable<JsonPosition> positions)
        {
            StringBuilder sb = new StringBuilder();
            foreach (JsonPosition position in positions)
            {
                position.WriteTo(sb);
            }
            return sb.ToString();
        }

        internal static string FormatMessage(IJsonLineInfo lineInfo, string path, string message)
        {
            if (!message.EndsWith(Environment.NewLine))
            {
                message = message.Trim();
                if (!message.EndsWith("."))
                {
                    message = message + ".";
                }
                message = message + " ";
            }
            message = message + "Path '{0}'".FormatWith(CultureInfo.InvariantCulture, path);
            if ((lineInfo != null) && lineInfo.HasLineInfo())
            {
                message = message + ", line {0}, position {1}".FormatWith(CultureInfo.InvariantCulture, lineInfo.LineNumber, lineInfo.LinePosition);
            }
            message = message + ".";
            return message;
        }
    }
}

