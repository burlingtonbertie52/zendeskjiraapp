﻿namespace Newtonsoft.Json.Bson
{
    using System;
    using System.Runtime.CompilerServices;

    internal abstract class BsonToken
    {
        protected BsonToken()
        {
        }

        public int CalculatedSize { get; set; }

        public BsonToken Parent { get; set; }

        public abstract BsonType Type { get; }
    }
}

