﻿namespace Newtonsoft.Json.Bson
{
    using System;

    internal class BsonValue : BsonToken
    {
        private readonly BsonType _type;
        private readonly object _value;

        public BsonValue(object value, BsonType type)
        {
            this._value = value;
            this._type = type;
        }

        public override BsonType Type
        {
            get
            {
                return this._type;
            }
        }

        public object Value
        {
            get
            {
                return this._value;
            }
        }
    }
}

