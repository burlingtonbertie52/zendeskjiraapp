﻿namespace Newtonsoft.Json
{
    using Newtonsoft.Json.Utilities;
    using System;
    using System.IO;

    public class JsonTextWriter : JsonWriter
    {
        private Newtonsoft.Json.Utilities.Base64Encoder _base64Encoder;
        private int _indentation;
        private char _indentChar;
        private char _quoteChar;
        private bool _quoteName;
        private readonly TextWriter _writer;

        public JsonTextWriter(TextWriter textWriter)
        {
            if (textWriter == null)
            {
                throw new ArgumentNullException("textWriter");
            }
            this._writer = textWriter;
            this._quoteChar = '"';
            this._quoteName = true;
            this._indentChar = ' ';
            this._indentation = 2;
        }

        public override void Close()
        {
            base.Close();
            if (base.CloseOutput && (this._writer != null))
            {
                this._writer.Close();
            }
        }

        public override void Flush()
        {
            this._writer.Flush();
        }

        private bool[] GetCharEscapeFlags()
        {
            if (base.StringEscapeHandling == StringEscapeHandling.EscapeHtml)
            {
                return JavaScriptUtils.HtmlCharEscapeFlags;
            }
            if (this._quoteChar == '"')
            {
                return JavaScriptUtils.DoubleQuoteCharEscapeFlags;
            }
            return JavaScriptUtils.SingleQuoteCharEscapeFlags;
        }

        public override void WriteComment(string text)
        {
            base.InternalWriteComment();
            this._writer.Write("/*");
            this._writer.Write(text);
            this._writer.Write("*/");
        }

        protected override void WriteEnd(JsonToken token)
        {
            switch (token)
            {
                case JsonToken.EndObject:
                    this._writer.Write("}");
                    return;

                case JsonToken.EndArray:
                    this._writer.Write("]");
                    return;

                case JsonToken.EndConstructor:
                    this._writer.Write(")");
                    return;
            }
            throw JsonWriterException.Create(this, "Invalid JsonToken: " + token, null);
        }

        protected override void WriteIndent()
        {
            int num2;
            this._writer.Write(Environment.NewLine);
            for (int i = base.Top * this._indentation; i > 0; i -= num2)
            {
                num2 = Math.Min(i, 10);
                this._writer.Write(new string(this._indentChar, num2));
            }
        }

        protected override void WriteIndentSpace()
        {
            this._writer.Write(' ');
        }

        public override void WriteNull()
        {
            base.InternalWriteNull();
            this.WriteValueInternal(JsonConvert.Null, JsonToken.Null);
        }

        public override void WritePropertyName(string name)
        {
            base.InternalWritePropertyName(name);
            JavaScriptUtils.WriteEscapedJavaScriptString(this._writer, name, this._quoteChar, this._quoteName, this.GetCharEscapeFlags(), base.StringEscapeHandling);
            this._writer.Write(':');
        }

        public override void WriteRaw(string json)
        {
            base.InternalWriteRaw();
            this._writer.Write(json);
        }

        public override void WriteStartArray()
        {
            base.InternalWriteStart(JsonToken.StartArray, JsonContainerType.Array);
            this._writer.Write("[");
        }

        public override void WriteStartConstructor(string name)
        {
            base.InternalWriteStart(JsonToken.StartConstructor, JsonContainerType.Constructor);
            this._writer.Write("new ");
            this._writer.Write(name);
            this._writer.Write("(");
        }

        public override void WriteStartObject()
        {
            base.InternalWriteStart(JsonToken.StartObject, JsonContainerType.Object);
            this._writer.Write("{");
        }

        public override void WriteUndefined()
        {
            base.InternalWriteUndefined();
            this.WriteValueInternal(JsonConvert.Undefined, JsonToken.Undefined);
        }

        public override void WriteValue(bool value)
        {
            base.InternalWriteValue(JsonToken.Boolean);
            this.WriteValueInternal(JsonConvert.ToString(value), JsonToken.Boolean);
        }

        public override void WriteValue(byte value)
        {
            base.InternalWriteValue(JsonToken.Integer);
            this.WriteValueInternal(JsonConvert.ToString(value), JsonToken.Integer);
        }

        public override void WriteValue(char value)
        {
            base.InternalWriteValue(JsonToken.String);
            this.WriteValueInternal(JsonConvert.ToString(value), JsonToken.String);
        }

        public override void WriteValue(DateTime value)
        {
            base.InternalWriteValue(JsonToken.Date);
            value = JsonConvert.EnsureDateTime(value, base.DateTimeZoneHandling);
            JsonConvert.WriteDateTimeString(this._writer, value, base.DateFormatHandling, this._quoteChar);
        }

        public override void WriteValue(DateTimeOffset value)
        {
            base.InternalWriteValue(JsonToken.Date);
            this.WriteValueInternal(JsonConvert.ToString(value, base.DateFormatHandling, this._quoteChar), JsonToken.Date);
        }

        public override void WriteValue(decimal value)
        {
            base.InternalWriteValue(JsonToken.Float);
            this.WriteValueInternal(JsonConvert.ToString(value), JsonToken.Float);
        }

        public override void WriteValue(double value)
        {
            base.InternalWriteValue(JsonToken.Float);
            this.WriteValueInternal(JsonConvert.ToString(value), JsonToken.Float);
        }

        public override void WriteValue(Guid value)
        {
            base.InternalWriteValue(JsonToken.String);
            this.WriteValueInternal(JsonConvert.ToString(value, this._quoteChar), JsonToken.String);
        }

        public override void WriteValue(short value)
        {
            base.InternalWriteValue(JsonToken.Integer);
            this.WriteValueInternal(JsonConvert.ToString(value), JsonToken.Integer);
        }

        public override void WriteValue(int value)
        {
            base.InternalWriteValue(JsonToken.Integer);
            this.WriteValueInternal(JsonConvert.ToString(value), JsonToken.Integer);
        }

        public override void WriteValue(long value)
        {
            base.InternalWriteValue(JsonToken.Integer);
            this.WriteValueInternal(JsonConvert.ToString(value), JsonToken.Integer);
        }

        [CLSCompliant(false)]
        public override void WriteValue(sbyte value)
        {
            base.InternalWriteValue(JsonToken.Integer);
            this.WriteValueInternal(JsonConvert.ToString(value), JsonToken.Integer);
        }

        public override void WriteValue(float value)
        {
            base.InternalWriteValue(JsonToken.Float);
            this.WriteValueInternal(JsonConvert.ToString(value), JsonToken.Float);
        }

        public override void WriteValue(string value)
        {
            base.InternalWriteValue(JsonToken.String);
            if (value == null)
            {
                this.WriteValueInternal(JsonConvert.Null, JsonToken.Null);
            }
            else
            {
                JavaScriptUtils.WriteEscapedJavaScriptString(this._writer, value, this._quoteChar, true, this.GetCharEscapeFlags(), base.StringEscapeHandling);
            }
        }

        public override void WriteValue(TimeSpan value)
        {
            base.InternalWriteValue(JsonToken.String);
            this.WriteValueInternal(JsonConvert.ToString(value, this._quoteChar), JsonToken.String);
        }

        [CLSCompliant(false)]
        public override void WriteValue(ushort value)
        {
            base.InternalWriteValue(JsonToken.Integer);
            this.WriteValueInternal(JsonConvert.ToString(value), JsonToken.Integer);
        }

        [CLSCompliant(false)]
        public override void WriteValue(uint value)
        {
            base.InternalWriteValue(JsonToken.Integer);
            this.WriteValueInternal(JsonConvert.ToString(value), JsonToken.Integer);
        }

        [CLSCompliant(false)]
        public override void WriteValue(ulong value)
        {
            base.InternalWriteValue(JsonToken.Integer);
            this.WriteValueInternal(JsonConvert.ToString(value), JsonToken.Integer);
        }

        public override void WriteValue(byte[] value)
        {
            if (value == null)
            {
                this.WriteNull();
            }
            else
            {
                base.InternalWriteValue(JsonToken.Bytes);
                this._writer.Write(this._quoteChar);
                this.Base64Encoder.Encode(value, 0, value.Length);
                this.Base64Encoder.Flush();
                this._writer.Write(this._quoteChar);
            }
        }

        public override void WriteValue(Uri value)
        {
            if (value == null)
            {
                this.WriteNull();
            }
            else
            {
                base.InternalWriteValue(JsonToken.String);
                this.WriteValueInternal(JsonConvert.ToString(value, this._quoteChar), JsonToken.String);
            }
        }

        protected override void WriteValueDelimiter()
        {
            this._writer.Write(',');
        }

        private void WriteValueInternal(string value, JsonToken token)
        {
            this._writer.Write(value);
        }

        public override void WriteWhitespace(string ws)
        {
            base.InternalWriteWhitespace(ws);
            this._writer.Write(ws);
        }

        private Newtonsoft.Json.Utilities.Base64Encoder Base64Encoder
        {
            get
            {
                if (this._base64Encoder == null)
                {
                    this._base64Encoder = new Newtonsoft.Json.Utilities.Base64Encoder(this._writer);
                }
                return this._base64Encoder;
            }
        }

        public int Indentation
        {
            get
            {
                return this._indentation;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("Indentation value must be greater than 0.");
                }
                this._indentation = value;
            }
        }

        public char IndentChar
        {
            get
            {
                return this._indentChar;
            }
            set
            {
                this._indentChar = value;
            }
        }

        public char QuoteChar
        {
            get
            {
                return this._quoteChar;
            }
            set
            {
                if ((value != '"') && (value != '\''))
                {
                    throw new ArgumentException("Invalid JavaScript string quote character. Valid quote characters are ' and \".");
                }
                this._quoteChar = value;
            }
        }

        public bool QuoteName
        {
            get
            {
                return this._quoteName;
            }
            set
            {
                this._quoteName = value;
            }
        }
    }
}

