﻿namespace DemoApplication
{
    using DemoApplication.Properties;
    using Mantin.Controls.Wpf.Notification;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Windows;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;

    internal class ViewModel : ObservableBase
    {
        private bool autoWidth;
        private string balloonTitle;
        private Mantin.Controls.Wpf.Notification.BalloonType balloonType;
        private Color borderColor;
        private Color endColor;
        private Color fontColor;
        private string helpText;
        private string hyperlinkText;
        private double maxHeight;
        private double maxWidth;
        private EnumMember selectedBalloonType;
        private EnumMember selectedNotificationType;
        private bool showBalloonCloseButton;
        private Color startColor;
        private string text;
        private string title;

        public ViewModel()
        {
            Action<object> execute = null;
            this.helpText = "Help Balloon will default to the bottom and right side unless it will move off of the screen, then it will shift to the left side.  Setting the Max Height property will auto enable vertical scrollbars.";
            this.hyperlinkText = "Click Me!";
            this.text = "This is unobtrusive text that I want my user to see.";
            this.title = "My Title";
            this.showBalloonCloseButton = true;
            this.SelectedNotificationType = this.NotificationTypeList.First<EnumMember>();
            this.SelectedBalloonType = this.BalloonTypeList.First<EnumMember>();
            if (execute == null)
            {
                execute = param => this.PopToastExecute(param);
            }
            this.PopToastCommand = new RelayCommand(execute);
            this.StartColor = Color.FromRgb(0xfd, 0xd5, 0xa7);
            this.EndColor = Color.FromRgb(0xfc, 0xe7, 0x9f);
            this.BorderColor = Color.FromRgb(0xa9, 0xa9, 0xa9);
            this.FontColor = Color.FromRgb(0, 0, 0);
        }

        public void PopToastExecute(object param)
        {
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, delegate {
                LinearGradientBrush brush = new LinearGradientBrush(this.StartColor, this.EndColor, 90.0);
                SolidColorBrush brush2 = new SolidColorBrush(this.BorderColor);
                SolidColorBrush brush3 = new SolidColorBrush(this.fontColor);
                NotificationType notificationType = (NotificationType) Enum.Parse(typeof(NotificationType), this.SelectedNotificationType.Value.ToString());
                string str = param.ToString();
                if (str != null)
                {
                    ToastPopUp up;
                    if (!(str == "1"))
                    {
                        if (!(str == "2"))
                        {
                            if (str == "3")
                            {
                                List<Inline> textInlines = new List<Inline>();
                                System.Windows.Documents.Run item = new System.Windows.Documents.Run {
                                    Text = this.Text
                                };
                                textInlines.Add(item);
                                System.Windows.Documents.Run run2 = new System.Windows.Documents.Run {
                                    Text = Environment.NewLine
                                };
                                textInlines.Add(run2);
                                System.Windows.Documents.Run run3 = new System.Windows.Documents.Run("This text will be italic.") {
                                    FontStyle = FontStyles.Italic
                                };
                                textInlines.Add(run3);
                                up = new ToastPopUp(this.Title, textInlines, this.HyperlinkText, notificationType, null) {
                                    Background = brush,
                                    BorderBrush = brush2,
                                    FontColor = brush3
                                };
                                up.HyperlinkClicked += new EventHandler<HyperLinkEventArgs>(this.ToastHyperlinkClicked);
                                up.ClosedByUser += new EventHandler<EventArgs>(this.ToastClosedByUser);
                                up.Show();
                            }
                            return;
                        }
                    }
                    else
                    {
                        up = new ToastPopUp(this.Title, this.Text, this.HyperlinkText, notificationType, null) {
                            Background = brush,
                            BorderBrush = brush2,
                            FontColor = brush3
                        };
                        up.HyperlinkClicked += new EventHandler<HyperLinkEventArgs>(this.ToastHyperlinkClicked);
                        up.ClosedByUser += new EventHandler<EventArgs>(this.ToastClosedByUser);
                        up.Show();
                        return;
                    }
                    up = new ToastPopUp(this.Title, this.Text, this.HyperlinkText, Resources.disk_blue, null) {
                        Background = brush,
                        BorderBrush = brush2,
                        FontColor = brush3
                    };
                    up.HyperlinkClicked += new EventHandler<HyperLinkEventArgs>(this.ToastHyperlinkClicked);
                    up.ClosedByUser += new EventHandler<EventArgs>(this.ToastClosedByUser);
                    up.Show();
                }
            });
        }

        private void ToastClosedByUser(object sender, EventArgs e)
        {
            MessageBox.Show("User closed the toast.");
        }

        private void ToastHyperlinkClicked(object sender, EventArgs e)
        {
            MessageBox.Show("Hyper link clicked.");
        }

        public bool AutoWidth
        {
            get
            {
                return this.autoWidth;
            }
            set
            {
                if (this.autoWidth != value)
                {
                    this.autoWidth = value;
                    this.OnPropertyChanged<bool>(System.Linq.Expressions.Expression.Lambda<Func<bool>>(System.Linq.Expressions.Expression.Property(System.Linq.Expressions.Expression.Constant(this, typeof(ViewModel)), (MethodInfo) methodof(ViewModel.get_AutoWidth)), new ParameterExpression[0]));
                }
            }
        }

        public string BalloonTitle
        {
            get
            {
                return this.balloonTitle;
            }
            set
            {
                if (this.balloonTitle != value)
                {
                    this.balloonTitle = value;
                    this.OnPropertyChanged<string>(System.Linq.Expressions.Expression.Lambda<Func<string>>(System.Linq.Expressions.Expression.Property(System.Linq.Expressions.Expression.Constant(this, typeof(ViewModel)), (MethodInfo) methodof(ViewModel.get_BalloonTitle)), new ParameterExpression[0]));
                }
            }
        }

        public Mantin.Controls.Wpf.Notification.BalloonType BalloonType
        {
            get
            {
                return this.balloonType;
            }
            set
            {
                if (this.balloonType != value)
                {
                    this.balloonType = value;
                    this.OnPropertyChanged<Mantin.Controls.Wpf.Notification.BalloonType>(System.Linq.Expressions.Expression.Lambda<Func<Mantin.Controls.Wpf.Notification.BalloonType>>(System.Linq.Expressions.Expression.Property(System.Linq.Expressions.Expression.Constant(this, typeof(ViewModel)), (MethodInfo) methodof(ViewModel.get_BalloonType)), new ParameterExpression[0]));
                }
            }
        }

        public List<EnumMember> BalloonTypeList
        {
            get
            {
                return EnumMember.ConvertToList<Mantin.Controls.Wpf.Notification.BalloonType>();
            }
        }

        public Color BorderColor
        {
            get
            {
                return this.borderColor;
            }
            set
            {
                if (this.borderColor != value)
                {
                    this.borderColor = value;
                    this.OnPropertyChanged<Color>(System.Linq.Expressions.Expression.Lambda<Func<Color>>(System.Linq.Expressions.Expression.Property(System.Linq.Expressions.Expression.Constant(this, typeof(ViewModel)), (MethodInfo) methodof(ViewModel.get_BorderColor)), new ParameterExpression[0]));
                }
            }
        }

        public Color EndColor
        {
            get
            {
                return this.endColor;
            }
            set
            {
                if (this.endColor != value)
                {
                    this.endColor = value;
                    this.OnPropertyChanged<Color>(System.Linq.Expressions.Expression.Lambda<Func<Color>>(System.Linq.Expressions.Expression.Property(System.Linq.Expressions.Expression.Constant(this, typeof(ViewModel)), (MethodInfo) methodof(ViewModel.get_EndColor)), new ParameterExpression[0]));
                }
            }
        }

        public Color FontColor
        {
            get
            {
                return this.fontColor;
            }
            set
            {
                if (this.fontColor != value)
                {
                    this.fontColor = value;
                    this.OnPropertyChanged<Color>(System.Linq.Expressions.Expression.Lambda<Func<Color>>(System.Linq.Expressions.Expression.Property(System.Linq.Expressions.Expression.Constant(this, typeof(ViewModel)), (MethodInfo) methodof(ViewModel.get_FontColor)), new ParameterExpression[0]));
                }
            }
        }

        public string HelpText
        {
            get
            {
                return this.helpText;
            }
            set
            {
                if (this.helpText != value)
                {
                    this.helpText = value;
                    this.OnPropertyChanged<string>(System.Linq.Expressions.Expression.Lambda<Func<string>>(System.Linq.Expressions.Expression.Property(System.Linq.Expressions.Expression.Constant(this, typeof(ViewModel)), (MethodInfo) methodof(ViewModel.get_HelpText)), new ParameterExpression[0]));
                }
            }
        }

        public string HyperlinkText
        {
            get
            {
                return this.hyperlinkText;
            }
            set
            {
                if (this.hyperlinkText != value)
                {
                    this.hyperlinkText = value;
                    this.OnPropertyChanged<string>(System.Linq.Expressions.Expression.Lambda<Func<string>>(System.Linq.Expressions.Expression.Property(System.Linq.Expressions.Expression.Constant(this, typeof(ViewModel)), (MethodInfo) methodof(ViewModel.get_HyperlinkText)), new ParameterExpression[0]));
                }
            }
        }

        public double MaxHeight
        {
            get
            {
                return this.maxHeight;
            }
            set
            {
                if (this.maxHeight != value)
                {
                    this.maxHeight = value;
                    this.OnPropertyChanged<double>(System.Linq.Expressions.Expression.Lambda<Func<double>>(System.Linq.Expressions.Expression.Property(System.Linq.Expressions.Expression.Constant(this, typeof(ViewModel)), (MethodInfo) methodof(ViewModel.get_MaxHeight)), new ParameterExpression[0]));
                }
            }
        }

        public double MaxWidth
        {
            get
            {
                return this.maxWidth;
            }
            set
            {
                if (this.maxWidth != value)
                {
                    this.maxWidth = value;
                    this.OnPropertyChanged<double>(System.Linq.Expressions.Expression.Lambda<Func<double>>(System.Linq.Expressions.Expression.Property(System.Linq.Expressions.Expression.Constant(this, typeof(ViewModel)), (MethodInfo) methodof(ViewModel.get_MaxWidth)), new ParameterExpression[0]));
                }
            }
        }

        public List<EnumMember> NotificationTypeList
        {
            get
            {
                return EnumMember.ConvertToList<NotificationType>();
            }
        }

        public ICommand PopToastCommand { get; set; }

        public EnumMember SelectedBalloonType
        {
            get
            {
                return this.selectedNotificationType;
            }
            set
            {
                if (this.selectedBalloonType != value)
                {
                    this.selectedBalloonType = value;
                    this.OnPropertyChanged<EnumMember>(System.Linq.Expressions.Expression.Lambda<Func<EnumMember>>(System.Linq.Expressions.Expression.Property(System.Linq.Expressions.Expression.Constant(this, typeof(ViewModel)), (MethodInfo) methodof(ViewModel.get_SelectedBalloonType)), new ParameterExpression[0]));
                    this.BalloonType = (Mantin.Controls.Wpf.Notification.BalloonType) Enum.Parse(typeof(Mantin.Controls.Wpf.Notification.BalloonType), value.Value.ToString());
                }
            }
        }

        public EnumMember SelectedNotificationType
        {
            get
            {
                return this.selectedNotificationType;
            }
            set
            {
                if (this.selectedNotificationType != value)
                {
                    this.selectedNotificationType = value;
                    this.OnPropertyChanged<EnumMember>(System.Linq.Expressions.Expression.Lambda<Func<EnumMember>>(System.Linq.Expressions.Expression.Property(System.Linq.Expressions.Expression.Constant(this, typeof(ViewModel)), (MethodInfo) methodof(ViewModel.get_SelectedNotificationType)), new ParameterExpression[0]));
                }
            }
        }

        public bool ShowBalloonCloseButton
        {
            get
            {
                return this.showBalloonCloseButton;
            }
            set
            {
                if (this.showBalloonCloseButton != value)
                {
                    this.showBalloonCloseButton = value;
                    this.OnPropertyChanged<bool>(System.Linq.Expressions.Expression.Lambda<Func<bool>>(System.Linq.Expressions.Expression.Property(System.Linq.Expressions.Expression.Constant(this, typeof(ViewModel)), (MethodInfo) methodof(ViewModel.get_ShowBalloonCloseButton)), new ParameterExpression[0]));
                }
            }
        }

        public Color StartColor
        {
            get
            {
                return this.startColor;
            }
            set
            {
                if (this.startColor != value)
                {
                    this.startColor = value;
                    this.OnPropertyChanged<Color>(System.Linq.Expressions.Expression.Lambda<Func<Color>>(System.Linq.Expressions.Expression.Property(System.Linq.Expressions.Expression.Constant(this, typeof(ViewModel)), (MethodInfo) methodof(ViewModel.get_StartColor)), new ParameterExpression[0]));
                }
            }
        }

        public string Text
        {
            get
            {
                return this.text;
            }
            set
            {
                if (this.text != value)
                {
                    this.text = value;
                    this.OnPropertyChanged<string>(System.Linq.Expressions.Expression.Lambda<Func<string>>(System.Linq.Expressions.Expression.Property(System.Linq.Expressions.Expression.Constant(this, typeof(ViewModel)), (MethodInfo) methodof(ViewModel.get_Text)), new ParameterExpression[0]));
                }
            }
        }

        public string Title
        {
            get
            {
                return this.title;
            }
            set
            {
                if (this.title != value)
                {
                    this.title = value;
                    this.OnPropertyChanged<string>(System.Linq.Expressions.Expression.Lambda<Func<string>>(System.Linq.Expressions.Expression.Property(System.Linq.Expressions.Expression.Constant(this, typeof(ViewModel)), (MethodInfo) methodof(ViewModel.get_Title)), new ParameterExpression[0]));
                }
            }
        }
    }
}

