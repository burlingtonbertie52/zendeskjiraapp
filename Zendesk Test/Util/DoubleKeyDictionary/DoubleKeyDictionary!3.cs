﻿namespace Util.DoubleKeyDictionary
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Reflection;
    using System.Runtime.CompilerServices;

    internal class DoubleKeyDictionary<K, T, V> : IEnumerable, IEnumerable<DoubleKeyPairValue<K, T, V>>, IEquatable<Util.DoubleKeyDictionary.DoubleKeyDictionary<K, T, V>>
    {
        public DoubleKeyDictionary()
        {
            this.OuterDictionary = new Dictionary<K, Dictionary<T, V>>();
        }

        public void Add(K key1, T key2, V value)
        {
            if (this.OuterDictionary.ContainsKey(key1))
            {
                if (this.OuterDictionary[key1].ContainsKey(key2))
                {
                    this.OuterDictionary[key1][key2] = value;
                }
                else
                {
                    Dictionary<T, V> dictionary = this.OuterDictionary[key1];
                    dictionary.Add(key2, value);
                    this.OuterDictionary[key1] = dictionary;
                }
            }
            else
            {
                Dictionary<T, V> dictionary2 = new Dictionary<T, V>();
                dictionary2[key2] = value;
                this.OuterDictionary.Add(key1, dictionary2);
            }
        }

        private static bool CheckInnerDictionary(KeyValuePair<K, Dictionary<T, V>> innerItems, Dictionary<T, V> otherInnerDictionary)
        {
            bool flag = true;
            foreach (KeyValuePair<T, V> pair in innerItems.Value)
            {
                if (!otherInnerDictionary.ContainsValue(pair.Value))
                {
                    flag = false;
                }
                if (!otherInnerDictionary.ContainsKey(pair.Key))
                {
                    flag = false;
                }
            }
            return flag;
        }

        public bool ContainsKey(K key1, T key2)
        {
            bool flag = false;
            if (this.OuterDictionary.ContainsKey(key1) && this.OuterDictionary[key1].ContainsKey(key2))
            {
                flag = true;
            }
            return flag;
        }

        public bool Equals(Util.DoubleKeyDictionary.DoubleKeyDictionary<K, T, V> other)
        {
            if (this.OuterDictionary.Keys.Count != other.OuterDictionary.Keys.Count)
            {
                return false;
            }
            bool flag = true;
            foreach (KeyValuePair<K, Dictionary<T, V>> pair in this.OuterDictionary)
            {
                if (!other.OuterDictionary.ContainsKey(pair.Key))
                {
                    flag = false;
                }
                if (!flag)
                {
                    return flag;
                }
                Dictionary<T, V> otherInnerDictionary = other.OuterDictionary[pair.Key];
                flag = Util.DoubleKeyDictionary.DoubleKeyDictionary<K, T, V>.CheckInnerDictionary(pair, otherInnerDictionary);
                if (!flag)
                {
                    return flag;
                }
            }
            return flag;
        }

        public IEnumerator<DoubleKeyPairValue<K, T, V>> GetEnumerator()
        {
            foreach (KeyValuePair<K, Dictionary<T, V>> iteratorVariable0 in this.OuterDictionary)
            {
                foreach (KeyValuePair<T, V> iteratorVariable1 in iteratorVariable0.Value)
                {
                    yield return new DoubleKeyPairValue<K, T, V>(iteratorVariable0.Key, iteratorVariable1.Key, iteratorVariable1.Value);
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public V this[K index1, T index2]
        {
            get
            {
                return this.OuterDictionary[index1][index2];
            }
            set
            {
                this.Add(index1, index2, value);
            }
        }

        private Dictionary<K, Dictionary<T, V>> OuterDictionary { get; set; }

        [CompilerGenerated]
        private sealed class <GetEnumerator>d__0 : IEnumerator<DoubleKeyPairValue<K, T, V>>, IDisposable, IEnumerator
        {
            private int <>1__state;
            private DoubleKeyPairValue<K, T, V> <>2__current;
            public Util.DoubleKeyDictionary.DoubleKeyDictionary<K, T, V> <>4__this;
            public Dictionary<K, Dictionary<T, V>>.Enumerator <>7__wrap3;
            public Dictionary<T, V>.Enumerator <>7__wrap5;
            public KeyValuePair<T, V> <inner>5__2;
            public KeyValuePair<K, Dictionary<T, V>> <outer>5__1;

            [DebuggerHidden]
            public <GetEnumerator>d__0(int <>1__state)
            {
                this.<>1__state = <>1__state;
            }

            private void <>m__Finally4()
            {
                this.<>1__state = -1;
                this.<>7__wrap3.Dispose();
            }

            private void <>m__Finally6()
            {
                this.<>1__state = 1;
                this.<>7__wrap5.Dispose();
            }

            private bool MoveNext()
            {
                try
                {
                    switch (this.<>1__state)
                    {
                        case 0:
                            this.<>1__state = -1;
                            this.<>7__wrap3 = this.<>4__this.OuterDictionary.GetEnumerator();
                            this.<>1__state = 1;
                            while (this.<>7__wrap3.MoveNext())
                            {
                                this.<outer>5__1 = this.<>7__wrap3.Current;
                                this.<>7__wrap5 = this.<outer>5__1.Value.GetEnumerator();
                                this.<>1__state = 2;
                                while (this.<>7__wrap5.MoveNext())
                                {
                                    this.<inner>5__2 = this.<>7__wrap5.Current;
                                    this.<>2__current = new DoubleKeyPairValue<K, T, V>(this.<outer>5__1.Key, this.<inner>5__2.Key, this.<inner>5__2.Value);
                                    this.<>1__state = 3;
                                    return true;
                                Label_00C6:
                                    this.<>1__state = 2;
                                }
                                this.<>m__Finally6();
                            }
                            this.<>m__Finally4();
                            break;

                        case 3:
                            goto Label_00C6;
                    }
                    return false;
                }
                fault
                {
                    this.System.IDisposable.Dispose();
                }
            }

            [DebuggerHidden]
            void IEnumerator.Reset()
            {
                throw new NotSupportedException();
            }

            void IDisposable.Dispose()
            {
                switch (this.<>1__state)
                {
                    case 1:
                    case 2:
                    case 3:
                        try
                        {
                            switch (this.<>1__state)
                            {
                                case 2:
                                case 3:
                                    try
                                    {
                                    }
                                    finally
                                    {
                                        this.<>m__Finally6();
                                    }
                                    return;
                            }
                        }
                        finally
                        {
                            this.<>m__Finally4();
                        }
                        break;

                    default:
                        return;
                }
            }

            DoubleKeyPairValue<K, T, V> IEnumerator<DoubleKeyPairValue<K, T, V>>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.<>2__current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.<>2__current;
                }
            }
        }
    }
}

