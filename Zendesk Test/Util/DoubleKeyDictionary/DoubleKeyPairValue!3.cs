﻿namespace Util.DoubleKeyDictionary
{
    using System;
    using System.Runtime.CompilerServices;

    internal class DoubleKeyPairValue<K, T, V>
    {
        public DoubleKeyPairValue(K key1, T key2, V value)
        {
            this.Key1 = key1;
            this.Key2 = key2;
            this.Value = value;
        }

        public override string ToString()
        {
            return (this.Key1.ToString() + " - " + this.Key2.ToString() + " - " + this.Value.ToString());
        }

        public K Key1 { get; set; }

        public T Key2 { get; set; }

        public V Value { get; set; }
    }
}

